<?php

namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'users_customers';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['full_name', 'username', 'email', 'password', 'is_active', 
									'uml_id', 'kota_id', 'kantor', 'alamat', 'nib', 'npwp', 'phone', 'token',
									'email_verified_at', 'uttp_owner_id', 'uut_owner_id',];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		'username'     	=> 'required|min_length[3]|max_length[254]|is_unique[users_customers.username,id,{id}]',
        'full_name'     => 'required|max_length[254]',
		// 'password' 		=> 'required',
		'email' 		=> 'required|valid_email|is_unique[users_customers.email,id,{id}]',
		//'nib' 			=> 'is_unique[users_customers.nib,id,{id}]',
		//'npwp' 			=> 'is_unique[users_customers.npwp,id,{id}]',
	];
	protected $validationMessages   = [
		'username' 		=> [
			'required'	=> 'Username (Email) wajib diisi',
			'is_unique' => 'Username (Email) sudah terdaftar sebelumnya',
		],
		'full_name'		=> [
			'required' 	=> 'Nama lengkap wajib diisi',
		],
		'email' 		=> [
			'required'	=> 'Email wajib diisi',
			'is_unique' => 'Email sudah terdaftar sebelumnya',
		],
		/*
		'nib' 			=> [
			'is_unique' => 'NIB sudah terdaftar sebelumnya',
		],
		'npwp' 			=> [
			'is_unique' => 'NPWP sudah terdaftar sebelumnya',
		],
		*/
	];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
