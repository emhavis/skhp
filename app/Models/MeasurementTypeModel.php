<?php

namespace App\Models;

use CodeIgniter\Model;

class MeasurementTypeModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'standard_measurement_types';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['owner_id', 'type_id', 'serial_no',
										'tool_brand', 'tool_model', 'tool_type', 'tool_capacity', 'tool_capacity_unit',
										'tool_factory', 'tool_factory_address', 'tool_made_in', 
										'tool_made_in_id', 'tool_media','class','jumlah'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		'type_id' 		=> 'required',
		'owner_id' 	=> 'required', 
		'serial_no' => 'required',
		'tool_brand' => 'required', 
		'tool_model' => 'required', 
		'tool_type' => 'required', 
		'tool_capacity' => 'required', 
		'tool_capacity_unit' => 'required',
		'tool_factory' => 'required', 
		'tool_factory_address' => 'required', 
		// 'tool_media' => 'required'
	];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
