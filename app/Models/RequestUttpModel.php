<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestUttpModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'service_request_uttps';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'label_sertifikat', 'addr_sertifikat',
		'status_id','payment_date','payment_code','spuh_payment_date','payment_status_id','an_kuitansi',
		'file_payment_ref', 'path_payment_ref','file_spuh_payment_ref', 'path_spuh_payment_ref',
		'payment_valid_date', 'spuh_payment_valid_date', 'inspection_loc', 'inspection_kabkot_id', 'inspection_prov_id',

		'no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan', 'path_surat_permohonan', 
		'spuh_payment_status_id',
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
