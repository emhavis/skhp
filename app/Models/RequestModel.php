<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'service_request_uttps';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'requestor_id', 'pic_id',
		'created_by', 'updated_by',
		'booking_no', 'total_price', 
		'label_sertifikat', 'stat_service_request',
		'addr_sertifikat', 'for_sertifikat',
		'received_date', 'jenis_layanan',
		'requested', 'lokasi_pengujian',
		'booking_id', 
		'no_register', 'no_order',
		'payment_code', 'estimated_date',
		'status_id',' payment_date',
		'requestor_name', 'requestor_phone_no',
		'requestor_type_id', 'requestor_id_no',
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
