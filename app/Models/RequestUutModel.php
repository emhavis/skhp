<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestUutModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'service_requests';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['status_id','payment_date','payment_code','spuh_payment_date','payment_status_id',
										'file_payment_ref','path_payment_ref','an_kuitansi',
										'file_spuh_payment_ref','path_spuh_payment_ref'
										,'label_sertifikat','addr_sertifikat',
										'spuh_payment_status_id', 'spuh_payment_valid_date',
										
										'no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan', 'path_surat_permohonan', 
									];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
