<?php

namespace App\Models;

use CodeIgniter\Model;

class UutOwnerModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'uut_owners';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['nama', 'alamat', 'email', 'kota_id', 'telepon', 'nib', 'npwp', 'penanggung_jawab', 'bentuk_usaha_id', 'uml_id', 'kode_daerah','ktp'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		'nama' 		=> 'required',
		// 'nib' 		=> 'required', 
		// 'nib' 		=> 'is_unique[uut_owners.nib,id,{id}]',
		'email'		=> 'required',
		'kota_id'	=> 'required',
		//'telepon'	=>  'required',
		//'npwp'		=> 'required'
		
	];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
