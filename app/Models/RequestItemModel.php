<?php

namespace App\Models;

use CodeIgniter\Model;

class RequestItemModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'service_request_uttp_items';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'request_id',
		'quantity',
		'subtotal', 
		'uttp_id',
		'location',
		'file_application_letter',
		'file_last_certificate',
		'file_manual_book',
		'file_calibration_manual',
		'reference_no',
		'file_type_approval_certificate',
		'path_application_letter',
		'path_last_certificate',
		'path_manual_book',
		'path_calibration_manual',
		'path_type_approval_certificate',
		'reference_date',
		'status_id',
	];

	// Dates
	protected $useTimestamps        = false;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
