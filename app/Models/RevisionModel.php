<?php

namespace App\Models;

use CodeIgniter\Model;

class RevisionModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'certificate_revision_uttps';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['requestor_id','booking_id','request_id','order_id','status','service_type_id',
										'uttp_type_id','tool_brand', 'tool_model', 'tool_type', 'tool_capacity',
										'tool_factory', 'tool_factory_address', 'tool_made_in', 
										'label_sertifikat', 'addr_sertifikat','others','jenis_dokumen',

										'tool_capacity_min', 'tool_serial_no', 'tool_media',

										
										];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		
	];
	protected $validationMessages   = [

	];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
