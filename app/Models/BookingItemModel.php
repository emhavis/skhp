<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingItemModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'service_booking_items';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['booking_id', 'uml_standard_id', 'quantity', 'est_subtotal', 'uttp_id', 'uut_id',
								'location', 'reference_no', 'reference_date',
								'file_type_approval_certificate',
								'file_last_certificate',
								'file_application_letter',
								'file_calibration_manual',
								'file_manual_book',
								'path_type_approval_certificate',
								'path_last_certificate',
								'path_application_letter',
								'path_calibration_manual',
								'path_manual_book','keterangan',
								'location_prov_id', 'location_kabkot_id',
								'location_lat', 'location_long', 'location_alat',
								'location_negara_id',
							];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		
	];
	protected $validationMessages   = [

	];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
