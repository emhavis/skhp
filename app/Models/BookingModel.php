<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'service_bookings';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [			
									'pic_id', 'booking_no', 'est_total_price', 'label_sertifikat', 'stat_service_request', 
									'addr_sertifikat', 'for_sertifikat', 'est_arrival_date', 'jenis_layanan', 'created_by', 'updated_by',
									'service_type_id', 'lokasi_pengujian', 'uttp_owner_id','uut_owner_id', 'est_schedule_date_from', 'est_schedule_date_to',
									'inspection_loc', 'inspection_prov_id', 'inspection_kabkot_id','file_certificate',

									'kabkota_sertifikat_id', 'prov_sertifikat_id',
								
									'no_surat_permohonan', 'tgl_surat_permohonan', 'file_surat_permohonan', 'path_surat_permohonan', 

									'kaji_ulang_insitu', 'kaji_ulang_insitu_at',

									'lokasi_dl',

									'keuangan_perusahaan', 'keuangan_pic', 'keuangan_jabatan', 'keuangan_hp',
								];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		
	];
	protected $validationMessages   = [

	];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];
}
