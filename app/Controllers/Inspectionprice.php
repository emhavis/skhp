<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

class Inspectionprice extends BaseController
{
	use ResponseTrait;

	public function price($id) {
		$db = \Config\Database::connect();

		$query = $db->query("select * from uttp_inspection_prices where id = " . $db->escape($id) );
		$price = $query->getRow();
		$results['price'] = $price;

		if ($price->has_range) {
			$query = $db->query("select * from uttp_inspection_price_ranges where inspection_price_id = " . $db->escape($id) );
			$ranges = $query->getResult();
			$results['ranges'] = $ranges;
		}

		return $this->respond($results, 200);
	}

	
}
