<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use App\Models\UttpOwnerModel;
use App\Models\UserUttpOwnerModel;

class Useruttpowner extends BaseController
{
	use ResponseTrait;
	
	public function index()
	{
		$ownerModel = new UserUttpOwnerModel();
		$owners = $ownerModel
			->select('uttp_owners.*, master_kabupatenkota.nama kota, user_uttp_owners.id map_id')
			->join('uttp_owners', 'uttp_owners.id = user_uttp_owners.owner_id')
			->join('master_kabupatenkota', 'master_kabupatenkota.id = uttp_owners.kota_id')
			->where('user_uttp_owners.user_id',  session('user_id'))
			->findAll();

		return view ('admin/userowners/index', [
			'owners' => $owners,
		]);
	}

	public function create() {
		$validation =  \Config\Services::validation();
		if ($this->request->getMethod() === 'get'){
			
			return view('admin/userowners/create');
		}

		$db = \Config\Database::connect();

		$mapModel = new UserUttpOwnerModel(); 

		$owner_id = $this->request->getPost('owner_id');
		if ($owner_id != null) {
			$count= $mapModel
				->where('user_id', session('user_id'))
				->where('owner_id', $owner_id)->countAllResults();
			if ($count > 0) {
				return redirect()->back()->withInput()->with('errors', ['owner_id' => 'Data pemilik sudah pernah ditambahkan']);
			}
		} else {
			$ownerModel = new UttpOwnerModel(); 
			
			$owner = [
				'nama' 				=> $this->request->getPost('nama'),
				'alamat' 			=> $this->request->getPost('alamat'),
				'email' 			=> $this->request->getPost('email'),
				'kota_id' 			=> $this->request->getPost('kota_id'),
				'telepon' 			=> $this->request->getPost('telepon'),
				'nib' 				=> $this->request->getPost('nib'),
				'npwp' 				=> $this->request->getPost('npwp'),
				'kode_daerah'		=> $this->request->getPost('kode_daerah'),
				'penanggung_jawab'  => $this->request->getPost('penanggung_jawab'),
				'bentuk_usaha_id'  	=> ($this->request->getPost('bentuk_usaha_id') !== null && !empty($this->request->getPost('bentuk_usaha_id'))) ? $this->request->getPost('bentuk_usaha_id') : null,
				'uml_id'			=> ($this->request->getPost('uml_id') !== null && !empty($this->request->getPost('uml_id'))) ? $this->request->getPost('uml_id') : null,
			];

			//dd($owner);

			if (! $ownerModel->save($owner)) {
				return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
			}

			$owner_id = $db->insertID();
		}

		
		
		$map = [
			'user_id'	=> session('user_id'),
			'owner_id'	=> $owner_id,
		];

		if (! $mapModel->save($map)) {
			return redirect()->back()->withInput()->with('errors', $mapModel->errors());
		}

        return redirect()->to(base_url('/useruttpowner'))->with('success', 'Berhasil menyimpan pemilik baru');
	}

	public function edit($id) {
		$ownerModel = new UttpOwnerModel();
		$owner = $ownerModel->find($id);

		if ($this->request->getMethod() === 'get'){
			$db = \Config\Database::connect();
			$kotas = $db->table('master_kabupatenkota')
				->get()
				->getResult();

			return view ('admin/owners/edit', [
				'owner' => $owner,
				'kotas' => $kotas,
			]);
		}
		
		$owner->nama = $this->request->getPost('nama');
		$owner->alamat = $this->request->getPost('alamat');
		$owner->email = $this->request->getPost('email');
		$owner->kota_id = $this->request->getPost('kota_id');
		$owner->telepon = $this->request->getPost('telepon');
		$owner->nib = $this->request->getPost('nib');
		$owner->npwp = $this->request->getPost('npwp');

		if (! $ownerModel->save($owner)) {
			return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
        }

        return redirect()->to(base_url('/uttpowner'))->with('success', 'Berhasil menyimpan perubahan data pemilik');
	}

	public function delete($id) {
		$ownerModel = new UserUttpOwnerModel();
		$ownerModel->delete($id);

		return redirect()->to(base_url('/useruttpowner'))->with('success', 'Berhasil menghapus pemilik alat');
	}

	public function find() {

		$db = \Config\Database::connect();

		$uttpModel = new UserUttpOwnerModel();
		$builder = $uttpModel;
		$builder = $builder->select('uttp_owners.*')
			->join('uttp_owners', 'uttp_owners.id = user_uttp_owners.owner_id')
			->where('user_uttp_owners.user_id',  session('user_id'));

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->where("upper(uttp_owners.nama) like '%".strtoupper($db->escapeLikeString($q))."%' or nib like '%".strtoupper($db->escapeLikeString($q))."%'");
		}
		$uttps = $builder
			->findAll();

		return $this->respond($uttps, 200);
	}

	public function get($id) {

		$uttpModel = new UttpOwnerModel();
		$uttp = $uttpModel->find($id);

		return $this->respond($uttp, 200);
	}

	public function findByIdentity() {

		$db = \Config\Database::connect();

		$uttpModel = new UttpOwnerModel();
		$builder = $uttpModel;

		$q = $this->request->getPost('q');
		$by = $this->request->getPost('by');
		if ($q && $by) {
			$builder = $builder->where($by, $q);
		}
		$uttp = $builder
			->first();

		return $this->respond($uttp, 200);
	}

	public function store() {
		$validation =  \Config\Services::validation();

		$ownerModel = new UttpOwnerModel();
		$validation->setRules([
			'nama' => 'required',
		],[
			'nama' => [
				'required' => 'Nama Harus Diisi'
			]
		]);
		$owner = [
			'nama' 				=> $this->request->getPost('nama'),
			'alamat' 			=> $this->request->getPost('alamat'),
			'email' 			=> $this->request->getPost('email'),
			'kota_id' 			=> $this->request->getPost('kota_id'),
			'telepon' 			=> $this->request->getPost('telepon'),
			'nib' 				=> $this->request->getPost('nib'),
			'npwp' 				=> $this->request->getPost('npwp'),
			'kode_daerah'		=> $this->request->getPost('kode_daerah'),
			'bentuk_usaha_id'	=> $this->request->getPost('bentuk_usaha_id'),
			'penanggung_jawab'	=> $this->request->getPost('penanggung_jawab'),
		];

		if (! $ownerModel->save($owner)) {
			return $this->fail($ownerModel->errors(), 400);
        }
		$owner['id'] = $ownerModel->insertID;

        return $this->respond($owner, 200);
	}
}
