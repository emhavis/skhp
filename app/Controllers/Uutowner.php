<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use App\Models\UttpOwnerModel;
use App\Models\UutOwnerModel;

class Uutowner extends BaseController
{
	use ResponseTrait;
	
	public function index()
	{
		$ownerModel = new UutOwnerModel();
		$owners = $ownerModel
			->select('uut_owners.*, master_kabupatenkota.nama kota')
			->join('master_kabupatenkota', 'master_kabupatenkota.id = uut_owners.kota_id')
			->findAll();

		return view ('admin/owners/index-uut', [
			'owners' => $owners,
		]);
	}

	public function create() {
		$validation =  \Config\Services::validation();
		if ($this->request->getMethod() === 'get'){
			$db = \Config\Database::connect();
			$kotas = $db->table('master_kabupatenkota')
				->get()
				->getResult();

			return view('admin/owners/create_uut', [
				'kotas' => $kotas,
			]);
		}

		$ownerModel = new UutOwnerModel();
		$validation->setRules([
			'nama' => 'required',
			'telepon' => 'required',
			'alamat' => 'required',
			'npwp' => 'required'
		],[
			'nama' => [
				'required' => 'Nama Harus Diisi'
			],
			'telepon' => ['required' => 'Nomor telepon dibutuhkan'],
			'npwp' => ['required' => 'Nomor NPWP dibutuhkan']

		]);
		$owner = [
			'nama' 				=> $this->request->getPost('nama'),
			'alamat' 			=> $this->request->getPost('alamat'),
			'email' 			=> $this->request->getPost('email'),
			'kota_id' 			=> $this->request->getPost('kota_id'),
			'telepon' 			=> $this->request->getPost('telepon'),
			'nib' 				=> $this->request->getPost('nib'),
			'npwp' 				=> $this->request->getPost('npwp'),
		];

		if (! $ownerModel->save($owner)) {
			return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
        }

        return redirect()->to(base_url('/uutowner'))->with('success', 'Berhasil menyimpan pemilik baru');
	}

	public function edit($id) {
		$ownerModel = new UutOwnerModel();
		$owner = $ownerModel->find($id);

		if ($this->request->getMethod() === 'get'){
			$db = \Config\Database::connect();
			$kotas = $db->table('master_kabupatenkota')
				->get()
				->getResult();

			return view ('admin/owners/edit_uut', [
				'owner' => $owner,
				'kotas' => $kotas,
			]);
		}
		
		$owner->nama = $this->request->getPost('nama');
		$owner->alamat = $this->request->getPost('alamat');
		$owner->email = $this->request->getPost('email');
		$owner->kota_id = $this->request->getPost('kota_id');
		$owner->telepon = $this->request->getPost('telepon');
		$owner->nib = $this->request->getPost('nib');
		$owner->npwp = $this->request->getPost('npwp');

		if (! $ownerModel->save($owner)) {
			return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
        }

        return redirect()->to(base_url('/uutowner'))->with('success', 'Berhasil menyimpan perubahan data pemilik');
	}

	public function find() {

		$db = \Config\Database::connect();

		$uttpModel = new UutOwnerModel();
		$builder = $uttpModel;

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->where("upper(nama) like '%".strtoupper($db->escapeLikeString($q))."%' or nib like '%".strtoupper($db->escapeLikeString($q))."%'");
		}
		$uttps = $builder
			->findAll();
		return $this->respond($uttps, 200);
	}

	public function get($id) {

		$uttpModel = new UutOwnerModel();
		$uttp = $uttpModel->find($id);

		$db = \Config\Database::connect(); 
		$kota = $db->query("select * from master_kabupatenkota where id = " . $uttp->kota_id)->getRow();
		$uttp->provinsi_id = $kota->provinsi_id;
		$uttp->kota = $kota->nama;
		
		return $this->respond($uttp, 200);
	}

	public function findByIdentity() {
		$validation =  \Config\Services::validation();
		$validation->setRules([
			'q' => 'required',
			'by' => 'required',
		],[
			'q' => [
				'required' => 'Keyword pencarian harus diisi'
			],
			'by' => [
				'required' => 'Kategori harus diisi'
			]
		]);
		if (! $validation->run([
				'q' 	=> $this->request->getPost('q'),
				'by' 	=> $this->request->getPost('by'),
			])) {
			return $this->respond($validation->getErrors(), 400);
		}

		$db = \Config\Database::connect();

		$uttpModel = new UutOwnerModel();
		$builder = $uttpModel;

		$q = $this->request->getPost('q');
		$by = $this->request->getPost('by');
		if ($q && $by) {
			$builder = $builder->where($by, $q);
		}
		$uttp = $builder
			->first();

		return $this->respond($uttp, 200);
	}

	public function findUml() {

		$db = \Config\Database::connect();

		$umlModel = $db->table('v_profil_uml');
		$builder = $umlModel;

		$q = $this->request->getPost('q');
		$by = $this->request->getPost('by');
		if ($q && $by) {
			$builder = $builder->where($by, $q);
		}
		$umls = $builder
			->get()
			->getRow();

		return $this->respond($umls, 200);
	}

	public function store() {
		$validation =  \Config\Services::validation();

		$ownerModel = new UutOwnerModel();
		$validation->setRules([
			'nama' => 'required',
		],[
			'nama' => [
				'required' => 'Nama Harus Diisi'
			]
		]);
		$owner = [
			'nama' 				=> $this->request->getPost('nama'),
			'alamat' 			=> $this->request->getPost('alamat'),
			'email' 			=> $this->request->getPost('email'),
			'kota_id' 			=> $this->request->getPost('kota_id'),
			'telepon' 			=> $this->request->getPost('telepon'),
			'nib' 				=> $this->request->getPost('nib'),
			'npwp' 				=> $this->request->getPost('npwp'),
			'bentuk_usaha_id'	=> $this->request->getPost('bentuk_usaha_id'),
			'penanggung_jawab'	=> $this->request->getPost('penanggung_jawab'),
		];

		if (! $ownerModel->save($owner)) {
			return $this->fail($ownerModel->errors(), 400);
        }
		$owner['id'] = $ownerModel->insertID;

        return $this->respond($owner, 200);
	}
}
