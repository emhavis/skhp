<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\HolidayModel;

class Holiday extends BaseController
{
	public function index()
	{
		$holidayModel = new HolidayModel();

		return view ('admin/holidays/index', [
			'holidays' => $holidayModel->findAll(),
		]);
	}

	public function create() {
		if ($this->request->getMethod() === 'get'){
			return view('admin/holidays/create');
		}

		$holidayModel = new HolidayModel();
		$holiday = [
			'holiday_date'		=> date("Y-m-d", strtotime($this->request->getPost('holiday_date'))),
			'name' 				=> $this->request->getPost('name'),
		];

		if (! $holidayModel->save($holiday)) {
			return redirect()->back()->withInput()->with('errors', $holidayModel->errors());
        }

        return redirect()->to(base_url('/holiday'))->with('success', 'Berhasil menyimpan hari libur baru');
	}

	public function edit($id) {
		$holidayModel = new HolidayModel();
		$holiday = $holidayModel->find($id);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/holidays/edit', [
				'holiday' => $holiday,
			]);
		}
		
		$holiday->holiday_date = date("Y-m-d", strtotime($this->request->getPost('holiday_date')));
		$holiday->name = $this->request->getPost('name');

		if (! $holidayModel->save($holiday)) {
			return redirect()->back()->withInput()->with('errors', $holidayModel->errors());
        }

        return redirect()->to(base_url('/holiday'))->with('success', 'Berhasil menyimpan perubahan hari libur');
	}
}
