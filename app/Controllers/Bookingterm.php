<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\BookingTermModel;

class Bookingterm extends BaseController
{
	
	public function create($id) {
		$db = \Config\Database::connect();

		$booking = $db->query('select sru.*  from service_bookings sru 
			where sru.id = '. $id)->getRow();

		$term = $db->query('select * from master_service_types where id = ' . $booking->service_type_id)->getRow();

		$kaji_ulang_insitu = $term->kaji_ulang_insitu;
		$kaji_ulang_insitu = str_replace('{{keuangan_perusahaan}}', $booking->keuangan_perusahaan, $kaji_ulang_insitu);
		$kaji_ulang_insitu = str_replace('{{keuangan_pic}}', $booking->keuangan_pic, $kaji_ulang_insitu);
		$kaji_ulang_insitu = str_replace('{{keuangan_jabatan}}', $booking->keuangan_jabatan, $kaji_ulang_insitu);
		$kaji_ulang_insitu = str_replace('{{keuangan_hp}}', $booking->keuangan_hp, $kaji_ulang_insitu);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/booking_terms/create_uttp', [
				'booking' => $booking,
				'term' => $term,
				'kaji_ulang_insitu' =>$kaji_ulang_insitu,
			]);
		}
		
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);
		$booking->kaji_ulang_insitu_at = date('Y-m-d H:i:s');
		$booking->kaji_ulang_insitu = true;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }

		return redirect()->to(base_url('/booking/edit/' . $id))->with('success', 'Berhasil menyimpan kaji ulang permintaan');
	}

	public function edit($id) {
		$db = \Config\Database::connect();

		$item = $db->query('select srui.*, 
			mut.uttp_type, u.type_id, 
			u.serial_no, u.tool_brand, u.tool_capacity, u.tool_factory, u.tool_factory_address,
			u.tool_model, u.tool_type , u.tool_made_in
			from service_request_uttp_items srui 
			inner join uttps u on srui.uttp_id = u.id
			inner join master_uttp_types mut on u.type_id = mut.id 
			where srui.id = ' . $id)->getRow();

		$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.id = '. $item->request_id)->getRow();

		$inspections = $db->query('select sruii.*,
			uip.inspection_type, uip.unit, mrs.status
			from service_request_uttp_item_inspections sruii 
			inner join service_request_uttp_items srui on sruii.request_item_id = srui.id 
			inner join uttp_inspection_prices uip on sruii.inspection_price_id = uip.id
			inner join master_request_status mrs on sruii.status_id = mrs.id
			where sruii.request_item_id = ' . $id)->getResult();

		/*
		$type = $db->query('select mut.*, mo.oiml_name from master_uttp_types mut 
			inner join master_oimls mo on mut.oiml_id = mo.id
			where mut.id = ' . $item->type_id)->getRow();
		*/

		$type = $db->query('select * from master_uttp_types mut 
			where mut.id = ' . $item->type_id)->getRow();

		$termModel = new TermModel();
		$term = $termModel->where('request_id', $request->id)->where('request_item_id', $id)->first();

		//$slas = $db->query("select * from master_uttp_sla where kelompok = '" . $type->kelompok . "'")->getResult();
		//$klausas = $db->query("select * from master_uttp_kajiklausa where kelompok = '" . $type->kelompok . "'")->getResult();

		//$termKaji = $db->query("select * from master_instalasi_kajiulang where service_type_id = ". $request->service_type_id ." and instalasi_id = ". $type->instalasi_id)->getRow();
		$termKaji = $db->query("select * from master_uttp_type_kajiulang where service_type_id = ". $request->service_type_id ." and uttp_type_id = ". $type->id)->getRow();
		

		if ($this->request->getMethod() === 'get'){
			return view ('admin/terms/edit', [
				'request' => $request,
				'item' => $item,
				'inspections' => $inspections,
				'type' => $type,
				'term' => $term,
				//'slas' => $slas,
				//'klausas' => $klausas,
				'termKaji' => $termKaji,
			]);
		}
		
		$termModel = new TermModel();
		$term->request_id = $request->id;
		$term->request_item_id = $id;
		$term->legalitas = $this->request->getPost('legalitas') == 'on' ? 1 : 0;
		$term->metode = $this->request->getPost('metode') == 'on' ? 1 : 0;
		$term->sign_at = date('Y-m-d H:i:s');
		$term->klausa = true;
		
		/*
		foreach ($klausas as $klausa) {
			if ($klausa->is_header != 't') {
				$skb = [
					'kajiulang_id' => $termModel->insertID,
					'klausa_id' => (int)$klausa->id,
					'checked' => $this->request->getPost('klausa_' . $klausa->id) == 'on',
				];

				$db->table('service_request_uttp_kajiulang_klausa')
					->insert($skb);
			}
		}
		*/

		if (! $termModel->save($term)) {
			return redirect()->back()->withInput()->with('errors', $termModel->errors());
        }
		
		return redirect()->to(base_url('/tracking/read/' . $request->booking_id))->with('success', 'Berhasil menyimpan kaji ulang permintaan');
	}
	
}
