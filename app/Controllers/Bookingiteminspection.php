<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\BookingItemInspectionModel;
use App\Models\UttpModel;
use App\Models\UttpTypeModel;

class Bookingiteminspection extends BaseController
{
	public function index($id, $idItem) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$bookingItemModel = new BookingItemModel();
		$item = $bookingItemModel->find($idItem);

		$db = \Config\Database::connect();

		$bookingItemInspectionModel = new BookingItemInspectionModel();
		

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {

			$inspections = $db->query("select sbii.id, sbii.quantity, sbii.price, " .
			"sip.inspection_type, smu.measurement_unit " .
			"from service_booking_item_inspections sbii " .
			"inner join standard_inspection_prices sip on sbii.inspection_price_id = sip.id " .
			"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
			"where booking_item_id = " . $idItem)->getResult();
			
			$query = $db->query("select s.tool_code, s.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name " . 
				"from standards s " .
				"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
				"inner join standard_tool_types stt on s.standard_tool_type_id = stt.id " .
				"inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id " .
				"where s.id = " . $item->uml_standard_id );
			$standard = $query->getRow();

			return view ('admin/bookingiteminspections/index', [
					'booking' => $booking,
					'item' => $item,
					'inspections' => $inspections,
					'standard' => $standard
				]);
		} else {
			$uttpModel = new UttpModel();
			$uttp = $uttpModel->find($item->uttp_id);
			$uttpTypeModel = new UttpTypeModel();
			$uttpType = $uttpTypeModel->find($uttp->type_id);

			$inspections = $db->query("select sbii.id, sbii.quantity, sbii.price, " .
			"sip.inspection_type, sip.unit " .
			"from service_booking_item_inspections sbii " .
			"inner join uttp_inspection_prices sip on sbii.inspection_price_id = sip.id " .
			"where booking_item_id = " . $idItem)->getResult();

			return view ('admin/bookingiteminspections/indexuttp', [
				'booking' => $booking,
				'item' => $item,
				'inspections' => $inspections,
				'uttp' => $uttp,
				'uttpType' => $uttpType,
 			]);
		}
	}

	public function indexread($id, $idItem) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$bookingItemModel = new BookingItemModel();
		$item = $bookingItemModel->find($idItem);

		$db = \Config\Database::connect();

		$bookingItemInspectionModel = new BookingItemInspectionModel();

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$inspections = $db->query("select sbii.id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, smu.measurement_unit " .
				"from service_booking_item_inspections sbii " .
				"inner join standard_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
				"where booking_item_id = " . $idItem)->getResult();

			$query = $db->query("select s.tool_code, s.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name " . 
				"from standards s " .
				"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
				"inner join standard_tool_types stt on s.standard_tool_type_id = stt.id " .
				"inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id " .
				"where s.id = " . $item->uml_standard_id );
			$standard = $query->getRow();

			return view ('admin/bookingiteminspections/indexread', [
				'booking' => $booking,
				'item' => $item,
				'inspections' => $inspections,
				'standard' => $standard
			]);
		} else {
			$uttpModel = new UttpModel();
			$uttp = $uttpModel->find($item->uttp_id);
			$uttpTypeModel = new UttpTypeModel();
			$uttpType = $uttpTypeModel->find($uttp->type_id);

			$inspections = $db->query("select sbii.id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, sip.unit " .
				"from service_booking_item_inspections sbii " .
				"inner join uttp_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"where booking_item_id = " . $idItem)->getResult();

			return view ('admin/bookingiteminspections/indexread_uttp', [
				'booking' => $booking,
				'item' => $item,
				'inspections' => $inspections,
				'uttp' => $uttp,
				'uttpType' => $uttpType,
 			]);
		}

		
	}

	public function create($id, $idItem) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$bookingItemModel = new BookingItemModel();
		$item = $bookingItemModel->find($idItem);

		$db = \Config\Database::connect();

		$query = $db->query("select s.tool_code, s.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name " . 
			"from standards s " .
			"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
			"inner join standard_tool_types stt on s.standard_tool_type_id = stt.id " .
			"inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id " .
			"where s.id = " . $item->uml_standard_id );
		$standard = $query->getRow();

		$query = $db->query("select sip.id, sip.inspection_type, sip.price, smu.measurement_unit  " . 
			"from standard_inspection_prices sip " .
			"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
			"where sip.standard_detail_type_id = " . $standard->standard_detail_type_id
			);
		$prices = $query->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/bookingiteminspections/create',[
				'booking' => $booking,
				'item' => $item,
				'standard' => $standard,
				'prices' => $prices
			]);
		}

		$price = $db->query("select sip.id, sip.price from standard_inspection_prices sip where id = " . 
			$this->request->getPost('inspection_price_id'))->getRow();

		$inspectionModel = new BookingItemInspectionModel();
		$inspection = [
			'booking_item_id'       => $item->id,
			'inspection_price_id' 	=> $this->request->getPost('inspection_price_id'),
			'quantity'			 	=> $this->request->getPost('quantity'),
			'price' 				=> $price->price,
		];

		if (! $inspectionModel->insert($inspection)) {
			return redirect()->back()->withInput()->with('errors', $inspectionModel->errors());
        }

		$aggregateItems = $db->query("select count(id) quantity, coalesce(sum(price * quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"where booking_item_id = " . $idItem)->getRow();
		
		$item->quantity = $aggregateItems->quantity;
		$item->est_subtotal = $aggregateItems->subtotal;

		if (! $bookingItemModel->save($item)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemModel->errors());
        }

		$aggregate = $db->query("select count(sbii.id) quantity, coalesce(sum(sbii.price * sbii.quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id ".
			"where sbi.booking_id = " . $id)->getRow();

		$booking->est_total_price = $aggregate->subtotal;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }


        return redirect()->to(base_url('/bookingiteminspection/index/' . $id . '/' . $idItem))->with('success', 'Berhasil menyimpan rincian pemeriksaan baru');
	}

	public function createuttp($id, $idItem) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$bookingItemModel = new BookingItemModel();
		$item = $bookingItemModel->find($idItem);

		$db = \Config\Database::connect();

		$uttpModel = new UttpModel();
		$uttp = $uttpModel->find($item->uttp_id);
		$uttpTypeModel = new UttpTypeModel();
		$uttpType = $uttpTypeModel->find($uttp->type_id);

		$query = $db->query("select uip.* from uttp_inspection_prices uip " .
			"inner join uttp_inspection_price_types uipt on uipt.inspection_price_id = uip.id " .
			"where uip.service_type_id = " . $booking->service_type_id . " and uipt.uttp_type_id = " . $uttp->type_id
			);
		$prices = $query->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/bookingiteminspections/createuttp',[
				'booking' => $booking,
				'item' => $item,
				'uttp' => $uttp,
				'uttpType' => $uttpType,
				'prices' => $prices
			]);
		}

		$price = $db->query("select sip.id, sip.price, sip.has_range from uttp_inspection_prices sip where id = " . 
			$this->request->getPost('inspection_price_id'))->getRow();

		$harga = $price->price;
		if ($price->has_range == 't') {
			$range = $db->query("select r.* from uttp_inspection_price_ranges r 
			where inspection_price_id = " . $this->request->getPost('inspection_price_id') . "
			and min_range <= " . $this->request->getPost('quantity') . "
			and coalesce(max_range, min_range ^ 2) > " . $this->request->getPost('quantity') )->getRow();
			$harga = $range->price;
		}

		$inspectionModel = new BookingItemInspectionModel();
		$inspection = [
			'booking_item_id'       => $item->id,
			'inspection_price_id' 	=> $this->request->getPost('inspection_price_id'),
			'quantity'			 	=> $this->request->getPost('quantity'),
			'price' 				=> $harga,
		];

		if (! $inspectionModel->insert($inspection)) {
			return redirect()->back()->withInput()->with('errors', $inspectionModel->errors());
        }

		$aggregateItems = $db->query("select count(id) quantity, coalesce(sum(price * quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"where booking_item_id = " . $idItem)->getRow();
		
		$item->quantity = $aggregateItems->quantity;
		$item->est_subtotal = $aggregateItems->subtotal;

		if (! $bookingItemModel->save($item)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemModel->errors());
        }

		$aggregate = $db->query("select count(sbii.id) quantity, coalesce(sum(sbii.price * sbii.quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id ".
			"where sbi.booking_id = " . $id)->getRow();

		$booking->est_total_price = $aggregate->subtotal;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }


        return redirect()->to(base_url('/bookingiteminspection/index/' . $id . '/' . $idItem))->with('success', 'Berhasil menyimpan rincian pemeriksaan baru');
	}

	public function edit($id, $idItem, $idInspection) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$bookingItemModel = new BookingItemModel();
		$item = $bookingItemModel->find($idItem);

		$bookingItemInspectionModel = new BookingItemInspectionModel();
		$inspection = $bookingItemInspectionModel->find($idInspection);

		$db = \Config\Database::connect();

		$query = $db->query("select s.tool_code, s.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name " . 
			"from standards s " .
			"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
			"inner join standard_tool_types stt on s.standard_tool_type_id = stt.id " .
			"inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id " .
			"where s.id = " . $item->uml_standard_id );
		$standard = $query->getRow();

		$query = $db->query("select sip.id, sip.inspection_type, sip.price, smu.measurement_unit  " . 
			"from standard_inspection_prices sip " .
			"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
			"where sip.standard_detail_type_id = " . $standard->standard_detail_type_id
			);
		$prices = $query->getResult();

		if ($this->request->getMethod() === 'get'){
			return view ('admin/bookingiteminspections/edit', [
				'booking' => $booking,
				'item' => $item,
				'inspection' => $inspection,
				'standard' => $standard,
				'prices' => $prices
			]);
		}

		$price = $db->query("select sip.id, sip.price from standard_inspection_prices sip where id = " . 
			$this->request->getPost('inspection_price_id'))->getRow();
		
		$inspection->inspection_price_id = $this->request->getPost('inspection_price_id');
		$inspection->quantity = $this->request->getPost('quantity');
		$inspection->price = $price->price;

		if (! $bookingItemInspectionModel->save($inspection)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemInspectionModel->errors());
        }

		$aggregateItems = $db->query("select count(id) quantity, coalesce(sum(price * quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"where booking_item_id = " . $idItem)->getRow();
		
		$item->quantity = $aggregateItems->quantity;
		$item->est_subtotal = $aggregateItems->subtotal;

		if (! $bookingItemModel->save($item)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemModel->errors());
        }

		$aggregate = $db->query("select count(sbii.id) quantity, coalesce(sum(sbii.price * sbii.quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id ".
			"where sbi.booking_id = " . $id)->getRow();

		$booking->est_total_price = $aggregate->subtotal;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }

        return redirect()->to(base_url('/bookingiteminspection/index/' . $id . '/' . $idItem))->with('success', 'Berhasil menyimpan perubahan rincian pemeriksaan');
	}

	public function read($id, $idItem, $idInspection) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$bookingItemModel = new BookingItemModel();
		$item = $bookingItemModel->find($idItem);

		$bookingItemInspectionModel = new BookingItemInspectionModel();
		$inspection = $bookingItemInspectionModel->find($idInspection);

		$db = \Config\Database::connect();

		$query = $db->query("select s.tool_code, s.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name " . 
			"from standards s " .
			"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
			"inner join standard_tool_types stt on s.standard_tool_type_id = stt.id " .
			"inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id " .
			"where s.id = " . $item->uml_standard_id );
		$standard = $query->getRow();

		$query = $db->query("select sip.id, sip.inspection_type, sip.price, smu.measurement_unit  " . 
			"from standard_inspection_prices sip " .
			"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
			"where sip.id = ".$inspection->inspection_price_id
			);
		$price = $query->getRow();

		return view ('admin/bookingiteminspections/read', [
			'booking' => $booking,
			'item' => $item,
			'inspection' => $inspection,
			'standard' => $standard,
			'price' => $price
		]);
	}

	public function deleteuttp($id) {
		$bookingModel = new BookingModel();;
		$bookingItemModel = new BookingItemModel();

		$db = \Config\Database::connect();

		$inspectionModel = new BookingItemInspectionModel();
		$inspection = $inspectionModel->find($id);
		$idItem = $inspection->booking_item_id;

		if (! $inspectionModel->delete($id)) {
			return redirect()->back()->withInput()->with('errors', $inspectionModel->errors());
        }

		$aggregateItems = $db->query("select count(id) quantity, coalesce(sum(price * quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"where booking_item_id = " . $idItem)->getRow();

		$item = $bookingItemModel->find($idItem);
		
		$item->quantity = $aggregateItems->quantity;
		$item->est_subtotal = $aggregateItems->subtotal;

		if (! $bookingItemModel->save($item)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemModel->errors());
        }

		$aggregate = $db->query("select count(sbii.id) quantity, coalesce(sum(sbii.price * sbii.quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id ".
			"where sbi.booking_id = " . $item->booking_id)->getRow();

		$booking = $bookingModel->find($item->booking_id);

		$booking->est_total_price = $aggregate->subtotal;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }


        return redirect()->to(base_url('/bookingiteminspection/index/' . $booking->id . '/' . $item->id))->with('success', 'Berhasil menyimpan rincian pemeriksaan baru');
	}
}
