<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\HolidayModel;
use App\Models\ClosedeventModel;
use App\Models\ServiceTypeModel;
use App\Models\User;
use App\Models\UttpOwnerModel;

class Booking extends BaseController
{
	public function index() {
	


		/*
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			//->where('booking_no is null', null, false)
			->where('pic_id', session('user_id'))
			->findAll();
		*/

		$db = \Config\Database::connect();
		$bookings = $db->query('select sb.* from service_bookings sb ' .
			'left join service_requests sr on sb.id = sr.booking_id ' .
			'where sr.booking_id is null and sb.service_type_id in (1,2) ' .
			'and sb.pic_id = ' . session('user_id') .
			'union ' . 
			'select sb.* from service_bookings sb ' .
			'left join service_request_uttps sr on sb.id = sr.booking_id ' .
			'where sr.booking_id is null and sb.service_type_id in (4,5,6,7) ' .
			'and sb.pic_id = ' . session('user_id'))->getResult();

		return view ('admin/bookings/index', [
			'bookings' => $bookings,
		]);
	}

	public function create() {
		/*
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('/auth/logout');
		}
		*/

		$holidayModel = new HolidayModel();
		$holidays = $holidayModel->where('holiday_date >= current_date')
			->findAll();
		$db = \Config\Database::connect();
		$events = $db->query('select generate_series(start_date, end_date, \'1 day\') closed_date
			from closed_events ce')->getResult();

		$quota = 2;
		$quotas = $db->query('select est_arrival_date as quota_date, count(id) cnt from service_bookings sb 
			where booking_no  is not null
			group by est_arrival_date
			having est_arrival_date >= current_date 
			and count(id) >= '.$quota)->getResult();

		$userModel = new User();
		$user = $userModel->find(session('user_id'));

		$serviceTypeModel = new ServiceTypeModel();
		$types = $serviceTypeModel->orderBy('id')->findAll();

		$ownerModel = new UttpOwnerModel();
		//$owners = $ownerModel->findAll();

		if ($this->request->getMethod() === 'get'){
			return view('admin/bookings/create', [
				'holidays' => $holidays,
				'events' => $events,
				'quotas' => $quotas,
				'addr_sertifikat' => $user->alamat,
				'label_sertifikat' => $user->full_name,
				'types' => $types,
				//'owners' => $owners,
			]);
		}

		$type = $serviceTypeModel->find($this->request->getPost('service_type_id'));

		$owner = $ownerModel->find($this->request->getPost('uttp_owner_id'));

		$bookings = new BookingModel();
		$booking = [
			'pic_id'         	=> session('user_id'),
			'service_type_id'	=> $this->request->getPost('service_type_id'),
			'jenis_layanan' 	=> $type->service_type,
			'for_sertifikat' 	=> $this->request->getPost('for_sertifikat'),
			'addr_sertifikat' 	=> $owner->alamat,
			'label_sertifikat' 	=> $owner->nama, 
			'uttp_owner_id'		=> $owner->id,
			//'est_arrival_date' 	=> date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))),

			'lokasi_pengujian'	=> $this->request->getPost('lokasi_pengujian'),

			'created_by'        => session('user_id'),
			'updated_by'        => session('user_id'),
		];

		if (! $bookings->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookings->errors());
        }

        //return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran baru');
		return redirect()->to(base_url('/booking/edit/'.$bookings->insertID))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function edit($id) {
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('auth/logout');
		}

		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$db = \Config\Database::connect();

		$holidayModel = new HolidayModel();
		$holidays = $holidayModel->where('holiday_date >= current_date')
			->findAll();
		$db = \Config\Database::connect();
		$events = $db->query('select generate_series(start_date, end_date, \'1 day\') closed_date
			from closed_events ce')->getResult();

		$serviceTypeModel = new ServiceTypeModel();
		$types = $serviceTypeModel->findAll();

		//$ownerModel = new UttpOwnerModel();
		//$owners = $ownerModel->findAll();

		$provinces = $db->query('select * from master_provinsi')->getResult();
		$cities = $db->query('select k.*, p.nama as provinsi from master_kabupatenkota k inner join master_provinsi p on k.provinsi_id = p.id')->getResult();

		if (!($booking->service_type_id == 1 || $booking->service_type_id == 2)) {
			$bookingItems = $db->query('select sbi.*,  ' . 
				"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
				's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, '.
				's.tool_model, s.tool_type , s.tool_made_in, s.tool_media, s.tool_capacity_unit, '.
				'mut.uttp_type as type ' .
				'from service_booking_items sbi ' .
				'inner join uttps s on sbi.uttp_id = s.id ' .
				'inner join master_uttp_types mut on s.type_id = mut.id ' .
				'where sbi.booking_id = ' . $id)->getResult();

			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, sip.unit " .
				"from service_booking_item_inspections sbii " .
				"inner join uttp_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();

			$quotas = $db->query('select sb.est_arrival_date as quota_date, 
				uip.instalasi_id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join uttp_inspection_prices uip on sbii.inspection_price_id = uip.id 
				inner join master_instalasi mi on uip.instalasi_id = mi.id
				where sb.booking_no  is not null
				group by sb.est_arrival_date, uip.instalasi_id, mi.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbii.id) >= mi.quota_online')->getResult();

			$view = 'admin/bookings/edit_uttp';

			if ($this->request->getMethod() === 'get'){
				return view ($view, [
					'booking' => $booking,
					'items' => $bookingItems,
					'inspections' => $bookingInspections,
					'holidays' => $holidays,
					'events' => $events,
					'quotas' => $quotas,
					'types' => $types,
					'provinces' => $provinces,
					'cities' => $cities,
					//'owners' => $owners,
				]);
			}

			$booking->inspection_loc = $this->request->getPost('inspection_loc');
			//$booking->inspection_prov_id = $this->request->getPost('inspection_prov_id');
			$booking->inspection_kabkot_id = $this->request->getPost('inspection_kabkot_id');

			if ($this->request->getPost('inspection_kabkot_id') != null) {
				$city = $db->query('select * from master_kabupatenkota where id = ' . $this->request->getPost('inspection_kabkot_id'))->getRow();
				$booking->inspection_prov_id = $city->provinsi_id;
			}

			
		} else {
			$bookingItems = $db->query('select sbi.*, s.tool_code, ' .
				's.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name, ' .
				's.brand, s.model, s.tipe, s.capacity ' .
				'from service_booking_items sbi ' .
				// 'inner join standards s on sbi.uml_standard_id = s.id ' .
				"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
				'inner join standard_tool_types stt on s.standard_tool_type_id = stt.id ' .
				'inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id ' .
				'where sbi.booking_id = ' . $id)->getResult();

			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, smu.measurement_unit unit " .
				"from service_booking_item_inspections sbii " .
				"inner join standard_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();
				
			$quotas = $db->query('select sb.est_arrival_date as quota_date,
				ml.id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				-- inner join standards s on sbi.uml_standard_id = s.id 
				inner join master_laboratory ml on s.standard_measurement_type_id = ml.id
				where sb.booking_no is not null 
				group by sb.est_arrival_date, ml.id, ml.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbii.id) >= ml.quota_online')->getResult();

			$view = 'admin/bookings/edit';

			if ($this->request->getMethod() === 'get'){
				return view ($view, [
					'booking' => $booking,
					'items' => $bookingItems,
					'inspections' => $bookingInspections,
					'holidays' => $holidays,
					'events' => $events,
					'quotas' => $quotas,
					'types' => $types,
					'provinces' => $provinces,
					'cities' => $cities,
					//'owners' => $owners,
				]);
			}
		}

		//$type = $serviceTypeModel->find($this->request->getPost('service_type_id'));
		
		//$booking->service_type_id = $this->request->getPost('service_type_id');
		//$booking->jenis_layanan = $type->service_type;
		//$booking->for_sertifikat = $this->request->getPost('for_sertifikat');
		//$booking->addr_sertifikat = $this->request->getPost('addr_sertifikat');
		//$booking->label_sertifikat = $this->request->getPost('label_sertifikat');

		if ($this->request->getPost('est_arrival_date') != null) {
			$booking->est_arrival_date = date("Y-m-d", strtotime($this->request->getPost('est_arrival_date')));
		}
		if ($this->request->getPost('est_schedule_date_from') != null) {
			$booking->est_schedule_date_from = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_from')));
			$booking->est_schedule_date_to = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_to')));
		}
		$booking->updated_by = session('user_id');
		

		if (! $bookings->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookings->errors());
        }

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function submit($id) {
		if (session()->get('logged_in') && session()->get('user_role') > -1) {
			return redirect()->to('auth/logout');
		}

		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();
		$bookingItems = $db->query('select sbi.*, s.tool_code ' .
			'from service_booking_items sbi ' .
			// 'inner join standards s on sbi.uml_standard_id = s.id ' .
			'where sbi.booking_id = ' . $id)->getResult();

		if (($booking->service_type_id == 1 || $booking->service_type_id == 2)) {
			/*
			$quota = 2;
			$bookingSameDay = $bookingModel->where('id <>', $id)
				->where('est_arrival_date', date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))))
				->where('booking_no is not null')
				->countAllResults();	
			$isOverQuota = $bookingSameDay >= $quota;
			*/

			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date,
				ml.id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				-- inner join standards s on sbi.uml_standard_id = s.id 
				inner join master_laboratory ml on s.standard_measurement_type_id = ml.id
				where sb.booking_no is not null 
				group by sb.est_arrival_date, ml.id, ml.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."'  
				and count(sbii.id) >= ml.quota_online")->getResult();
			$isOverQuota = count($bookingSameDay) > 0;

		} else {
			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date, 
				uip.instalasi_id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join uttp_inspection_prices uip on sbii.inspection_price_id = uip.id 
				inner join master_instalasi mi on uip.instalasi_id = mi.id
				where sb.booking_no  is not null
				group by sb.est_arrival_date, uip.instalasi_id, mi.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."' 
				and count(sbii.id) >= mi.quota_online")->getResult();
			$isOverQuota = count($bookingSameDay) > 0;
		}
		
		if ($isOverQuota) {
			return redirect()->back()->withInput()->with('errors', [
				'quota' => 'Jumlah kuota yang terpakai sudah terpenuhi']);
		}

		/*	
		$quota_used = $db->query('select count(id) count_used from service_bookings sb 
			where id <> '. $id .' and est_arrival_date = \''. $booking->est_arrival_date .'\'')->getResult();
		*/

		//$type = $serviceTypeModel->find($this->request->getPost('service_type_id'));

		//$booking->service_type_id = $this->request->getPost('service_type_id');
		//$booking->jenis_layanan = $type->service_type;
		$booking->for_sertifikat = $this->request->getPost('for_sertifikat');
		$booking->addr_sertifikat = $this->request->getPost('addr_sertifikat');
		$booking->label_sertifikat = $this->request->getPost('label_sertifikat');
		if ($this->request->getPost('est_arrival_date') != null) {
			$booking->est_arrival_date = date("Y-m-d", strtotime($this->request->getPost('est_arrival_date')));
		}
		if ($this->request->getPost('est_schedule_date_from') != null) {
			$booking->est_schedule_date_from = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_from')));
			$booking->est_schedule_date_to = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_to')));
		}
		$booking->booking_no = date("Ymd") . str_pad($booking->id, 3, '0', STR_PAD_LEFT);
		$booking->updated_by = session('user_id');

		$booking->inspection_loc = $this->request->getPost('inspection_loc');
		//$booking->inspection_prov_id = $this->request->getPost('inspection_prov_id');
		$booking->inspection_kabkot_id = $this->request->getPost('inspection_kabkot_id');

		if ($this->request->getPost('inspection_kabkot_id') != null) {
			$city = $db->query('select * from master_kabupatenkota where id = ' . $this->request->getPost('inspection_kabkot_id'))->getRow();
			$booking->inspection_prov_id = $city->provinsi_id;
		}
		
		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }

		$userModel = new User();
		$user = $userModel->find(session('user_id'));

		$uttpOwner = new UttpOwnerModel();
		$uttpOwner = $uttpOwner->find($booking->uttp_owner_id);

		$email = \Config\Services::email();

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.mailtrap.io',
			'smtp_port' => 2525,
			'smtp_user' => 'a14db434093c11',
			'smtp_pass' => '3524293f1ca830',
			'crlf' => "\r\n",
			'newline' => "\r\n"
		  );
		//$email->initialize($config);

		$email->setTo($user->email);
		
		$email->setSubject('Booking Pengujian/Pemeriksaan dengan Nomor : '. $booking->booking_no .' Telah Diterima');

		$emailBody = "<p>Yth. Bapak/Ibu, ". $user->full_name ."</p>

		<p>Kami beritahukan bahwa kami telah menerima Booking Anda untuk permohonan sebagai berikut:</p>
		
		<p>
		Nomor Booking: ". $booking->booking_no ."<br/>";

		if ($uttpOwner != null) {
			$emailBody .= "Nama Perusahaan: ". $uttpOwner->nama ."<br/>
			Alamat Perusahaan: ". $uttpOwner->alamat ."<br/>
			NIB Perusahaan: ". $uttpOwner->nib ."<br/>
			NPWP Perusahaan: ". $uttpOwner->npwp ."<br/>
			Telepon: ". $uttpOwner->telepon ."<br/>
			Penanggung Jawab: ". $uttpOwner->penanggung_jawab ."<br/>
			E-mail: ". $uttpOwner->email ."<br/>";
		}
		
		$uttps = $db->query('select u.*,
			mut.uttp_type 
			from service_booking_items sbi 
			inner join uttps u on sbi.uttp_id = u.id 
			inner join master_uttp_types mut on mut.id = u.type_id 
			where booking_id = ' . $id)->getResult();

		foreach ($uttps as $uttp) {
			$emailBody .= "Nama Alat: ". $uttp->uttp_type ."<br/>
				Merk Alat: ". $uttp->tool_brand ."<br/>
				Model/Type Alat: ". $uttp->tool_model ."<br/>
				No Seri Alat: ". $uttp->serial_no ."<br/>";
		}
		
		$emailBody .= "Nama Pemilik Alat pada Sertifikat: ". $booking->label_sertifikat ."<br/>
		Alamat Pemilik Alat pada Sertifikat: ". $booking->addr_sertifikat ."<br/>
		</p>";

		if ($booking->lokasi_pengujian == 'dalam') {
			$emailBody .=
			"<p>Silakan kirim alat UTTP yang akan diuji/diperiksa pada tanggal ". date("d M Y", strtotime($booking->est_arrival_date));	
		}
		
		$emailBody .=
		"<p>Direktorat Metrologi</p>
		
		<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>";
		
		$email->setMessage($emailBody);
		$email->send();

		//dd($email);

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function read($id) {
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('auth/logout');
		}

		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$db = \Config\Database::connect();
		if (!($booking->service_type_id == 1 || $booking->service_type_id == 2)) {

			$bookingItems = $db->query('select sbi.*,  ' . 
				"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
				's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, '.
				's.tool_model, s.tool_type , s.tool_made_in, '.
				'mut.uttp_type as type ' .
				'from service_booking_items sbi ' .
				'inner join uttps s on sbi.uttp_id = s.id ' .
				'inner join master_uttp_types mut on s.type_id = mut.id ' .
				'where sbi.booking_id = ' . $id)->getResult();

			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, sip.unit " .
				"from service_booking_item_inspections sbii " .
				"inner join uttp_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();
			
			return view ('admin/bookings/read_uttp', [
				'booking' => $booking,
				'items' => $bookingItems,
				'inspections' => $bookingInspections
			]);
		} else {
			$bookingItems = $db->query('select sbi.*, s.tool_code, ' .
				's.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name, ' .
				's.brand, s.model, s.tipe, s.capacity ' .
				'from service_booking_items sbi ' .
				// 'inner join standards s on sbi.uml_standard_id = s.id ' .
				"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
				'inner join standard_tool_types stt on s.standard_tool_type_id = stt.id ' .
				'inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id ' .
				'where sbi.booking_id = ' . $id)->getResult();

			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, smu.measurement_unit unit " .
				"from service_booking_item_inspections sbii " .
				"inner join standard_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();

			return view ('admin/bookings/read', [
				'booking' => $booking,
				'items' => $bookingItems,
				'inspections' => $bookingInspections
			]);
		}
		
	}

	public function delete($id) {
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('auth/logout');
		}
		
		$db = \Config\Database::connect();

		$db->transStart(); 
		
		$itemBuilder = $db->table('service_booking_items');
		$items = $itemBuilder->where('booking_id', $id)->get()->getResult();
		foreach ($items as $item) {
			$inspectionBuilder = $db->table('service_booking_item_inspections');
			$inspectionBuilder->where('booking_item_id', $item->id)->delete();
		}

		$itemBuilder->where('booking_id', $id)->delete();

		$bookingBuilder = $db->table('service_bookings');
		$bookingBuilder->where('id', $id)->delete();

		$db->transComplete();

		if ($db->transStatus() === FALSE)
		{
			return redirect()->back()->withInput()->with('errors', 'Gagal menghapus data booking');
		}

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menghapus data booking');
	}
}
