<?php

namespace App\Controllers;
use App\Models\User;
use App\Models\InternalUser;
use App\Models\UttpOwnerModel;
use App\Models\UutOwnerModel;
use App\Models\UserUutOwnerModel;
use App\Models\UserUttpOwnerModel;
use App\Models\ApiResponseModel;
use CodeIgniter\API\ResponseTrait;

class Auth extends BaseController
{
	use ResponseTrait;

	public function login()
	{
		if ($this->request->getMethod() === 'get'){
			$db = \Config\Database::connect();
			$articles = $db->query("select * from article_posts where published = true order by coalesce(priority_index, 1000000), created_at desc limit 5")->getResult();

			return view('admin/auth/login', [
				'articles' => $articles
			]);
		}

		$userModel = new User();
		$where = "(username='".  $this->request->getPost('username') . "' or email='" 
			. $this->request->getPost('username') . "' or nib='" 
			. $this->request->getPost('username') . "' or npwp='"
			. $this->request->getPost('username') . "')" ;
		$user = $userModel->where($where)
			->where('is_active', true)
			->first();
		//$db = \Config\Database::connect();
		//dd($db->getLastQuery());

		//dd($user);
		if ($user) {
			$verify_pass = password_verify($this->request->getPost('password'), $user->password);
			if ($verify_pass) {
				$sesion_data = [
                    'user_id'       	=> $user->id,
					'user_uml_id'       	=> $user->uml_id,
                    'user_username'     => $user->username,
					'user_fullname'     => $user->full_name,
                    'user_email'    	=> $user->email,
                    'logged_in'     	=> TRUE,
					'role'				=> -1,
                ];
				$session = session();
                $session->set($sesion_data);
				return redirect()->to(base_url('/dashboard'));
			} 
		} 

		return redirect()->back()->withInput()->with('errors', 'Username dan/atau password salah');
	}

	public function logout() {
		if ($this->request->getMethod() === 'get'){
			$session = session();
			$session->destroy();
			return redirect()->to('/auth/login');
		}
	}

	public function register()
	{
		$db = \Config\Database::connect();
		//$types = $db->query("select * from master_doctypes")->getResult();
		$umls = $db->query("select * from v_profil_uml")->getResult();
		$kotas = $db->query("select * from master_kabupatenkota")->getResult();
		$provinsis = $db->table("master_provinsi")->select("id,nama")->get()->getResult();

		// var_dump(json_encode(json_encode($provinsis)));

		if ($this->request->getMethod() === 'get'){
			return view('admin/auth/register', [
				//'types' => $types,
				'umls' => $umls,
				'kotas' => $kotas,
				'provinsis' => json_encode($provinsis)
			]);
		}

		helper('text');

		$users = new User();
		$username = $this->request->getPost('email');

		$errors = [];
		$client = \Config\Services::curlrequest();
		if ($this->request->getPost('kantor') == 'Pribadi') {
			if ($this->request->getPost('npwp') == null || $this->request->getPost('npwp') == '') {
				$errors['npwp'] = 'NPWP wajib diisi untuk pendaftar Pribadi';
			}
		}
		if ($this->request->getPost('kantor') == 'Mahasiswa') {
			if ($this->request->getPost('ktp') == null || $this->request->getPost('ktp') == '') {
				$errors['ktp'] = 'KTP wajib diisi untuk pendaftar Pribadi';
			}
		}
		if ($this->request->getPost('kantor') == 'Perusahaan') {
			if ($this->request->getPost('nib') == null || $this->request->getPost('nib') == '') {
				$errors['nib'] = 'NIB wajib diisi untuk pendaftar Perusahaan';
			}
		}
		if ($this->request->getPost('kantor') == 'Pribadi' && $this->request->getPost('npwp') != null && $this->request->getPost('npwp') != '') {

			$npwp = $client->request('POST', getenv('NPWP_url'), [
				'headers' => [
					'x-api-key' => getenv('NPWP_apikey'),
				],
				'form_params' => [
					getenv('NPWP_body_param') => $this->request->getPost('npwp'),
				],
			]);

			$body = $npwp->getBody();
			$json = json_decode($body);

			// save log first
			$responseLog = [
				'api_request_function' 	=> 'register',
				'api_request_type' 		=> 'NPWP',
				'url'					=> getenv('NPWP_url'),
				'body_param'			=> $this->request->getPost('npwp'),
				'body_response_raw'		=> $body,
			];
			$responseLogs = new ApiResponseModel();
			$responseLogs->save($responseLog);

			if ($json->kode != 200) {
				$errors['npwp'] = 'NPWP tidak dikenali';
			}

			$userList = $users->where('npwp', $this->request->getPost('npwp'))->findAll();
			/* NPWP tidak jadi mandatory
			if (count($userList) > 0) {
				$errors['npwp'] = 'NPWP sudah terdaftar sebelumnya';
			}
			*/
		}
		if ($this->request->getPost('kantor') == 'Perusahaan' && $this->request->getPost('nib') != null && $this->request->getPost('nib') != '') {

			$nib = $client->request('POST', getenv('NIB_url'), [
				'headers' => [
					'x-api-key' => getenv('NIB_apikey'),
				],
				'form_params' => [
					getenv('NIB_body_param') => $this->request->getPost('nib'),
				],
			]);

			$body = $nib->getBody();
			$json = json_decode($body);

			// save log first
			$responseLog = [
				'api_request_function' 	=> 'register',
				'api_request_type' 		=> 'NIB',
				'url'					=> getenv('NIB_url'),
				'body_param'			=> $this->request->getPost('nib'),
				'body_response_raw'		=> $body,
			];
			$responseLogs = new ApiResponseModel();
			$responseLogs->save($responseLog);

			if ($json->responinqueryNIB->kode != 200) {
				$errors['nib'] = 'NIB tidak dikenali';
			}

			$userList = $users->where('nib', $this->request->getPost('nib'))->findAll();
			/* NIB tidak menjadi mandatory
			if (count($userList) > 0) {
				$errors['nib'] = 'NIB sudah terdaftar sebelumnya';
			}
			*/
		};
		// var_dump($this->request->getPost());
        $user = [
            'full_name'         => $this->request->getPost('full_name'),
			'email'         	=> $this->request->getPost('email'),
            'username'         	=> $username,
            'password'     		=> password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
            'is_active' 		=> 0,

			'uml_id'			=> $this->request->getPost('uml_id'),
			'kota_id'			=> $this->request->getPost('kota_id'),
			'nip'				=> $this->request->getPost('nip'),
			'kantor'			=> $this->request->getPost('kantor'),

			'phone'				=> $this->request->getPost('phone'),
			'npwp'				=> $this->request->getPost('npwp'),
			'nib'				=> $this->request->getPost('nib') == '' ? null : $this->request->getPost('nib'),
			'ktp'				=> $this->request->getPost('ktp') == '' ? null : $this->request->getPost('ktp'),

			'alamat'			=> $this->request->getPost('alamat'),

			'token'				=> random_string('alnum', 8),

			//'uttp_owner_id'		=> $uttpOwnerId,
			//'uut_owner_id'		=> $uutOwnerId,
        ];

		if (count($errors) > 0) {
			return redirect()->back()->withInput()->with('errors', $errors);
		}
        if (! $users->save($user)) {
			return redirect()->back()->withInput()->with('errors', $users->errors());
			//$errors = array_merge($errors, $users->errors());
        }
		/*
		if (count($errors) > 0) {
			return redirect()->back()->withInput()->with('errors', $errors);
		}
		*/
		$user = $users->find($db->insertID());

		if ($user->kantor == 'Unit Metrologi Legal') {
			$this->activateUML($user->token);

			return redirect()->to(base_url('auth/registerSuccessUML'))->with('success', 'Berhasil menyimpan pengguna baru.');
		} else {
			$client = \Config\Services::curlrequest();
			$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
				'form_params' => [
					'username' => 'customer',
					'password' => 'cerber0s',
					'client' => 'skhp',
					'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
				]
			]);

			$loginBody = $login->getBody();
			$loginJson = json_decode($loginBody);

			$token = $loginJson->token;
			
			if ($token) {
				$email = $client->request('POST', config('App')->siksURL ."mailer/send", [
					'form_params' => [
						'token' => $token,
						'customer_id' => $user->id,
						'url' => base_url('auth/activate/' . $user->token),
						'type' => 'registration',
					]
				]);
			}

			return redirect()->to(base_url('auth/registerSuccess'))->with('success', 'Berhasil menyimpan pengguna baru.');
		}
	}

	public function registerSuccess()
	{
		return view('admin/auth/register-success');
	}

	public function registerSuccessUML()
	{
		return view('admin/auth/register-success-uml');
	}

	public function activate($token)
	{
		$db = \Config\Database::connect();
		$users = new User();
		$user = $users->where('token', $token)
			->where('is_active', false)
			->first();

		if ($user == null) {
			return view('admin/auth/activate-failed');
		}

		$user->is_active = true;
		$user->email_verified_at = date("Y m d H:i:s");
		//$user->token = null;

        if (! $users->save($user)) {
			return redirect()->back()->withInput()->with('errors', $users->errors());
        }

		$uttpOwnerModel = new UttpOwnerModel();
		$uutOwnerModel = new UutOwnerModel();

		$uttpOwner = null;
		$uutOwner = null;

		if ($user->kantor == 'Perusahaan') {
			// UTTP 
			$uttpOwner = $uttpOwnerModel->where('nib', $user->nib)->first();
			$uutOwner = $uutOwnerModel->where('nib', $user->nib)->first();
		} elseif ($user->kantor == 'Pribadi') {
			// UTTP 
			$uttpOwner = $uttpOwnerModel->where('npwp', $user->npwp)->first();
			$uutOwner = $uutOwnerModel->where('npwp', $user->npwp)->first();
		}elseif ($user->kantor == 'Mahasiswa') {
			// UTTP 
			$uttpOwner = $uttpOwnerModel->where('ktp', $user->ktp)->first();
			$uutOwner = $uutOwnerModel->where('ktp', $user->ktp)->first();
		}
		 elseif ($user->kantor == 'Unit Metrologi Legal') {
			// UTTP 
			$uttpOwner = $uttpOwnerModel->where('uml_id', $user->uml_id)->first();
			$uutOwner = $uutOwnerModel->where('uml_id', $user->uml_id)->first();
		}

		if ($uttpOwner == null) {
			$uttpOwner = $this->addUttpOwner($user);
		}

		$userUttpOwnerModel = new UserUttpOwnerModel();
		$userUttpOwner = [
			'user_id' => $user->id,
			'owner_id' => $uttpOwner->id,
		];

		if (! $userUttpOwnerModel->save($userUttpOwner) ) {
			return redirect()->back()->withInput()->with('errors', $userUttpOwnerModel->errors());
		}

		if ($uutOwner == null) {
			$uutOwner = $this->addUutOwner($user);
		}

		$userUutOwnerModel = new UserUutOwnerModel();
		$userUutOwner = [
			'user_id' => $user->id,
			'owner_id' => $uutOwner->id,
		];

		if (! $userUutOwnerModel->save($userUutOwner) ) {
			return redirect()->back()->withInput()->with('errors', $userUutOwnerModel->errors());
		}


		$client = \Config\Services::curlrequest();
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			$email = $client->request('POST', config('App')->siksURL ."mailer/send", [
				'form_params' => [
					'token' => $token,
					'customer_id' => $user->id,
					'url' => base_url('auth/login'),
					'type' => 'activation',
				]
			]);
		}

        return view('admin/auth/activate-success');
	}

	private function activateUML($token)
	{
		$db = \Config\Database::connect();
		$users = new User();
		$user = $users->where('token', $token)
			->where('is_active', false)
			->first();

		if ($user == null) {
			return view('admin/auth/activate-failed');
		}

		$user->is_active = true;
		$user->email_verified_at = date("Y m d H:i:s");
		//$user->token = null;

        if (! $users->save($user)) {
			return redirect()->back()->withInput()->with('errors', $users->errors());
        }

		$uttpOwnerModel = new UttpOwnerModel();
		$uutOwnerModel = new UutOwnerModel();

		$uttpOwner = null;
		$uutOwner = null;

		if ($user->kantor == 'Unit Metrologi Legal') {
			// UTTP 
			$uttpOwner = $uttpOwnerModel->where('uml_id', $user->uml_id)->first();
			$uutOwner = $uutOwnerModel->where('uml_id', $user->uml_id)->first();
		}

		if ($uttpOwner == null) {
			$uttpOwner = $this->addUttpOwner($user);
		}

		$userUttpOwnerModel = new UserUttpOwnerModel();
		$userUttpOwner = [
			'user_id' => $user->id,
			'owner_id' => $uttpOwner->id,
		];

		if (! $userUttpOwnerModel->save($userUttpOwner) ) {
			return redirect()->back()->withInput()->with('errors', $userUttpOwnerModel->errors());
		}

		if ($uutOwner == null) {
			$uutOwner = $this->addUutOwner($user);
		}

		$userUutOwnerModel = new UserUutOwnerModel();
		$userUutOwner = [
			'user_id' => $user->id,
			'owner_id' => $uutOwner->id,
		];

		if (! $userUutOwnerModel->save($userUutOwner) ) {
			return redirect()->back()->withInput()->with('errors', $userUutOwnerModel->errors());
		}

        return;
	}

	private function addUttpOwner($user) {
		$db = \Config\Database::connect();

		$uttpOwnerModel = new UttpOwnerModel();
		$uttpOwner = [
			'nama' 				=> $user->full_name,
			'alamat' 			=> $user->alamat,
			'email' 			=> $user->email,
			'kota_id' 			=> $user->kota_id,
			'telepon' 			=> $user->phone,
			'nib' 				=> $user->nib,
			'npwp' 				=> $user->npwp,
			'penanggung_jawab'  => $user->penanggung_jawab,
			'bentuk_usaha_id'  	=> $user->bentuk_usaha_id,
			'uml_id'  			=> $user->uml_id > 0 ? $user->uml_id : null,
			'ktp'  			=> $user->ktp > 0 ? $user->ktp : null,
		];

		if (! $uttpOwnerModel->save($uttpOwner) ) {
			return redirect()->back()->withInput()->with('errors', $uttpOwnerModel->errors());
		}

		return $uttpOwnerModel->find($db->insertID());
	}

	private function addUutOwner($user) {
		$db = \Config\Database::connect();

		$uutOwnerModel = new UutOwnerModel();
		$uutOwner = [
			'nama' 				=> $user->full_name,
			'alamat' 			=> $user->alamat,
			'email' 			=> $user->email,
			'kota_id' 			=> $user->kota_id,
			'telepon' 			=> $user->phone,
			'nib' 				=> $user->nib,
			'npwp' 				=> $user->npwp,
			'penanggung_jawab'  => $user->penanggung_jawab,
			'bentuk_usaha_id'  	=> $user->bentuk_usaha_id,
			'uml_id'  			=> $user->uml_id > 0 ? $user->uml_id : null,
			'ktp'  			=> $user->ktp > 0 ? $user->ktp : null,
		];
		

		if (! $uutOwnerModel->save($uutOwner) ) {
			return redirect()->back()->withInput()->with('errors', $uutOwnerModel->errors());
		}

		return $uutOwnerModel->find($db->insertID());
	}


	public function internalLogin()
	{
		if ($this->request->getMethod() === 'get'){
			return view('admin/auth/internal-login');
		}

		$user = new InternalUser();
		$user = $user->where('username', $this->request->getPost('username'))
			->where('is_active', true)
			->first();
		$db = \Config\Database::connect();
		
		if ($user) {
			$verify_pass = password_verify($this->request->getPost('password'), $user->password);
			if ($verify_pass) {
				
				$sesion_data = [
                    'user_id'       	=> $user->id,
                    'user_username'     => $user->username,
					'user_fullname'     => $user->full_name,
                    'user_email'    	=> $user->email,
                    'logged_in'     	=> TRUE,
					'role'				=> $user->user_role
                ];
				$session = session();
                $session->set($sesion_data);

				if(session()->get('login_referrer')!=""){
					$tmp_referrer = session()->get('login_referrer');
					session()->remove('login_referrer');
					redirect($tmp_referrer);
				}

				return redirect()->to(base_url('/'));
			} 
		} 

		return redirect()->back()->withInput()->with('errors', 'Username dan/atau password salah');
	}

	public function check_data($type) {
		$client = \Config\Services::curlrequest();

		if ($type == 'npwp') {

			$npwp = $client->request('POST', getenv('NPWP_url'), [
				'headers' => [
					'x-api-key' => getenv('NPWP_apikey'),
				],
				'form_params' => [
					getenv('NPWP_body_param') => $this->request->getPost('value'),
				],
				'http_errors' => false
			]);

			$body = $npwp->getBody();
			$json = json_decode($body);
			// save log first
			$responseLog = [
				'api_request_function' 	=> 'check_data',
				'api_request_type' 		=> 'NPWP',
				'url'					=> getenv('NPWP_url'),
				'body_param'			=> $this->request->getPost('value'),
				'body_response_raw'		=> $body,
			];
			$responseLogs = new ApiResponseModel();
			$responseLogs->save($responseLog);

			if ($npwp->getStatusCode() != 200) {
				return $this->respond(["kode" => 500, "error" => "Internal error"], 500);
			}

			if ($json != null) {
				if ($json->kode == 200) {
					/*
					$kotaNama = $json->data->KABKOT;
						
					$db = \Config\Database::connect();
					$kota = $db->query("select * from master_kabupatenkota where upper(nama) = '" .$kotaNama. "'")->getRow();
					if ($kota != null) {
						$kota_id = $kota->id;
						$kota_nama = $kota->nama;
						
						$prov = $db->query("select * from master_provinsi where id = " .$kota->provinsi_id)->getRow();
						$prov_id = $prov->id;
						$prov_nama = $prov->nama;
					}
					*/

					$badan_usaha_id = 1;
					/*
					$jenis_perseroan = $json->data->BADAN_HUKUM;
					if ($jenis_perseroan != null) {
						switch ($jenis_perseroan) {
							case 'PERSEROAN TERBATAS':
								$badan_usaha_id = 3;
								break;
							case '-':
								$badan_usaha_id = 1;
								break;
							case 'PERSEROAN KOMANDITER':
								$badan_usaha_id = 2;
								break;
							case 'BENTUK USAHA TETAP':
								$badan_usaha_id = 4;
								break;
							
							default:
								$badan_usaha_id = null;
								break;
						}
					}
					*/

					$response = [
						"kode" => $json->kode,
						"keterangan" => $json->keterangan,
						"data" => [
							"full_name" => ucwords(strtolower($json->data->NAMA)),
							"npwp" => $json->data->NPWP,
							//"alamat" => ucwords(strtolower($json->data->ALAMAT)),
							//"email" => strtolower($json->data->EMAIL),
							//"telepon" => $json->data->TELP,
							//"kota_id" => $kota_id,
							//"kota" => $kota_nama,
							//"provinsi_id" => $prov_id,
							//"provinsi" => $prov_nama,
							"bentuk_badan_usaha_id" => $badan_usaha_id,
						],
						"json" => $json->data,
					];
					$kode = $json->kode;
				} else {
					$response = [
						"kode" => 404,
						"keterangan" => $json->keterangan,
					];
					$kode = 404;
				}

				return $this->respond($response, $kode);
			}

			return $this->respond(["kode" => 500, "error" => "Internal error"], 500);
		};
		if ($type == 'nib') {
			
			$npwp = $client->request('POST', getenv('NIB_url'), [
				'headers' => [
					'x-api-key' => getenv('NIB_apikey'),
				],
				'form_params' => [
					getenv('NIB_body_param') => $this->request->getPost('value'),
				],
				'http_errors' => false
			]);

			$body = $npwp->getBody();
			$json = json_decode($body,false);
			// save log first
			$responseLog = [
				'api_request_function' 	=> 'check_data',
				'api_request_type' 		=> 'NIB',
				'url'					=> getenv('NIB_url'),
				'body_param'			=> $this->request->getPost('value'),
				'body_response_raw'		=> $body,
			];
			$responseLogs = new ApiResponseModel();
			$responseLogs->save($responseLog);

			if ($npwp->getStatusCode() != 200) {
				return $this->respond(["kode" => 500, "error" => "Internal error"], 500);
			}
			
			if ($json != null && $json->responinqueryNIB != null) {
				if ($json->responinqueryNIB->kode == 200) {
					$kota_id = null;
					$kota_nama = null;
					$prov_id = null;
					$prov_nama = null;	

					if ($json->responinqueryNIB->dataNIB->perseroan_daerah_id != null && 
						$json->responinqueryNIB->dataNIB->perseroan_daerah_id != '-') {
						$kelDesa = $json->responinqueryNIB->dataNIB->perseroan_daerah_id;
						$kotaKode = substr($kelDesa,0,2) . '.' .substr($kelDesa,2,2);
							
						$db = \Config\Database::connect();
						$kota = $db->query("select * from master_kabupatenkota where kode = '" .$kotaKode. "'")->getRow();
						if ($kota != null) {
							$kota_id = $kota->id;
							$kota_nama = $kota->nama;
							
							$prov = $db->query("select * from master_provinsi where id = " .$kota->provinsi_id)->getRow();
							$prov_id = $prov->id;
							$prov_nama = $prov->nama;
						}
					}
					$jenis_perseroan = $json->responinqueryNIB->dataNIB->jenis_perseroan;
					if ($jenis_perseroan != null) {
						switch ($jenis_perseroan) {
							case '01':
								$badan_usaha_id = 3;
								break;
							case '17':
								$badan_usaha_id = 1;
								break;
							case '02':
								$badan_usaha_id = 2;
								break;
							case '16':
								$badan_usaha_id = 4;
								break;
							
							default:
								$badan_usaha_id = null;
								break;
						}
					}
					if (count($json->responinqueryNIB->dataNIB->penanggung_jwb) > 0) {
						$penanggung_jawab = $json->responinqueryNIB->dataNIB->penanggung_jwb[0]->nama_penanggung_jwb;
					} else {
						$penanggung_jawab = null;
					}

					$response = [
						"kode" => $json->responinqueryNIB->kode,
						"keterangan" => $json->responinqueryNIB->keterangan,
						"data" => [
							"nib" => $json->responinqueryNIB->dataNIB->nib,
							"full_name" => ucwords(strtolower($json->responinqueryNIB->dataNIB->nama_perseroan)),
							"npwp" => $json->responinqueryNIB->dataNIB->npwp_perseroan,
							"alamat" => ucwords(strtolower($json->responinqueryNIB->dataNIB->alamat_perseroan)),
							"email" => strtolower($json->responinqueryNIB->dataNIB->email_perusahaan),
							"telepon" => $json->responinqueryNIB->dataNIB->nomor_telpon_perseroan,
							"kota_id" => $kota_id,
							"kota" => $kota_nama,
							"provinsi_id" => $prov_id,
							"provinsi" => $prov_nama,
							"bentuk_badan_usaha_id" => $badan_usaha_id,
							"penanggung_jawab" => ucwords(strtolower($penanggung_jawab)),
						],
						"json" => $json->responinqueryNIB->dataNIB,
					];
					$kode = $json->responinqueryNIB->kode;
				} else {
					$response = [
						"kode" => 404,
						"keterangan" => $json->responinqueryNIB->keterangan,
					];
					$kode = 404;
				}

				return $this->respond($response, $kode);
			}

			return $this->respond(["kode" => 500, "error" => "Internal error"], 500);
		};

		return $this->respond(["kode" => 500, "error" => "Unknown method"], 500);
	}

	public function forget()
	{
		if ($this->request->getMethod() === 'get'){
			return view('admin/auth/forget');
		}

		helper('text');

		$userModel = new User();
		$where = "(username='".  $this->request->getPost('username') . "' or email='" 
			. $this->request->getPost('username') . "' or nib='" 
			. $this->request->getPost('username') . "' or npwp='"
			. $this->request->getPost('username') . "')" ;
		$user = $userModel->where($where)
			->where('is_active', true)
			->first();
		// dd($user);
		if ($user) {
			$user->token = random_string('alnum', 8);

			if (! $userModel->save($user)) {
				return redirect()->back()->withInput()->with('errors', $errors);
			}

			//$user = $users->find($db->insertID());

			$client = \Config\Services::curlrequest();
			$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
				'form_params' => [
					'username' => 'customer',
					'password' => 'cerber0s',
					'client' => 'skhp',
					'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
				]
			]);

			$loginBody = $login->getBody();
			$loginJson = json_decode($loginBody);

			$token = $loginJson->token;
		
			if ($token) {
				$email = $client->request('POST', config('App')->siksURL ."mailer/send", [
					'form_params' => [
						'token' => $token,
						'customer_id' => $user->id,
						'url' => base_url('auth/changePassword/' . $user->token),
						'type' => 'forget',
					]
				]);
			}
	
			// redirect()->to(base_url('/'))->with('success', 'Berhasil mengatur perubahan password');
			return redirect()->to(base_url('auth/forgetSuccess'))->with('success', 'Berhasil meengatur perubahan password.');

			/*
			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setSubject('Lupa Password, Panduan Ubah Password Akun Pengguna Anda');
			$emailBody = "<p>Yth. Bapak/Ibu, ". $user->full_name ."</p>

				<p>Kami beritahukan bahwa kami telah menerima permohonan perubahan password akun pada SIMPEL UPTP IV sebagai berikut:</p>
				
				<p>
				Nama Lengkap: ". $user->full_name ."<br/>
				Username: " . $user->username . "</p>
		
				<p>Ubah password user Anda <a href=\"" .base_url('auth/changePassword/' . $user->token). 
				"\">disini</a> atau copy alamat ini: 
				<a href=\"" .base_url('auth/changePassword/' . $user->token)."\">".base_url('auth/changePassword/' . $user->token)."</a> pada browser yang biasa Anda gunakan.</p>

				<p>Direktorat Metrologi</p>

				<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>";

			$email->setMessage($emailBody);
			if($email->send()){
				return redirect()->to(base_url('/'))->with('success', 'Berhasil mengatur perubahan password');
			} else {
				return redirect()->to(base_url('/'))->with('success', 'Berhasil mengatur perubahan password, namun gagal mengirim email. Hubungi administrator. '.$email->printDebugger());
			}
			*/
		} 

		return redirect()->back()->withInput()->with('errors', 'User tidak dikenali atau belum aktif');
	}

	public function forgetSuccess()
	{
		return view('admin/auth/forget-success');
	}

	public function changePassword($token)
	{
		$db = \Config\Database::connect();
		$userModel = new User();
		$user = $userModel->where('token', $token)->first();

		if (!$user) {
			return redirect()->to(base_url('/'))->with('error', 'User tidak dikenali');
		}

		if ($this->request->getMethod() === 'get'){
			return view('admin/auth/change-password', [
				'user' => $user,
			]);
		}

		helper('text');

		if ($user) {
			//$user->token = null;
			$user->password = password_hash($this->request->getPost('new_password'), PASSWORD_DEFAULT);

			if (! $userModel->save($user)) {
				return redirect()->back()->withInput()->with('errors', $errors);
			}

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setSubject('Password Anda Telah Berubah');
			$emailBody = "<p>Yth. Bapak/Ibu, ". $user->full_name ."</p>

				<p>Kami beritahukan bahwa Anda telah berhasil mengubah password akun pada SIMPEL UPTP IV sebagai berikut:</p>
				
				<p>
				Nama Lengkap: ". $user->full_name ."<br/>
				Username: " . $user->username . "</p>
		
				<p>Klik <a href=\"" .base_url('auth/login'). 
				"\">disini</a> untuk masuk ke aplikasi SIMPEL UPTP IV.</p>
				
				<p>Direktorat Metrologi</p>

				<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>";

			$email->setMessage($emailBody);
			if($email->send()){
				return redirect()->to(base_url('/'))->with('success', 'Berhasil mengatur perubahan password');
			} else {
				//dd($email->printDebugger());
				return redirect()->to(base_url('/'))->with('success', 'Berhasil mengatur perubahan password, namun gagal mengirim email. Hubungi administrator. '.$email->printDebugger());
			}
		} 

		return redirect()->back()->withInput()->with('errors', 'User tidak dikenali');
	}

	public function profile() 
	{
		$userModel = new User();
		$user = $userModel->find(session('user_id'));
		
		$db = \Config\Database::connect();
		$uml = $db->query("select * from uml where id = " . $user->id)->getRow();
		$kota = $db->query("select * from master_kabupatenkota where id = " . $user->kota_id)->getRow();
		$provinsi = $db->query("select * from master_provinsi where id = " . $kota->provinsi_id)->getRow();

		$kotaList = $db->query("select * from master_kabupatenkota")->getResult();
		$provinsiList = $db->query("select * from master_provinsi")->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/auth/profile', [
				'user' => $user,
				'uml' => $uml,
				'kota' => $kota,
				'provinsi' => $provinsi,
				'kotaList' => $kotaList,
				'provinsiList' => $provinsiList,
			]);
		}

		$user->full_name = $this->request->getPost('full_name');
		$user->email = $this->request->getPost('email');
		$user->phone = $this->request->getPost('phone');
		$user->alamat = $this->request->getPost('alamat');
		$user->kota_id = $this->request->getPost('kota_id');
		$user->ktp = $this->request->getPost('ktp');

        if (! $userModel->save($user)) {
			return redirect()->back()->withInput()->with('errors', $userModel->errors());
        }

        return redirect()->to(base_url('/auth/profile'))->with('success', 'Berhasil menyimpan perubahan profil pengguna');
	}

	public function profilePassword() 
	{
		$userModel = new User();
		$user = $userModel->find(session('user_id'));

		if (!$user) {
			return redirect()->to(base_url('/'))->with('error', 'User tidak dikenali');
		}

		if ($this->request->getMethod() === 'get'){
			return view('admin/auth/change-password', [
				'user' => $user,
			]);
		}

		helper('text');

		if ($user) {
			$verify_pass = password_verify($this->request->getPost('password'), $user->password);
			
			if ($verify_pass) {
				$user->password = password_hash($this->request->getPost('new_password'), PASSWORD_DEFAULT);

				if (! $userModel->save($user)) {
					return redirect()->back()->withInput()->with('errors', $errors);
				}

				return redirect()->to(base_url('auth/profile'))->with('success', 'Berhasil mengubah password');
			}

			return redirect()->back()->withInput()->with('errors', ['password' => 'Password lama tidak sesuai']);
		} 

		return redirect()->back()->withInput()->with('errors', ['user' => 'User tidak dikenali']);
	}
	
	

	public function findUmlById() {

		$db = \Config\Database::connect();

		$umlModel = $db->table('v_profil_uml');
		$builder = $umlModel;

		$id = $this->request->getPost('id');
		if ($id) {
			$builder = $builder->where('id', $id);
		}
		$umls = $builder
			->get()
			->getRow();

		return $this->respond($umls, 200);
	}

	public function findKota($id) {
		$db = \Config\Database::connect();
		$model = $db->table('master_kabupatenkota');
		$builder = $model->select("id,nama")->where('provinsi_id', $id);

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->like('lower(nama)', strtolower($q));
		}
		
		return $this->respond($builder->get()->getResult(), 200);
	}

	public function findProv() {
		$id = $this->request->getPost('id');
		$db = \Config\Database::connect();
		$model = $db->table('master_kabupatenkota');
		$builder = $model->where('id', $id)->get()->getRow();
		// dd($builder);

		$prov = $db->table('master_provinsi');
		$data = $prov->where('id',$builder->provinsi_id);

		$q = $this->request->getGet('q');
		if ($q) {
			$data = $data->like('lower(nama)', strtolower($q));
		}

		$data = $data->get()->getRow();
		
		return $this->respond($data, 200);
	}
}
