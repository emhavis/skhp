<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\BookingModel;
use App\Models\BookingItemModel;


class Standard extends BaseController
{
	use ResponseTrait;

	public function read($tool_code) {
		$db = \Config\Database::connect();

		$query = $db->query("select s.id, s.tool_code, s.jumlah_per_set, s.standard_detail_type_id, smt.standard_type, stt.attribute_name, sdt.standard_detail_type_name " . 
			"from standards s " .
			"inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
			"inner join standard_tool_types stt on s.standard_tool_type_id = stt.id " .
			"inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id " .
			"where tool_code = " . $db->escape($tool_code) );
		$results = $query->getRow();

		return $this->respond($results, 200);
	}

	
}
