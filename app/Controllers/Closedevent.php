<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ClosedeventModel;

class Closedevent extends BaseController
{
	public function index()
	{
		$eventModel = new ClosedeventModel();

		return view ('admin/events/index', [
			'events' => $eventModel->findAll(),
		]);
	}

	public function create() {
		if ($this->request->getMethod() === 'get'){
			return view('admin/events/create');
		}

		$eventModel = new ClosedeventModel();
		$event = [
			'start_date'		=> date("Y-m-d", strtotime($this->request->getPost('start_date'))),
			'end_date'			=> date("Y-m-d", strtotime($this->request->getPost('end_date'))),
			'name' 				=> $this->request->getPost('name'),
		];

		if (! $eventModel->save($event)) {
			return redirect()->back()->withInput()->with('errors', $eventModel->errors());
        }

        return redirect()->to(base_url('/closedevent'))->with('success', 'Berhasil menyimpan informasi tutup layanan');
	}

	public function edit($id) {
		$eventModel = new ClosedeventModel();
		$event = $eventModel->find($id);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/events/edit', [
				'event' => $event,
			]);
		}
		
		$event->start_date = date("Y-m-d", strtotime($this->request->getPost('start_date')));
		$event->end_date = date("Y-m-d", strtotime($this->request->getPost('end_date')));
		$event->name = $this->request->getPost('name');

		if (! $eventModel->save($event)) {
			return redirect()->back()->withInput()->with('errors', $eventModel->errors());
        }

        return redirect()->to(base_url('/closedevent'))->with('success', 'Berhasil menyimpan perubahan informasi tutup layanan');
	}
}
