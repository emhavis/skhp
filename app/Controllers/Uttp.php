<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\UttpModel;
use App\Models\UttpOwnerModel;
use App\Models\UttpTypeModel;
use App\Models\UttpUnitModel;

use function PHPUnit\Framework\isEmpty;

class Uttp extends BaseController
{
	use ResponseTrait;

	public function index() {

		$uttpModel = new UttpModel();
		$uttps = $uttpModel
			->select('uttps.*, master_uttp_types.uttp_type, uttp_owners.nama as owner')
			->join('uttp_owners', 'uttp_owners.id = uttps.owner_id')
			->join('master_uttp_types', 'master_uttp_types.id = uttps.type_id')
			->findAll();

		return view ('admin/uttps/index', [
			'uttps' => $uttps,
		]);
	}

	public function create() {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owners = $ownerModel->findAll();

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->where('is_active', true)->findAll();

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();


		if ($this->request->getMethod() === 'get'){
			return view('admin/uttps/create', [
				'owners' => $owners,
				'uttpTypes' => $uttpTypes,
				'negaras' => $negaras,
			]);
		}
		if($this->request->getPost('tool_made_in_id') != null){
			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
			->getRow();
		}else{
			$negara =['id' => '64', 'nama_negara' => 'Indonesia'];
			$negara = (object)$negara;
			// dd($negara->nama_negara);
		}
		

		$uttp = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),

		];

		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

        return redirect()->to(base_url('/uttp'))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function createForBooking($owner_id, $type_id, $booking_id) {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owner = $ownerModel->find($owner_id);

		$uttpTypeModel = new UttpTypeModel();
		$uttpType = $uttpTypeModel->where('is_active', true)->find($type_id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uttps/create_forbooking', [
				'owner' => $owner,
				'uttpType' => $uttpType,
				'negaras' => $negaras,
				'bookingId' => $booking_id,
			]);
		}
		if($this->request->getPost('tool_made_in_id') != null){
			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
			->getRow();
		}else{
			$negara =['id' => '64', 'nama_negara' => 'Indonesia'];
			$negara = (object)$negara;
			// dd($negara->nama_negara);
		}
		

		$uttp = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),

		];

		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

        return redirect()->to(base_url('/bookingitem/create/' .$booking_id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function edit($id) {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owners = $ownerModel->findAll();

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->where('is_active', true)->findAll();

		$uttp = $uttpModel->find($id);
		$owner = $ownerModel->find($uttp->owner_id);
		$uttpType = $uttpTypeModel->find($uttp->type_id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uttps/edit', [
				'uttp' => $uttp,
				'owners' => $owners,
				'owner' => $owner,
				'uttpTypes' => $uttpTypes,
				'uttpType' => $uttpType,
				'negaras' => $negaras,
			]);
		}
		
		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();

		//$uttp->type_id = $this->request->getPost('type_id');
		$uttp->serial_no = $this->request->getPost('serial_no');
		$uttp->tool_brand = $this->request->getPost('tool_brand');
		$uttp->tool_model = $this->request->getPost('tool_model');
		$uttp->tool_type = $this->request->getPost('tool_type');
		$uttp->tool_capacity = $this->request->getPost('tool_capacity');
		$uttp->tool_capacity_unit = $this->request->getPost('tool_capacity_unit');
		$uttp->tool_factory = $this->request->getPost('tool_factory');
		$uttp->tool_factory_address = $this->request->getPost('tool_factory_address');
		//$uttp->tool_made_in = $this->request->getPost('tool_made_in');
		$uttp->tool_made_in = $negara->nama_negara;
		$uttp->tool_made_in_id =  $negara->id;
		//$uttp->owner_id = $this->request->getPost('owner_id');
		$uttp->tool_media = $this->request->getPost('tool_media');
		$uttp->tool_name = $this->request->getPost('tool_name');
		$uttp->tool_capacity_min = $this->request->getPost('tool_capacity_min');

		$uttp->location_lat = $this->request->getPost('location_lat');
		$uttp->location_long = $this->request->getPost('location_long');
		$uttp->location = $this->request->getPost('location');
		
		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

        //return redirect()->to(base_url('/uttp'))->with('success', 'Berhasil menyimpan perubahan data');
		return redirect()->to(base_url('/useruttp'))->with('success', 'Berhasil menyimpan perubahan data');
	}

	public function copy($id) {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owners = $ownerModel->findAll();

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->where('is_active', true)->findAll();

		$uttp = $uttpModel->find($id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uttps/copy', [
				'uttp' => $uttp,
				'owners' => $owners,
				'uttpTypes' => $uttpTypes,
				'negaras' => $negaras,
			]);
		}

		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();
		
		$uttp = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),
		];

		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

        return redirect()->to(base_url('/uttp'))->with('success', 'Berhasil menyimpan penambahan data');
	}
	
	public function delete($id) {
		$db = \Config\Database::connect();

		$db->transStart(); 
		
		$itemBuilder = $db->table('service_booking_items');
		$items = $itemBuilder->where('booking_id', $id)->get()->getResult();
		foreach ($items as $item) {
			$inspectionBuilder = $db->table('service_booking_item_inspections');
			$inspectionBuilder->where('booking_item_id', $item->id)->delete();
		}

		$itemBuilder->where('booking_id', $id)->delete();

		$bookingBuilder = $db->table('service_bookings');
		$bookingBuilder->where('id', $id)->delete();

		$db->transComplete();

		if ($db->transStatus() === FALSE)
		{
			return redirect()->back()->withInput()->with('errors', 'Gagal menghapus data booking');
		}

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menghapus data booking');
	}

	public function find($type_id, $owner_id) {

		$db = \Config\Database::connect();

		$uttpModel = new UttpModel();
		$builder = $uttpModel
			->where('type_id', $type_id)
			->where('owner_id', $owner_id);

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->where("serial_no like '%".$db->escapeLikeString($q)."%'");
		}
		$uttps = $builder
			->findAll();
		
		return $this->respond($uttps, 200);
	}

	public function get($id) {

		$uttpModel = new UttpModel();
		$uttp = $uttpModel
			->select('uttps.*, master_uttp_types.uttp_type, uttp_owners.nama as owner')
			->join('uttp_owners', 'uttp_owners.id = uttps.owner_id')
			->join('master_uttp_types', 'master_uttp_types.id = uttps.type_id')
			->where('uttps.id', $id)
			->first();

		return $this->respond($uttp, 200);
	}

	public function getSertifikat($id) {

		$uttpModel = new UttpModel();
		$uttp = $uttpModel
			->select('uttps.*, master_uttp_types.uttp_type, uttp_owners.nama as owner')
			->join('uttp_owners', 'uttp_owners.id = uttps.owner_id')
			->join('master_uttp_types', 'master_uttp_types.id = uttps.type_id')
			->where('uttps.id', $id)
			->first();

			$db = \Config\Database::connect();

		$sertifikat = $db->query(
			'select sou.no_sertifikat, to_char(sou.kabalai_date, \'dd-mm-yyyy\') kabalai_date,
			sru.service_type_id
			from service_order_uttps sou 
			inner join service_request_uttps sru on sou.service_request_id = sru.id
			where 
			sou.path_skhp is not null and sou.kabalai_date is not null
			and sou.uttp_id = ' . $uttp->id . ' 
			order by sou.no_sertifikat_year desc, sou.no_sertifikat_int desc 
			limit 1')->getRow();;

		return $this->respond([
			'uttp' => $uttp,
			'sertifikat' => $sertifikat
		], 200);
	}

	public function unit($type_id) {

		$db = \Config\Database::connect();

		$unitModel = new UttpUnitModel();
		$builder = $unitModel
			->where('uttp_type_id', $type_id);

		$units = $builder
			->findAll();

		return $this->respond($units, 200);
	}

	public function getType($id) {

		$uttpTypeModel = new UttpTypeModel();
		$uttpType = $uttpTypeModel->find($id);

		return $this->respond($uttpType, 200);
	}

	public function findBySeries() {

		$db = \Config\Database::connect();

		$uttpModel = new UttpModel();
		$builder = $uttpModel
			->where('type_id', $this->request->getPost('type_id'))
			->where('owner_id', $this->request->getPost('owner_id'))
			->where('serial_no', $this->request->getPost('q'));

		$uttps = $builder
			->findAll();
		
		return $this->respond($uttps, 200);
	}
}
