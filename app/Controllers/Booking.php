<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\HolidayModel;
use App\Models\ClosedeventModel;
use App\Models\ServiceTypeModel;
use App\Models\User;
use App\Models\UttpOwnerModel;
use App\Models\UutOwnerModel;

class Booking extends BaseController
{
	public function index() {
	

		/*
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			//->where('booking_no is null', null, false)
			->where('pic_id', session('user_id'))
			->findAll();
		*/

		$db = \Config\Database::connect();
		$bookings = $db->query('select sb.*,mrs.status from service_bookings sb ' .
			'left join service_requests sr on sb.id = sr.booking_id ' .
			'left join master_request_status mrs on mrs.id = sr.status_id ' .
			'where sr.booking_id is null and sb.service_type_id in (1,2) ' .
			'and sb.pic_id = ' . session('user_id') .
			' union ' . 
			'select sb.*,mrs.status from service_bookings sb ' .
			'left join service_request_uttps sr on sb.id = sr.booking_id ' .
			'left join master_request_status mrs on mrs.id = sr.status_id ' .
			'where sr.booking_id is null and sb.service_type_id in (4,5,6,7) ' .
			'and sb.pic_id = ' . session('user_id') .  
			' ORDER BY created_at DESC ')->getResult();

		// dd($bookings,session('user_id'));

		return view ('admin/bookings/index', [
			'bookings' => $bookings,
		]);
	}

	public function create() {
		/*
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('/auth/logout');
		}
		*/
		$ktp ='';
		$uml_id = session()->get('user_uml_id');
		$holidayModel = new HolidayModel();
		$holidays = $holidayModel->where('holiday_date >= current_date')
			->findAll();
		$db = \Config\Database::connect();
		$events = $db->query('select generate_series(start_date, end_date, \'1 day\') closed_date
			from closed_events ce')->getResult();
		// dd($uml_id);
		$quota = 2;
		$quotas = $db->query('select est_arrival_date as quota_date, count(id) cnt from service_bookings sb 
			where booking_no  is not null
			group by est_arrival_date
			having est_arrival_date >= current_date 
			and count(id) >= '.$quota)->getResult();

		$userModel = new User();
		$user = $userModel->find(session('user_id'));
		$serviceTypeModel = new ServiceTypeModel();

		if($user->id ==1){
			$types = $serviceTypeModel->find($user->id);
		}else{
			$types = $serviceTypeModel->orderBy('id')->findAll();
		}
		// $types = $serviceTypeModel->orderBy('id')->findAll();

		$ownerModel = new UttpOwnerModel();
		$ownerUutModel = new UutOwnerModel();
		$ownerIDUUT =null;
		$ownerIDUTTP =null;
		$alamatOwner = null;
		$namaOwner =null;
		//$owners = $ownerModel->findAll();

		$kotaList = $db->query("select * from master_kabupatenkota")->getResult();
		$provinsiList = $db->query("select * from master_provinsi")->getResult();
		
		if ($this->request->getMethod() === 'get'){
			return view('admin/bookings/create', [
				'holidays' => $holidays,
				'events' => $events,
				'quotas' => $quotas,
				'addr_sertifikat' => $user->alamat,
				'label_sertifikat' => $user->full_name,
				'types' => $types,
				'uml_id' => $uml_id,
				'user' => $user,
				//'owners' => $owners,
				'kotaList' => $kotaList,
				'provinsiList' => $provinsiList,
			]);
		}

		$type = $serviceTypeModel->find($this->request->getPost('service_type_id'));
		// dd($this->request->getPost('uut_owner_id'));
		if($this->request->getPost('uttp_owner_id') !=null || $this->request->getPost('uttp_owner_id') > 0){
			$owner = $ownerModel->find($this->request->getPost('uttp_owner_id'));

			$ownerIDUTTP =$owner->id;
			$alamatOwner = $owner->alamat;
			$namaOwner =$owner->nama;
			$ownerIDUUT =null;

		}elseif($this->request->getPost('uut_owner_id') !=null){
			$ownerUut = $ownerUutModel->find($this->request->getPost('uut_owner_id'));
			$ownerIDUUT =$ownerUut->id;
			$alamatOwner = $ownerUut->alamat;
			$namaOwner =$ownerUut->nama;
			$ownerIDUTTP=null;
		}

		$kalibrasi= '';//$this->request->getFile('certificate_calibration');
		// $perifikasi= '';
		$perifikasi=$this->request->getFile('certificate_perification');
		$file_certificate='';
		// if(isset($kalibrasi)){
		// 	$kalibrasi->store();
		// 	$file_certificate = $kalibrasi->getClientName();
		// }else
		if(isset($perifikasi) && is_file($perifikasi)){
			$file_certificate = $perifikasi->store();
			// $file_certificate = $perifikasi->getClientName();
		}

		$bookings = new BookingModel();
		$booking = [
			'pic_id'         	=> session('user_id'),
			'service_type_id'	=> $this->request->getPost('service_type_id'),
			'jenis_layanan' 	=> $type->service_type,
			'for_sertifikat' 	=> $this->request->getPost('for_sertifikat'),
			//'addr_sertifikat' 	=> $alamatOwner,
			//'label_sertifikat' 	=> $namaOwner, 
			'addr_sertifikat' 	=> $this->request->getPost('addr_sertifikat'),
			'label_sertifikat' 	=> $this->request->getPost('label_sertifikat'),
			'prov_sertifikat_id' => $this->request->getPost('prov_sertifikat_id'),
			'kabkota_sertifikat_id' => $this->request->getPost('kabkota_sertifikat_id'),

			'uttp_owner_id'		=> $ownerIDUTTP,
			'uut_owner_id'		=> $ownerIDUUT,
			//'est_arrival_date' 	=> date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))),

			'lokasi_pengujian'	=> $this->request->getPost('lokasi_pengujian'),

			'created_by'        => session('user_id'),
			'updated_by'        => session('user_id'),
			'file_certificate' 	=> $file_certificate ? $file_certificate : null,

			'lokasi_dl'				=> $this->request->getPost('lokasi_dl'),
			'keuangan_perusahaan'	=> $this->request->getPost('keuangan_perusahaan'),
			'keuangan_pic'			=> $this->request->getPost('keuangan_pic'),
			'keuangan_jabatan'		=> $this->request->getPost('keuangan_jabatan'),
			'keuangan_hp'			=> $this->request->getPost('keuangan_hp'),
		];
		
		if (! $bookings->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookings->errors());
        }

        //return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran baru');
		return redirect()->to(base_url('/booking/edit/'.$bookings->insertID))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function edit($id) {
		// if (session()->get('logged_in') && session()->get('role') > -1) {
		// 	return redirect()->to('auth/logout');
		// }
		
		$bookings = new BookingModel();
		$booking = $bookings->find($id);
		$db = \Config\Database::connect();

		$holidayModel = new HolidayModel();
		$holidays = $holidayModel->where('holiday_date >= current_date')
			->findAll();
		$db = \Config\Database::connect();
		
		$serviceTypeModel = new ServiceTypeModel();
		$types = $serviceTypeModel->findAll();

		//$ownerModel = new UttpOwnerModel();
		//$owners = $ownerModel->findAll();

		$provinces = $db->query('select * from master_provinsi')->getResult();
		$cities = $db->query(
			'select k.*, p.nama as provinsi 
			from master_kabupatenkota k inner 
			join master_provinsi p on k.provinsi_id = p.id')->getResult();

		$kota = $db->query("select * from master_kabupatenkota where id = " . $booking->kabkota_sertifikat_id)->getRow();
		$provinsi = $db->query("select * from master_provinsi where id = " . $booking->prov_sertifikat_id)->getRow();

		$terms = $db->query('select sruk.*
				from service_booking_kajiulang sruk 
				where booking_id = '. $booking->id)->getResult();

		if ($booking->lokasi_pengujian == 'dalam') {
			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_booking_kajiulang sruk 
				where booking_id = '. $booking->id)->getRow();
		} else {
			$check = $db->query('select kaji_ulang_insitu from service_bookings where id = ' . $booking->id)->getRow();
		}

		if (!($booking->service_type_id == 1 || $booking->service_type_id == 2)) {
			$events = $db->query('select generate_series(start_date, end_date, \'1 day\') closed_date
			from closed_events ce where layanan=\'uttp\'')->getResult();
			
			if ($booking->lokasi_pengujian == 'dalam') {
				$bookingItems = $db->query('select sbi.*,  ' . 
					"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
					's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, '.
					's.tool_model, s.tool_type , s.tool_made_in, s.tool_media, s.tool_capacity_unit, s.tool_capacity_min, '.
					'mut.uttp_type as type ' .
					'from service_booking_items sbi ' .
					'inner join uttps s on sbi.uttp_id = s.id ' .
					'inner join master_uttp_types mut on s.type_id = mut.id ' .
					'where sbi.booking_id = ' . $id)->getResult();
			} else {
				$bookingItems = $db->query('select sbi.*,  ' . 
					"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
					's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, '.
					's.tool_model, s.tool_type , s.tool_made_in, s.tool_media, s.tool_capacity_unit, s.tool_capacity_min, '.
					'mut.uttp_type as type, mp.nama as provinsi, mk.nama as kabkot, ng.nama_negara as negara ' .
					'from service_booking_items sbi ' .
					'inner join uttps s on sbi.uttp_id = s.id ' .
					'inner join master_uttp_types mut on s.type_id = mut.id ' .
					'left join master_provinsi mp on mp.id = sbi.location_prov_id ' .
					'left join master_kabupatenkota mk on mk.id = sbi.location_kabkot_id '.
					'left join master_negara ng on ng.id = sbi.location_negara_id '.
					'where sbi.booking_id = ' . $id)->getResult();
			}

			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, sip.unit " .
				"from service_booking_item_inspections sbii " .
				"inner join uut_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();

			/*
			$quotas = $db->query('select sb.est_arrival_date as quota_date, 
				uip.instalasi_id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join uttp_inspection_prices uip on sbii.inspection_price_id = uip.id 
				inner join master_instalasi mi on uip.instalasi_id = mi.id
				where sb.booking_no  is not null
				group by sb.est_arrival_date, uip.instalasi_id, mi.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbii.id) >= mi.quota_online')->getResult();
			*/
			$quotas = $db->query('select sb.est_arrival_date as quota_date, 
				mi.id as instalasi_id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join uttps u on sbi.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_instalasi mi on mut.instalasi_id = mi.id 
				where sb.booking_no is not null
				group by sb.est_arrival_date, mi.id, mi.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbi.id) >= mi.quota_online')->getResult();

			$ttuPerlengkapans = $db->query('select i.*, 
				u.tool_name,
				u.tool_brand,
				u.tool_model,
				u.serial_no,
				u.tool_media,
				u.tool_made_in,
				u.tool_capacity,
				u.tool_capacity_unit,
				mut.uttp_type
				from service_booking_item_ttu_perlengkapan i
				inner join uttps u on i.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id
				inner join service_booking_items bi on i.booking_item_id = bi.id
				where bi.booking_id = '.$id)->getResult();

			$view = 'admin/bookings/edit_uttp';

			if ($this->request->getMethod() === 'get'){
				return view ($view, [
					'booking' => $booking,
					'items' => $bookingItems,
					'inspections' => $bookingInspections,
					'holidays' => $holidays,
					'events' => $events,
					'quotas' => $quotas,
					'types' => $types,
					'provinces' => $provinces,
					'cities' => $cities,
					'ttuPerlengkapans' => $ttuPerlengkapans,
					//'owners' => $owners,
					'kota' => $kota,
					'provinsi' => $provinsi,
					'terms' => $terms,
					'check_terms' => $check,
				]);
			}

			$rules = [];
			$error_msg = [];

			if ($booking->lokasi_pengujian == 'dalam') {
				$rules = [
					'est_arrival_date' => ['required']
				];
				$error_msg = [
					'est_arrival_date' => [
						'required' => 'Rencana pengantaran wajib diisi'
					]
				];
			} else {
				$rules = [
					'est_schedule_date_from' => ['required'],
					'est_schedule_date_to' => ['required'],
					'no_surat_permohonan' => ['required'],
					'tgl_surat_permohonan' => ['required'],
					'file_surat_permohonan' => [
						'uploaded[file_surat_permohonan]', 
						'mime_in[file_surat_permohonan,application/pdf,application/octet-stream]',
						//'ext_in[file_surat_permohonan,pdf]'
					],
					'keuangan_perusahaan'	=> ['required'],
					'keuangan_pic'			=> ['required'],
					'keuangan_jabatan'		=> ['required'],
					'keuangan_hp'			=> ['required'],
				];
				$error_msg = [
					'est_schedule_date_from' => ['required' => 'Usulan jadwal wajib diisi'],
					'est_schedule_date_to' => ['required' => 'Usulan jadwal wajib diisi'],
					'no_surat_permohonan' => ['required' => 'Nomor surat permohonan wajib diisi'],
					'tgl_surat_permohonan' => ['required' => 'Tanggal surat permohonan wajib diisi'],
					'file_surat_permohonan' => [
						'uploaded' => 'Surat permohonan wajib diupload',
						'mime_in' => 'Surat permohonan wajib diupload dalam format PDF',
						//'ext_in' => 'Surat permohonan wajib di-upload dengan extension PDF'
					],
					'keuangan_perusahaan'	=> ['required' => 'Perusahaan penanggung jawab administri keuangan wajib diisi'],
					'keuangan_pic'			=> ['required' => 'PIC penanggung jawab administri keuangan wajib diisi'],
					'keuangan_jabatan'		=> ['required' => 'Jabatan penanggung jawab administri keuangan wajib diisi'],
					'keuangan_hp'			=> ['required' => 'HP penanggung jawab administri keuangan wajib diisi'],
				];
			}

			if (! $this->validate($rules, $error_msg)) {
				//dd([$rules, $error_msg, $this->validator->getErrors()]);
				return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
			}

			$booking->inspection_loc = $this->request->getPost('inspection_loc');
			//$booking->inspection_prov_id = $this->request->getPost('inspection_prov_id');
			$booking->inspection_kabkot_id = $this->request->getPost('inspection_kabkot_id');

			if ($this->request->getPost('inspection_kabkot_id') != null) {
				$city = $db->query('select * from master_kabupatenkota where id = ' . $this->request->getPost('inspection_kabkot_id'))->getRow();
				$booking->inspection_prov_id = $city->provinsi_id;
			}
			if ($booking->lokasi_pengujian == 'luar') {
				$booking->no_surat_permohonan = $this->request->getPost('no_surat_permohonan');
				
				$file_surat_permohonan = $this->request->getFile('file_surat_permohonan');
				if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() != '') { $path_surat_permohonan = $file_surat_permohonan->store(); }
		
				if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() ) {
					$booking->file_surat_permohonan = isset($file_surat_permohonan) ? $file_surat_permohonan->getClientName() : null;
					$booking->path_surat_permohonan = isset($path_surat_permohonan) ? $path_surat_permohonan : null;
				}

				$booking->tgl_surat_permohonan = date("Y-m-d", strtotime($this->request->getPost('tgl_surat_permohonan')));

				$booking->lokasi_dl = $this->request->getPost('lokasi_dl');
				$booking->keuangan_perusahaan = $this->request->getPost('keuangan_perusahaan');
				$booking->keuangan_pic = $this->request->getPost('keuangan_pic');
				$booking->keuangan_jabatan = $this->request->getPost('keuangan_jabatan');
				$booking->keuangan_hp = $this->request->getPost('keuangan_hp');
			}
			
		} else {
			$uml_id = session('user_uml_id');
			$events = $db->query('select generate_series(start_date, end_date, \'1 day\') closed_date
			from closed_events ce where layanan=\'snsu\'')->getResult();
			
			if ($booking->lokasi_pengujian == 'dalam') {
				$bookingItems = $db->query("
					select sbi.*, 
					concat(su.tool_brand, '/', su.tool_model, '/', su.tool_type, ' (', su.serial_no, ')') as tool_code,
					su.serial_no, su.tool_brand, su.tool_capacity, su.tool_factory, su.tool_factory_address,
					su.tool_model, su.tool_type , su.tool_made_in, su.tool_media, su.tool_capacity_unit, su.tool_capacity_min,
					ms.uut_type as type 
					from service_booking_items sbi 
					inner join standard_uut su on sbi.uut_id = su.id 
					inner join master_standard_types ms on su.type_id = ms.id 
					where sbi.booking_id = " . $id)->getResult();
			} else {
				$bookingItems = $db->query("
					select sbi.*, 
					concat(su.tool_brand, '/', su.tool_model, '/', su.tool_type, ' (', su.serial_no, ')') as tool_code,
					su.serial_no, su.tool_brand, su.tool_capacity, su.tool_factory, su.tool_factory_address,
					su.tool_model, su.tool_type , su.tool_made_in, su.tool_media, su.tool_capacity_unit, su.tool_capacity_min,
					ms.uut_type as type,
					mp.nama as provinsi, mk.nama as kabkot
					from service_booking_items sbi 
					inner join standard_uut su on sbi.uut_id = su.id 
					inner join master_standard_types ms on su.type_id = ms.id 
					left join master_provinsi mp on mp.id = sbi.location_prov_id 
					left join master_kabupatenkota mk on mk.id = sbi.location_kabkot_id 
					where sbi.booking_id = " . $id)->getResult();
			}

			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, sip.unit unit " .
				"from service_booking_item_inspections sbii " .
				"inner join uut_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				//"inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();
				
			$quotas = $db->query('select sb.est_arrival_date as quota_date,
				ml.id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join standards s on sbi.uut_id = s.id 
				inner join master_laboratory ml on s.standard_measurement_type_id = ml.id
				where sb.booking_no is not null 
				group by sb.est_arrival_date, ml.id, ml.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbii.id) >= ml.quota_online')->getResult();

			$view = 'admin/bookings/edit';

			if ($this->request->getMethod() === 'get'){
				return view ($view, [
					'booking' => $booking,
					'items' => $bookingItems,
					'inspections' => $bookingInspections,
					'holidays' => $holidays,
					'events' => $events,
					'quotas' => $quotas,
					'types' => $types,
					'provinces' => $provinces,
					'cities' => $cities,
					'uml_id' => $uml_id,
					'kota' => $kota,
					'provinsi' => $provinsi,
					'check_terms' => $check,
				]);
			}

			$booking->inspection_loc = $this->request->getPost('inspection_loc');
			//$booking->inspection_prov_id = $this->request->getPost('inspection_prov_id');
			$booking->inspection_kabkot_id = $this->request->getPost('inspection_kabkot_id');

			if ($this->request->getPost('inspection_kabkot_id') != null) {
				$city = $db->query('select * from master_kabupatenkota where id = ' . $this->request->getPost('inspection_kabkot_id'))->getRow();
				$booking->inspection_prov_id = $city->provinsi_id;
			}
			if ($booking->lokasi_pengujian == 'luar') {
				$booking->no_surat_permohonan = $this->request->getPost('no_surat_permohonan');
				
				$file_surat_permohonan = $this->request->getFile('file_surat_permohonan');
				if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() != '') { $path_surat_permohonan = $file_surat_permohonan->store(); }
		
				if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() ) {
					$booking->file_surat_permohonan = isset($file_surat_permohonan) ? $file_surat_permohonan->getClientName() : null;
					$booking->path_surat_permohonan = isset($path_surat_permohonan) ? $path_surat_permohonan : null;
				}

				$booking->tgl_surat_permohonan = date("Y-m-d", strtotime($this->request->getPost('tgl_surat_permohonan')));
			}
		}

		//$type = $serviceTypeModel->find($this->request->getPost('service_type_id'));
		
		//$booking->service_type_id = $this->request->getPost('service_type_id');
		//$booking->jenis_layanan = $type->service_type;
		//$booking->for_sertifikat = $this->request->getPost('for_sertifikat');
		//$booking->addr_sertifikat = $this->request->getPost('addr_sertifikat');
		//$booking->label_sertifikat = $this->request->getPost('label_sertifikat');

		$verifikasi=$this->request->getFile('certificate_perification');
		$file_certificate='';
		
		if(isset($verifikasi) && is_file($verifikasi)){
			$verifikasi->store();
			$file_certificate = $verifikasi->getClientName();
			$booking->file_certificate =$file_certificate;
		}

		if ($this->request->getPost('est_arrival_date') != null) {
			$booking->est_arrival_date = date("Y-m-d", strtotime($this->request->getPost('est_arrival_date')));
		}
		if ($this->request->getPost('est_schedule_date_from') != null) {
			$booking->est_schedule_date_from = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_from')));
			$booking->est_schedule_date_to = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_to')));
		}
		$booking->updated_by = session('user_id');
		//dd($booking);

		if (! $bookings->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookings->errors());
        }

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function submit($id) {
		if (session()->get('logged_in') && session()->get('user_role') > -1) {
			return redirect()->to('auth/logout');
		}

		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$rules = [];
		$error_msg = [];

		$cek_permohonan = $this->request->getFile('file_surat_permohonan');

		if ($booking->lokasi_pengujian == 'dalam') {
			$rules = [
				'est_arrival_date' => ['required']
			];
			$error_msg = [
				'est_arrival_date' => [
					'required' => 'Rencana pengantaran wajib diisi'
				]
			];
		} else {
			$rules = [
				'est_schedule_date_from' => ['required'],
				'est_schedule_date_to' => ['required'],
				'no_surat_permohonan' => ['required'],
				'tgl_surat_permohonan' => ['required'],
			];

			if($cek_permohonan){
				$rules['file_surat_permohonan'] = ['uploaded[file_surat_permohonan]', 'mime_in[file_surat_permohonan,application/pdf,application/octet-stream]'];
			}
			
			if((!$cek_permohonan) && ($booking->path_surat_permohonan == '' || $booking->path_surat_permohonan == null)) {
				return redirect()->back()->withInput()->with('errors', ['file_surat_permohonan' => 'File permohonan harus diupload.']);
			}
			
			$error_msg = [
				'est_schedule_date_from' => ['required' => 'Usulan jadwal wajib diisi'],
				'est_schedule_date_to' => ['required' => 'Usulan jadwal wajib diisi'],
				'no_surat_permohonan' => ['required' => 'Nomor surat permohonan wajib diisi'],
				'tgl_surat_permohonan' => ['required' => 'Tanggal surat permohonan wajib diisi'],
				'file_surat_permohonan' => [
					'uploaded' => 'Surat permohonan wajib di-upload dalam format PDF',
					'mime_in' => 'Surat permohonan wajib di-upload dalam format PDF'
				]
			];
		}

		if (! $this->validate($rules, $error_msg)) {
			//dd([$rules, $error_msg, $this->validator->getErrors()]);
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		if ($this->request->getPost('est_arrival_date') == null && $booking->lokasi_pengujian == 'dalam') {
			return redirect()->back()->withInput()->with('errors', [
				"est_arrival_date" => 'Rencana pengantaran wajib diisi'
			]);
		}
		if (($this->request->getPost('est_schedule_date_from') == null || $this->request->getPost('est_schedule_date_to') == null)
			&& $booking->lokasi_pengujian == 'luar') {
			return redirect()->back()->withInput()->with('errors', [
				"est_schedule_date_from" => 'Usulan jadwal wajib diisi'
			]);
		}

		$db = \Config\Database::connect();
		$bookingItems = $db->query('select sbi.*, s.tool_code ' .
			'from service_booking_items sbi ' .
			'inner join standards s on sbi.uut_id = s.id ' .
			'where sbi.booking_id = ' . $id)->getResult();

		if (($booking->service_type_id == 1 || $booking->service_type_id == 2)) {
			/*
			$quota = 2;
			$bookingSameDay = $bookingModel->where('id <>', $id)
				->where('est_arrival_date', date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))))
				->where('booking_no is not null')
				->countAllResults();	
			$isOverQuota = $bookingSameDay >= $quota;
			*/

			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date,
				ml.id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join standards s on sbi.uut_id = s.id 
				inner join master_laboratory ml on s.standard_measurement_type_id = ml.id
				where sb.booking_no is not null 
				group by sb.est_arrival_date, ml.id, ml.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."'  
				and count(sbii.id) >= ml.quota_online")->getResult();
			$isOverQuota = count($bookingSameDay) > 0;

		} else {
			$quotas = $db->query('select sb.est_arrival_date as quota_date, 
				mi.id as instalasi_id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join uttps u on sbi.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_instalasi mi on mut.instalasi_id = mi.id 
				where sb.booking_no is not null
				group by sb.est_arrival_date, mi.id, mi.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbi.id) >= mi.quota_online')->getResult();

			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date, 
				mi.id as instalasi_id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join uttps u on sbi.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_instalasi mi on mut.instalasi_id = mi.id 
				where sb.booking_no is not null
				group by sb.est_arrival_date, mi.id, mi.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."' 
				and count(sbi.id) >= mi.quota_online")->getResult();

			/*
			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date, 
				uip.instalasi_id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join uttp_inspection_prices uip on sbii.inspection_price_id = uip.id 
				inner join master_instalasi mi on uip.instalasi_id = mi.id
				where sb.booking_no  is not null
				group by sb.est_arrival_date, uip.instalasi_id, mi.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."' 
				and count(sbii.id) >= mi.quota_online")->getResult();
			*/
			$isOverQuota = count($bookingSameDay) > 0;
		}
		
		if ($isOverQuota) {
			return redirect()->back()->withInput()->with('errors', [
				'quota' => 'Jumlah kuota yang terpakai sudah terpenuhi']);
		}

		/*	
		$quota_used = $db->query('select count(id) count_used from service_bookings sb 
			where id <> '. $id .' and est_arrival_date = \''. $booking->est_arrival_date .'\'')->getResult();
		*/

		//$type = $serviceTypeModel->find($this->request->getPost('service_type_id'));

		//$booking->service_type_id = $this->request->getPost('service_type_id');
		//$booking->jenis_layanan = $type->service_type;
		$booking->for_sertifikat = $this->request->getPost('for_sertifikat');
		$booking->addr_sertifikat = $this->request->getPost('addr_sertifikat');
		$booking->label_sertifikat = $this->request->getPost('label_sertifikat');
		if ($this->request->getPost('est_arrival_date') != null) {
			$booking->est_arrival_date = date("Y-m-d", strtotime($this->request->getPost('est_arrival_date')));
		}
		if ($this->request->getPost('est_schedule_date_from') != null) {
			$booking->est_schedule_date_from = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_from')));
			$booking->est_schedule_date_to = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_to')));
		}
		$booking->booking_no = date("Ymd") . str_pad($booking->id, 3, '0', STR_PAD_LEFT);
		$booking->updated_by = session('user_id');

		$booking->inspection_loc = $this->request->getPost('inspection_loc');
		//$booking->inspection_prov_id = $this->request->getPost('inspection_prov_id');
		$booking->inspection_kabkot_id = $this->request->getPost('inspection_kabkot_id');

		if ($this->request->getPost('inspection_kabkot_id') != null) {
			$city = $db->query('select * from master_kabupatenkota where id = ' . $this->request->getPost('inspection_kabkot_id'))->getRow();
			$booking->inspection_prov_id = $city->provinsi_id;
		}

		if ($booking->lokasi_pengujian == 'luar') {
			$booking->no_surat_permohonan = $this->request->getPost('no_surat_permohonan');
			
			$file_surat_permohonan = $this->request->getFile('file_surat_permohonan');
			if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() != '') { $path_surat_permohonan = $file_surat_permohonan->store(); }
	
			if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() ) {
				$booking->file_surat_permohonan = isset($file_surat_permohonan) ? $file_surat_permohonan->getClientName() : null;
				$booking->path_surat_permohonan = isset($path_surat_permohonan) ? $path_surat_permohonan : null;
			}

			$booking->tgl_surat_permohonan = date("Y-m-d", strtotime($this->request->getPost('tgl_surat_permohonan')));
		}
		
		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }

		$userModel = new User();
		$user = $userModel->find(session('user_id'));

		if($booking->uttp_owner_id !=null){

			$uttpOwner = new UttpOwnerModel();
			$uttpOwner = $uttpOwner->find($booking->uttp_owner_id);
		}else{
			$uttpOwner = new UutOwnerModel();
			$uttpOwner = $uttpOwner->find($booking->uut_owner_id);

		}
		
		/*
		$email = \Config\Services::email();

		$email->setTo($user->email);
		
		$email->setSubject('Booking Pengujian/Pemeriksaan dengan Nomor : '. $booking->booking_no .' Telah Diterima');

		$emailBody = "<p>Yth. Bapak/Ibu, ". $user->full_name ."</p>

		<p>Kami beritahukan bahwa kami telah menerima Booking Anda untuk permohonan sebagai berikut:</p>
		
		<p>
		Nomor Booking: ". $booking->booking_no ."<br/>";

		if ($uttpOwner != null) {
			$emailBody .= "Nama Perusahaan: ". $uttpOwner->nama ."<br/>
			Alamat Perusahaan: ". $uttpOwner->alamat ."<br/>
			NIB Perusahaan: ". $uttpOwner->nib ."<br/>
			NPWP Perusahaan: ". $uttpOwner->npwp ."<br/>
			Telepon: ". $uttpOwner->telepon ."<br/>
			Penanggung Jawab: ". $uttpOwner->penanggung_jawab ."<br/>
			E-mail: ". $uttpOwner->email ."<br/>";
		}
		
		$uttps = $db->query('select u.*,
			mut.uttp_type 
			from service_booking_items sbi 
			inner join uttps u on sbi.uttp_id = u.id 
			inner join master_uttp_types mut on mut.id = u.type_id 
			where booking_id = ' . $id)->getResult();

		foreach ($uttps as $uttp) {
			$emailBody .= "Nama Alat: ". $uttp->uttp_type ."<br/>
				Merk Alat: ". $uttp->tool_brand ."<br/>
				Model/Type Alat: ". $uttp->tool_model ."<br/>
				No Seri Alat: ". $uttp->serial_no ."<br/>";
		}
		
		$emailBody .= "Nama Pemilik Alat pada Sertifikat: ". $booking->label_sertifikat ."<br/>
		Alamat Pemilik Alat pada Sertifikat: ". $booking->addr_sertifikat ."<br/>
		</p>";

		if ($booking->lokasi_pengujian == 'dalam') {
			$emailBody .=
			"<p>Silakan kirim alat UTTP yang akan diuji/diperiksa pada tanggal ". date("d M Y", strtotime($booking->est_arrival_date));	
		}
		
		$emailBody .=
		"<p>Direktorat Metrologi</p>
		
		<p>Email pemberitahuan ini di-<i>generate</i> oleh sistem dan mohon tidak me-<i>reply</i> email ini.</p>";
		
		$email->setMessage($emailBody);
		if($email->send()){
			//echo 'Success!';
			return redirect()->to(base_url('booking'))->with('success', 'Berhasil menyimpan pengguna baru');
		}else {
			
			return redirect()->to(base_url('booking'))->with('warning', 'Berhasil menyimpan pengguna baru, namun gagal mengirim email. Hubungi administrator.'.$email->printDebugger());
		}
		*/

		$body = 
		"<p>Kami beritahukan bahwa kami telah menerima Booking Anda untuk permohonan sebagai berikut:</p>
		
		<p>
		Nomor Booking: ". $booking->booking_no ."<br/>";

		if ($uttpOwner != null) {
			$body .= "Nama Perusahaan: ". $uttpOwner->nama ."<br/>
			Alamat Perusahaan: ". $uttpOwner->alamat ."<br/>
			NIB Perusahaan: ". $uttpOwner->nib ."<br/>
			NPWP Perusahaan: ". $uttpOwner->npwp ."<br/>
			Telepon: ". $uttpOwner->telepon ."<br/>
			Penanggung Jawab: ". $uttpOwner->penanggung_jawab ."<br/>
			E-mail: ". $uttpOwner->email ."<br/>";
		}
		
		$uttps = $db->query('select u.*,
			mut.uttp_type 
			from service_booking_items sbi 
			inner join uttps u on sbi.uttp_id = u.id 
			inner join master_uttp_types mut on mut.id = u.type_id 
			where booking_id = ' . $id)->getResult();

		foreach ($uttps as $uttp) {
			$body .= "Nama Alat: ". $uttp->uttp_type ."<br/>
				Merk Alat: ". $uttp->tool_brand ."<br/>
				Model/Type Alat: ". $uttp->tool_model ."<br/>
				No Seri Alat: ". $uttp->serial_no ."<br/>";
		}
		
		$body .= "Nama Pemilik Alat pada Sertifikat: ". $booking->label_sertifikat ."<br/>
		Alamat Pemilik Alat pada Sertifikat: ". $booking->addr_sertifikat ."<br/>
		</p>";

		if ($booking->lokasi_pengujian == 'dalam') {
			$body .=
			"<p>UTTP yang akan diuji/diperiksa silakan dibawa pada tanggal ". date("d M Y", strtotime($booking->est_arrival_date));	
		}

		$success = $this->sendEmail(
			'Booking Pengujian/Pemeriksaan dengan Nomor : '. $booking->booking_no .' Telah Diterima',
			$body
		);

        return redirect()->to(base_url('booking'))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function resubmit($id) {
		if (session()->get('logged_in') && session()->get('user_role') > -1) {
			return redirect()->to('auth/logout');
		}

		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		if ($this->request->getPost('est_arrival_date') == null && $booking->lokasi_pengujian == 'dalam') {
			return redirect()->back()->withInput()->with('errors', [
				"est_arrival_date" => 'Rencana pengantaran wajib diisi'
			]);
		}
		if (($this->request->getPost('est_schedule_date_from') == null || $this->request->getPost('est_schedule_date_to') == null)
			&& $booking->lokasi_pengujian == 'luar') {
			return redirect()->back()->withInput()->with('errors', [
				"est_schedule_date_from" => 'Usulan jadwal wajib diisi'
			]);
		}

		$db = \Config\Database::connect();
		$bookingItems = $db->query('select sbi.*, s.tool_code ' .
			'from service_booking_items sbi ' .
			'inner join standards s on sbi.uut_id = s.id ' .
			'where sbi.booking_id = ' . $id)->getResult();

		if (($booking->service_type_id == 1 || $booking->service_type_id == 2)) {
	
			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date,
				ml.id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join standards s on sbi.uut_id = s.id 
				inner join master_laboratory ml on s.standard_measurement_type_id = ml.id
				where sb.booking_no is not null 
				group by sb.est_arrival_date, ml.id, ml.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."'  
				and count(sbii.id) >= ml.quota_online")->getResult();
			$isOverQuota = count($bookingSameDay) > 0;

		} else {
			$quotas = $db->query('select sb.est_arrival_date as quota_date, 
				mi.id as instalasi_id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join uttps u on sbi.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_instalasi mi on mut.instalasi_id = mi.id 
				where sb.booking_no is not null
				group by sb.est_arrival_date, mi.id, mi.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbi.id) >= mi.quota_online')->getResult();

			$bookingSameDay = $db->query("select sb.est_arrival_date as quota_date, 
				mi.id as instalasi_id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join uttps u on sbi.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_instalasi mi on mut.instalasi_id = mi.id 
				where sb.booking_no is not null
				group by sb.est_arrival_date, mi.id, mi.quota_online
				having sb.est_arrival_date = '". date("Y-m-d", strtotime($this->request->getPost('est_arrival_date'))) ."' 
				and count(sbi.id) >= mi.quota_online")->getResult();

			
			$isOverQuota = count($bookingSameDay) > 0;
		}
	
		if ($isOverQuota) {
			return redirect()->back()->withInput()->with('errors', [
				'quota' => 'Jumlah kuota yang terpakai sudah terpenuhi']);
		}

		if ($this->request->getPost('est_arrival_date') != null) {
			$booking->est_arrival_date = date("Y-m-d", strtotime($this->request->getPost('est_arrival_date')));
		}
		if ($this->request->getPost('est_schedule_date_from') != null) {
			$booking->est_schedule_date_from = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_from')));
			$booking->est_schedule_date_to = date("Y-m-d", strtotime($this->request->getPost('est_schedule_date_to')));
		}
		$booking->updated_by = session('user_id');

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }

		$userModel = new User();
		$user = $userModel->find(session('user_id'));

		if($booking->uttp_owner_id !=null){
			$uttpOwner = new UttpOwnerModel();
			$uttpOwner = $uttpOwner->find($booking->uttp_owner_id);
		}else{
			$uttpOwner = new UutOwnerModel();
			$uttpOwner = $uttpOwner->find($booking->uut_owner_id);
		}

		$body = 
		"<p>Kami beritahukan bahwa kami telah menerima perubahan rencana pengantaran alat Booking Anda untuk permohonan sebagai berikut:</p>
		
		<p>
		Nomor Booking: ". $booking->booking_no ."<br/>";

		if ($uttpOwner != null) {
			$body .= "Nama Perusahaan: ". $uttpOwner->nama ."<br/>
			Alamat Perusahaan: ". $uttpOwner->alamat ."<br/>
			NIB Perusahaan: ". $uttpOwner->nib ."<br/>
			NPWP Perusahaan: ". $uttpOwner->npwp ."<br/>
			Telepon: ". $uttpOwner->telepon ."<br/>
			Penanggung Jawab: ". $uttpOwner->penanggung_jawab ."<br/>
			E-mail: ". $uttpOwner->email ."<br/>";
		}
		
		$uttps = $db->query('select u.*,
			mut.uttp_type 
			from service_booking_items sbi 
			inner join uttps u on sbi.uttp_id = u.id 
			inner join master_uttp_types mut on mut.id = u.type_id 
			where booking_id = ' . $id)->getResult();

		foreach ($uttps as $uttp) {
			$body .= "Nama Alat: ". $uttp->uttp_type ."<br/>
				Merk Alat: ". $uttp->tool_brand ."<br/>
				Model/Type Alat: ". $uttp->tool_model ."<br/>
				No Seri Alat: ". $uttp->serial_no ."<br/>";
		}
		
		$body .= "Nama Pemilik Alat pada Sertifikat: ". $booking->label_sertifikat ."<br/>
		Alamat Pemilik Alat pada Sertifikat: ". $booking->addr_sertifikat ."<br/>
		</p>";

		if ($booking->lokasi_pengujian == 'dalam') {
			$body .=
			"<p>UTTP yang akan diuji/diperiksa silakan dibawa pada tanggal ". date("d M Y", strtotime($booking->est_arrival_date));	
		}

		$success = $this->sendEmail(
			'Booking Pengujian/Pemeriksaan dengan Nomor : '. $booking->booking_no .' Telah Diterima',
			$body
		);

        return redirect()->to(base_url('booking'))->with('success', 'Berhasil menyimpan pendaftaran');
	}

	public function read($id) {
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('auth/logout');
		}

		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$holidayModel = new HolidayModel();
		$holidays = $holidayModel->where('holiday_date >= current_date')
			->findAll();
		$db = \Config\Database::connect();
		$events = $db->query('select generate_series(start_date, end_date, \'1 day\') closed_date
			from closed_events ce')->getResult();

		$db = \Config\Database::connect();

		$kota = $db->query("select * from master_kabupatenkota where id = " . $booking->kabkota_sertifikat_id)->getRow();
		$provinsi = $db->query("select * from master_provinsi where id = " . $booking->prov_sertifikat_id)->getRow();

		if (!($booking->service_type_id == 1 || $booking->service_type_id == 2)) {

			$bookingItems = $db->query('select sbi.*,  ' . 
				"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
				's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, s.tool_capacity_min, s.tool_capacity_unit, '.
				's.tool_model, s.tool_type , s.tool_made_in, s.tool_media, '.
				'mut.uttp_type as type ' .
				'from service_booking_items sbi ' .
				'inner join uttps s on sbi.uttp_id = s.id ' .
				'inner join master_uttp_types mut on s.type_id = mut.id ' .
				'where sbi.booking_id = ' . $id)->getResult();
				
			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type, sip.unit " .
				"from service_booking_item_inspections sbii " .
				"inner join uttp_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult();

			$quotas = $db->query('select sb.est_arrival_date as quota_date, 
				mi.id as instalasi_id,
				count(sbi.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join uttps u on sbi.uttp_id = u.id 
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_instalasi mi on mut.instalasi_id = mi.id 
				where sb.booking_no is not null
				group by sb.est_arrival_date, mi.id, mi.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbi.id) >= mi.quota_online')->getResult();

			return view ('admin/bookings/read_uttp', [
				'booking' => $booking,
				'items' => $bookingItems,
				'inspections' => $bookingInspections,
				'holidays' => $holidays,
				'events' => $events,
				'quotas' => $quotas,
				'kota' => $kota,
				'provinsi' => $provinsi,
			]);
		} else {
			// $bookingItems = $db->query('select sbi.*, s.tool_code, ' .
			// 	's.jumlah_per_set, s.standard_detail_type_id, 
			// 	smt.standard_type, 
			// 	stt.attribute_name, sdt.standard_detail_type_name, ' .
			// 	's.brand, s.model, s.tipe, s.capacity ' .
			// 	'from service_booking_items sbi ' .
			// 	'inner join standards s on sbi.uut_id = s.id ' .
			// 	// "inner join standard_measurement_types smt on s.standard_measurement_type_id = smt.id " .
			// 	'inner join standard_tool_types stt on s.standard_tool_type_id = stt.id ' .
			// 	'inner join standard_detail_types sdt on s.standard_detail_type_id = sdt.id ' .
			// 	'where sbi.booking_id = ' . $id)->getResult();

			$bookingItems = $db->query(" select sbi.* ,".
					"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code,". 
					"s.jumlah as jumlah_per_set,".
					" s.tool_brand as brand,
					  s.tool_brand AS attribute_name, 
					  s.tool_model as model, 
					  s.tool_type as tipe, 
					  s.tool_capacity as capacity
					  ,s.tool_brand as standard_type, s.tool_brand as standard_detail_type_name  ".
					"from service_booking_items sbi ".
					"inner join standard_uut s on sbi.uut_id = s.id ".
					"where sbi.booking_id =". $id)->getResult();
					/*
			$bookingInspections = $db->query("select sbii.id, sbii.booking_item_id, sbii.quantity, sbii.price, " .
				"sip.inspection_type ".
				// , smu.measurement_unit unit " .
				"from service_booking_item_inspections sbii " .
				"inner join standard_inspection_prices sip on sbii.inspection_price_id = sip.id " .
				// "inner join standard_measurement_units smu on sip.standard_measurement_unit_id = smu.id " .
				"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id " .
				"where sbi.booking_id = " . $id)->getResult(); */
			$bookingInspections = $db->query("
				select * from service_requests sr
				join service_bookings sb on sb.id = sr.booking_id 
				where sb.id = ".$id)->getResult();

			$quotas = $db->query('select sb.est_arrival_date as quota_date,
				ml.id,
				count(sbii.id) cnt 
				from service_bookings sb 
				inner join service_booking_items sbi on sbi.booking_id = sb.id 
				inner join service_booking_item_inspections sbii on sbii.booking_item_id = sbi.id
				inner join standards s on sbi.uut_id = s.id 
				inner join master_laboratory ml on s.standard_measurement_type_id = ml.id
				where sb.booking_no is not null 
				group by sb.est_arrival_date, ml.id, ml.quota_online
				having sb.est_arrival_date >= current_date 
				and count(sbii.id) >= ml.quota_online')->getResult();

			return view ('admin/bookings/read', [
				'booking' => $booking,
				'items' => $bookingItems,
				'inspections' => $bookingInspections,
				'holidays' => $holidays,
				'events' => $events,
				'quotas' => $quotas,
				'kota' => $kota,
				'provinsi' => $provinsi,
			]);
		}
		
	}

	public function delete($id) {
		if (session()->get('logged_in') && session()->get('role') > -1) {
			return redirect()->to('auth/logout');
		}
		
		$db = \Config\Database::connect();

		$db->transStart(); 
		
		$itemBuilder = $db->table('service_booking_items');
		$items = $itemBuilder->where('booking_id', $id)->get()->getResult();
		foreach ($items as $item) {
			$inspectionBuilder = $db->table('service_booking_item_inspections');
			$inspectionBuilder->where('booking_item_id', $item->id)->delete();
		}

		$itemBuilder->where('booking_id', $id)->delete();

		$bookingBuilder = $db->table('service_bookings');
		$bookingBuilder->where('id', $id)->delete();

		$db->transComplete();

		if ($db->transStatus() === FALSE)
		{
			return redirect()->back()->withInput()->with('errors', 'Gagal menghapus data booking');
		}

        return redirect()->to(base_url('booking'))->with('success', 'Berhasil menghapus data booking');
	}

	private function sendEmail($title, $body)
	{
		$client = \Config\Services::curlrequest();
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			$email = $client->request('POST', config('App')->siksURL ."mailer/send", [
				'form_params' => [
					'token' => $token,
					'customer_id' => session('user_id'),
					'type' => 'generic',
					'title' => $title,
					'body' => $body,
				]
			]); 
			return $email;
		}

		return false;
	}

	public function download_permohonan($data, $id){
		$db = \Config\Database::connect();
		$booking = $db->table('service_bookings')->where('id',$id)->get()->getRowArray();
		$path = $data;
		$file = 'file_'.$data;
		$file_target = 'uploads/'.$booking[$file];
		if(file_exists($file_target)){
			return $this->response->download('uploads/'.$booking[$file], null)->setFileName($booking[$file]);	
		}
	}
}

