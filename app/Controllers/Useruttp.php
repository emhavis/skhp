<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\UttpModel;
use App\Models\UserUttpModel;
use App\Models\UttpOwnerModel;
use App\Models\UserUttpOwnerModel;
use App\Models\UttpTypeModel;
use App\Models\UttpUnitModel;

use function PHPUnit\Framework\isEmpty;

class Useruttp extends BaseController
{
	use ResponseTrait;

	public function index() {

		$uttpModel = new UserUttpModel();
		$uttps = $uttpModel
			->select('uttps.*, master_uttp_types.uttp_type, uttp_owners.nama as owner, user_uttps.id map_id')
			->join('uttps', 'user_uttps.uttp_id = uttps.id')
			->join('uttp_owners', 'uttp_owners.id = uttps.owner_id')
			->join('master_uttp_types', 'master_uttp_types.id = uttps.type_id')
			->where('user_uttps.user_id',  session('user_id'))
			->findAll();

		return view ('admin/useruttps/index', [
			'uttps' => $uttps,
		]);
	}

	public function create() {
		$uttpModel = new UttpModel();

		$ownerModel = new UserUttpOwnerModel();
		$owners = $ownerModel
			->select('uttp_owners.*')
			->join('uttp_owners', 'uttp_owners.id = user_uttp_owners.owner_id')
			->where('user_uttp_owners.user_id',  session('user_id'))
			->findAll();

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->where('is_active', true)->findAll();

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/useruttps/create', [
				'owners' => $owners,
				'uttpTypes' => $uttpTypes,
				'negaras' => $negaras,
			]);
		}
		$mapModel = new UserUttpModel();

		$count= $mapModel->where('uttp_id', $this->request->getPost('uttp_id'))
			->countAllResults();
		if ($count > 0) {
			return redirect()->back()->withInput()->with('errors', ['uttp_id' => 'Data alat sudah pernah ditambahkan']);
		}
		
		$map = [
			'user_id'	=> session('user_id'),
			'uttp_id'	=> $this->request->getPost('uttp_id'),
		];

		if (! $mapModel->save($map)) {
			return redirect()->back()->withInput()->with('errors', $mapModel->errors());
		}


        return redirect()->to(base_url('/useruttp'))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function createNew($owner_id, $type_id) {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owner = $ownerModel->find($owner_id);

		$uttpTypeModel = new UttpTypeModel();
		$uttpType = $uttpTypeModel->where('is_active', true)->find($type_id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/useruttps/create_new', [
				'owner' => $owner,
				'uttpType' => $uttpType,
				'negaras' => $negaras,
			]);
		}
		if($this->request->getPost('tool_made_in_id') != null){
			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
			->getRow();
		}else{
			$negara =['id' => '64', 'nama_negara' => 'Indonesia'];
			$negara = (object)$negara;
			// dd($negara->nama_negara);
		}
		
		$uttp = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),

			'location_lat'			=> $this->request->getPost('location_lat') != null ? $this->request->getPost('location_lat') : null,
			'location_long'			=> $this->request->getPost('location_long') != null ? $this->request->getPost('location_long') : null,

			'location'				=> $this->request->getPost('location'),

		];

		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

		$uttpId = $db->insertID();

		$mapModel = new UserUttpModel();
		$map = [
			'user_id'	=> session('user_id'),
			'uttp_id'	=> $uttpId,
		];

		if (! $mapModel->save($map)) {
			return redirect()->back()->withInput()->with('errors', $mapModel->errors());
		}

        return redirect()->to(base_url('/useruttp'))->with('success', 'Berhasil menyimpan alat baru');
	}

	public function edit($id) {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owners = $ownerModel->findAll();

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->where('is_active', true)->findAll();

		$uttp = $uttpModel->find($id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uttps/edit', [
				'uttp' => $uttp,
				'owners' => $owners,
				'uttpTypes' => $uttpTypes,
				'negaras' => $negaras,
			]);
		}
		
		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();

		$uttp->type_id = $this->request->getPost('type_id');
		$uttp->serial_no = $this->request->getPost('serial_no');
		$uttp->tool_brand = $this->request->getPost('tool_brand');
		$uttp->tool_model = $this->request->getPost('tool_model');
		$uttp->tool_type = $this->request->getPost('tool_type');
		$uttp->tool_capacity = $this->request->getPost('tool_capacity');
		$uttp->tool_capacity_unit = $this->request->getPost('tool_capacity_unit');
		$uttp->tool_factory = $this->request->getPost('tool_factory');
		$uttp->tool_factory_address = $this->request->getPost('tool_factory_address');
		//$uttp->tool_made_in = $this->request->getPost('tool_made_in');
		$uttp->tool_made_in = $negara->nama_negara;
		$uttp->tool_made_in_id =  $negara->id;
		$uttp->owner_id = $this->request->getPost('owner_id');
		$uttp->tool_media = $this->request->getPost('tool_media');
		$uttp->tool_name = $this->request->getPost('tool_name');
		$uttp->tool_capacity_min = $this->request->getPost('tool_capacity_min');
		$uttp->location_lat = $this->request->getPost('location_lat') != null ? $this->request->getPost('location_lat') : null;
		$uttp->location_long = $this->request->getPost('location_long') != null ? $this->request->getPost('location_long') : null;
		$uttp->location = $this->request->getPost('location');
		
		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

        return redirect()->to(base_url('/uttp'))->with('success', 'Berhasil menyimpan perubahan data');
	}

	public function copy($id) {
		$uttpModel = new UttpModel();

		$ownerModel = new UttpOwnerModel();
		$owners = $ownerModel->findAll();

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->where('is_active', true)->findAll();

		$uttp = $uttpModel->find($id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uttps/copy', [
				'uttp' => $uttp,
				'owners' => $owners,
				'uttpTypes' => $uttpTypes,
				'negaras' => $negaras,
			]);
		}

		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();
		
		$uttp = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),
		];

		if (! $uttpModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
        }

        return redirect()->to(base_url('/uttp'))->with('success', 'Berhasil menyimpan penambahan data');
	}

	public function delete($id) {
		$uttpModel = new UserUttpModel();
		$uttpModel->delete($id);

        return redirect()->to(base_url('/useruttp'))->with('success', 'Berhasil menghapus data alat');
	}
	
	public function delete_old($id) {
		$db = \Config\Database::connect();

		$db->transStart(); 
		
		$itemBuilder = $db->table('service_booking_items');
		$items = $itemBuilder->where('booking_id', $id)->get()->getResult();
		foreach ($items as $item) {
			$inspectionBuilder = $db->table('service_booking_item_inspections');
			$inspectionBuilder->where('booking_item_id', $item->id)->delete();
		}

		$itemBuilder->where('booking_id', $id)->delete();

		$bookingBuilder = $db->table('service_bookings');
		$bookingBuilder->where('id', $id)->delete();

		$db->transComplete();

		if ($db->transStatus() === FALSE)
		{
			return redirect()->back()->withInput()->with('errors', 'Gagal menghapus data booking');
		}

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menghapus data booking');
	}

	public function find($owner_id) {

		$db = \Config\Database::connect();

		$uttpModel = new UserUttpModel();
		$builder = $uttpModel
			->select('uttps.*, master_uttp_types.uttp_type, uttp_owners.nama as owner, user_uttps.id map_id')
			->join('uttps', 'user_uttps.uttp_id = uttps.id')
			->join('uttp_owners', 'uttp_owners.id = uttps.owner_id')
			->join('master_uttp_types', 'master_uttp_types.id = uttps.type_id')
			->where('user_uttps.user_id',  session('user_id'))
			->where('owner_id', $owner_id);

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->where("serial_no like '%".$db->escapeLikeString($q)."%'");
		}
		$uttps = $builder
			->findAll();

		return $this->respond($uttps, 200);
	}

	public function get($id) {

		$uttpModel = new UttpModel();
		$uttp = $uttpModel->find($id);

		return $this->respond($uttp, 200);
	}

	public function unit($type_id) {

		$db = \Config\Database::connect();

		$unitModel = new UttpUnitModel();
		$builder = $unitModel
			->where('uttp_type_id', $type_id);

		$units = $builder
			->findAll();

		return $this->respond($units, 200);
	}

	public function getType($id) {

		$uttpTypeModel = new UttpTypeModel();
		$uttpType = $uttpTypeModel->find($id);

		return $this->respond($uttpType, 200);
	}

	public function findBySeries() {

		$db = \Config\Database::connect();

		$uttpModel = new UttpModel();
		$builder = $uttpModel
			->where('type_id', $this->request->getPost('type_id'))
			->where('owner_id', $this->request->getPost('owner_id'))
			->where('serial_no', $this->request->getPost('q'));

		$uttp = $builder
			->first();
		
		return $this->respond($uttp, 200);
	}

	public function import() {
		$uttpModel = new UttpModel();

		$ownerModel = new UserUttpOwnerModel();
		$owner = $ownerModel
			->select('uttp_owners.*')
			->join('uttp_owners', 'uttp_owners.id = user_uttp_owners.owner_id')
			->where('user_uttp_owners.user_id',  session('user_id'))
			->first();
		//dd($owner);

		$uttpTypeModel = new UttpTypeModel();
		

		$file_excel = $this->request->getFile('file_import');
		$ext = $file_excel->getClientExtension();
		if($ext == 'xls') {
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		} else {
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		}
		$spreadsheet = $render->load($file_excel);

		$data_sheet = $spreadsheet->getSheetByName('data');
		if (! $data_sheet) {
			return redirect()->back()->withInput()->with('errors', 'Data harus berada di sheet \'data\'');
		}

		$data = $data_sheet->toArray();


		$db = \Config\Database::connect();

		foreach($data as $idx => $row) {
			if ($idx == 0) {
				continue;
			}

			$uttp_type = $row[0];
			$uttpType = $uttpTypeModel->where('is_active', true)
				->where('uttp_type', $uttp_type)
				->get()
				->getRow();

			$nama_negara = $row[10];
			$negara = $db->table('master_negara')
			->where('lower(nama_negara)', strtolower($nama_negara))
			->get()
			->getRow();

			$uttp = [
				'owner_id' => $owner->id,
				'type_id' => $uttpType != null ? $uttpType->id : null,
				'tool_brand' => $row[1],
				'tool_model' => $row[2],
				'serial_no' => $row[3],
				'tool_media' => $row[4],
				'tool_capacity' => $row[5],
				'tool_capacity_min' => $row[6],
				'tool_capacity_unit' => $row[7],
				'tool_factory' => $row[8],
				'tool_factory_address' => $row[9],
				'tool_made_in' => $row[10],
				'tool_made_in_id' => $negara != null ? $negara->id : null,
				'location_lat' => $row[11],
				'location_long' => $row[12],
			];

			if (! $uttpModel->save($uttp)) {
				dd($uttp);
				//return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
			}
			
			$uttpId = $db->insertID();

			$mapModel = new UserUttpModel();
			$map = [
				'user_id'	=> session('user_id'),
				'uttp_id'	=> $uttpId,
			];

			if (! $mapModel->save($map)) {
				//return redirect()->back()->withInput()->with('errors', $mapModel->errors());
			}
		}

		return redirect()->to(base_url('/useruttp'))->with('success', 'Berhasil meng-impor pendaftaran baru');
	}

	public function downloadtemplate() {
		return $this->response->download('uttp_template.xlsx', null);
	}
}
