<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\BookingItemInspectionModel;
use App\Models\UttpModel;
use App\Models\UttpTypeModel;

class Verify extends BaseController
{
	public function create($id) {
		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->findAll();

		if ($this->request->getMethod() === 'get'){
			return view('admin/bookingitems/create',[
				'booking' => $booking,
				'uttpTypes' => $uttpTypes,
			]);
		}

		/*
		$uttpId = null;
		if (!($booking->service_type_id == 1 || $booking->service_type_id == 2)) {
			$uttpModel = new UttpModel();
			$uttp = [
				'type_id'				=> $this->request->getPost('type_id'),
				'serial_no'				=> $this->request->getPost('serial_no'),
				'tool_brand'			=> $this->request->getPost('tool_brand'),
				'tool_model'			=> $this->request->getPost('tool_model'),
				'tool_type'				=> $this->request->getPost('tool_type'),
				'tool_capacity'			=> $this->request->getPost('tool_capacity'),
				'tool_factory'			=> $this->request->getPost('tool_factory'),
				'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
				'tool_made_in'			=> $this->request->getPost('tool_made_in'),
				
			];

			if (! $uttpModel->insert($uttp)) {
				return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
			}
			
			$uttpId = $uttpModel->insertID;
		}
		*/

		$uttpId = $this->request->getPost('uttp_id');

		$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
		$file_last_certificate = $this->request->getFile('file_last_certificate');
		$file_application_letter = $this->request->getFile('file_application_letter');
		$file_calibration_manual = $this->request->getFile('file_calibration_manual');
		$file_manual_book = $this->request->getFile('file_manual_book');

		if (isset($file_type_approval_certificate)) { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
		if (isset($file_last_certificate)) { $path_last_certificate = $file_last_certificate->store(); }
		if (isset($file_application_letter)) { $path_application_letter = $file_application_letter->store(); }
		if (isset($file_calibration_manual)) { $path_calibration_manual = $file_calibration_manual->store(); }
		if (isset($file_manual_book)) { $path_manual_book = $file_manual_book->store(); }

		$items = new BookingItemModel();
		$item = [
			'booking_id'        => $booking->id,
			// 'uml_standard_id' 	=> $this->request->getPost('uml_standard_id'),
			'uttp_id'			=> $uttpId,
			'quantity'			=> 0,
			'est_subtotal'		=> 0,

			'location' 			=> $this->request->getPost('location'),
			'reference_no' 		=> $this->request->getPost('reference_no'),
			'reference_date' 	=> date("Y-m-d", strtotime($this->request->getPost('reference_date'))),


			'file_type_approval_certificate' 	=> isset($file_type_approval_certificate) ? $file_type_approval_certificate->getClientName() : null,
			'file_last_certificate' 			=> isset($file_last_certificate) ? $file_last_certificate->getClientName() : null,
			'file_application_letter' 			=> isset($file_application_letter) ? $file_application_letter->getClientName() : null,
			'file_calibration_manual' 			=> isset($file_calibration_manual) ? $file_calibration_manual->getClientName() : null,
			'file_manual_book' 					=> isset($file_manual_book) ? $file_manual_book->getClientName() : null,

			'path_type_approval_certificate' 	=> isset($path_type_approval_certificate) ? $path_type_approval_certificate : null,
			'path_last_certificate' 			=> isset($path_last_certificate) ? $path_last_certificate : null,
			'path_application_letter' 			=> isset($path_application_letter) ? $path_application_letter : null,
			'path_calibration_manual' 			=> isset($path_calibration_manual) ? $path_calibration_manual : null,
			'path_manual_book' 					=> isset($path_manual_book) ? $path_manual_book : null,
		];

		if (! $items->insert($item)) {
			return redirect()->back()->withInput()->with('errors', $items->errors());
        }

        //return redirect()->to(base_url('/bookingiteminspection/index/' . $id . '/' . $items->insertID))->with('success', 'Berhasil menyimpan pendaftaran baru');
		return redirect()->to(base_url('/booking/edit/'.$id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function edit($id, $idItem) {
		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$bookingItems = new BookingItemModel();
		$item = $bookingItems->find($idItem);

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->findAll();

		$uttpModel = new UttpModel();
		$uttp = $uttpModel->find($item->uttp_id);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/bookingitems/edit', [
				'booking' => $booking,
				'item' => $item,
				'uttpTypes' => $uttpTypes,
				'uttp' => $uttp,
			]);
		}
		
		$uttpId = $this->request->getPost('uttp_id');

		$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
		$file_last_certificate = $this->request->getFile('file_last_certificate');
		$file_application_letter = $this->request->getFile('file_application_letter');
		$file_calibration_manual = $this->request->getFile('file_calibration_manual');
		$file_manual_book = $this->request->getFile('file_manual_book');

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() != '') { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
		if (isset($file_last_certificate) && $file_last_certificate->getName() != '') { $path_last_certificate = $file_last_certificate->store(); }
		if (isset($file_application_letter) && $file_application_letter->getName() != '') { $path_application_letter = $file_application_letter->store(); }
		if (isset($file_calibration_manual) && $file_calibration_manual->getName() != '') { $path_calibration_manual = $file_calibration_manual->store(); }
		if (isset($file_manual_book) && $file_manual_book->getName() != '') { $path_manual_book = $file_manual_book->store(); }

		//$item->booking_id = $booking->id,
		// $item->uml_standard_id = $this->request->getPost('uml_standard_id');
		$item->uttp_id = $uttpId;
		$item->quantity = 0;
		$item->est_subtotal = 0;

		$item->location = $this->request->getPost('location');
		$item->reference_no = $this->request->getPost('reference_no');
		$item->reference_date = date("Y-m-d", strtotime($this->request->getPost('reference_date')));

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() ) {
			$item->file_type_approval_certificate = isset($file_type_approval_certificate) ? $file_type_approval_certificate->getClientName() : null;
			$item->path_type_approval_certificate = isset($path_type_approval_certificate) ? $path_type_approval_certificate : null;
		}
		if (isset($file_last_certificate) && $file_last_certificate->getName() ) {
			$item->file_last_certificate = isset($file_last_certificate) ? $file_last_certificate->getClientName() : null;
			$item->path_last_certificate = isset($path_last_certificate) ? $path_last_certificate : null;
		}
		if (isset($file_application_letter) && $file_application_letter->getName() ) {
			$item->file_application_letter = isset($file_application_letter) ? $file_application_letter->getClientName() : null;
			$item->path_application_letter = isset($path_application_letter) ? $path_application_letter : null;
		}
		if (isset($file_calibration_manual) && $file_calibration_manual->getName() ) {
			$item->file_calibration_manual = isset($file_calibration_manual) ? $file_calibration_manual->getClientName() : null;
			$item->path_calibration_manual = isset($path_calibration_manual) ? $path_calibration_manual : null;
		}
		if (isset($file_manual_book) && $file_manual_book->getName() ) {
			$item->file_manual_book = isset($file_manual_book) ? $file_manual_book->getClientName() : null;
			$item->path_manual_book = isset($path_manual_book) ? $path_manual_book : null;
		}

		if (! $bookingItems->save($item)) {
			return redirect()->back()->withInput()->with('errors', $bookingItems->errors());
        }

        //return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
		return redirect()->to(base_url('/booking/edit/'.$id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function copy($id, $idItem) {
		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$bookingItems = new BookingItemModel();
		$item = $bookingItems->find($idItem);

		$uttpTypeModel = new UttpTypeModel();
		$uttpTypes = $uttpTypeModel->findAll();

		$uttpModel = new UttpModel();
		$uttp = $uttpModel->find($item->uttp_id);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/bookingitems/copy', [
				'booking' => $booking,
				'item' => $item,
				'uttpTypes' => $uttpTypes,
				'uttp' => $uttp,
			]);
		}
		
		$uttpId = $this->request->getPost('uttp_id');

		$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
		$file_last_certificate = $this->request->getFile('file_last_certificate');
		$file_application_letter = $this->request->getFile('file_application_letter');
		$file_calibration_manual = $this->request->getFile('file_calibration_manual');
		$file_manual_book = $this->request->getFile('file_manual_book');

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() != '') { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
		if (isset($file_last_certificate) && $file_last_certificate->getName() != '') { $path_last_certificate = $file_last_certificate->store(); }
		if (isset($file_application_letter) && $file_application_letter->getName() != '') { $path_application_letter = $file_application_letter->store(); }
		if (isset($file_calibration_manual) && $file_calibration_manual->getName() != '') { $path_calibration_manual = $file_calibration_manual->store(); }
		if (isset($file_manual_book) && $file_manual_book->getName() != '') { $path_manual_book = $file_manual_book->store(); }

		$newitem = [
			'booking_id'        => $booking->id,
			// 'uml_standard_id' 	=> $this->request->getPost('uml_standard_id'),
			'uttp_id'			=> $uttpId,
			'quantity'			=> 0,
			'est_subtotal'		=> 0,

			'location' 			=> $this->request->getPost('location'),
			'reference_no' 		=> $this->request->getPost('reference_no'),
			'reference_date' 	=> date("Y-m-d", strtotime($this->request->getPost('reference_date'))),


			'file_type_approval_certificate' 	=> isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() != '' ? $file_type_approval_certificate->getClientName() : $item->file_type_approval_certificate,
			'file_last_certificate' 			=> isset($file_last_certificate) && $file_last_certificate->getName() != '' ? $file_last_certificate->getClientName() : $item->file_last_certificate,
			'file_application_letter' 			=> isset($file_application_letter) && $file_application_letter->getName() != '' ? $file_application_letter->getClientName() : $item->file_application_letter,
			'file_calibration_manual' 			=> isset($file_calibration_manual) && $file_calibration_manual->getName() != '' ? $file_calibration_manual->getClientName() : $item->file_calibration_manual,
			'file_manual_book' 					=> isset($file_manual_book) && $file_manual_book->getName() != '' ? $file_manual_book->getClientName() : $item->file_manual_book,

			'path_type_approval_certificate' 	=> isset($path_type_approval_certificate) ? $path_type_approval_certificate : $item->path_type_approval_certificate,
			'path_last_certificate' 			=> isset($path_last_certificate) ? $path_last_certificate : $item->path_last_certificate,
			'path_application_letter' 			=> isset($path_application_letter) ? $path_application_letter : $item->path_application_letter,
			'path_calibration_manual' 			=> isset($path_calibration_manual) ? $path_calibration_manual : $item->path_calibration_manual,
			'path_manual_book' 					=> isset($path_manual_book) ? $path_manual_book : $item->path_manual_book,
		];

		if (! $bookingItems->insert($newitem)) {
			return redirect()->back()->withInput()->with('errors', $items->errors());
        }

        //return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
		return redirect()->to(base_url('/booking/edit/'.$id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function deleteuttp($id) {
		$bookingModel = new BookingModel();;
		$bookingItemModel = new BookingItemModel();

		$db = \Config\Database::connect();

		$inspectionModel = new BookingItemInspectionModel();

		if (! $inspectionModel->where('booking_item_id', $id)->delete()) {
			return redirect()->back()->withInput()->with('errors', $inspectionModel->errors());
        }

		$item = $bookingItemModel->find($id);
		if (! $bookingItemModel->delete($id)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemModel->errors());
        }

		$aggregate = $db->query("select count(sbii.id) quantity, coalesce(sum(sbii.price * sbii.quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id ".
			"where sbi.booking_id = " . $item->booking_id)->getRow();

		$booking = $bookingModel->find($item->booking_id);

		$booking->est_total_price = $aggregate->subtotal;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }


        return redirect()->to(base_url('/booking/edit/' . $booking->id))->with('success', 'Berhasil menyimpan rincian pemeriksaan baru');
	}
}
