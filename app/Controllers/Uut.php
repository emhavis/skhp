<?php

namespace App\Controllers;
use App\Models\MasterStandardType;
use CodeIgniter\API\ResponseTrait;
use App\Models\UutModel;
use App\Models\UutOwnerModel;
use App\Models\UutTypeModel;
use App\Models\UutUnitModel;

use function PHPUnit\Framework\isEmpty;

class Uut extends BaseController
{
	use ResponseTrait;

	public function index() {

		$uutModel = new UutModel();
		$uuts = $uutModel
			->select('standard_uut.*, master_standard.standard_type, uut_owners.nama as owner')
			->join('uut_owners', 'uut_owners.id = standard_uut.owner_id')
			->join('master_standard', 'master_standard.id = standard_uut.type_id')
			->findAll();

		return view ('admin/uut/index', [
			'uuts' => $uuts,
		]);
	}

	public function create() {
		$db = \Config\Database::connect();
		$uutModel = new UutModel();

		$ownerModel = new UutOwnerModel();
		$owners = $ownerModel->findAll();

		$type_ids = $db->table('master_standard_types')
		->join('uut_inspection_price_types','uut_inspection_price_types.uut_type_id = master_standard_types.id')
		->select('master_standard_types.id')->get()->getResult();

		$uutpTypesModel = new MasterStandardType();
		$uutTypes = $uutpTypesModel->orderBy('uut_type','ASC')->get()->getResult();

		
		$negaras = $db->table('master_negara')
			->get()
			->getResult();


		if ($this->request->getMethod() === 'get'){
			return view('admin/uut/create', [
				'owners' => $owners,
				'uutTypes' => $uutTypes,
				'negaras' => $negaras,
			]);
		}

		if(!empty($this->request->getPost('tool_made_in_id'))){
			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
			->getRow();
		}else{
			$negara =['id' => '1', 'nama_negara' => ''];
			$negara = (object)$negara;
			// dd($negara->nama_negara);
		}
		$tool_capacity ='';
		$tool_capacity_unit ='';
		$tool_dayabaca_unit='';
		$tool_dayabaca ='';
		if(!empty($this->request->getPost('tool_capacity'))){
			$tool_capacity = $this->request->getPost('tool_capacity');
		}else{
			$tool_capacity = $this->request->getPost('tool_capacity_1');
		}
		if(!empty($this->request->getPost('tool_capacity_unit'))){
			$tool_capacity_unit = $this->request->getPost('tool_capacity_unit');
		}else{
				$tool_capacity_unit = $this->request->getPost('tool_capacity_unit_1');
		}
		
		if(!empty($this->request->getPost('tool_dayabaca'))){
			$tool_dayabaca = $this->request->getPost('tool_dayabaca');
		}else{//(!empty($this->request->getPost('tool_dayabaca_1'))){
			$tool_dayabaca = $this->request->getPost('tool_dayabaca_1');
		}

		if(!empty($this->request->getPost('tool_dayabaca_unit'))){
			$tool_dayabaca_unit = $this->request->getPost('tool_dayabaca_unit');
		}else{//(!empty($this->request->getPost('tool_dayabaca_unit_1'))){
			$tool_dayabaca_unit = $this->request->getPost('tool_dayabaca_unit_1');
		}

		$uut = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			// 'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $tool_capacity,//$this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $tool_capacity_unit,//$this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'class'					=> $this->request->getPost('class'),
			'jumlah'				=> $this->request->getPost('jumlah'),
			'tool_dayabaca'			=> $tool_dayabaca,
			'tool_dayabaca_unit'	=> $tool_dayabaca_unit,
			'tool_media'			=> $this->request->getPost('tool_media'),
		];
		// dd([$this->request->getPostGet('tool_capacity_unit'),$tool_capacity]);
		// $this->load->helper('url', 'form'); 
		// $this->load->library('form_validation');

		if (! $uutModel->save($uut)) {
			return redirect()->back()->withInput()->with('errors', $uutModel->errors());
        }

        return redirect()->to(base_url('/uut'))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function edit($id) {
		$uutModel = new UutModel();

		$ownerModel = new UutOwnerModel();
		$owners = $ownerModel->findAll();

		$uutTypeModel = new UutTypeModel();
		$uutTypes = $uutTypeModel->findAll();

		$uut = $uutModel->find($id);
		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uut/edit', [
				'uut' => $uut,
				'owners' => $owners,
				'uutTypes' => $uutTypes,
				'negaras' => $negaras,
			]);
		}
		
		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();
		$unit = $db->table('master_standard_units')->where('id',$this->request->getPost('tool_capacity_unit'))
		->get()
		->getResult();
		// dd();
		$uut->type_id = $this->request->getPost('type_id');
		$uut->serial_no = $this->request->getPost('serial_no');//
		$uut->tool_brand = $this->request->getPost('tool_brand');//
		$uut->tool_model = $this->request->getPost('tool_model');//
		$uut->tool_type = $this->request->getPost('tool_type');
		$uut->tool_capacity = $this->request->getPost('tool_capacity');//
		$uut->tool_capacity_unit = $unit[0]->unit;//
		$uut->tool_factory = $this->request->getPost('tool_factory');//
		$uut->tool_factory_address = $this->request->getPost('tool_factory_address');
		// $uut->tool_made_in = $this->request->getPost('tool_made_in');
		$uut->tool_made_in = $negara->nama_negara;//
		$uut->tool_made_in_id =  $negara->id;//
		$uut->tool_dayabaca = $this->request->getPost('tool_dayabaca');//
		$uut->tool_dayabaca_unit = $this->request->getPost('tool_dayabaca_unit');//
		$uut->tool_media = $this->request->getPost('tool_media');//
		$uut->class =   $this->request->getPost('class');//
		$uut->jumlah =   $this->request->getPost('jumlah');//
		// dd($uut);
		if (! $uutModel->save($uut)) {
			// dd($uutModel->errors());
			return redirect()->back()->withInput()->with('errors', $uutModel->errors());
        }

        return redirect()->to(base_url('/uut'))->with('success', 'Berhasil menyimpan perubahan data');
	}

	public function copy($id) {
		$uutModel = new UutModel();

		$ownerModel = new UutOwnerModel();
		$owners = $ownerModel->findAll();

		$uutTypeModel = new UutTypeModel();
		$uutTypes = $uutTypeModel->findAll();

		$uut = $uutModel->find($id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uut/copy', [
				'uut' => $uut,
				'owners' => $owners,
				'uutTypes' => $uutTypes,
				'negaras' => $negaras,
			]);
		}

		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();
		
			$tool_capacity ='';
			$tool_capacity_unit ='';
			$tool_dayabaca_unit='';
			$tool_dayabaca ='';
			if(!empty($this->request->getPost('tool_capacity'))){
				$tool_capacity = $this->request->getPost('tool_capacity');
			}else{
				$tool_capacity = $this->request->getPost('tool_capacity_1');
			}
			if(!empty($this->request->getPost('tool_capacity_unit'))){
				$tool_capacity_unit = $this->request->getPost('tool_capacity_unit');
			}else{
					$tool_capacity_unit = $this->request->getPost('tool_capacity_unit_1');
			}
			
			if(!empty($this->request->getPost('tool_dayabaca'))){
				$tool_dayabaca = $this->request->getPost('tool_dayabaca');
			}else{//(!empty($this->request->getPost('tool_dayabaca_1'))){
				$tool_dayabaca = $this->request->getPost('tool_dayabaca_1');
			}
	
			if(!empty($this->request->getPost('tool_dayabaca_unit'))){
				$tool_dayabaca_unit = $this->request->getPost('tool_dayabaca_unit');
			}else{//(!empty($this->request->getPost('tool_dayabaca_unit_1'))){
				$tool_dayabaca_unit = $this->request->getPost('tool_dayabaca_unit_1');
			}	
		
		$uttp = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			// 'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $tool_capacity,//$this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $tool_capacity_unit,//$this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'class'					=> $this->request->getPost('class'),
			'jumlah'				=> $this->request->getPost('jumlah'),
			'tool_dayabaca'			=> $tool_dayabaca,
			'tool_dayabaca_unit'	=> $tool_dayabaca_unit,
			'tool_media'			=> $this->request->getPost('tool_media'),
		];

		if (! $uutModel->save($uttp)) {
			return redirect()->back()->withInput()->with('errors', $uutModel->errors());
        }

        return redirect()->to(base_url('/uut'))->with('success', 'Berhasil menyimpan penambahan data');
	}
	
	public function delete($id) {
		$db = \Config\Database::connect();

		$db->transStart(); 
		
		$itemBuilder = $db->table('service_booking_items');
		$items = $itemBuilder->where('booking_id', $id)->get()->getResult();
		foreach ($items as $item) {
			$inspectionBuilder = $db->table('service_booking_item_inspections');
			$inspectionBuilder->where('booking_item_id', $item->id)->delete();
		}

		$itemBuilder->where('booking_id', $id)->delete();

		$bookingBuilder = $db->table('service_bookings');
		$bookingBuilder->where('id', $id)->delete();

		$db->transComplete();

		if ($db->transStatus() === FALSE)
		{
			return redirect()->back()->withInput()->with('errors', 'Gagal menghapus data booking');
		}

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menghapus data booking');
	}

	public function find($type_id, $owner_id) {

		$db = \Config\Database::connect();

		$uutModel = new UutModel();
		$builder = $uutModel
			->where('type_id', $type_id)
			->where('owner_id', $owner_id);

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->where("serial_no like '%".$db->escapeLikeString($q)."%'");
		}

		$uttps = $builder
			->findAll();
			
		return $this->respond($uttps, 200);
	}

	public function get($id) {

		$uutModel = new UutModel();
		// $uut = $uutModel->find($id);
		$uut = $uutModel
		->select('standard_uut.*, master_standard_types.uut_type, uut_owners.nama as owner')
			->join('uut_owners', 'uut_owners.id = standard_uut.owner_id')
			->join('master_standard_types', 'master_standard_types.id = standard_uut.type_id')
			->where('standard_uut.id', $id)
			->first();	
		return $this->respond($uut, 200);
	}

	public function unit($type_id) {
		$db = \Config\Database::connect();

		$unitModel = new UutUnitModel();
		$standard = new MasterStandardType();
		$standard->where('id',$type_id);
		$uid = $standard->first();
		$uid = $uid->unit_id;
		$builder = $unitModel
			->where('uut_type_id', $uid);
		$units = $builder
			->findAll();
		//dd($builder);
		return $this->respond($units, 200);
	}

	public function findBySeries() {
		// dd($this->request->getPost('type_id'));

		$db = \Config\Database::connect();

		$uutModel = new UutModel();
		$builder = $uutModel
			->where('type_id', $this->request->getPost('type_id'))
			->where('owner_id', $this->request->getPost('owner_id'))
			->where('serial_no', $this->request->getPost('q'));

		$uut = $builder
			->first();
		
		return $this->respond($uut, 200);
	}
}
