<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UttpTypeModel;
use App\Models\RevisionModel;

class Revision extends BaseController
{
	use ResponseTrait;

	public function index()
	{
	
		$db = \Config\Database::connect();

		$basic_query = "select
				cru.*,
				sb.booking_no,
				sr.no_register,
				so.no_sertifikat,
				mts.service_type,
				case when cru.uttp_type_id is not null then 'Jenis Alat: ' || mut.uttp_type || '<br/>' else '' end 
				|| case when cru.tool_brand is not null then 'Merek: ' || cru.tool_brand || '<br/>' else '' end 
				|| case when cru.tool_model is not null then 'Model/Tipe: ' || cru.tool_model || '<br/>' else '' end 
				|| case when cru.tool_serial_no is not null then 'No Seri: ' || cru.tool_serial_no || '<br/>' else '' end
				|| case when cru.tool_media is not null then 'Media: ' || cru.tool_media || '<br/>' else '' end
				|| case when cru.tool_capacity is not null then 'Kapasitas Maks.: ' || cru.tool_capacity || '<br/>' else '' end
				|| case when cru.tool_capacity_min is not null then 'Kapasitas Min.: ' || cru.tool_capacity_min || '<br/>' else '' end
				|| case when cru.tool_factory is not null then 'Pabrikan: ' || cru.tool_factory || '<br/>' else '' end
				|| case when cru.tool_factory_address is not null then 'Alamat Pabrikan: ' || cru.tool_factory_address || '<br/>' else '' end
				|| case when cru.tool_made_in is not null then 'Buatan: ' || cru.tool_made_in || '<br/>' else '' end
				|| case when cru.label_sertifikat is not null then 'Pemilik: ' || cru.label_sertifikat || '<br/>' else '' end
				|| case when cru.addr_sertifikat is not null then 'Alamat Pemilik: ' || cru.addr_sertifikat || '<br/>' else '' end
				|| coalesce(cru.others, '')
				perbaikan,
				mrs.status status_text
			from
				certificate_revision_uttps cru
			inner join service_bookings sb on
				cru.booking_id = sb.id
			inner join service_request_uttps sr on
				cru.request_id = sr.id
			inner join service_order_uttps so on
				cru.order_id = so.id
			inner join master_service_types mts on
				cru.service_type_id = mts.id
			left join master_revision_status mrs on
				cru.status = mrs.id
			left join master_uttp_types mut on 
				cru.uttp_type_id = mut.id ";

		$revisions_pengaduan = $db->query(
			$basic_query .
			'where ' . 
			'cru.status is null and ' .
			'cru.requestor_id = ' . session('user_id')
			)->getResult();

		$revisions_proses = $db->query(
			$basic_query .
			'where ' . 
			'cru.status in (1,2,3) and ' .
			'cru.requestor_id = ' . session('user_id')
			)->getResult();

		$revisions_riwayat = $db->query(
			$basic_query .
			'where ' . 
			'cru.status = 4 and ' .
			'cru.requestor_id = ' . session('user_id')
			)->getResult();

		return view ('admin/revisions/index', [
			'revisions_pengaduan' => $revisions_pengaduan,
			'revisions_proses' => $revisions_proses,
			'revisions_riwayat' => $revisions_riwayat,
		]);
	}

	public function create() {

		$db = \Config\Database::connect();
		$orders = $db->query(
			'select so.*, sr.no_register, sb.booking_no ' . 
			'from service_order_uttps so ' . 
			'inner join service_request_uttps sr on so.service_request_id = sr.id ' . 
			'inner join service_bookings sb on sr.booking_id = sb.id ' . 
			'where ' .
			'sr.status_id = 14 ' .
 			'and sb.pic_id = ' . session('user_id')
		)->getResult();

		$uttpTypeModel = new UttpTypeModel();
		$types = $uttpTypeModel->findAll();

		if ($this->request->getMethod() === 'get'){
			return view('admin/revisions/create_uttp', [
				'orders' => $orders,
				'types' => $types,
			]);
		}

		$order = $db->query(
			'select so.id as order_id, sr.id as request_id, sb.id as booking_id, ' .
			'sr.service_type_id ' .
			'from service_order_uttps so ' . 
			'inner join service_request_uttps sr on so.service_request_id = sr.id ' . 
			'inner join service_bookings sb on sr.booking_id = sb.id ' . 
			'where ' .
			'so.id = ' . $this->request->getPost('order_id'))->getRow();

		$revisionModel = new RevisionModel();
		$revision['order_id'] = $this->request->getPost('order_id');
		$revision['booking_id'] = $order->booking_id;
		$revision['request_id'] = $order->request_id;
		$revision['requestor_id'] = session('user_id');
		$revision['created_by'] = session('user_id');
		$revision['updated_by'] = session('user_id');
		$revision['status'] = null;
		$revision['service_type_id'] = $order->service_type_id;

		$revision['jenis_dokumen'] = $this->request->getPost('jenis_dokumen');
		 
		$revision['uttp_type_id'] = 
			$this->request->getPost('is_type_id') == 'on' 
				&& $this->request->getPost('type_id_new') != '' ?
					$this->request->getPost('type_id_new') : null;
		$revision['tool_serial_no'] = 
			$this->request->getPost('is_tool_serial_no') == 'on' 
				&& $this->request->getPost('tool_serial_no_new') != '' ?
					$this->request->getPost('tool_serial_no_new') : null;
		$revision['tool_brand'] = 
			$this->request->getPost('is_tool_brand') == 'on' 
				&& $this->request->getPost('tool_brand_new') != '' ?
					$this->request->getPost('tool_brand_new') : null;
		$revision['tool_model'] = 
			$this->request->getPost('is_tool_model') == 'on' 
				&& $this->request->getPost('tool_model_new') != '' ?
					$this->request->getPost('tool_model_new') : null;
		$revision['tool_media'] = 
			$this->request->getPost('is_tool_media') == 'on' 
				&& $this->request->getPost('tool_media_new') != '' ?
					$this->request->getPost('tool_media_new') : null;
		$revision['tool_type'] = 
			$this->request->getPost('is_tool_type') == 'on' 
				&& $this->request->getPost('tool_type_new') != '' ?
					$this->request->getPost('tool_type_new') : null;
		$revision['tool_capacity'] = 
			$this->request->getPost('is_tool_capacity') == 'on' 
				&& $this->request->getPost('tool_capacity_new') != '' ?
					$this->request->getPost('tool_capacity_new') : null;
		$revision['tool_capacity_min'] = 
			$this->request->getPost('is_tool_capacity_min') == 'on' 
				&& $this->request->getPost('tool_capacity_min_new') != '' ?
					$this->request->getPost('tool_capacity_min_new') : null;
		$revision['tool_factory'] = 
			$this->request->getPost('is_tool_factory') == 'on' 
				&& $this->request->getPost('tool_factory_new') != '' ?
					$this->request->getPost('tool_factory_new') : null;
		$revision['tool_factory_address'] = 
			$this->request->getPost('is_tool_factory_address') == 'on' 
				&& $this->request->getPost('tool_factory_address_new') != '' ?
					$this->request->getPost('tool_factory_address_new') : null;
		$revision['tool_made_in'] = 
			$this->request->getPost('is_tool_made_in') == 'on' 
				&& $this->request->getPost('tool_made_in_new') != '' ?
					$this->request->getPost('tool_made_in_new') : null;
		$revision['label_sertifikat'] = 
			$this->request->getPost('is_label_sertifikat') == 'on' 
				&& $this->request->getPost('label_sertifikat_new') != '' ?
					$this->request->getPost('label_sertifikat_new') : null;
		$revision['addr_sertifikat'] = 
			$this->request->getPost('is_addr_sertifikat') == 'on' 
				&& $this->request->getPost('addr_sertifikat_new') != '' ?
					$this->request->getPost('addr_sertifikat_new') : null;

		$revision['others'] = $this->request->getPost('others');

		if (! $revisionModel->save($revision)) {
			return redirect()->back()->withInput()->with('errors', $revisionModel->errors());
		}

		return redirect()->to(base_url('/revision'))->with('success', 'Berhasil menyimpan pengaduan baru');
			
	}

	public function edit($id) {

		$db = \Config\Database::connect();
		$revision = $db->query(
			"select
				cru.*,
				sb.booking_no,
				sr.no_register,
				so.no_sertifikat,
				mts.service_type,
				mut.uttp_type,
				sr.jenis_layanan jenis_layanan_o,
				sr.label_sertifikat label_sertifikat_o,
				sr.addr_sertifikat addr_sertifikat_o,
				so.tool_serial_no tool_serial_no_o,
				so.tool_brand tool_brand_o,
				so.tool_capacity tool_capacity_o,
				so.tool_capacity_min tool_capacity_min_o,
				so.tool_factory tool_factory_o,
				so.tool_factory_address tool_factory_address_o,
				so.tool_made_in tool_made_in_o,
				so.tool_model tool_model_o,
				so.tool_type tool_type_o,
				so.tool_media tool_media_o,
				mut_o.id uttp_type_id_o,
				mut_o.uttp_type uttp_type_o
			from
				certificate_revision_uttps cru
			inner join service_bookings sb on
				cru.booking_id = sb.id
			inner join service_request_uttps sr on
				cru.request_id = sr.id
			inner join service_order_uttps so on
				cru.order_id = so.id
			inner join master_service_types mts on
				cru.service_type_id = mts.id
			inner join service_request_uttp_items sri on
				so.service_request_item_id = sri.id
			inner join uttps u on
				sri.uttp_id = u.id
			inner join master_uttp_types mut_o on
				u.type_id = mut_o.id
			left join master_uttp_types mut on 
				cru.uttp_type_id = mut.id " .
			'where ' . 
			'cru.id = ' . $id
			)->getRow();


		$uttpTypeModel = new UttpTypeModel();
		$types = $uttpTypeModel->findAll();

		if ($this->request->getMethod() === 'get'){
			return view('admin/revisions/edit_uttp', [
				'revision' => $revision,
				'types' => $types,
			]);
		}

		$revisionModel = new RevisionModel();
		$revision = $revisionModel->find($id);
		
		$revision->uttp_type_id = 
			$this->request->getPost('is_type_id') == 'on' 
				&& $this->request->getPost('type_id_new') != '' ?
					$this->request->getPost('type_id_new') : null;
		$revision->tool_brand = 
			$this->request->getPost('is_tool_brand') == 'on' 
				&& $this->request->getPost('tool_brand_new') != '' ?
					$this->request->getPost('tool_brand_new') : null;
		$revision->tool_model = 
			$this->request->getPost('is_tool_model') == 'on' 
				&& $this->request->getPost('tool_model_new') != '' ?
					$this->request->getPost('tool_model_new') : null;
		$revision->tool_type = 
			$this->request->getPost('is_tool_type') == 'on' 
				&& $this->request->getPost('tool_type_new') != '' ?
					$this->request->getPost('tool_type_new') : null;
		$revision->tool_serial_no = 
			$this->request->getPost('is_tool_serial_no') == 'on' 
				&& $this->request->getPost('tool_serial_no_new') != '' ?
					$this->request->getPost('tool_serial_no_new') : null;
		$revision->tool_media = 
			$this->request->getPost('is_tool_media') == 'on' 
				&& $this->request->getPost('tool_media_new') != '' ?
					$this->request->getPost('tool_media_new') : null;
		$revision->tool_capacity = 
			$this->request->getPost('is_tool_capacity') == 'on' 
				&& $this->request->getPost('tool_capacity_new') != '' ?
					$this->request->getPost('tool_capacity_new') : null;
		$revision->tool_capacity_min = 
			$this->request->getPost('is_tool_capacity_min') == 'on' 
				&& $this->request->getPost('tool_capacity_min_new') != '' ?
					$this->request->getPost('tool_capacity_min_new') : null;
		$revision->tool_factory = 
			$this->request->getPost('is_tool_factory') == 'on' 
				&& $this->request->getPost('tool_factory_new') != '' ?
					$this->request->getPost('tool_factory_new') : null;
		$revision->tool_factory_address = 
			$this->request->getPost('is_tool_factory_address') == 'on' 
				&& $this->request->getPost('tool_factory_address_new') != '' ?
					$this->request->getPost('tool_factory_address_new') : null;
		$revision->tool_made_in = 
			$this->request->getPost('is_tool_made_in') == 'on' 
				&& $this->request->getPost('tool_made_in_new') != '' ?
					$this->request->getPost('tool_made_in_new') : null;
		$revision->label_sertifikat = 
			$this->request->getPost('is_label_sertifikat') == 'on' 
				&& $this->request->getPost('label_sertifikat_new') != '' ?
					$this->request->getPost('label_sertifikat_new') : null;
		$revision->addr_sertifikat = 
			$this->request->getPost('is_addr_sertifikat') == 'on' 
				&& $this->request->getPost('addr_sertifikat_new') != '' ?
					$this->request->getPost('addr_sertifikat_new') : null;
				
		
		if (! $revisionModel->save($revision)) {
			return redirect()->back()->withInput()->with('errors', $revisionModel->errors());
		}

		return redirect()->to(base_url('/revision'))->with('success', 'Berhasil menyimpan perubahan pengaduan');

	}

	public function editsubmit($id) {

		$db = \Config\Database::connect();
		
		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('/revision/edit/'.$id));
		}

		$revisionModel = new RevisionModel();
		$revision = $revisionModel->find($id);
		
		$revision->uttp_type_id = 
			$this->request->getPost('is_type_id') == 'on' 
				&& $this->request->getPost('type_id_new') != '' ?
					$this->request->getPost('type_id_new') : null;
		$revision->tool_brand = 
			$this->request->getPost('is_tool_brand') == 'on' 
				&& $this->request->getPost('tool_brand_new') != '' ?
					$this->request->getPost('tool_brand_new') : null;
		$revision->tool_model = 
			$this->request->getPost('is_tool_model') == 'on' 
				&& $this->request->getPost('tool_model_new') != '' ?
					$this->request->getPost('tool_model_new') : null;
		$revision->tool_type = 
			$this->request->getPost('is_tool_type') == 'on' 
				&& $this->request->getPost('tool_type_new') != '' ?
					$this->request->getPost('tool_type_new') : null;
		$revision->tool_capacity = 
			$this->request->getPost('is_tool_capacity') == 'on' 
				&& $this->request->getPost('tool_capacity_new') != '' ?
					$this->request->getPost('tool_capacity_new') : null;
		$revision->tool_capacity_min = 
			$this->request->getPost('is_tool_capacity_min') == 'on' 
				&& $this->request->getPost('tool_capacity_min_new') != '' ?
					$this->request->getPost('tool_capacity_min_new') : null;
		$revision->tool_factory = 
			$this->request->getPost('is_tool_factory') == 'on' 
				&& $this->request->getPost('tool_factory_new') != '' ?
					$this->request->getPost('tool_factory_new') : null;
		$revision->tool_factory_address = 
			$this->request->getPost('is_tool_factory_address') == 'on' 
				&& $this->request->getPost('tool_factory_address_new') != '' ?
					$this->request->getPost('tool_factory_address_new') : null;
		$revision->tool_made_in = 
			$this->request->getPost('is_tool_made_in') == 'on' 
				&& $this->request->getPost('tool_made_in_new') != '' ?
					$this->request->getPost('tool_made_in_new') : null;
		$revision->label_sertifikat = 
			$this->request->getPost('is_label_sertifikat') == 'on' 
				&& $this->request->getPost('label_sertifikat_new') != '' ?
					$this->request->getPost('label_sertifikat_new') : null;
		$revision->addr_sertifikat = 
			$this->request->getPost('is_addr_sertifikat') == 'on' 
				&& $this->request->getPost('addr_sertifikat_new') != '' ?
					$this->request->getPost('addr_sertifikat_new') : null;
		$revision->status = 1;
		
		if (! $revisionModel->save($revision)) {
			return redirect()->back()->withInput()->with('errors', $revisionModel->errors());
		}

		return redirect()->to(base_url('/revision'))->with('success', 'Berhasil menyimpan perubahan pengaduan');

	}

	public function delete($id) {

		$revisionModel = new RevisionModel();
		$revision = $revisionModel->find($id);
		
		if ($revision == null) {
			return redirect()->back()->withInput()->with('errors', 'Data tidak ditemukan');
		}

		$revisionModel->delete($id);

		return redirect()->to(base_url('/revision'))->with('success', 'Berhasil menghapus perubahan pengaduan');

	}

	public function order($id) {
		$db = \Config\Database::connect();

		$query = $db->query(
			'select
				so.id,
				sr.jenis_layanan,
				sr.label_sertifikat,
				sr.addr_sertifikat,
				so.tool_serial_no,
				so.tool_brand,
				so.tool_capacity,
				so.tool_capacity_min,
				so.tool_factory,
				so.tool_factory_address,
				so.tool_made_in,
				so.tool_model,
				so.tool_type,
				so.tool_media,
				mut.uttp_type
			from
				service_order_uttps so
			inner join service_request_uttp_items sri on
				so.service_request_item_id = sri.id
			inner join service_request_uttps sr on
				sri.request_id = sr.id
			inner join uttps u on
				sri.uttp_id = u.id
			inner join master_uttp_types mut on
				u.type_id = mut.id
			where
				so.id = ' . $db->escape($id) );
		$order = $query->getRow();
		$results['order'] = $order;

		return $this->respond($results, 200);
	}

	public function monitoring()
	{
		$db = \Config\Database::connect();

		$basic_query = "select
				cru.*,
				sb.booking_no,
				sr.no_register,
				so.no_sertifikat,
				mts.service_type,
				case when cru.uttp_type_id is not null then 'Jenis Alat: ' || mut.uttp_type || '<br/>' else '' end 
				|| case when cru.tool_brand is not null then 'Merek: ' || cru.tool_brand || '<br/>' else '' end 
				|| case when cru.tool_model is not null then 'Model: ' || cru.tool_model || '<br/>' else '' end 
				|| case when cru.tool_type is not null then 'Tipe: ' || cru.tool_type || '<br/>' else '' end
				|| case when cru.tool_capacity is not null then 'Kapasitas Maks.: ' || cru.tool_capacity || '<br/>' else '' end
				|| case when cru.tool_factory is not null then 'Pabrikan: ' || cru.tool_factory || '<br/>' else '' end
				|| case when cru.tool_factory_address is not null then 'Alamat Pabrikan: ' || cru.tool_factory_address || '<br/>' else '' end
				|| case when cru.tool_made_in is not null then 'Buatan: ' || cru.tool_made_in || '<br/>' else '' end
				|| case when cru.label_sertifikat is not null then 'Pemilik: ' || cru.label_sertifikat || '<br/>' else '' end
				|| case when cru.addr_sertifikat is not null then 'Alamat Pemilik: ' || cru.addr_sertifikat || '<br/>' else '' end
				perbaikan
			from
				certificate_revision_uttps cru
			inner join service_bookings sb on
				cru.booking_id = sb.id
			inner join service_request_uttps sr on
				cru.request_id = sr.id
			inner join service_order_uttps so on
				cru.order_id = so.id
			inner join master_service_types mts on
				cru.service_type_id = mts.id
			left join master_uttp_types mut on 
				cru.uttp_type_id = mut.id ";

		$revisions = $db->query(
			$basic_query .
			'where ' . 
			'cru.status is null and ' .
			'cru.requestor_id = ' . session('user_id')
			)->getResult();

		return view ('admin/revisions/index', [
			'revisions' => $revisions,
		]);
	}

	public function sertifikat($type) {
		$db = \Config\Database::connect();

		$keyword = $this->request->getGet('q');

		$table = 'service_order_uttps';
		$field = 'so.no_sertifikat';

		$userId = session('user_id');

		switch ($type) {
			case 'skhpt':
				$field = 'so.no_sertifikat_tipe';
				break;
			case 'set':
				$field = 'so.no_surat_tipe';
				break;

			default:
				break;
		}

		$queryStr = 'select
					so.id, ' . 
					$field . ' as no_sertifikat ' . 
				'from ' . $table . ' so
				inner join service_request_uttps sr on so.service_request_id = sr.id 
				inner join service_bookings sb on sr.booking_id = sb.id 
				where stat_sertifikat = 3 
				and sb.pic_id = ' . $userId . ' 
				and ' . $field . ' like \'%' .$db->escapeLikeString($keyword). '%\'';
		
		//dd([$keyword, $queryStr]);
		$query = $db->query($queryStr);
		
		$orders = $query->getResult();
		$results['orders'] = $orders;

		return $this->respond($results, 200);
	}
}
