<?php

namespace App\Controllers;

use CodeIgniter\Database\Config;

class Article extends BaseController
{
	public function read($id)
	{
		$db = \Config\Database::connect();
		$article = $db->query("select * from article_posts where id = ?", [$id])->getRow();

		return view('public/article/read', [
			'article' => $article
		]);
	}

}
