<?php

namespace App\Controllers;

use CodeIgniter\Database\Config;

class Dashboard extends BaseController
{
	public function index()
	{
		$Ynow = date("Y");
		$tahun =$Ynow;
		$db = \Config\Database::connect();
		
		$booking_current_count = $db->query("select count(id) from service_bookings sb 
			where requested = false and pic_id = " . session('user_id'))->getRow();
		$booking_all_count = $db->query("select count(id) from service_bookings sb 
			where pic_id = " . session('user_id'))->getRow();

		$request_front_count = $db->query("select count(id) from service_request_uttps sru 
			where status_id <= 8 and requestor_id = " . session('user_id')
			. " union all " .
			"select count(id) from service_requests sru 
			where status_id <= 8 and requestor_id = " . session('user_id')
			)->getRow();
		$request_all_count = $db->query("select count(id) from service_request_uttps sru 
			where requestor_id = " . session('user_id')
			. " union all " .
			"select count(id) from service_requests sru 
			where requestor_id = " . session('user_id')
			)->getRow();
		$request_confirm_count = $db->query("select count(id) from service_request_uttps sru 
			where status_id = 2 and requestor_id = " . session('user_id')
			. " union all " .
			"select count(id) from service_requests sru 
			where status_id = 2 and requestor_id = " . session('user_id')
			)->getRow();
		$request_pay_count = $db->query("select count(id) from service_request_uttps sru 
			where status_id = 6 and requestor_id = " . session('user_id')
			. " union all " .
			"select count(id) from service_requests sru 
			where status_id = 6 and requestor_id = " . session('user_id')
			)->getRow();
		$request_done_count = $db->query("select count(id) from service_request_uttps sru 
			where status_id = 14 and requestor_id = " . session('user_id')
			. " union all " .
			"select count(id) from service_requests sru 
			where status_id = 14 and requestor_id = " . session('user_id')
			)->getRow();
		$request_intest_count = $db->query("select count(id) from service_request_uttps sru 
			where status_id in (12,13) and requestor_id = " . session('user_id')
			. " union all " .
			"select count(id) from service_requests sru 
			where status_id in (12,13) and requestor_id = " . session('user_id')
			)->getRow();
			
		$tahunList = $db->query(
			"select distinct(TO_CHAR(a.created_at ,'yyyy')) as tahun from service_request_uttps a 
			 order by TO_CHAR(a.created_at ,'yyyy') ASC"
		);
		$tahunLayanan = $db->query(
			"select DISTINCT(to_char(staff_entry_datein,'yyyy')) as tahun from service_order_uttps 
			 order by TO_CHAR(staff_entry_datein ,'yyyy') ASC"
		);
		$listLayanan = $db->query(
			"select id, service_type as layanan from master_service_types 
			order by id ASC"
		);
		$tahunList = $tahunList->getResult();
		$tahunLayanan = $tahunLayanan->getResult();
		$listLayanan = $listLayanan->getResult();
		$data = $db->query("
				select 
					case 
						when TO_CHAR(a.created_at ,'mm') = '01' then 'Januari'
						when TO_CHAR(a.created_at ,'mm') = '02' then 'Februari' 
						when TO_CHAR(a.created_at ,'mm') = '03' then 'Maret'
						when TO_CHAR(a.created_at ,'mm') = '04' then 'April'
						when TO_CHAR(a.created_at ,'mm') = '05' then 'Mei'
						when TO_CHAR(a.created_at ,'mm') = '06' then 'Juni'
						when TO_CHAR(a.created_at ,'mm') = '07' then 'Juli'
						when TO_CHAR(a.created_at ,'mm') = '08' then 'Agustus'
						when TO_CHAR(a.created_at ,'mm') = '09' then 'September'
						when TO_CHAR(a.created_at ,'mm') = '10' then 'Oktober'
						when TO_CHAR(a.created_at ,'mm') = '11' then 'November'
						when TO_CHAR(a.created_at ,'mm') = '12' then 'Desember'
						else 'undefined' end
					as bulan,
					TO_CHAR(a.created_at ,'yyyy') as tahun,
					sum(
						case when a.status_id =14 then 1 else 0 end
					) as selesai,
					sum(	
						case when a.status_id =16 then 1 else 0 end
					) as batal,
					sum(
						case when (a.status_id >=1 and a.status_id  <=14) or  a.status_id =15 then 1 else 0 end
					) as onprogress
				from service_request_uttps a
				where  TO_CHAR(a.created_at ,'yyyy') ='$Ynow'
				group by TO_CHAR(a.created_at ,'mm'), TO_CHAR(a.created_at ,'yyyy')
				order  by TO_CHAR(a.created_at ,'mm') ASC
		");
		$dataUttp = $db->query("
			select 
				count(a.id) jumlah_alat, 
				c.uttp_type as jenis_alat
			from
				service_order_uttps a
			join service_request_uttp_items aa on
				aa.id = a.service_request_item_id
			join uttps b on
				b.id = aa.uttp_id
			join master_uttp_types c on
				c.id = b.type_id
			join service_request_uttps d on
				d.id = a.service_request_id
			where
				d.jenis_layanan is not null
				and to_char(a.staff_entry_datein, 'yyyy') = '$Ynow'
			group by
				c.uttp_type
		");
		$dataUut = $db->query("
			select 
				count(a.id) jumlah_alat, 
				c.uut_type as jenis_alat
			from
				service_orders a
			join service_request_items aa on
				aa.id = a.service_request_item_id
			join standard_uut b on
				b.id = aa.uut_id 
			join master_standard_types c on
				c.id = b.type_id
			join service_requests d on
				d.id = a.service_request_id
			where
				d.jenis_layanan is not null
				and to_char(a.staff_entry_datein, 'yyyy') = '$Ynow'
			group by
				c.uut_type 
		");
		$dataUut = $dataUut->getResult();
		$data = $data->getResult();
		$data =json_encode($data);
		$dataUttp = $dataUttp->getResult();
		$dataUttp =json_encode($dataUttp);
		$dataUut = json_encode($dataUut);
		return view('admin/home/index', [
			'data' => $data,
			'dataUttp' => $dataUttp,
			'dataUut' => $dataUut,
			'booking_current_count' => $booking_current_count,
			'booking_all_count' => $booking_all_count,
			'request_front_count' => $request_front_count,
			'request_all_count' => $request_all_count,
			'request_confirm_count' => $request_confirm_count,
			'request_pay_count' => $request_pay_count,
			'request_done_count' => $request_done_count,
			'request_intest_count' => $request_intest_count,
			'tahunList' => $tahunList,
			'Ynow' => $Ynow,
			'tahunLayanan' => $tahunLayanan,
			'listLayanan' => $listLayanan
		]);
	}

	public function getDataByYear(){
		$db = \Config\Database::connect();
		$Ynow = date("Y");
		if(isset($_POST['tahun'])){
			$tahun = $_POST['tahun'];
			$Ynow =$tahun;
		}else{
			$tahun =$Ynow;
		}
		$tahunList = $db->query(
			"select distinct(TO_CHAR(a.created_at ,'yyyy')) as tahun from service_request_uttps a"
		);
		$tahunList = $tahunList->getResult();
		$data = $db->query("
					select 
								case 
									when TO_CHAR(a.created_at ,'mm') = '01' then 'Januari'
									when TO_CHAR(a.created_at ,'mm') = '02' then 'Februari' 
									when TO_CHAR(a.created_at ,'mm') = '03' then 'Maret'
									when TO_CHAR(a.created_at ,'mm') = '04' then 'April'
									when TO_CHAR(a.created_at ,'mm') = '05' then 'Mei'
									when TO_CHAR(a.created_at ,'mm') = '06' then 'Juni'
									when TO_CHAR(a.created_at ,'mm') = '07' then 'Juli'
									when TO_CHAR(a.created_at ,'mm') = '08' then 'Agustus'
									when TO_CHAR(a.created_at ,'mm') = '09' then 'September'
									when TO_CHAR(a.created_at ,'mm') = '10' then 'Oktober'
									when TO_CHAR(a.created_at ,'mm') = '11' then 'November'
									when TO_CHAR(a.created_at ,'mm') = '12' then 'Desember'
									else 'undefined' end
								as bulan,
								TO_CHAR(a.created_at ,'yyyy') as tahun,
								sum(
									case when a.status_id =14 then 1 else 0 end
								) as selesai,
								sum(	
									case when a.status_id =16 then 1 else 0 end
								) as batal,
								sum(
									case when a.status_id >=1 and status_id  <=14 or  status_id =15 then 1 else 0 end
								) as onprogress
							from service_request_uttps a
							where  TO_CHAR(a.created_at ,'yyyy') ='$tahun'
							group by TO_CHAR(a.created_at ,'mm'), TO_CHAR(a.created_at ,'yyyy')
							order  by TO_CHAR(a.created_at ,'mm') ASC
			");
			$data = $data->getResultObject();
			$data =json_encode($data);
		return  $data;
	}

	public function getDataFilter(){	
		$Ynow = date('Y');
		$tahun = $_REQUEST['tahunLayanan'];
		$layanan = $_REQUEST['layanan'];
		if(empty($tahun)){
			$tahun =$Ynow;
		}
		if(empty($layanan)){
			$layanan = ' d.jenis_layanan IS NOT NULL ';
		}else{
			$layanan = " d.jenis_layanan ='".$layanan."'";
		}
		$db = \Config\Database::connect();

		$dataUttp = $db->query("
						select 
							count(a.id) jumlah_alat, 
							c.uttp_type as jenis_alat
						from service_order_uttps a
							join service_request_uttp_items aa on aa.id  = a.service_request_item_id 
							join uttps b on b.id = aa.uttp_id 
							join master_uttp_types c on c.id = b.type_id 
							join service_request_uttps d on d.id = a.service_request_id
						where to_char(a.staff_entry_datein,'yyyy') ='$tahun' and $layanan
						group by c.uttp_type");
		
		$dataUttp = $dataUttp->getResultObject();
		$dataUttp = json_encode($dataUttp);
		return $dataUttp;
	}
}
