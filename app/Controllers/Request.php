<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\BookingItemInspectionModel;
use App\Models\RequestUttpModel;
use App\Models\RequestUttpItemModel;
use App\Models\RequestUttpItemInspectionModel;

class Request extends BaseController
{
	public function index()
	{
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			->join('users_customers', 'service_bookings.pic_id = users_customers.id')
            ->whereIn('service_type_id', [4,5,6,7])
            ->where('booking_no is not', null)
            ->where('requested', false)
            //->where('est_arrival_date', date('Y-m-d'))
            ->orderBy('service_bookings.id')
            ->findAll();

		$requestModel = new RequestUttpModel;
		$todos = $requestModel
			->asObject()
			->select('service_request_uttps.*, users_customers.full_name')
			->join('users_customers', 'service_request_uttps.requestor_id = users_customers.id')
			->whereIn('status_id', [1,2,3,4])
			->orderBy('received_date','desc')
			->findAll();

		$processings = $requestModel
			->asObject()
			->select('service_request_uttps.*, users_customers.full_name')
			->join('users_customers', 'service_request_uttps.requestor_id = users_customers.id')
			->whereIn('status_id', [5,6,7,8])
			->orderBy('received_date','desc')
			->findAll();

		$dones = $requestModel
			->asObject()
			->select('service_request_uttps.*, users_customers.full_name')
			->join('users_customers', 'service_request_uttps.requestor_id = users_customers.id')
			->whereIn('status_id', [9])
			->orderBy('received_date','desc')
			->findAll();

		return view ('admin/requests/index', [
			'bookings' => $bookings,
			'todos' => $todos,
			'processings' => $processings,
			'dones' => $dones,
		]);
	}

	public function create() {
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			->select('service_bookings.id, service_bookings.booking_no')
			->join('users_customers', 'service_bookings.pic_id = users_customers.id')
            ->whereIn('service_type_id', [4,5,6,7])
            ->where('booking_no is not', null)
            ->where('requested', false)
            //->where('est_arrival_date', date('Y-m-d'))
            ->orderBy('service_bookings.id')
            ->findAll();

		if ($this->request->getMethod() === 'get'){
			return view('admin/requests/create', [
				'bookings' => $bookings
			]);
		}

		if (! $this->validate([
			'booking_id' => "required",
		])) {
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		$booking = $bookingModel->find($this->request->getPost('booking_id'));

		$db = \Config\Database::connect();
		$no_register = $db->table('master_docnumber')
			->select('param_value')
			->where('init', 'no_register_uttp')
			->get()
			->getRow();
		$noreg_num = intval($no_register->param_value)+1;
		$noreg = sprintf("%04d",$noreg_num).date("-m-Y");

		$db->transStart();

		$requestBuilder = $db->table('service_request_uttps');
		$request = [
			'no_register' => $noreg,
			'booking_id' => $this->request->getPost('booking_id'),
			'received_date' => date("Y-m-d"),
			'estimated_date' => date("Y-m-d"),
			'created_by' => session()->get('user_id'),
			'pic_id' => session()->get('user_id'),
			'booking_no' => $booking->booking_no,
			'label_sertifikat' => $booking->label_sertifikat,
			'addr_sertifikat' => $booking->addr_sertifikat,
			'for_sertifikat' => $booking->for_sertifikat,
			'total_price' => $booking->est_total_price,
			'jenis_layanan' => $booking->jenis_layanan,
			'service_type_id' => $booking->service_type_id,
			'lokasi_pengujian' => $booking->lokasi_pengujian,
			'requestor_id' => $booking->pic_id,
			'status_id' => 1,
		];

		$received_date = date("Y-m-d");
		$estimated_date = date("Y-m-d");

		$requestBuilder->insert($request);
		$requestID = $db->insertID();

		if (isset($requestID)) {
			$db->table('service_bookings')
				->set('requested', true)
				->where('id', $booking->id)
				->update();

			$listInstalasiId = [];

			$bookingItemModel = new BookingItemModel();
			$bookingItems = $bookingItemModel->where('booking_id', $booking->id)->findAll();
			
			foreach ($bookingItems as $item) {
				$requestItemBuilder = $db->table('service_request_uttp_items');
				$requestItem = [
					'request_id' => $requestID,
					'quantity' => $item->quantity,
					'subtotal' => $item->est_subtotal,
					'uttp_id' => $item->uttp_id,
					'location' => $item->location,
					'file_application_letter' => $item->file_application_letter,
					'file_last_certificate' => $item->file_last_certificate,
					'file_manual_book' => $item->file_manual_book,
					'file_type_approval_certificate' => $item->file_type_approval_certificate,
					'file_calibration_manual' => $item->file_calibration_manual,
					'path_application_letter' => $item->path_application_letter,
					'path_last_certificate' => $item->path_last_certificate ,
					'path_manual_book' => $item->path_manual_book,
					'path_type_approval_certificate' => $item->path_type_approval_certificate ,
					'path_calibration_manual' => $item->path_calibration_manual,
					'reference_no' => $item->reference_no,
					'reference_date' => $item->reference_date,
					'status_id' => 1,
				];

				$requestItemBuilder->insert($requestItem);
				$itemID = $db->insertID();

				if (isset($itemID)) {
					$bookingItemInspectionModel = new BookingItemInspectionModel();
					$bookingItemInspections = $bookingItemInspectionModel->where('booking_item_id', $item->id)->findAll();

					foreach ($bookingItemInspections as $inspection) {
						$requestItemInspectionBuilder = $db->table('service_request_uttp_item_inspections');
						$requestItemInspection = [
							'request_item_id' => $itemID,
							'quantity' => $inspection->quantity,
							'price' => $inspection->price,
							'inspection_price_id' => $inspection->inspection_price_id,
							'status_id' => 1
						];

						$requestItemInspectionBuilder->insert($requestItemInspection);

						$inspectionPrice = $db->table('uttp_inspection_prices')
							->where('id', $inspection->inspection_price_id)
							->get()
							->getRow();

                        array_push($listInstalasiId, $inspectionPrice->instalasi_id);
					}
				}
			}

			$sla = $db->table('master_instalasi')
				->selectMax('sla_day')
				->whereIn('id', $listInstalasiId)
				->get()
				->getRow();
			$formatted_date = new \DateTime( $estimated_date );
			$date_timestamp = $formatted_date->getTimestamp();
			for ($i=0; $i < $sla->sla_day; $i++) { 
				$nextDay = date('w', strtotime('+1day', $date_timestamp) );
				if( $nextDay == 0 || $nextDay == 6 ) { $i--; }
				$date_timestamp = strtotime("+1day", $date_timestamp);
			}
			$formatted_date->setTimestamp($date_timestamp);
			$estimated_date = $formatted_date->format("Y-m-d");

			$holidays = $db->table('holidays')
				->distinct('holiday_date')
				->where('holiday_date >=', $received_date)
				->where('holiday_date <=', $estimated_date)
				->countAllResults();
			$formatted_date = new \DateTime( $estimated_date );
			$date_timestamp = $formatted_date->getTimestamp();
			for ($i=0; $i < $holidays; $i++) { 
				$nextDay = date('w', strtotime('+1day', $date_timestamp) );
				if( $nextDay == 0 || $nextDay == 6 ) { $i--; }
				$date_timestamp = strtotime("+1day", $date_timestamp);
			}
			$formatted_date->setTimestamp($date_timestamp);
			$estimated_date = $formatted_date->format("Y-m-d");

			$db->table('service_request_uttps')
				->set('estimated_date', $estimated_date)
				->where('id', $requestID)
				->update();

			$db->table('master_docnumber')
				->set('param_value', $noreg_num)
				->where('init', 'no_register_uttp')
				->update();
		}

		$db->transComplete();

        return redirect()->to(base_url('/request/edit/' .$requestID))->with('success', 'Berhasil mendaftar ulang booking');
	}

	public function edit($id) {
		$requestModel = new RequestUttpModel();
		$request = $requestModel
			->asObject()
			->select('service_request_uttps.*')
			->select('users_customers.full_name, users_customers.email, users_customers.nib, users_customers.npwp, users_customers.kantor, users_customers.phone')
			->join('users_customers', 'service_request_uttps.requestor_id = users_customers.id')
            ->find($id);

		
		$db = \Config\Database::connect();
		$items = $db->query('select sbi.*,  ' . 
			"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
			's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, '.
			's.tool_model, s.tool_type , s.tool_made_in, '.
			'mut.uttp_type as type ' .
			'from service_request_uttp_items sbi ' .
			'inner join uttps s on sbi.uttp_id = s.id ' .
			'inner join master_uttp_types mut on s.type_id = mut.id ' .
			'where sbi.request_id = ' . $id)->getResult();

		$inspections = $db->query("select sbii.id, sbii.request_item_id, sbii.quantity, sbii.price, " .
			"sip.inspection_type, sip.unit " .
			"from service_request_uttp_item_inspections sbii " .
			"inner join uttp_inspection_prices sip on sbii.inspection_price_id = sip.id " .
			"inner join service_request_uttp_items sbi on sbii.request_item_id = sbi.id " .
			"where sbi.request_id = " . $id)->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/requests/edit', [
				'request' => $request,
				'items' => $items,
				'inspections' => $inspections,
			]);
		}
	}

	public function getBooking($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel
			->join('users_customers', 'service_bookings.pic_id = users_customers.id')
			->find($id);

		return $this->response->setJSON($booking);
	}
}
