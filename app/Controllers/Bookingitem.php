<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\BookingItemInspectionModel;
use App\Models\MasterStandardType;
use App\Models\UttpModel;
use App\Models\UttpTypeModel;
use App\Models\UutTypeModel;
use Illuminate\Http\Request;
use App\Models\BookingItemTTUPerlengkapanModel;
use App\Models\UutModel;
use App\Models\UutOwnerModel;

class Bookingitem extends BaseController
{
	public function create($id) {
		$bookings = new BookingModel();
		$booking = $bookings->find($id);
		$ktp = null;

		if(!empty($booking->uut_owner_id)){
			$ktps = new UutOwnerModel();
			$ktp = $ktps->find($booking->uut_owner_id);
			$ktp = $ktp->ktp;
		}

		/*
		$TypeModel = new UttpTypeModel();
		if ($booking->service_type_id == 4 || $booking->service_type_id == 5) {
			$uttpTypes = $TypeModel->where('is_tera', true)->findAll();
		} else if ($booking->service_type_id == 6 || $booking->service_type_id == 7) {
			$uttpTypes = $TypeModel->where('is_ujitipe', true)->findAll();
		} else {
			$uttpTypes = $TypeModel->findAll();
		}
		*/
		
		// $standardType = new MasterStandardType();
		// $standard = $standardType->findAll();
		// define for uut
		$uutType = new UutTypeModel();
		$uutTypes = $uutType->findAll();
		$standardType = new MasterStandardType();
		$standard = $standardType->orderBy("uut_type")
		->findAll();

		$db = \Config\Database::connect();
		$cities = $db->query(
			'select k.*, p.nama as provinsi 
			from master_kabupatenkota k inner 
			join master_provinsi p on k.provinsi_id = p.id')->getResult();
	
		$negaras = $db->query(
			'select * from master_negara')->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/bookingitems/create',[
				'booking' => $booking,
				//'uttpTypes' => $uttpTypes,
				'standard' => $standard,
				'uutTypes' => $uutTypes,
				'cities' => $cities,
				'negaras' => $negaras,
				'ktp' => $ktp,
			]);
		}

		if(!empty($booking->uut_owner_id)){
			$rules = [
				'uut_id' => ['required'],
			];
			$error_msg = [
				'uut_id' => [
					'required' => 'Alat wajib dipilih'
				]
			];
		} else {
			$rules = [
				'uttp_id' => ['required'],
			];
			$error_msg = [
				'uttp_id' => [
					'required' => 'Alat wajib dipilih'
				]
			];
		}

		if ($booking->lokasi_pengujian == 'luar') {
			$rules['location'] = ['required'];
			$error_msg['location']['required'] = 'Lokasi pengujian alat wajib diisi';
			if ($booking->lokasi_dl == 'dalam') {
				$rules['location_kabkot_id'] = ['greater_than[0]'];
				$error_msg['location_kabkot_id']['greater_than'] = 'Kabupaten lokasi pengujian wajib dipilih';
			} else {
				$rules['location_negara_id'] = ['greater_than[0]'];
				$error_msg['location_negara_id']['greater_than'] = 'Negara lokasi pengujian wajib dipilih';
			}
		} else {
			if ($booking->service_type_id == 5) {
				$rules['file_last_certificate'] = ['uploaded[file_last_certificate]'];
				$error_msg['file_last_certificate']['uploaded'] = 'File sertifikat sebelumnya wajib diupload dalam format PDF';
			}
		}
		if ($booking->service_type_id == 4 || $booking->service_type_id == 5) {
			//$rules['file_type_approval_certificate'] = ['uploaded[file_type_approval_certificate]'];
			//$error_msg['file_type_approval_certificate']['uploaded'] = 'File surat persetujuan wajib diupload dalam format PDF';
			$rules['reference_no'] = ['required'];
			$error_msg['reference_no']['required'] = 'Nomor surat wajib diisi';
			$rules['reference_date'] = ['required'];
			$error_msg['reference_date']['required'] = 'Tanggal surat wajib diisi dengan format yang benar';
		}
		if ($booking->service_type_id == 6 || $booking->service_type_id == 7) {
			$rules['file_application_letter'] = ['uploaded[file_application_letter]'];
			$error_msg['file_application_letter']['uploaded'] = 'File surat permohonan wajib diupload dalam format PDF';
			$rules['file_manual_book'] = ['uploaded[file_manual_book]'];
			$error_msg['file_manual_book']['uploaded'] = 'File buku manual wajib diupload dalam format PDF';
		}
		if ($this->request->getFile('file_last_certificate')) {
			$rules['file_last_certificate'][] = 'mime_in[file_last_certificate,application/pdf,application/octet-stream]';
			$error_msg['file_last_certificate']['mime_in'] = 'File sertifikat sebelumnya wajib diupload dalam format PDF';
		}
		if ($this->request->getFile('file_type_approval_certificate')) {
			$rules['file_type_approval_certificate'][] = 'mime_in[file_type_approval_certificate,application/pdf,application/octet-stream]';
			$error_msg['file_type_approval_certificate']['mime_in'] = 'File surat persetujuan wajib diupload dalam format PDF';
		}
		if ($this->request->getFile('file_application_letter')) {
			$rules['file_application_letter'][] = 'mime_in[file_application_letter,application/pdf,application/octet-stream]';
			$error_msg['file_application_letter']['mime_in'] = 'File surat permohonan wajib diupload dalam format PDF';
		}
		if ($this->request->getFile('file_manual_book')) {
			$rules['file_manual_book'][] = 'mime_in[file_manual_book,application/pdf,application/octet-stream]';
			$error_msg['file_manual_book']['mime_in'] = 'File buku manual wajib diupload dalam format PDF';
		}

		//$data = $this->request->getPost(array_keys($rules));
		//dd([$booking->service_type_id, $this->request->getFile('file_last_certificate'),$rules, $error_msg]);
		

		if (! $this->validate($rules, $error_msg)) {
			dd([$this->request->getFile('file_type_approval_certificate'),$rules, $error_msg, $this->validator->getErrors()]);
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

		$uttpId = $this->request->getPost('uttp_id');
		$uutId = $this->request->getPost('uut_id');
		

		$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
		$file_last_certificate = $this->request->getFile('file_last_certificate');
		$file_application_letter = $this->request->getFile('file_application_letter');
		$file_calibration_manual = $this->request->getFile('file_calibration_manual');
		$file_manual_book = $this->request->getFile('file_manual_book');

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->isValid()) { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
		if (isset($file_last_certificate) && $file_last_certificate->isValid()) { $path_last_certificate = $file_last_certificate->store(); }
		if (isset($file_application_letter) && $file_application_letter->isValid()) { $path_application_letter = $file_application_letter->store(); }
		if (isset($file_calibration_manual)  && $file_calibration_manual->isValid()) { $path_calibration_manual = $file_calibration_manual->store(); }
		if (isset($file_manual_book)  && $file_manual_book->isValid()) { $path_manual_book = $file_manual_book->store(); }

		// dd([$path_application_letter,$path_application_letter]);
		$location_prov_id = null;
		if ($this->request->getPost('location_kabkot_id') != null) {
			$city = $db->query('select * from master_kabupatenkota where id = ' . $this->request->getPost('location_kabkot_id'))->getRow();
			$location_prov_id = $city->provinsi_id;
		}

		$items = new BookingItemModel();
		$item = [
			'booking_id'        => $booking->id,
			// 'uml_standard_id' 	=> $this->request->getPost('uml_standard_id'),
			'uut_id'			=> $uutId,
			'uttp_id'			=> $uttpId,
			'quantity'			=> 0,
			'est_subtotal'		=> 0,

			'location' 			=> $this->request->getPost('location'),
			'reference_no' 		=> $this->request->getPost('reference_no'),
			'reference_date' 	=> date("Y-m-d", strtotime($this->request->getPost('reference_date'))),

			'location_kabkot_id' 	=> $this->request->getPost('location_kabkot_id'),
			'location_prov_id' 		=> $location_prov_id,
			'location_negara_id' 	=> $this->request->getPost('location_negara_id'),

			'location_lat'			=> $this->request->getPost('location_lat') != null ? $this->request->getPost('location_lat') : null,
			'location_long'			=> $this->request->getPost('location_long') != null ? $this->request->getPost('location_long') : null,
			'location_alat' 				=> $this->request->getPost('location_alat'),

			'file_type_approval_certificate' 	=> isset($file_type_approval_certificate) ? $file_type_approval_certificate->getClientName() : null,
			'file_last_certificate' 			=> isset($file_last_certificate) ? $file_last_certificate->getClientName() : null,
			'file_application_letter' 			=> isset($file_application_letter) ? $file_application_letter->getClientName() : null,
			'file_calibration_manual' 			=> isset($file_calibration_manual) ? $file_calibration_manual->getClientName() : null,
			'file_manual_book' 					=> isset($file_manual_book) ? $file_manual_book->getClientName() : null,

			'path_type_approval_certificate' 	=> isset($path_type_approval_certificate) ? $path_type_approval_certificate : null,
			'path_last_certificate' 			=> isset($path_last_certificate) ? $path_last_certificate : null,
			'path_application_letter' 			=> isset($path_application_letter) ? $path_application_letter : null,
			'path_calibration_manual' 			=> isset($path_calibration_manual) ? $path_calibration_manual : null,
			'path_manual_book' 					=> isset($path_manual_book) ? $path_manual_book : null,
		];

		if (! $items->insert($item)) {
			return redirect()->back()->withInput()->with('errors', $items->errors());
        }

		/*
		if ($booking->service_type_id == 4 || $booking->service_type_id == 5) {
			if ($booking->lokasi_pengujian == 'luar') {
				return redirect()->to(base_url('/bookingitem/edit/'.$id.'/'.$items->insertID))->with('success', 'Berhasil menyimpan pendaftaran baru');
			}
		}
		*/

        //return redirect()->to(base_url('/bookingiteminspection/index/' . $id . '/' . $items->insertID))->with('success', 'Berhasil menyimpan pendaftaran baru');
		return redirect()->to(base_url('/booking/edit/'.$id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function edit($id, $idItem) {
		$db = \Config\Database::connect();
		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$bookingItems = new BookingItemModel();
		$item = $bookingItems->find($idItem);

		$uttpTypeModel = new UttpTypeModel();
		if ($booking->service_type_id == 4 || $booking->service_type_id == 5) {
			$uttpTypes = $uttpTypeModel->where('is_tera', true)->findAll();
		} else if ($booking->service_type_id == 6 || $booking->service_type_id == 7) {
			$uttpTypes = $uttpTypeModel->where('is_ujitipe', true)->findAll();
		} else {
			$uttpTypes = $uttpTypeModel->findAll();
		}

		$uttpModel = new UttpModel();
		$uutModel =  new UutModel();
		$uttp = $uttpModel->find($item->uttp_id);

		//for uut
		$uutType = new UutTypeModel();
		$uutTypes = $uutType->findAll();
		$standardType = new MasterStandardType();
		$standard = $standardType->findAll();
		$uut = $uutModel->find($item->uut_id);
		// dd($db-> getLastQuery());
		
		$ttuPerlengkapan = $db->query('select i.*, 
			u.tool_name,
			u.tool_brand,
			u.tool_model,
			u.serial_no,
			u.tool_media,
			u.tool_made_in,
			u.tool_capacity,
			u.tool_capacity_unit
			from service_booking_item_ttu_perlengkapan i
			inner join uttps u on i.uttp_id = u.id 
			where i.booking_item_id = '.$item->id)->getResult();

		if ($this->request->getMethod() === 'get'){
			return view ('admin/bookingitems/edit', [
				'booking' => $booking,
				'item' => $item,
				'uttpTypes' => $uttpTypes,
				'uttp' => $uttp,
				'ttuPerlengkapan' => $ttuPerlengkapan,
				'standard' => $standard,
				'uutTypes' => $uutTypes,
				'uut'	=> $uut,
			]);
		}
		
		$uttpId = $this->request->getPost('uttp_id');

		$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
		$file_last_certificate = $this->request->getFile('file_last_certificate');
		$file_application_letter = $this->request->getFile('file_application_letter');
		$file_calibration_manual = $this->request->getFile('file_calibration_manual');
		$file_manual_book = $this->request->getFile('file_manual_book');

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() != '') { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
		if (isset($file_last_certificate) && $file_last_certificate->getName() != '') { $path_last_certificate = $file_last_certificate->store(); }
		if (isset($file_application_letter) && $file_application_letter->getName() != '') { $path_application_letter = $file_application_letter->store(); }
		if (isset($file_calibration_manual) && $file_calibration_manual->getName() != '') { $path_calibration_manual = $file_calibration_manual->store(); }
		if (isset($file_manual_book) && $file_manual_book->getName() != '') { $path_manual_book = $file_manual_book->store(); }

		//$item->booking_id = $booking->id,
		// $item->uml_standard_id = $this->request->getPost('uml_standard_id');
		$item->uttp_id = $uttpId;
		$item->quantity = 0;
		$item->est_subtotal = 0;

		$item->location = $this->request->getPost('location');
		$item->reference_no = $this->request->getPost('reference_no');
		$item->reference_date = date("Y-m-d", strtotime($this->request->getPost('reference_date')));

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() ) {
			$item->file_type_approval_certificate = isset($file_type_approval_certificate) ? $file_type_approval_certificate->getClientName() : null;
			$item->path_type_approval_certificate = isset($path_type_approval_certificate) ? $path_type_approval_certificate : null;
		}
		if (isset($file_last_certificate) && $file_last_certificate->getName() ) {
			$item->file_last_certificate = isset($file_last_certificate) ? $file_last_certificate->getClientName() : null;
			$item->path_last_certificate = isset($path_last_certificate) ? $path_last_certificate : null;
		}
		if (isset($file_application_letter) && $file_application_letter->getName() ) {
			$item->file_application_letter = isset($file_application_letter) ? $file_application_letter->getClientName() : null;
			$item->path_application_letter = isset($path_application_letter) ? $path_application_letter : null;
		}
		if (isset($file_calibration_manual) && $file_calibration_manual->getName() ) {
			$item->file_calibration_manual = isset($file_calibration_manual) ? $file_calibration_manual->getClientName() : null;
			$item->path_calibration_manual = isset($path_calibration_manual) ? $path_calibration_manual : null;
		}
		if (isset($file_manual_book) && $file_manual_book->getName() ) {
			$item->file_manual_book = isset($file_manual_book) ? $file_manual_book->getClientName() : null;
			$item->path_manual_book = isset($path_manual_book) ? $path_manual_book : null;
		}

		if (! $bookingItems->save($item)) {
			return redirect()->back()->withInput()->with('errors', $bookingItems->errors());
        }

        //return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
		if ($booking->service_type_id == 4 || $booking->service_type_id == 5) {
			if ($booking->lokasi_pengujian == 'luar') {
				return redirect()->to(base_url('/bookingitem/edit/'.$id.'/'.$item->id))->with('success', 'Berhasil menyimpan pendaftaran baru');
			}
		}
		return redirect()->to(base_url('/booking/edit/'.$id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function copy($id, $idItem) {
		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		$bookingItems = new BookingItemModel();
		$item = $bookingItems->find($idItem);

		$uttpTypeModel = new UttpTypeModel();
		$uutTypeModel = new UutTypeModel();
		$uttpTypes = $uttpTypeModel->findAll();
		$uutType = $uutTypeModel->findAll();

		$uttpModel = new UttpModel();
		$uutModel = new UutModel();
		$uttp = $uttpModel->find($item->uttp_id);
		$uut = $uutModel->find($item->uut_id);

		// dd($booking);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/bookingitems/copy', [
				'booking' => $booking,
				'item' => $item,
				'uttpTypes' => $uttpTypes,
				'uttp' => $uttp,
				'uut'	=> $uut,
				'uutTypes'	=> $uutType,
			]);
		}
		
		$uttpId = $this->request->getPost('uttp_id');

		$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
		$file_last_certificate = $this->request->getFile('file_last_certificate');
		$file_application_letter = $this->request->getFile('file_application_letter');
		$file_calibration_manual = $this->request->getFile('file_calibration_manual');
		$file_manual_book = $this->request->getFile('file_manual_book');

		if (isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() != '') { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
		if (isset($file_last_certificate) && $file_last_certificate->getName() != '') { $path_last_certificate = $file_last_certificate->store(); }
		if (isset($file_application_letter) && $file_application_letter->getName() != '') { $path_application_letter = $file_application_letter->store(); }
		if (isset($file_calibration_manual) && $file_calibration_manual->getName() != '') { $path_calibration_manual = $file_calibration_manual->store(); }
		if (isset($file_manual_book) && $file_manual_book->getName() != '') { 
			$this->upload($this->$request);
			$path_manual_book = $file_manual_book->store(); 
		}

		$newitem = [
			'booking_id'        => $booking->id,
			// 'uml_standard_id' 	=> $this->request->getPost('uml_standard_id'),
			'uttp_id'			=> $uttpId,
			'quantity'			=> 0,
			'est_subtotal'		=> 0,

			'location' 			=> $this->request->getPost('location'),
			'reference_no' 		=> $this->request->getPost('reference_no'),
			'reference_date' 	=> date("Y-m-d", strtotime($this->request->getPost('reference_date'))),


			'file_type_approval_certificate' 	=> isset($file_type_approval_certificate) && $file_type_approval_certificate->getName() != '' ? $file_type_approval_certificate->getClientName() : $item->file_type_approval_certificate,
			'file_last_certificate' 			=> isset($file_last_certificate) && $file_last_certificate->getName() != '' ? $file_last_certificate->getClientName() : $item->file_last_certificate,
			'file_application_letter' 			=> isset($file_application_letter) && $file_application_letter->getName() != '' ? $file_application_letter->getClientName() : $item->file_application_letter,
			'file_calibration_manual' 			=> isset($file_calibration_manual) && $file_calibration_manual->getName() != '' ? $file_calibration_manual->getClientName() : $item->file_calibration_manual,
			'file_manual_book' 					=> isset($file_manual_book) && $file_manual_book->getName() != '' ? $file_manual_book->getClientName() : $item->file_manual_book,

			'path_type_approval_certificate' 	=> isset($path_type_approval_certificate) ? $path_type_approval_certificate : $item->path_type_approval_certificate,
			'path_last_certificate' 			=> isset($path_last_certificate) ? $path_last_certificate : $item->path_last_certificate,
			'path_application_letter' 			=> isset($path_application_letter) ? $path_application_letter : $item->path_application_letter,
			'path_calibration_manual' 			=> isset($path_calibration_manual) ? $path_calibration_manual : $item->path_calibration_manual,
			'path_manual_book' 					=> isset($path_manual_book) ? $path_manual_book : $item->path_manual_book,
		];

		if (! $bookingItems->insert($newitem)) {
			return redirect()->back()->withInput()->with('errors', $items->errors());
        }

        //return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menyimpan pendaftaran');
		return redirect()->to(base_url('/booking/edit/'.$id))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function deleteuttp($id) {
		$bookingModel = new BookingModel();;
		$bookingItemModel = new BookingItemModel();
		

		$db = \Config\Database::connect();

		$inspectionModel = new BookingItemInspectionModel();

		if (! $inspectionModel->where('booking_item_id', $id)->delete()) {
			return redirect()->back()->withInput()->with('errors', $inspectionModel->errors());
        }

		$perlengkapanModel = new BookingItemTTUPerlengkapanModel();
		$perlengkapanModel->where('booking_item_id', $id)->delete();

		$item = $bookingItemModel->find($id);
		if (! $bookingItemModel->delete($id)) {
			return redirect()->back()->withInput()->with('errors', $bookingItemModel->errors());
        }

		$aggregate = $db->query("select count(sbii.id) quantity, coalesce(sum(sbii.price * sbii.quantity), 0) subtotal " . 
			"from service_booking_item_inspections sbii " .
			"inner join service_booking_items sbi on sbii.booking_item_id = sbi.id ".
			"where sbi.booking_id = " . $item->booking_id)->getRow();

		$booking = $bookingModel->find($item->booking_id);

		$booking->est_total_price = $aggregate->subtotal;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }


        return redirect()->to(base_url('/booking/edit/' . $booking->id))->with('success', 'Berhasil menyimpan rincian pemeriksaan baru');
	}

	public function addperlengkapan($id, $idItem) {
		if ($this->request->getMethod() === 'post'){ 
			$perlengkapans = new BookingItemTTUPerlengkapanModel();
			
			$idPerlengkapan = $this->request->getPost('uttp_perlengkapan_id');
			$perlengkapan = [
				'booking_item_id'   => $idItem,
				'uttp_id'			=> $idPerlengkapan,
			];

			$perlengkapans->save($perlengkapan);
		}

		return redirect()->to(base_url('/bookingitem/edit/'.$id.'/'.$idItem))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function deleteperlengkapan($id, $idItem, $idPerlengkapan) {
		$perlengkapans = new BookingItemTTUPerlengkapanModel();
		$perlengkapans->delete($idPerlengkapan);

		return redirect()->to(base_url('/bookingitem/edit/'.$id.'/'.$idItem))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function editbulk($id) {
		$db = \Config\Database::connect();
		$bookings = new BookingModel();
		$booking = $bookings->find($id);

		//$bookingItems = new BookingItemModel();
		//$items = $bookingItems->where('booking_id', $id)->findAll();

		$items = $db->query(
			'select sbi.*, u.*, mut.uttp_type
			from service_booking_items sbi
			inner join uttps u on sbi.uttp_id = u.id  
			inner join master_uttp_types mut on u.type_id = mut.id
			where sbi.booking_id = ' .$id
		)->getResult();

		$cities = $db->query(
			'select k.*, p.nama as provinsi 
			from master_kabupatenkota k inner 
			join master_provinsi p on k.provinsi_id = p.id')->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/bookingitems/editbulk',[
				'booking' => $booking,
				//'uttpTypes' => $uttpTypes,
				//'standard' => $standard,
				//'uutTypes' => $uutTypes,
				'cities' => $cities,
				//'ktp' => $ktp,
				'items' => $items,
			]);
		}
	}
}
