<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\UttpModel;
use App\Models\UserUttpModel;
use App\Models\UttpOwnerModel;
use App\Models\UserUttpOwnerModel;
use App\Models\UserUutModel;
use App\Models\UserUutOwnerModel;
use App\Models\UttpTypeModel;
use App\Models\UttpUnitModel;
use App\Models\UutModel;
use App\Models\UutOwnerModel;
use App\Models\UutTypeModel;
use App\Models\UutUnitModel;

use function PHPUnit\Framework\isEmpty;

class Useruut extends BaseController
{
	use ResponseTrait;

	public function index() {

		$uutModel = new UserUutModel();
		$uuts = $uutModel
			->select('uut.*, master_standard_types.uut_type, uut_owners.nama as owner, user_uuts.id map_id')
			->join('standard_uut uut', 'user_uuts.uut_id = uut.id')
			->join('uut_owners', 'uut_owners.id = uut.owner_id')
			->join('master_standard_types', 'master_standard_types.id = uut.type_id')
			->where('user_uuts.user_id',  session('user_id'))
			->findAll();
		
		return view ('admin/useruuts/index', [
			'uttps' => $uuts,
		]);
	}

	public function create() {
		$uutModel = new UutModel();

		$ownerModel = new UserUutOwnerModel();
		$owners = $ownerModel
			->select('uut_owners.*')
			->join('uut_owners', 'uut_owners.id = user_uut_owners.owner_id')
			->where('user_uut_owners.user_id',  session('user_id'))
			->findAll();
		// dd($this->request->getPost('uut_id'));
		$uutTypeModel = new UutTypeModel();
		$uttpTypes = $uutTypeModel->where('is_active', true)->findAll();

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/useruuts/create', [
				'owners' => $owners,
				'uttpTypes' => $uttpTypes,
				'negaras' => $negaras,
			]);
		}
		
		$mapModel = new UserUutModel();
		$map = [
			'user_id'	=> session('user_id'),
			'uut_id'	=> $this->request->getPost('uut_id'),
		];

		if (! $mapModel->save($map)) {
			return redirect()->back()->withInput()->with('errors', $mapModel->errors());
		}


        return redirect()->to(base_url('/useruut'))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}

	public function createNew($owner_id, $type_id) {
		$uutModel = new UutModel();
		define( 't_excepts', [265,543,210]);

		$prep_capacity='';
		$prep_dayabaca='';

		$ownerModel = new UutOwnerModel();
		$owner = $ownerModel->find($owner_id);

		$uutTypeModel = new UutTypeModel();
		$uutType = $uutTypeModel->where('is_active', true)->find($type_id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->orderBy("nama_negara","ASC")
			->get()
			->getResult();
		if ($this->request->getMethod() === 'get'){
			return view('admin/useruuts/create_new', [
				'owner' => $owner,
				'uttpType' => $uutType,
				'negaras' => $negaras,
				't_excepts' => t_excepts
			]);
		}
		if($this->request->getPost('tool_made_in_id') != null){
			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
			->getRow();
		}else{
			$negara =['id' => '64', 'nama_negara' => 'Indonesia'];
			$negara = (object)$negara;
		}

		$cap_and_unit = $this->request->getPost("capacity_opt");
		$baca_and_unit = $this->request->getPost("dayabaca_opt");

		if(($cap_and_unit && $baca_and_unit !=null) || ($cap_and_unit && $baca_and_unit !='')){
			$prep_capacity = $cap_and_unit;
			$prep_dayabaca = $baca_and_unit;
		}else{
			$prep_capacity = $this->request->getPost('tool_capacity');
			$prep_dayabaca = $this->request->getPost('tool_dayabaca');
		}


		$kapasitas ='';
		$kapasitas_dayabaca = '';
		$unit='';
		$dayabaca='';

		if($this->request->getPost('tool_capacity_unit') != '' || $this->request->getPost('tool_capacity_unit') != null)
		{
			$kapasitas = $this->request->getPost('tool_capacity_unit');
		}elseif($this->request->getPost('tool_capacity_unit_1') !='' || $this->request->getPost('tool_capacity_unit_1') != null){
			$kapasitas = $this->request->getPost('tool_capacity_unit_1');
		}

		if($this->request->getPost('tool_dayabca_unit') != '' || $this->request->getPost('tool_dayabaca_unit') != null)
		{
			$kapasitas_dayabaca = $this->request->getPost('tool_dayabaca_unit');
		}elseif($this->request->getPost('tool_dayabaca_unit_1') !='' || $this->request->getPost('tool_dayabaca_unit_1') != null){
			$kapasitas_dayabaca = $this->request->getPost('tool_dayabaca_unit_1');
		}
		$kapasitas= trim(preg_replace('/\s+/',' ', $kapasitas));
		$kapasitas_dayabaca = trim(preg_replace('/\s+/',' ', $kapasitas_dayabaca));
		if($kapasitas =="°C" || $kapasitas =="⁰C" || $kapasitas =="⁰C"){
			$unit='&deg;C';
		}elseif($kapasitas =="°F" || $kapasitas =="⁰F"){
			$unit='&deg;F';
		}elseif($kapasitas =="°K" || $kapasitas == "⁰K"){
			$unit='&deg;K';
		}else{
			$unit = $kapasitas;
		}

		if($kapasitas_dayabaca =="°C" || $kapasitas_dayabaca == "⁰C"){
			$dayabaca='&deg;C';
		}elseif($kapasitas_dayabaca =="°F" || $kapasitas_dayabaca =="⁰F"){
			$dayabaca='&deg;F';
		}elseif($kapasitas_dayabaca =="°K" || $kapasitas_dayabaca == "⁰K"){
			$dayabaca='&deg;K';
		}else{
			$dayabaca = $kapasitas_dayabaca;
		}

		
		$uut = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $prep_capacity,//$this->request->getPost('tool_capacity'),
			'tool_dayabaca'			=> $prep_dayabaca,//$this->request->getPost('tool_dayabaca'),
			'tool_capacity_unit'	=> $unit,
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_dayabaca_unit'	=> $this->request->getPost('tool_dayabaca_unit'),
			'tool_dayabaca_unit'	=> $dayabaca,
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),
			'class'					=> $this->request->getPost('class'),
			'jumlah'				=> $this->request->getPost('jumlah'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			// 'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),
			'tool_capacity_min_unit'=> $this->request->getPost('tool_capacity_min_unit'),

			'location_lat'			=> $this->request->getPost('location_lat') != null ? $this->request->getPost('location_lat') : null,
			'location_long'			=> $this->request->getPost('location_long') != null ? $this->request->getPost('location_long') : null,

		];

		if (! $uutModel->save($uut)) {
			return redirect()->back()->withInput()->with('errors', $uutModel->errors());
        }

		$uutId = $db->insertID();

		$mapModel = new UserUutModel();
		$map = [
			'user_id'	=> session('user_id'),
			'uut_id'	=> $uutId,
		];

		if (! $mapModel->save($map)) {
			return redirect()->back()->withInput()->with('errors', $mapModel->errors());
		}

        return redirect()->to(base_url('/useruut'))->with('success', 'Berhasil menyimpan alat baru');
	}

	public function edit($id) {
		$uutModel = new UutModel();

		$ownerModel = new UutOwnerModel();
		$owners = $ownerModel->findAll();

		$uutTypeModel = new UutTypeModel();
		$uutTypes = $uutTypeModel->where('is_active', true)->findAll();

		$uut = $uutModel->find($id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uut/edit', [
				'uttp' => $uut,
				'owners' => $owners,
				'uttpTypes' => $uutTypes,
				'negaras' => $negaras,
			]);
		}
		
		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();

		$uut->type_id = $this->request->getPost('type_id');
		$uut->serial_no = $this->request->getPost('serial_no');
		$uut->tool_brand = $this->request->getPost('tool_brand');
		$uut->tool_model = $this->request->getPost('tool_model');
		$uut->tool_type = $this->request->getPost('tool_type');
		$uut->tool_capacity = $this->request->getPost('tool_capacity');
		$uut->tool_capacity_unit = $this->request->getPost('tool_capacity_unit');
		$uut->tool_factory = $this->request->getPost('tool_factory');
		$uut->tool_factory_address = $this->request->getPost('tool_factory_address');
		//$uttp->tool_made_in = $this->request->getPost('tool_made_in');
		$uut->tool_made_in = $negara->nama_negara;
		$uut->tool_made_in_id =  $negara->id;
		$uut->owner_id = $this->request->getPost('owner_id');
		$uut->tool_media = $this->request->getPost('tool_media');
		$uut->tool_name = $this->request->getPost('tool_name');
		$uut->tool_capacity_min = $this->request->getPost('tool_capacity_min');
		
		if (! $uutModel->save($uut)) {
			return redirect()->back()->withInput()->with('errors', $uutModel->errors());
        }

        return redirect()->to(base_url('/uut'))->with('success', 'Berhasil menyimpan perubahan data');
	}

	public function copy($id) {
		$uutModel = new UutModel();

		$ownerModel = new UutOwnerModel();
		$owners = $ownerModel->findAll();

		$uutTypeModel = new UutTypeModel();
		$uutTypes = $uutTypeModel->where('is_active', true)->findAll();

		$uut = $uutModel->find($id);

		$db = \Config\Database::connect();
		$negaras = $db->table('master_negara')
			->get()
			->getResult();

		if ($this->request->getMethod() === 'get'){
			return view('admin/uut/copy', [
				'uttp' => $uut,
				'owners' => $owners,
				'uttpTypes' => $uutTypes,
				'negaras' => $negaras,
			]);
		}

		$negara = $db->table('master_negara')
			->where('id', $this->request->getPost('tool_made_in_id'))
			->get()
			->getRow();
		
		$uut = [
			'type_id'				=> $this->request->getPost('type_id'),
			'serial_no'				=> $this->request->getPost('serial_no'),
			'tool_brand'			=> $this->request->getPost('tool_brand'),
			'tool_model'			=> $this->request->getPost('tool_model'),
			'tool_type'				=> $this->request->getPost('tool_type'),
			'tool_capacity'			=> $this->request->getPost('tool_capacity'),
			'tool_capacity_unit'	=> $this->request->getPost('tool_capacity_unit'),
			'tool_factory'			=> $this->request->getPost('tool_factory'),
			'tool_factory_address'	=> $this->request->getPost('tool_factory_address'),
			//'tool_made_in'			=> $this->request->getPost('tool_made_in'),
			'tool_made_in'			=> $negara->nama_negara,
			'tool_made_in_id'		=> $negara->id,
			'owner_id'				=> $this->request->getPost('owner_id'),
			'tool_media'			=> $this->request->getPost('tool_media'),
			'tool_name'				=> $this->request->getPost('tool_name'),
			'tool_capacity_min'		=> $this->request->getPost('tool_capacity_min'),
		];

		if (! $uutModel->save($uut)) {
			return redirect()->back()->withInput()->with('errors', $uutModel->errors());
        }

        return redirect()->to(base_url('/uut'))->with('success', 'Berhasil menyimpan penambahan data');
	}

	public function delete($id) {
		$uttpModel = new UserUutModel();
		$uttpModel->delete($id);

        return redirect()->to(base_url('/useruut'))->with('success', 'Berhasil menghapus data alat');
	}
	
	public function delete_old($id) {
		$db = \Config\Database::connect();

		$db->transStart(); 
		
		$itemBuilder = $db->table('service_booking_items');
		$items = $itemBuilder->where('booking_id', $id)->get()->getResult();
		foreach ($items as $item) {
			$inspectionBuilder = $db->table('service_booking_item_inspections');
			$inspectionBuilder->where('booking_item_id', $item->id)->delete();
		}

		$itemBuilder->where('booking_id', $id)->delete();

		$bookingBuilder = $db->table('service_bookings');
		$bookingBuilder->where('id', $id)->delete();

		$db->transComplete();

		if ($db->transStatus() === FALSE)
		{
			return redirect()->back()->withInput()->with('errors', 'Gagal menghapus data booking');
		}

        return redirect()->to(base_url('/booking'))->with('success', 'Berhasil menghapus data booking');
	}

	public function find($owner_id) {

		$db = \Config\Database::connect();
		$uttpModel = new UserUutModel();
		$builder = $uttpModel
			->select('standard_uut.*, master_standard_types.uut_type, uut_owners.nama as owner, user_uuts.id map_id')
			->join('standard_uut', 'user_uuts.uut_id = standard_uut.id')
			->join('uut_owners', 'uut_owners.id = standard_uut.owner_id')
			->join('master_standard_types', 'master_standard_types.id = standard_uut.type_id')
			->join('master_laboratory', 'master_laboratory.id = master_standard_types.lab_id')
			->where('master_laboratory.is_active', true)
			->where('master_standard_types.is_active', true)
			->where('user_uuts.user_id',  session('user_id'))
			->where('owner_id', $owner_id);

		$q = $this->request->getGet('q');

		if ($q) {
			$escapedQuery = $db->escapeLikeString($q);
			$builder = $builder->groupStart()
				->like('LOWER(serial_no)', strtolower($escapedQuery))
				->orLike('LOWER(tool_brand)', strtolower($escapedQuery))
				->orLike('LOWER(tool_model)', strtolower($escapedQuery))
				->orLike('LOWER(tool_name)', strtolower($escapedQuery))
				->groupEnd();
		}

		$uttps = $builder
			->findAll();
			
		return $this->respond($uttps, 200);
	}

	public function get($id) {

		$uutModel = new UutModel();
		$uut = $uutModel->find($id);

		return $this->respond($uut, 200);
	}

	public function unit($type_id) {

		$db = \Config\Database::connect();

		$unitModel = new UutUnitModel();
		$builder = $unitModel
			->where('uut_type_id', $type_id);

		$units = $builder
			->findAll();

		return $this->respond($units, 200);
	}

	public function getType($id) {

		$uutTypeModel = new UutTypeModel();
		$uutType = $uutTypeModel->find($id);

		return $this->respond($uutType, 200);
	}

	public function findBySeries() {

		$db = \Config\Database::connect();
		$uutModel = new UutModel();
		$builder = $uutModel
			->where('type_id', $this->request->getPost('type_id'))
			->where('owner_id', $this->request->getPost('owner_id'))
			->where('serial_no', $this->request->getPost('q'));

		$uut = $builder
			->first();
		
		return $this->respond($uut, 200);
	}
}
