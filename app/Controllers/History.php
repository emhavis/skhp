<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\RequestUttpModel;
use App\Models\RequestUttpItemModel;
use App\Models\RequestUttpItemInspectionModel;
use App\Models\RequestUutModel;
use App\Models\HistoryUttpModel;

class History extends BaseController
{
	public function index() {
		/*
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			//->where('booking_no is null', null, false)
			->where('pic_id', session('user_id'))
			->findAll();
		*/

		$db = \Config\Database::connect();
		/*
		$bookings = $db->query(
			'select sb.*, sr.total_price, sr.status_id, null as payment_status_id, mrs.status ' . 
			'from service_bookings sb ' .
			'inner join service_requests sr on sb.id = sr.booking_id ' .
			'inner join master_request_status mrs on sr.status_id  = mrs.id ' .
			'where ' .
			'sb.pic_id = ' . session('user_id') .
			'union ' .
			'select sb.*, sr.total_price, sr.status_id, sr.payment_status_id, mrs.status from service_bookings sb ' .
			'inner join service_request_uttps sr on sb.id = sr.booking_id ' .
			'inner join master_request_status mrs on sr.status_id  = mrs.id ' .
			'where ' .
			'sb.pic_id = ' . session('user_id'))->getResult();
		*/
		// dd(session('user_id'));
		$bookings = $db->query(
			'select sb.id, 
			sb.booking_no, sru.no_register, sru.no_order, sru.jenis_layanan, sru.payment_status_id,
			srui.status_id, sou.stat_service_order, null as stat_warehouse, 
			mrs.status, mut.uut_type as tool_type 
			from service_request_items srui
			left join service_orders sou  on sou.service_request_item_id = srui.id
			inner join service_requests sru on srui.service_request_id = sru.id 
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			inner join standard_uut u on srui.uut_id = u.id 
			inner join master_standard_types mut on u.type_id = mut.id
			where
			sb.pic_id = '. session('user_id') .
			' and sru.status_id in (14, 16) ' .
			' union all ' .
			'select sb.id, 
			sb.booking_no, sru.no_register, srui.no_order, sru.jenis_layanan, sru.payment_status_id,
			srui.status_id, sou.stat_service_order, sou.stat_warehouse, 
			mrs.status, mut.uttp_type as tool_type 
			from service_request_uttp_items srui
			left join service_order_uttps sou  on sou.service_request_item_id = srui.id
			inner join service_request_uttps sru on srui.request_id = sru.id 
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			inner join uttps u on srui.uttp_id = u.id 
			inner join master_uttp_types mut on u.type_id = mut.id
			where
			sb.pic_id = '. session('user_id') .
			' and sru.status_id in (14, 16, 17, 25) ' 
		)->getResult();
			
		return view ('admin/history/index', [
			'bookings' => $bookings,
		]);
	}

	public function read($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();
		// dd($booking);
		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$request = $db->query('select sr.*, mrs.status from service_requests sr 
			inner join master_request_status mrs on sr.status_id = mrs.id
			where sr.booking_id = '. $booking->id)->getRow();
			
			$items = $db->query('select sri.*, ms.uut_type as uut_type,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in
				from service_request_items sri 
				inner join standard_uut u on sri.uut_id = u.id
				inner join master_standard_types ms on ms.id = u.type_id
				where sri.service_request_id = '. $request->id)->getResult();
				
			$inspections = $db->query('select srii.*,
				sip.inspection_type, sip.unit, mrs.status
				from service_request_item_inspections srii 
				inner join service_request_items sri on srii.service_request_item_id = sri.id 
				inner join uut_inspection_prices sip on srii.inspection_price_id = sip.id
				inner join master_request_status mrs on srii.status_id = mrs.id
				where sri.service_request_id = ' . $request->id)->getResult();
			// dd([$inspections,$items]);
			$orders = $db->query('select so.*
				from service_orders so
				where so.service_request_id = ' . $request->id)->getResult();

			$terms = $db->query('select sruk.*
				from service_request_standard_kajiulang sruk 
				where request_id = '. $request->id)->getResult();

				$histories = $db->query('select hu.*, mrs.status from history_uut hu
				inner join master_request_status mrs on hu.request_status_id = mrs.id
				where hu.request_id = ' .$request->id . ' order by hu.id desc')->getResult();
				
			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_request_standard_kajiulang sruk 
			where request_id = '. $request->id)->getRow();
			return view ('admin/history/read_uttp', [
						'booking' => $booking,
						'request' => $request,
						'items' => $items,
						'inspections' => $inspections,
						'orders' => $orders,
						'terms' => $terms,
						'check_terms' => $check,
						'histories' => $histories,
					]);
		} else {
			
			$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.booking_id = '. $booking->id)->getRow();
			
			$items = $db->query('select srui.*, 
				mut.uttp_type,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in
				from service_request_uttp_items srui 
				inner join uttps u on srui.uttp_id = u.id
				inner join master_uttp_types mut on u.type_id = mut.id 
				where srui.request_id = ' . $request->id)->getResult();

			$inspections = $db->query('select sruii.*,
				uip.inspection_type, uip.unit, mrs.status
				from service_request_uttp_item_inspections sruii 
				inner join service_request_uttp_items srui on sruii.request_item_id = srui.id 
				inner join uttp_inspection_prices uip on sruii.inspection_price_id = uip.id
				inner join master_request_status mrs on sruii.status_id = mrs.id
				where srui.request_id = ' . $request->id)->getResult();

			$orders = $db->query('select sou.*
				from service_order_uttps sou
				where sou.service_request_id = ' . $request->id)->getResult();

			$terms = $db->query('select sruk.*
				from service_request_uttp_kajiulang sruk 
				where request_id = '. $request->id)->getResult();

			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_request_uttp_kajiulang sruk 
				where request_id = '. $request->id)->getRow();

			$histories = $db->query('select hu.*, mrs.status from history_uttp hu
				inner join master_request_status mrs on hu.request_status_id = mrs.id
				where hu.request_id = ' .$request->id . ' order by hu.id desc')->getResult();

			return view ('admin/history/read_uttp', [
				'booking' => $booking,
				'request' => $request,
				'items' => $items,
				'inspections' => $inspections,
				'orders' => $orders,
				'terms' => $terms,
				'check_terms' => $check,
				'histories' => $histories,
			]);
		}
	}

	public function download($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_uttp_items")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function download_paymentref($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_uttps")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function download_uut($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_items")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function invoice($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/invoice/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function invoice_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/invoice/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function kuitansi($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		// dd($id);
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);
		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/kuitansi/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}
	public function kuitansi_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		// dd($id);
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);
		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/kuitansi/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function buktiorder($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/buktiorder/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}
	public function buktiorder_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/buktiorder/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}
}
