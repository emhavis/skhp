<?php

namespace App\Controllers;

use CodeIgniter\Database\Config;

class Home extends BaseController
{
	public function index()
	{
		$db = \Config\Database::connect();
		$articles = $db->query("select * from article_posts where published = true order by coalesce(priority_index, 1000000), created_at desc limit 5")->getResult();

		return view('public/home/index', [
			'articles' => $articles
		]);
	}

}
