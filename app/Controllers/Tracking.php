<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\RequestUttpModel;
use App\Models\RequestUttpItemModel;
use App\Models\RequestUttpItemInspectionModel;
use App\Models\RequestUutModel;
use App\Models\HistoryUttpModel;
use App\Models\HistoryUutModel;
use App\Models\RequestUutItemModel;
use App\Models\UttpOwnerModel;
use App\Models\UutOwnerModel;
use App\Models\UttpModel;
use App\Models\UutModel;

class Tracking extends BaseController
{
	public function index() {
		/*
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			//->where('booking_no is null', null, false)
			->where('pic_id', session('user_id'))
			->findAll();
		*/

		$db = \Config\Database::connect();
		/*
		$bookings = $db->query(
			'select sb.*, sr.total_price, sr.status_id, null as payment_status_id, mrs.status ' . 
			'from service_bookings sb ' .
			'inner join service_requests sr on sb.id = sr.booking_id ' .
			'inner join master_request_status mrs on sr.status_id  = mrs.id ' .
			'where ' .
			'sb.pic_id = ' . session('user_id') .
			'union ' .
			'select sb.*, sr.total_price, sr.status_id, sr.payment_status_id, mrs.status from service_bookings sb ' .
			'inner join service_request_uttps sr on sb.id = sr.booking_id ' .
			'inner join master_request_status mrs on sr.status_id  = mrs.id ' .
			'where ' .
			'sb.pic_id = ' . session('user_id'))->getResult();
		*/
		$bookings = $db->query(
			'select sb.id, 
			sb.booking_no, sru.no_register, srui.no_order, sru.jenis_layanan, sru.payment_status_id,
			srui.status_id, sou.stat_service_order, null as stat_warehouse, 
			mrs.status, mut.uut_type as tool_type, sru.payment_status_id, sru.spuh_payment_status_id
			from service_request_items srui
			left join service_orders sou  on sou.service_request_item_id = srui.id
			inner join service_requests sru on srui.service_request_id = sru.id 
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			inner join standard_uut u on srui.uut_id = u.id 
			inner join master_standard_types mut on u.type_id = mut.id
			where
			sb.pic_id = '. session('user_id') .
			' and sru.status_id not in (14, 16, 17) ' .
			' union all ' .
			'select sb.id, 
			sb.booking_no, sru.no_register, srui.no_order, sru.jenis_layanan, sru.payment_status_id,
			srui.status_id, sou.stat_service_order, sou.stat_warehouse, 
			mrs.status, mut.uttp_type as tool_type, sru.payment_status_id, sru.spuh_payment_status_id
			from service_request_uttp_items srui
			left join service_order_uttps sou  on sou.service_request_item_id = srui.id
			inner join service_request_uttps sru on srui.request_id = sru.id 
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			inner join uttps u on srui.uttp_id = u.id 
			inner join master_uttp_types mut on u.type_id = mut.id
			where
			sb.pic_id = '. session('user_id') .
			' and srui.status_id not in (14, 16, 17, 25) '
		)->getResult();
			
		return view ('admin/trackings/index', [
			'bookings' => $bookings,
		]);
	}

	public function read($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();
		// dd($booking);
		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {

			$request = $db->query('select sr.* ,sri.status_id as status_id, mrs.status from service_requests sr 
				inner join service_request_items sri on sri.service_request_id = sr.id
				inner join master_request_status mrs on sr.status_id = mrs.id
				where sr.booking_id = '. $booking->id)->getRow();
			
			$items = $db->query('select sri.*, ms.uut_type as uut_type,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in,
				u.tool_media, u.tool_dayabaca, u.tool_dayabaca_unit,tool_name,
				u.class, u.jumlah
				from service_request_items sri 
				inner join standard_uut u on sri.uut_id = u.id
				inner join master_standard_types ms on ms.id = u.type_id
				where sri.service_request_id = '. $request->id)->getResult();

			$inspections = $db->query('select srii.*,
				sip.inspection_type, sip.unit, mrs.status
				from service_request_item_inspections srii 
				inner join service_request_items sri on srii.service_request_item_id = sri.id 
				inner join uut_inspection_prices sip on srii.inspection_price_id = sip.id
				inner join master_request_status mrs on srii.status_id = mrs.id
				where sri.service_request_id = ' . $request->id)->getResult();

			$orders = $db->query('select so.*
				from service_orders so
				where so.service_request_id = ' . $request->id)->getResult();

			$terms = $db->query('select sruk.*
				from service_request_standard_kajiulang sruk 
				where request_id = '. $request->id)->getResult();

			$histories = $db->query('select hu.*, mrs.status from history_uut hu
				inner join master_request_status mrs on hu.request_status_id = mrs.id
				where hu.request_id = ' .$request->id . ' order by hu.id desc')->getResult();
			
			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_request_standard_kajiulang sruk 
			where request_id = '. $request->id)->getRow();

			// dd([$id,$histories,$items,$orders,$request]);

			return view ('admin/trackings/read_uttp', [
						'booking' => $booking,
						'request' => $request,
						'items' => $items,
						'inspections' => $inspections,
						'orders' => $orders,
						'terms' => $terms,
						'check_terms' => $check,
						'histories' => $histories,
					]);
		} else {
			
			$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.booking_id = '. $booking->id)->getRow();
			
			$items = $db->query('select srui.*, 
				mut.uttp_type,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit, u.tool_capacity_min,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in, u.tool_media,
				mrs.status
				from service_request_uttp_items srui 
				inner join uttps u on srui.uttp_id = u.id
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_request_status mrs on srui.status_id = mrs.id
				where srui.request_id = ' . $request->id)->getResult();

			$inspections = $db->query('select sruii.*,
				uip.inspection_type, uip.unit 
				from service_request_uttp_item_inspections sruii 
				inner join service_request_uttp_items srui on sruii.request_item_id = srui.id 
				inner join uttp_inspection_prices uip on sruii.inspection_price_id = uip.id
				where srui.request_id = ' . $request->id)->getResult();

			$orders = $db->query('select sou.*
				from service_order_uttps sou
				where sou.service_request_id = ' . $request->id)->getResult();

			$terms = $db->query('select sruk.*
				from service_request_uttp_kajiulang sruk 
				where request_id = '. $request->id)->getResult();

			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_request_uttp_kajiulang sruk 
				where request_id = '. $request->id)->getRow();

			$histories = $db->query('select hu.*, mrs.status from history_uttp hu
				inner join master_request_status mrs on hu.request_status_id = mrs.id
				where hu.request_id = ' .$request->id . ' order by hu.id desc')->getResult();

			$provinces = $db->query('select * from master_provinsi')->getResult();
			$cities = $db->query(
				'select k.*, p.nama as provinsi 
				from master_kabupatenkota k inner 
				join master_provinsi p on k.provinsi_id = p.id')->getResult();
			
			$negaras = $db->query('select * from master_negara')->getResult();

			return view ('admin/trackings/read_uttp', [
				'booking' => $booking,
				'request' => $request,
				'items' => $items,
				'inspections' => $inspections,
				'orders' => $orders,
				'terms' => $terms,
				'check_terms' => $check,
				'histories' => $histories,
				'provinces' => $provinces,
				'cities' => $cities,
				'negaras' => $negaras,
			]);
		}
	}

	public function edit($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();
		// dd($booking);
		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$request = $db->query('select sr.*, mrs.status from service_requests sr 
				inner join master_request_status mrs on sr.status_id = mrs.id
				where sr.booking_id = '. $booking->id)->getRow();
			
			$items = $db->query('select sri.*, ms.uut_type as uut_type,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in,
				u.tool_media, u.tool_dayabaca, u.tool_dayabaca_unit,tool_name,
				u.class, u.jumlah
				from service_request_items sri 
				inner join standard_uut u on sri.uut_id = u.id
				inner join master_standard_types ms on ms.id = u.type_id
				where sri.service_request_id = '. $request->id)->getResult();

			$inspections = $db->query('select srii.*,
				sip.inspection_type, sip.unit, mrs.status
				from service_request_item_inspections srii 
				inner join service_request_items sri on srii.service_request_item_id = sri.id 
				inner join uut_inspection_prices sip on srii.inspection_price_id = sip.id
				inner join master_request_status mrs on srii.status_id = mrs.id
				where sri.service_request_id = ' . $request->id)->getResult();

			$orders = $db->query('select so.*
				from service_orders so
				where so.service_request_id = ' . $request->id)->getResult();

			$terms = $db->query('select sruk.*
				from service_request_standard_kajiulang sruk 
				where request_id = '. $request->id)->getResult();

			$histories = $db->query('select hu.*, mrs.status from history_uut hu
				inner join master_request_status mrs on hu.request_status_id = mrs.id
				where hu.request_id = ' .$request->id . ' order by hu.id desc')->getResult();
			
			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_request_standard_kajiulang sruk 
			where request_id = '. $request->id)->getRow();

			// dd([$id,$histories,$items,$orders,$request]);

			return view ('admin/trackings/read_uttp', [
						'booking' => $booking,
						'request' => $request,
						'items' => $items,
						'inspections' => $inspections,
						'orders' => $orders,
						'terms' => $terms,
						'check_terms' => $check,
						'histories' => $histories,
					]);
		} else {
			
			$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.booking_id = '. $booking->id)->getRow();
			
			$items = $db->query('select srui.*, 
				mut.uttp_type,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit, u.tool_capacity_min,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in, u.tool_media,
				mrs.status
				from service_request_uttp_items srui 
				inner join uttps u on srui.uttp_id = u.id
				inner join master_uttp_types mut on u.type_id = mut.id 
				inner join master_request_status mrs on srui.status_id = mrs.id
				where srui.request_id = ' . $request->id)->getResult();

			$inspections = $db->query('select sruii.*,
				uip.inspection_type, uip.unit 
				from service_request_uttp_item_inspections sruii 
				inner join service_request_uttp_items srui on sruii.request_item_id = srui.id 
				inner join uttp_inspection_prices uip on sruii.inspection_price_id = uip.id
				where srui.request_id = ' . $request->id)->getResult();

			$orders = $db->query('select sou.*
				from service_order_uttps sou
				where sou.service_request_id = ' . $request->id)->getResult();

			$terms = $db->query('select sruk.*
				from service_request_uttp_kajiulang sruk 
				where request_id = '. $request->id)->getResult();

			$check = $db->query('select (legalitas and metode and klausa) all_ok from service_request_uttp_kajiulang sruk 
				where request_id = '. $request->id)->getRow();

			$histories = $db->query('select hu.*, mrs.status from history_uttp hu
				inner join master_request_status mrs on hu.request_status_id = mrs.id
				where hu.request_id = ' .$request->id . ' order by hu.id desc')->getResult();

			$provinces = $db->query('select * from master_provinsi')->getResult();
			$cities = $db->query(
				'select k.*, p.nama as provinsi 
				from master_kabupatenkota k inner 
				join master_provinsi p on k.provinsi_id = p.id')->getResult();

			if ($this->request->getMethod() === 'get'){	
				return view ('admin/trackings/edit_uttp', [
					'booking' => $booking,
					'request' => $request,
					'items' => $items,
					'inspections' => $inspections,
					'orders' => $orders,
					'terms' => $terms,
					'check_terms' => $check,
					'histories' => $histories,
					'provinces' => $provinces,
					'cities' => $cities,
				]);
			}

			if ($booking->lokasi_pengujian == 'luar') {
				$reqModel = new RequestUttpModel();
				$req = $reqModel->find($request->id);

				$booking->no_surat_permohonan = $this->request->getPost('no_surat_permohonan');
				$req->no_surat_permohonan = $booking->no_surat_permohonan;

				$file_surat_permohonan = $this->request->getFile('file_surat_permohonan');
				if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() != '') { $path_surat_permohonan = $file_surat_permohonan->store(); }
		
				if (isset($file_surat_permohonan) && $file_surat_permohonan->getName() ) {
					$booking->file_surat_permohonan = isset($file_surat_permohonan) ? $file_surat_permohonan->getClientName() : null;
					$req->path_surat_permohonan = isset($path_surat_permohonan) ? $path_surat_permohonan : null;

					$request->file_surat_permohonan = $booking->file_surat_permohonan;
					$req->path_surat_permohonan = $booking->path_surat_permohonan;
				}

				$booking->tgl_surat_permohonan = date("Y-m-d", strtotime($this->request->getPost('tgl_surat_permohonan')));
				$req->tgl_surat_permohonan = $booking->tgl_surat_permohonan;

				if (! ($bookingModel->save($booking) && $reqModel->save($req)) ) {
					return redirect()->back()->withInput()->with('errors', $bookings->errors());
				}
				
			}

			return redirect()->to(base_url('/tracking/read/' . $id))->with('success', 'Berhasil menyimpan pendaftaran');
		}
	}


	public function edit_pemilik($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();
		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			if ($this->request->getMethod() === 'get'){ 
				$request = $db->query('select sr.*, mrs.status from service_requests sr 
				inner join master_request_status mrs on sr.status_id = mrs.id
				where sr.booking_id = '. $booking->id)->getRow();
				
				return view ('admin/trackings/edit_pemilik_uttp', [
							'booking' => $booking,
							'request' => $request,
						]);
			}

			$reqModel = new RequestUutModel();
			$request = $reqModel->where('booking_id', $id)->first();

			$ownerModel = new UutOwnerModel();
			$owner = $ownerModel->find($request->uut_owner_id);
			
			$owner->nama = $this->request->getPost('label_sertifikat');
			$owner->alamat = $this->request->getPost('addr_sertifikat');

			if (! $ownerModel->save($owner) ) {
				return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
			}

			$request->label_sertifikat = $this->request->getPost('label_sertifikat');
			$request->addr_sertifikat = $this->request->getPost('addr_sertifikat');

			if (! $reqModel->save($request) ) {
				return redirect()->back()->withInput()->with('errors', $reqModel->errors());
			}

			return redirect()->to(base_url('tracking/read/'.$id));
		} else {
			if ($this->request->getMethod() === 'get'){
				$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
				inner join master_request_status mrs on sru.status_id = mrs.id
				where sru.booking_id = '. $booking->id)->getRow();
			

				return view ('admin/trackings/edit_pemilik_uttp', [
					'booking' => $booking,
					'request' => $request,
				]);
			}

			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();

			$uttpOwnerModel = new UttpOwnerModel();
			$uttpOwner = $uttpOwnerModel->find($request->uttp_owner_id);
			
			$uttpOwner->nama = $this->request->getPost('label_sertifikat');
			$uttpOwner->alamat = $this->request->getPost('addr_sertifikat');

			if (! $uttpOwnerModel->save($uttpOwner) ) {
				return redirect()->back()->withInput()->with('errors', $uttpOwnerModel->errors());
			}

			$request->label_sertifikat = $this->request->getPost('label_sertifikat');
			$request->addr_sertifikat = $this->request->getPost('addr_sertifikat');

			if (! $requestModel->save($request) ) {
				return redirect()->back()->withInput()->with('errors', $requestModel->errors());
			}

			return redirect()->to(base_url('tracking/read/'.$id));
		}
	}

	public function edit_alat($id, $idItem) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();
		// dd($booking);
		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$request = $db->query('select sr.*, mrs.status from service_requests sr 
				inner join master_request_status mrs on sr.status_id = mrs.id
				where sr.booking_id = '. $booking->id)->getRow();
				// dd($request->status_id);
			$item = $db->query('select sri.*, ms.uut_type as uut_type,u.type_id,
				u.tool_media,tool_dayabaca,u.tool_made_in_id,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in,u.jumlah,u.class,
				u.tool_dayabaca,tool_dayabaca_unit, u.tool_name
				from service_request_items sri 
				inner join standard_uut u on sri.uut_id = u.id
				inner join master_standard_types ms on ms.id = u.type_id
				where sri.service_request_id = '. $request->id .
				'and sri.id = ' .$idItem)->getRow();
				// dd($request);

			$negaras = $db->table('master_negara')
				->get()
				->getResult();

			if ($this->request->getMethod() === 'get'){
				return view ('admin/trackings/edit_alat_uttp', [
					'booking' => $booking,
					'request' => $request,
					'item' => $item,
					'negaras' => $negaras,
				]);
			}

			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();

			$itemModel = new RequestUutItemModel();
			$item = $itemModel->find($idItem);

			$uutModel = new UutModel();
			$uut = $uutModel->find($item->uut_id);

			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
				->getRow();
			// dd($this->request->getPost());

			$uut->type_id = $this->request->getPost('type_id');
			$uut->serial_no = $this->request->getPost('serial_no');
			$uut->tool_brand = $this->request->getPost('tool_brand');
			$uut->tool_model = $this->request->getPost('tool_model');
			$uut->tool_type = $this->request->getPost('tool_type');
			$uut->tool_capacity = $this->request->getPost('tool_capacity');
			$uut->tool_capacity_unit = $this->request->getPost('tool_capacity_unit_uut');
			$uut->tool_factory = $this->request->getPost('tool_factory');
			$uut->tool_factory_address = $this->request->getPost('tool_factory_address');
			//$uttp->tool_made_in = $this->request->getPost('tool_made_in');
			$uut->tool_made_in = $negara->nama_negara;
			$uut->tool_made_in_id =  $negara->id;
			//$uttp->owner_id = $this->request->getPost('owner_id');
			$uut->tool_media = $this->request->getPost('tool_media');
			$uut->tool_name = $this->request->getPost('tool_name');
			$uut->tool_capacity_min = $this->request->getPost('tool_capacity_min');
			$uut->tool_dayabaca = $this->request->getPost('tool_dayabaca');
			$uut->tool_dayabaca_unit = $this->request->getPost('tool_dayabaca_unit');
			
			if (! $uutModel->save($uut)) {
				dd($uut);
				return redirect()->back()->withInput()->with('errors', $uutModel->errors());
			}

			return redirect()->to(base_url('tracking/read/'.$id));

			
			
		} else {

			if ($this->request->getMethod() === 'get'){
			
				$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
					inner join master_request_status mrs on sru.status_id = mrs.id
					where sru.booking_id = '. $booking->id)->getRow();
				
				$item = $db->query('select srui.*, 
					mut.uttp_type, u.type_id,
					u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_min, u.tool_capacity_unit,
					u.tool_factory, u.tool_factory_address,
					u.tool_model, u.tool_type , u.tool_made_in, u.tool_media, u.tool_made_in_id
					from service_request_uttp_items srui 
					inner join uttps u on srui.uttp_id = u.id
					inner join master_uttp_types mut on u.type_id = mut.id 
					where srui.request_id = ' . $request->id . 
					' and srui.id = ' . $idItem)->getRow();

				$negaras = $db->table('master_negara')
					->get()
					->getResult();
			
				return view ('admin/trackings/edit_alat_uttp', [
					'booking' => $booking,
					'request' => $request,
					'item' => $item,
					'negaras' => $negaras,
				]);
			}

			// upload
			$file_type_approval_certificate = $this->request->getFile('file_type_approval_certificate');
			$file_last_certificate = $this->request->getFile('file_last_certificate');
			$file_application_letter = $this->request->getFile('file_application_letter');
			$file_calibration_manual = $this->request->getFile('file_calibration_manual');
			$file_manual_book = $this->request->getFile('file_manual_book');

			if (isset($file_type_approval_certificate) && $file_type_approval_certificate->isValid()) { $path_type_approval_certificate = $file_type_approval_certificate->store(); }
			if (isset($file_last_certificate) && $file_last_certificate->isValid()) { $path_last_certificate = $file_last_certificate->store(); }
			if (isset($file_application_letter) && $file_application_letter->isValid()) { $path_application_letter = $file_application_letter->store(); }
			if (isset($file_calibration_manual)  && $file_calibration_manual->isValid()) { $path_calibration_manual = $file_calibration_manual->store(); }
			if (isset($file_manual_book)  && $file_manual_book->isValid()) { $path_manual_book = $file_manual_book->store(); }
			// end upload

			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();

			$itemModel = new RequestUttpItemModel();
			$item = $itemModel->find($idItem);

			$uttpModel = new UttpModel();
			$uttp = $uttpModel->find($item->uttp_id);

			$negara = $db->table('master_negara')
				->where('id', $this->request->getPost('tool_made_in_id'))
				->get()
				->getRow();

			$uttp->type_id = $this->request->getPost('type_id');
			$uttp->serial_no = $this->request->getPost('serial_no');
			$uttp->tool_brand = $this->request->getPost('tool_brand');
			$uttp->tool_model = $this->request->getPost('tool_model');
			$uttp->tool_type = $this->request->getPost('tool_type');
			$uttp->tool_capacity = $this->request->getPost('tool_capacity');
			$uttp->tool_capacity_unit = $this->request->getPost('tool_capacity_unit');
			$uttp->tool_factory = $this->request->getPost('tool_factory');
			$uttp->tool_factory_address = $this->request->getPost('tool_factory_address');
			//$uttp->tool_made_in = $this->request->getPost('tool_made_in');
			$uttp->tool_made_in = $negara->nama_negara;
			$uttp->tool_made_in_id =  $negara->id;
			//$uttp->owner_id = $this->request->getPost('owner_id');
			$uttp->tool_media = $this->request->getPost('tool_media');
			$uttp->tool_name = $this->request->getPost('tool_name');
			$uttp->tool_capacity_min = $this->request->getPost('tool_capacity_min');



			$item->reference_no = $this->request->getPost('reference_no');
			$item->reference_date = date("Y-m-d", strtotime($this->request->getPost('reference_date')));


			$item->file_type_approval_certificate = isset($file_type_approval_certificate) ? $file_type_approval_certificate->getClientName() : null;
			$item->file_last_certificate = isset($file_last_certificate) ? $file_last_certificate->getClientName() : null;
			$item->file_application_letter = isset($file_application_letter) ? $file_application_letter->getClientName() : null;
			$item->file_calibration_manual = isset($file_calibration_manual) ? $file_calibration_manual->getClientName() : null;
			$item->file_manual_book = isset($file_manual_book) ? $file_manual_book->getClientName() : null;

			$item->path_type_approval_certificate = isset($path_type_approval_certificate) ? $path_type_approval_certificate : null;
			$item->path_last_certificate = isset($path_last_certificate) ? $path_last_certificate : null;
			$item->path_application_letter = isset($path_application_letter) ? $path_application_letter : null;
			$item->path_calibration_manual = isset($path_calibration_manual) ? $path_calibration_manual : null;
			$item->path_manual_book = isset($path_manual_book) ? $path_manual_book : null;
			
			if (! $uttpModel->save($uttp)) {
				return redirect()->back()->withInput()->with('errors', $uttpModel->errors());
			}

			return redirect()->to(base_url('tracking/read/'.$id));
		}
	}

	public function cancel($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);
		// dd($booking);
		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('tracking/read/'.$id));
		}

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = 17;

			$requestModel->save($request);

			$historyModel = new HistoryUutModel();

			$itemModel = new RequestUutItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil membatalkan data pendaftaran');
		} else {
			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = 17;

			$requestModel->save($request);

			$historyModel = new HistoryUttpModel();

			$itemModel = new RequestUttpItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = 17;
				$itemModel->save($item);

				$history = [
					'request_status_id' => 17,
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil membatalkan data pendaftaran');
		}
		 
	}

	public function confirm($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);
		// dd($booking);
		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('tracking/read/'.$id));
		}

		$db = \Config\Database::connect();
		
		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = $this->request->getPost('status_id');

			$requestModel->save($request);

			$historyModel = new HistoryUutModel();

			$itemModel = new RequestUutItemModel();
			$items = $itemModel->where('service_request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil mengkonfirmasi data pendaftaran');
		} else {
			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = $this->request->getPost('status_id');

			if ($request->lokasi_pengujian == 'luar') {
				$rates = $db->query("select
						srui.id,
						srui.location_kabkot_id,
						srui.location_prov_id,
						s.luar,
						s.dalam,
						case
							when srui.location_kabkot_id = " .getenv("KABKOT_DALAM"). " then s.dalam
							else s.luar
						end rate
					from
						service_request_uttp_items srui
					inner join master_provinsi mp on
						srui.location_prov_id = mp.id
					inner join sbm s on
						mp.id = s.provinsi_id
					where
						srui.request_id = " .$request->id. "
					order by
						case
							when srui.location_kabkot_id = " .getenv("KABKOT_DALAM"). " then s.dalam
							else s.luar
						end desc,
						srui.location_prov_id asc,
						srui.id asc")
					->getResult();

				$request->inspection_prov_id = $rates[0]->location_prov_id;
				$request->inspection_kabkot_id = $rates[0]->location_kabkot_id;
			}

			$requestModel->save($request);

			$historyModel = new HistoryUttpModel();

			$itemModel = new RequestUttpItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}


			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil mengkonfirmasi data pendaftaran');
		}
		 
	}

	public function confirmschedule($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('/tracking/read/'.$id));
		}

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = $this->request->getPost('status_id');

			$requestModel->save($request);

			$historyModel = new HistoryUutModel();

			$itemModel = new RequestUutItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('/tracking'))->with('success', 'Berhasil mengkonfirmasi data pendaftaran');
		} else {
			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = $this->request->getPost('status_id');

			$requestModel->save($request);

			$historyModel = new HistoryUttpModel();

			$itemModel = new RequestUttpItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('/tracking'))->with('success', 'Berhasil mengkonfirmasi data pendaftaran');
		}
		 
	}

	public function download($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_uttp_items")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function download_paymentref($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_uttps")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		// dd([$item[$path],$path,$file]);
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function download_uut($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_items")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		
		return $this->response->download(WRITEPATH.'uploads/'.$item[$path], null)->setFileName($item[$file]);
	}
	public function download_paymentref_uut($field, $id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_requests")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_' . $field;
		$file = 'file_' . $field;
		
		return $this->response->download(WRITEPATH.'uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function pay($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();

		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('tracking/read/'.$id));
		}

		

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();
			if ($request->total_price > 0 && $request->payment_date == null) {
				$request->payment_code = $this->request->getPost('payment_code');
				$request->payment_date = date("Y-m-d", strtotime($this->request->getPost('payment_date')));
				$request->payment_valid_date = null;
			}
			
			/*
			if ($request->status_id == "19") {
				$request->payment_status_id = 7;
				$request->status_id = 20;
			} else if ($request->status_id == "15") {
				$request->payment_status_id = 7;
			
			} else {			
				$request->payment_status_id = 7;
				$request->status_id = 7;
			}
			*/

			$file_payment_ref = $this->request->getFile('file_payment_ref');
			if (isset($file_payment_ref)) { $path_payment_ref = $file_payment_ref->store(); }
			
			$request->file_payment_ref = isset($file_payment_ref) ? $file_payment_ref->getClientName() : null;
			$request->path_payment_ref = isset($path_payment_ref) ? $path_payment_ref : null;
			
			$request->an_kuitansi = $this->request->getPost('an_kuitansi');

			$file_spuh_payment_ref = $this->request->getFile('file_spuh_payment_ref');
			if (isset($file_spuh_payment_ref)) { $path_spuh_payment_ref = $file_spuh_payment_ref->store(); }
			
			$request->file_spuh_payment_ref = isset($file_spuh_payment_ref) ? $file_spuh_payment_ref->getClientName() : null;
			$request->path_spuh_payment_ref = isset($path_spuh_payment_ref) ? $path_spuh_payment_ref : null;

			$request->payment_status_id = 7;
			if ($request->lokasi_pengujian == 'dalam') {
				$request->status_id = 7;
			}
			
			$requestModel->save($request);
			$historyModel = new HistoryUutModel();

			$itemModel = new RequestUutItemModel();
			$items = $itemModel->where('service_request_id', $request->id)->findAll();
			foreach($items as $item) {
				
				$item->status_id = $request->status_id;
				$itemModel->save($item);

				$history = [
					'request_status_id' => $request->status_id,
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil menyimpan data pembayaran');
		} else {
			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();
			if ($request->total_price > 0 && $request->payment_date == null) {
				$request->payment_code = $this->request->getPost('payment_code');
				$request->payment_date = date("Y-m-d", strtotime($this->request->getPost('payment_date')));
				$request->payment_valid_date = null;
			}
			if ($request->lokasi_pengujian == 'luar' && $request->spuh_payment_date == null) {
				//$request->spuh_payment_date = date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date')));
				//$request->spuh_payment_valid_date = null;
			}
			if ($request->lokasi_pengujian == 'luar') {

				/*
				$db->table("service_request_uttp_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->where('payment_date is null')
					->set('payment_date', date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date'))))
					->update();

				$db->table("service_request_uttp_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->where('act_price is not null')
					->where('act_price > price')
					->where('act_payment_date is null')
					->set('act_payment_date', date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date'))))
					->update();
					*/
			}
			/*
			if ($request->status_id == "19") {
				$request->payment_status_id = 7;
				$request->status_id = 20;
			} else if ($request->status_id == "15") {
				$request->payment_status_id = 7;
			} else {			
				$request->payment_status_id = 7;
				$request->status_id = 7;
			}
			*/

			$file_payment_ref = $this->request->getFile('file_payment_ref');
			if (isset($file_payment_ref)) { $path_payment_ref = $file_payment_ref->store(); }
			
			$request->file_payment_ref = isset($file_payment_ref) ? $file_payment_ref->getClientName() : null;
			$request->path_payment_ref = isset($path_payment_ref) ? $path_payment_ref : null;
			
			$request->an_kuitansi = $this->request->getPost('an_kuitansi');

			$file_spuh_payment_ref = $this->request->getFile('file_spuh_payment_ref');
			if (isset($file_spuh_payment_ref)) { $path_spuh_payment_ref = $file_spuh_payment_ref->store(); }
			
			$request->file_spuh_payment_ref = isset($file_spuh_payment_ref) ? $file_spuh_payment_ref->getClientName() : null;
			$request->path_spuh_payment_ref = isset($path_spuh_payment_ref) ? $path_spuh_payment_ref : null;

			$request->payment_status_id = 7;
			if ($request->lokasi_pengujian == 'dalam') {
				$request->status_id = 7;
			}

			$requestModel->save($request);

			$historyModel = new HistoryUttpModel();

			$itemModel = new RequestUttpItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $request->status_id;
				$itemModel->save($item);

				$history = [
					'request_status_id' => $request->status_id,
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'payment_status_id' => 7,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil menyimpan data pembayaran');
		}
		 
	}

	public function paytuhp($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		$db = \Config\Database::connect();

		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('tracking/read/'.$id));
		}

		$rules = [
			'spuh_payment_date' => ['required'],
			'file_spuh_payment_ref' => ['uploaded[file_spuh_payment_ref]', 'mime_in[file_spuh_payment_ref,application/pdf,image/png,image/gif,image/jpeg]'],
		];
		$error_msg = [
			'spuh_payment_date' => [
				'required' => 'Tanggal pembayaran wajib diisi'
			],
			'file_spuh_payment_ref' => [
					'uploaded' => 'Bukti pembayaran wajib diupload',
					'mime_in' => 'Bukti pembayaran wajib dalam format PDF atau image (png, gif, jpeg)'
			]
		];

		if (! $this->validate($rules, $error_msg)) {
			//dd([$rules, $error_msg, $this->validator->getErrors()]);
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();
			/*
			if ($request->total_price > 0 && $request->payment_date == null) {
				$request->payment_code = $this->request->getPost('payment_code');
				$request->payment_date = date("Y-m-d", strtotime($this->request->getPost('payment_date')));
			}
			*/
			if ($request->lokasi_pengujian == 'luar' && $request->spuh_payment_date == null) {
				$request->spuh_payment_date = date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date')));
				$request->spuh_payment_valid_date = null;
			}
			if ($request->lokasi_pengujian == 'luar') {

				$db->table("service_request_uut_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->where('payment_date is null')
					->set('payment_date', date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date'))))
					->update();

				$db->table("service_request_uut_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->where('act_price is not null')
					->where('act_price > price')
					->where('act_payment_date is null')
					->set('act_payment_date', date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date'))))
					->update();
			}
			/*
			if ($request->status_id == "19") {
				$request->payment_status_id = 7;
				$request->status_id = 20;
			} else if ($request->status_id == "15") {
				$request->payment_status_id = 7;
			
			} else {			
				$request->payment_status_id = 7;
				$request->status_id = 7;
			}
			*/
			
			$file_payment_ref = $this->request->getFile('file_payment_ref');
			if (isset($file_payment_ref)) { $path_payment_ref = $file_payment_ref->store(); }
			
			$request->file_payment_ref = isset($file_payment_ref) ? $file_payment_ref->getClientName() : null;
			$request->path_payment_ref = isset($path_payment_ref) ? $path_payment_ref : null;
			
			$request->an_kuitansi = $this->request->getPost('an_kuitansi');

			$file_spuh_payment_ref = $this->request->getFile('file_spuh_payment_ref');
			if (isset($file_spuh_payment_ref)) { $path_spuh_payment_ref = $file_spuh_payment_ref->store(); }
			
			$request->file_spuh_payment_ref = isset($file_spuh_payment_ref) ? $file_spuh_payment_ref->getClientName() : null;
			$request->path_spuh_payment_ref = isset($path_spuh_payment_ref) ? $path_spuh_payment_ref : null;

			$request->spuh_payment_status_id = 7;
			
			$requestModel->save($request);

			$historyModel = new HistoryUutModel();

			$itemModel = new RequestUutItemModel();
			$items = $itemModel->where('service_request_id', $request->id)->findAll();
			foreach($items as $item) {
				
				$item->status_id = $request->status_id;
				$itemModel->save($item);

				$history = [
					'request_status_id' => $request->status_id,
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'spuh_payment_status_id' => 7,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil menyimpan data pembayaran');
		} else {
			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();
			/*
			if ($request->total_price > 0 && $request->payment_date == null) {
				$request->payment_code = $this->request->getPost('payment_code');
				$request->payment_date = date("Y-m-d", strtotime($this->request->getPost('payment_date')));
				$request->payment_valid_date = null;
			}
			*/
			if ($request->lokasi_pengujian == 'luar' && $request->spuh_payment_date == null) {
				$request->spuh_payment_date = date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date')));
				$request->spuh_payment_valid_date = null;
			}
			if ($request->lokasi_pengujian == 'luar') {

				$db->table("service_request_uttp_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->where('payment_date is null')
					->set('payment_date', date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date'))))
					->update();

				$db->table("service_request_uttp_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->where('act_price is not null')
					->where('act_price > price')
					->where('act_payment_date is null')
					->set('act_payment_date', date("Y-m-d", strtotime($this->request->getPost('spuh_payment_date'))))
					->update();
			}
			/*
			if ($request->status_id == "19") {
				$request->payment_status_id = 7;
				$request->status_id = 20;
			} else if ($request->status_id == "15") {
				$request->payment_status_id = 7;
			} else {			
				$request->payment_status_id = 7;
				$request->status_id = 7;
			}
			*/

			$file_payment_ref = $this->request->getFile('file_payment_ref');
			if (isset($file_payment_ref)) { $path_payment_ref = $file_payment_ref->store(); }
			
			$request->file_payment_ref = isset($file_payment_ref) ? $file_payment_ref->getClientName() : null;
			$request->path_payment_ref = isset($path_payment_ref) ? $path_payment_ref : null;
			
			$request->an_kuitansi = $this->request->getPost('an_kuitansi');

			$file_spuh_payment_ref = $this->request->getFile('file_spuh_payment_ref');
			if (isset($file_spuh_payment_ref)) { $path_spuh_payment_ref = $file_spuh_payment_ref->store(); }
			
			$request->file_spuh_payment_ref = isset($file_spuh_payment_ref) ? $file_spuh_payment_ref->getClientName() : null;
			$request->path_spuh_payment_ref = isset($path_spuh_payment_ref) ? $path_spuh_payment_ref : null;

			$request->spuh_payment_status_id = 7;

			$requestModel->save($request);

			if ($request->lokasi_pengujian == 'luar') {

				$db->table("service_request_uttp_insitu_doc")
					->where('id', $request->spuh_doc_id)
					->set('file_bukti_bayar', $request->file_spuh_payment_ref)
					->set('path_bukti_bayar', $request->path_spuh_payment_ref)
					->update();

			}

			$historyModel = new HistoryUttpModel();

			$itemModel = new RequestUttpItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $request->status_id;
				$itemModel->save($item);

				$history = [
					'request_status_id' => $request->status_id,
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'spuh_payment_status_id' => 7,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('tracking'))->with('success', 'Berhasil menyimpan data pembayaran');
		}
		 
	}

	public function invoice($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/invoice/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function invoicetuhp($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/invoicetuhp/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function invoice_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/invoice/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function invoicetuhp_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/invoicetuhp/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function kuitansi($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		// dd($id);
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);
		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/kuitansi/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}
	public function kuitansi_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		// dd($id);
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);
		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/kuitansi/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function buktiorder($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/buktiorder/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}
	public function buktiorder_uut($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/buktiorder/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	private function sendEmail($title, $body)
	{
		$client = \Config\Services::curlrequest();
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			$email = $client->request('POST', config('App')->siksURL ."mailer/send", [
				'form_params' => [
					'token' => $token,
					'customer_id' => session('user_id'),
					'type' => 'generic',
					'title' => $title,
					'body' => $body,
				]
			]); 
			return $email;
		}

		return false;
	}

	public function surattugas($id) {
		$db = \Config\Database::connect();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");
		// dd($id);
		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);
		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/surattugas/' . $id .'/'. $token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function daftarulang($id) {
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);

		if ($this->request->getMethod() === 'get'){
			return redirect()->to(base_url('/tracking/read/'.$id));
		}

		if ($booking->service_type_id == 1 || $booking->service_type_id == 2) {
			$requestModel = new RequestUutModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = 1;
			$requestModel->save($request);

			$historyModel = new HistoryUutModel();

			$itemModel = new RequestUutItemModel();
			$items = $itemModel->where('service_request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('/tracking'))->with('success', 'Berhasil mengkonfirmasi data pendaftaran');
		} else {
			$requestModel = new RequestUttpModel();
			$request = $requestModel->where('booking_id', $id)->first();
			$request->status_id = 1;

			$requestModel->save($request);

			$historyModel = new HistoryUttpModel();

			$itemModel = new RequestUttpItemModel();
			$items = $itemModel->where('request_id', $request->id)->findAll();
			foreach($items as $item) {
				$item->status_id = $this->request->getPost('status_id');
				$itemModel->save($item);

				$history = [
					'request_status_id' => $this->request->getPost('status_id'),
					'request_id' => $request->id,
					'request_item_id' => $item->id,
					'user_customer_id' => session('user_id'),
				];
				$historyModel->save($history);
			}

			return redirect()->to(base_url('/tracking'))->with('success', 'Berhasil mengirim ulang pendaftaran');
		}
		 
	}

	public function download_permohonan($id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_request_uttps")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_surat_permohonan';
		$file = 'file_surat_permohonan';
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}

	public function download_permohonan_uut($id) {
		$db = \Config\Database::connect();
		$item = $db->table("service_requests")->where('id', $id)->get()->getRowArray();
		
		$path = 'path_surat_permohonan';
		$file = 'file_surat_permohonan';
		
		return $this->response->download('uploads/'.$item[$path], null)->setFileName($item[$file]);
	}
}
