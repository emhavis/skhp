<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use App\Models\UttpOwnerModel;
use App\Models\UserUttpOwnerModel;
use App\Models\UserUutOwnerModel;
use App\Models\UutOwnerModel;

class Useruutowner extends BaseController
{
	use ResponseTrait;
	
	public function index()
	{
		$ownerModel = new UserUutOwnerModel();
		$owners = $ownerModel
			->select('uut_owners.*, master_kabupatenkota.nama kota, user_uut_owners.id map_id')
			->join('uut_owners', 'uut_owners.id = user_uut_owners.owner_id')
			->join('master_kabupatenkota', 'master_kabupatenkota.id = uut_owners.kota_id')
			->where('user_uut_owners.user_id',  session('user_id'))
			->findAll();
		$is_uml = session('user_uml_id');
		$customerLogin	= session('user_id');

		return view ('admin/useruutowners/index', [
			'owners' => $owners,
			'is_uml' => $is_uml,
			'customerLogin' => $customerLogin,
		]);
	}

	public function create() {
		$validation =  \Config\Services::validation();
		if ($this->request->getMethod() === 'get'){
			
			return view('admin/useruutowners/create');
		}

		$db = \Config\Database::connect();

		$mapModel = new UserUutOwnerModel(); 

		$owner_id = $this->request->getPost('owner_id');
		if ($owner_id != null) {
			$count= $mapModel
			->where('owner_id', $owner_id)
			->where('user_id', session('user_id'))
			->countAllResults();
			if ($count > 0) {
				return redirect()->back()->withInput()->with('errors', ['owner_id' => 'Data pemilik sudah pernah ditambahkan']);
			}
		} else {
			$ownerModel = new UutOwnerModel(); 
			
			$owner = [
				'nama' 				=> $this->request->getPost('nama'),
				'alamat' 			=> $this->request->getPost('alamat'),
				'email' 			=> $this->request->getPost('email'),
				'kota_id' 			=> $this->request->getPost('kota_id'),
				'telepon' 			=> $this->request->getPost('telepon'),
				'nib' 				=> $this->request->getPost('nib'),
				'npwp' 				=> $this->request->getPost('npwp'),
				'penanggung_jawab'  => $this->request->getPost('penanggung_jawab'),
				'bentuk_usaha_id'  	=> ($this->request->getPost('bentuk_usaha_id') !== null && !empty($this->request->getPost('bentuk_usaha_id'))) ? $this->request->getPost('bentuk_usaha_id') : null,
			];

			if (! $ownerModel->save($owner)) {
				return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
			}

			$owner_id = $db->insertID();
		}
		
		$map = [
			'user_id'	=> session('user_id'),
			'owner_id'	=> $owner_id,
		];

		if (! $mapModel->save($map)) {
			return redirect()->back()->withInput()->with('errors', $mapModel->errors());
		}

        return redirect()->to(base_url('/useruutowner'))->with('success', 'Berhasil menyimpan pemilik baru');
	}

	public function edit($id) {
		$ownerModel = new UutOwnerModel();
		$owner = $ownerModel->find($id);

		if ($this->request->getMethod() === 'get'){
			$db = \Config\Database::connect();
			$kotas = $db->table('master_kabupatenkota')
				->get()
				->getResult();

			return view ('admin/uutowner/edit', [
				'owner' => $owner,
				'kotas' => $kotas,
			]);
		}
		
		$owner->nama = $this->request->getPost('nama');
		$owner->alamat = $this->request->getPost('alamat');
		$owner->email = $this->request->getPost('email');
		$owner->kota_id = $this->request->getPost('kota_id');
		$owner->telepon = $this->request->getPost('telepon');
		$owner->nib = $this->request->getPost('nib');
		$owner->npwp = $this->request->getPost('npwp');

		if (! $ownerModel->save($owner)) {
			return redirect()->back()->withInput()->with('errors', $ownerModel->errors());
        }

        return redirect()->to(base_url('/uutowner'))->with('success', 'Berhasil menyimpan perubahan data pemilik');
	}

	public function delete($id) {
		$ownerModel = new UttpOwnerModel();
		$ownerModel->delete($id);

		return redirect()->to(base_url('/useruutpowner'))->with('success', 'Berhasil menghapus pemilik alat');
	}

	public function find() {

		$db = \Config\Database::connect();

		$uutModel = new UserUutOwnerModel();
		$builder = $uutModel;
		$builder = $builder->select('uut_owners.*')
			->join('uut_owners', 'uut_owners.id = user_uut_owners.owner_id')
			->where('user_uut_owners.user_id',  session('user_id'));

		$q = $this->request->getGet('q');
		if ($q) {
			$builder = $builder->where("upper(uut_owners.nama) like '%".strtoupper($db->escapeLikeString($q))."%' or nib like '%".strtoupper($db->escapeLikeString($q))."%'");
		}
		$uuts = $builder
			->findAll();

		return $this->respond($uuts, 200);
	}

	public function get($id) {

		$uutModel = new UutOwnerModel();
		$uut = $uutModel->find($id);

		return $this->respond($uut, 200);
	}

	public function findByIdentity() {
		$validation =  \Config\Services::validation();
		$validation->setRules([
			'q' => 'required',
			'by' => 'required',
		],[
			'q' => [
				'required' => 'Keyword pencarian harus diisi'
			],
			'by' => [
				'required' => 'Kategori harus diisi'
			]
		]);
		if (! $validation->run([
				'q' 	=> $this->request->getPost('q'),
				'by' 	=> $this->request->getPost('by'),
			])) {
			return $this->respond($validation->getErrors(), 400);
		}

		$db = \Config\Database::connect();

		$uutModel = new UutOwnerModel();
		$builder = $uutModel;
		$builder = $builder->select('uut_owners.*, master_kabupatenkota.nama as kota, master_provinsi.nama as provinsi')
			->join('master_kabupatenkota', 'master_kabupatenkota.id = uut_owners.kota_id')
			->join('master_provinsi', 'master_provinsi.id = master_kabupatenkota.provinsi_id');

		$q = $this->request->getPost('q');
		$by = $this->request->getPost('by');
		if ($q && $by) {
			$builder = $builder->where($by, $q);
		}
		$uut = $builder
			->first();

		return $this->respond($uut, 200);
	}

	public function store() {
		$validation =  \Config\Services::validation();

		$ownerModel = new UutOwnerModel();
		$validation->setRules([
			'nama' => 'required',
		],[
			'nama' => [
				'required' => 'Nama Harus Diisi'
			]
		]);
		$owner = [
			'nama' 				=> $this->request->getPost('nama'),
			'alamat' 			=> $this->request->getPost('alamat'),
			'email' 			=> $this->request->getPost('email'),
			'kota_id' 			=> $this->request->getPost('kota_id'),
			'telepon' 			=> $this->request->getPost('telepon'),
			'nib' 				=> $this->request->getPost('nib'),
			'npwp' 				=> $this->request->getPost('npwp'),
			'bentuk_usaha_id'	=> $this->request->getPost('bentuk_usaha_id'),
			'penanggung_jawab'	=> $this->request->getPost('penanggung_jawab'),
		];

		if (! $ownerModel->save($owner)) {
			return $this->fail($ownerModel->errors(), 400);
        }
		$owner['id'] = $ownerModel->insertID;

        return $this->respond($owner, 200);
	}
}
