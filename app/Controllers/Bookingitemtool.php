<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\UserUttpModel;
use App\Models\UttpTypeModel;
use App\Models\UttpModel;
use App\Models\BookingItemTTUPerlengkapanModel;

class Bookingitemtool extends BaseController
{
    public function create($id, $idItem) {
        $db = \Config\Database::connect();

		$bookings = new BookingModel();
		$booking = $bookings->find($id);

        $bookingItems = new BookingItemModel();
		$item = $bookingItems->find($idItem);

        $item = $db->query('select sbi.*,  ' . 
				"concat(s.tool_brand, '/', s.tool_model, '/', s.tool_type, ' (', s.serial_no, ')') as tool_code, " .
				's.serial_no, s.tool_brand, s.tool_capacity, s.tool_factory, s.tool_factory_address, s.tool_capacity_min, s.tool_capacity_unit, '.
				's.tool_model, s.tool_type , s.tool_made_in, s.tool_media, '.
				'mut.uttp_type as type ' .
				'from service_booking_items sbi ' .
				'inner join uttps s on sbi.uttp_id = s.id ' .
				'inner join master_uttp_types mut on s.type_id = mut.id ' .
				'where sbi.id = ' . $idItem)->getRow();
        
        $uttpTypeModel = new UttpTypeModel();
        $uttpTypes = $uttpTypeModel->findAll();

        $userUttpModel = new UserUttpModel();
		$uttps = $userUttpModel
			->select('uttps.*, master_uttp_types.uttp_type, uttp_owners.nama as owner, user_uttps.id map_id')
			->join('uttps', 'user_uttps.uttp_id = uttps.id')
			->join('uttp_owners', 'uttp_owners.id = uttps.owner_id')
			->join('master_uttp_types', 'master_uttp_types.id = uttps.type_id')
			->where('user_uttps.user_id',  session('user_id'))
			->findAll();

        $uttpModel = new UttpModel();
        $uttp = $uttpModel->find($item->uttp_id);

        $ttuPerlengkapan = $db->query('select i.*, 
			u.tool_name,
			u.tool_brand,
			u.tool_model,
			u.serial_no,
			u.tool_media,
			u.tool_made_in,
			u.tool_capacity,
			u.tool_capacity_unit,
            mut.uttp_type
			from service_booking_item_ttu_perlengkapan i
			inner join uttps u on i.uttp_id = u.id 
            inner join master_uttp_types mut on u.type_id = mut.id
			where i.booking_item_id = '.$item->id)->getResult();

        if ($this->request->getMethod() === 'get'){
            return view ('admin/bookingitemtools/create', [
                'booking' => $booking,
                'item' => $item,
                'uttps' => $uttps,
                'uttp' => $uttp,
                'ttuPerlengkapan' => $ttuPerlengkapan,
                'uttpTypes' => $uttpTypes,
            ]);
        }

        $idTool = $this->request->getPost('uttp_perlengkapan_id');

        $perlengkapans = new BookingItemTTUPerlengkapanModel();
		$perlengkapans->insert([
            'booking_item_id' => $idItem,
            'uttp_id' => $idTool,
        ]);

        return redirect()->to(base_url('/bookingitemtool/create/'.$id.'/'.$idItem))->with('success', 'Berhasil menyimpan pendaftaran baru');
    }

    public function delete($id, $idItem, $idPerlengkapan) {
		$perlengkapans = new BookingItemTTUPerlengkapanModel();
		$perlengkapans->delete($idPerlengkapan);

		return redirect()->to(base_url('/bookingitemtool/create/'.$id.'/'.$idItem))->with('success', 'Berhasil menyimpan pendaftaran baru');
	}
}