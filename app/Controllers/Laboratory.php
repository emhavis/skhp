<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\LaboratoryModel;

class Laboratory extends BaseController
{
	public function index()
	{
		$labModel = new LaboratoryModel();

		return view ('admin/labs/index', [
			'labs' => $labModel->findAll(),
		]);
	}

	public function edit($id) {
		$labModel = new LaboratoryModel();
		$lab = $labModel->find($id);

		if ($this->request->getMethod() === 'get'){
			return view ('admin/labs/edit', [
				'lab' => $lab,
			]);
		}
		
		$lab->quota_online = $this->request->getPost('quota_online');
		$lab->quota_offline = $this->request->getPost('quota_offline');

		if (! $labModel->save($lab)) {
			return redirect()->back()->withInput()->with('errors', $labModel->errors());
        }

        return redirect()->to(base_url('/lab'))->with('success', 'Berhasil menyimpan perubahan informasi kuota laboratorium');
	}
}
