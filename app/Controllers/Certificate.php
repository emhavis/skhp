<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\RequestUttpModel;
use App\Models\RequestUttpItemModel;
use App\Models\RequestUttpItemInspectionModel;
use App\Models\SurveyInputModel;

class Certificate extends BaseController
{
	
	public function index() {
		/*
		$bookingModel = new BookingModel();
		$bookings = $bookingModel
			//->where('booking_no is null', null, false)
			->where('pic_id', session('user_id'))
			->findAll();
		*/

		$db = \Config\Database::connect();

		$requests = $db->query("select
				sru.service_type_id,sou.id,sru.jenis_layanan,sou.no_sertifikat,sou.no_sertifikat_tipe,sou.no_surat_tipe,
				concat(coalesce(sou.tool_brand, u.tool_brand), '/', coalesce(sou.tool_model,u.tool_model), ' (', coalesce(sou.tool_serial_no, u.serial_no), ')') as tool_code,
				mut.uttp_type tool_type,
				concat('". config('App')->siksURL ."serviceuttp/print/', sou.id) url_certificate_download,
				concat('". config('App')->siksURL ."serviceuttp/download/', sou.id) url_attachment_download,
				case when cru.id is not null
				then concat('". config('App')->siksURL ."revisionapproveuttp/print/', cru.id) 
				else null 
				end url_certificate_rev_download,
				sou.sertifikat_expired_at,
				sou.sertifikat_expired_at < current_date as is_expired
			from
				service_order_uttps sou
			inner join service_request_uttps sru on
				sou.service_request_id = sru.id
			inner join service_request_uttp_items srui on
				sou.service_request_item_id = srui.id
				and srui.request_id = sru.id
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join uttps u on srui.uttp_id = u.id
			inner join master_uttp_types mut on u.type_id = mut.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			left join (
				select
					cru.*
				from
					certificate_revision_uttps cru
				inner join (
					select
						order_id,
						max(id) max_id
					from
						certificate_revision_uttps cru
					where
						status = 4
					group by
						order_id) mcru on
					cru.id = mcru.max_id) cru on
				cru.order_id = sou.id 
				and cru.request_id = sru.id
			where srui.status_id in (13, 14) and sou.stat_sertifikat = 3 and sou.no_sertifikat is not null and sb.pic_id = " . session('user_id').
			" and (
				(	sru.lokasi_pengujian = 'luar' and 
						(sru.status_revisi_spt is null or sru.status_revisi_spt = 3) 
						and
						(sru.payment_valid_date is not null and sru.spuh_payment_valid_date is not null)
				) 
			or sru.lokasi_pengujian = 'dalam')" .

			" union all ".

			"select 
				sru.service_type_id,sou.id,sru.jenis_layanan,sou.no_sertifikat,null as no_sertifikat_tipe,null as no_surat_tipe,
				concat(u.tool_brand, '/', u.tool_model, '/', u.tool_type, ' (', u.serial_no, ')') as tool_code,
				mut.uut_type tool_type,
				concat('". config('App')->siksURL ."serviceuut/print/', sou.id) url_certificate_download,
				concat('". config('App')->siksURL ."serviceuut/download/', sou.id) url_attachment_download,
				case when cru.id is not null
				then concat('". config('App')->siksURL ."revisionapproveuttp/print/', cru.id) 
				else null 
				end url_certificate_rev_download,
				sou.sertifikat_expired_at,
				sou.sertifikat_expired_at < current_date as is_expired
			from
				service_orders sou
			inner join service_requests sru on
				sou.service_request_id = sru.id
			inner join service_request_items srui on
				sou.service_request_item_id = srui.id
				and srui.service_request_id = sru.id
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join standard_uut u on srui.uut_id = u.id
			inner join master_standard_types mut on u.type_id = mut.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			left join (
				select
					cru.*
				from
					certificate_revision_uttps cru
				inner join (
					select
						order_id,
						max(id) max_id
					from
						certificate_revision_uttps cru
					where
						status = 4
					group by
						order_id) mcru on
					cru.id = mcru.max_id) cru on
				cru.order_id = sou.id 
				and cru.request_id = sru.id

				where srui.status_id in (13, 14,16) and (sou.stat_sertifikat = 3 or sou.stat_sertifikat = 0 ) and sou.no_sertifikat is not null and sb.pic_id = " . session('user_id') .
				" and (
				(	sru.lokasi_pengujian = 'luar' and 
						(sru.status_revisi_spt is null or sru.status_revisi_spt = 3) 
						and
						(sru.payment_valid_date is not null and sru.spuh_payment_valid_date is not null)
				) 
			or sru.lokasi_pengujian = 'dalam')" 
				)->getResult();

		return view ('admin/certificates/index', [
			'requests' => $requests,
		]);
	}

	public function read($id) {
		// TO BE BLOCKED
		$db = \Config\Database::connect();

		$order = $db->query("select
				sou.*, 
				sru.no_register, sru.no_order, sru.jenis_layanan,
				coalesce(sou.tool_brand, u.tool_brand) tool_brand,
				coalesce(sou.tool_model, u.tool_model) tool_model,
				coalesce(sou.tool_serial_no, u.serial_no) serial_no,
				mut.uttp_type tool_type,
				mrs.status,
				cru.id as rev_id,
				sru.service_type_id
			from
				service_order_uttps sou
			inner join service_request_uttps sru on
				sou.service_request_id = sru.id
			inner join service_request_uttp_items srui on
				sou.service_request_item_id = srui.id
				and srui.request_id = sru.id
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join uttps u on srui.uttp_id = u.id
			inner join master_uttp_types mut on u.type_id = mut.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			left join (
				select
					cru.*
				from
					certificate_revision_uttps cru
				inner join (
					select
						order_id,
						max(id) max_id
					from
						certificate_revision_uttps cru
					where
						status = 4
					group by
						order_id) mcru on
					cru.id = mcru.max_id) cru on
				cru.order_id = sou.id 
				and cru.request_id = sru.id
			where srui.status_id in (13, 14) and sou.stat_sertifikat = 3 and sou.no_sertifikat is not null and sb.pic_id = " . session('user_id').
			" and (
				(	sru.lokasi_pengujian = 'luar' and 
						(sru.status_revisi_spt is null or sru.status_revisi_spt = 3) 
						and
						(sru.payment_valid_date is not null and sru.spuh_payment_valid_date is not null)
				) 
			or sru.lokasi_pengujian = 'dalam') 
			and sou.id = " . $id)->getRow();
			
		if ($order != null) {
			return view ('admin/certificates/read_uttp', [
				'order' => $order,
			]);
		} else {
			return redirect()->to(base_url('/certificate'))->with('error', 'Sertifikat tidak ditemukan atau belum selesai');
		}
	}

	public function readuut($id) {
		$db = \Config\Database::connect();

		$order = $db->query("select
				sou.*, 
				sru.no_register, sru.no_order, sru.jenis_layanan,
				u.tool_brand,
				u.tool_model,
				u.serial_no,
				mut.uut_type tool_type,
				mrs.status,
				cru.id as rev_id
				from
				service_orders sou
			inner join service_requests sru on
				sou.service_request_id = sru.id
			inner join service_request_items srui on
				sou.service_request_item_id = srui.id
				and srui.service_request_id = sru.id
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join standard_uut u on srui.uut_id = u.id
			inner join master_standard_types mut on u.type_id = mut.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			left join (
				select
					cru.*
				from
					certificate_revision_uttps cru
				inner join (
					select
						order_id,
						max(id) max_id
					from
						certificate_revision_uttps cru
					where
						status = 4
					group by
						order_id) mcru on
					cru.id = mcru.max_id) cru on
				cru.order_id = sou.id 
				and cru.request_id = sru.id
				where srui.status_id in (13, 14) and sou.stat_sertifikat = 3 and sou.no_sertifikat is not null and sb.pic_id = " . session('user_id').
			" and (
				(	sru.lokasi_pengujian = 'luar' and 
						(sru.status_revisi_spt is null or sru.status_revisi_spt = 3) 
						and
						(sru.payment_valid_date is not null and sru.spuh_payment_valid_date is not null)
				) 
			or sru.lokasi_pengujian = 'dalam') 
			and sou.id = " . $id)->getRow();

		return view ('admin/certificates/read_uut', [
			'order' => $order,
		]);
	}

	public function print($id, $jenis_sertifikat) {
		$db = \Config\Database::connect();

		// CHECK SUDAH ISI ATAU BELUM
		$checkOrder = $db->query("select * from service_order_uttps where id = " . $id)->getRow();
		$request = $db->query("select * from service_request_uttps where id = " . $checkOrder->service_request_id)->getRow();
		
		$inputModel = new SurveyInputModel();
		$input = $inputModel
			->where('request_id', $request->id)
			->where('user_id', session('user_id'))
			->findAll();

		if (count($input) == 0) {
			return redirect()->to(base_url('/survey/take/'.$id.'/'.$jenis_sertifikat));
		}



		// $order = $db->query("select * from service_order_uttps where id = " . $id)->getRow();

		$order = $db->query("select
				sou.*, 
				sru.no_register, sru.no_order, sru.jenis_layanan,
				coalesce(sou.tool_brand, u.tool_brand) tool_brand,
				coalesce(sou.tool_model, u.tool_model) tool_model,
				coalesce(sou.tool_serial_no, u.serial_no) serial_no,
				mut.uttp_type tool_type,
				mrs.status,
				cru.id as rev_id,
				sru.service_type_id
			from
				service_order_uttps sou
			inner join service_request_uttps sru on
				sou.service_request_id = sru.id
			inner join service_request_uttp_items srui on
				sou.service_request_item_id = srui.id
				and srui.request_id = sru.id
			inner join service_bookings sb on sru.booking_id = sb.id
			inner join uttps u on srui.uttp_id = u.id
			inner join master_uttp_types mut on u.type_id = mut.id
			inner join master_request_status mrs on srui.status_id = mrs.id 
			left join (
				select
					cru.*
				from
					certificate_revision_uttps cru
				inner join (
					select
						order_id,
						max(id) max_id
					from
						certificate_revision_uttps cru
					where
						status = 4
					group by
						order_id) mcru on
					cru.id = mcru.max_id) cru on
				cru.order_id = sou.id 
				and cru.request_id = sru.id
			where srui.status_id in (13, 14) and sou.stat_sertifikat = 3 and sou.no_sertifikat is not null and sb.pic_id = " . session('user_id').
			" and (
				(	sru.lokasi_pengujian = 'luar' and 
						(sru.status_revisi_spt is null or sru.status_revisi_spt = 3) 
						and
						(sru.payment_valid_date is not null and sru.spuh_payment_valid_date is not null)
				) 
			or sru.lokasi_pengujian = 'dalam') 
			and sou.id = " . $id)->getRow();

		if ($order == null) {
			return redirect()->to(base_url('/certificate'))->with('error', 'Sertifikat tidak ditemukan atau belum selesai');
		}

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuttp/print/' . $id .'/'. $jenis_sertifikat .'/'. $token);
		}
	}

	public function print_uut($id,$jenis_sertifikat) {
		$db = \Config\Database::connect();

		// CHECK SUDAH ISI ATAU BELUM
		$order = $db->query("select * from service_orders where id = " . $id)->getRow();
		$request = $db->query("select * from service_requests where id = " . $order->service_request_id)->getRow();

		$inputModel = new SurveyInputModel();
		$input = $inputModel
			->where('request_uut_id', $request->id)
			->where('user_id', session('user_id'))
			->findAll();

		if (count($input) == 0) {
			return redirect()->to(base_url('/survey/takeuut/'.$id.'/'.$jenis_sertifikat));
		}

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		if ($token) {
			return redirect()->to(config('App')->siksURL . 'documentuut/print/' . $id .'/'. $jenis_sertifikat .'/'.$token);
		}

		//return redirect()->to(base_url('/certificate'));
	}

	public function download($id) {
		$db = \Config\Database::connect();

		$order = $db->query("select * from service_order_uttps where id = " . $id)->getRow();

		$client = \Config\Services::curlrequest();
		//$token = $client->request('GET', config('App')->siksURL ."secure/csrf-token");

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			$print = $client->request('GET', config('App')->siksURL ."documentuttp/download/".$id."/".$token);

			header('Cache-Control: public'); 
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="Lampiran '.$order->no_sertifikat.'.pdf"');
			header('Content-Length: '.strlen($print->getBody()));
			echo $print->getBody();
		}

		// return redirect()->to(base_url('/certificate'));
	}

	public function downloaduut($id) {
		$db = \Config\Database::connect();

		$order = $db->query("select * from service_orders where id = " . $id)->getRow();

		$client = \Config\Services::curlrequest();

		$login = $client->request('POST', config('App')->siksURL ."secure/auth", [
			'form_params' => [
				'username' => 'customer',
				'password' => 'cerber0s',
				'client' => 'skhp',
				'secret_key' => '$2y$10$Arox7k/XLD7aYVBbiY1ZtujtEHIuEBve/JjTpDgr0fp9.xETU/5Dq',
			]
		]);

		$loginBody = $login->getBody();
		$loginJson = json_decode($loginBody);

		$token = $loginJson->token;
		
		if ($token) {
			$print = $client->request('GET', config('App')->siksURL ."documentuut/download/".$id."/".$token);

			header('Cache-Control: public'); 
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="Lampiran '.$order->no_sertifikat.'.pdf"');
			header('Content-Length: '.strlen($print->getBody()));
			echo $print->getBody();
		}
	}
}
