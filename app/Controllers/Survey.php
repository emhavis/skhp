<?php

namespace App\Controllers;
use App\Models\SurveyModel;
use App\Models\SurveyPageModel;
use App\Models\SurveyQuestionModel;
use App\Models\SurveyInputModel;
use App\Models\SurveyInputLineModel;

class Survey extends BaseController
{
	
	public function take($order_id, $jenis_sertifikat, $page_id = 0) {
		//dd($order_id);
		$surveyModel = new SurveyModel();
		$pageModel = new SurveyPageModel();

		$db = \Config\Database::connect();
		$order = $db->query("select * from service_order_uttps where id = " . $order_id)->getRow();
		$request = $db->query("select * from service_request_uttps where id = " . $order->service_request_id)->getRow();
		if ($request->lokasi_pengujian == 'luar') {
			$doc = $db->query("select * from service_request_uttp_insitu_doc where id = " . $request->spuh_doc_id)->getRow();
		} else {
			$doc = null;
		}
		
		
		$petugas = $db->query("select pu.* from service_request_uttp_insitu_staff sruis 
			inner join service_request_uttps sru on sruis.doc_id  = sru.spuh_doc_id 
			inner join petugas_uttp pu on sruis.scheduled_id  = pu.id 
			where sru.id = " . $order->service_request_id . ' order by sruis.id')->getResult();
		
		if ($page_id == 0) {
			$inputModel = new SurveyInputModel();
			$input = $inputModel
				->where('request_id', $request->id)
				//->where('order_id', $order_id)
				->where('user_id', session('user_id'))
				->findAll();

			if (count($input) > 0) {
				return redirect()->to(base_url('/certificate/print/'.$order_id.'/'.$jenis_sertifikat));
			}

			$survey = $surveyModel->where('active', true)->orderBy('created_at', 'desc')->first();
			$page = $pageModel->where('survey_id', $survey->id)
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->first();

			$nextPage = $pageModel
				->where('survey_id', $survey->id)
				->where('sequence >', $page->id)
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->first();
		} else {
			$page = $pageModel->find($page_id);
			$survey = $surveyModel->find($page->survey_id);

			$nextPage = $pageModel
				->where('survey_id', $survey->id)
				->where('sequence >', $page->id)
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->first();
		}

		//dd([$page, $nextPage]);

		if ($page != null) {
			$questionModel = new SurveyQuestionModel();
			$questions = $questionModel->where('page_id', $page->id)->where('deleted_at is null')
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->findAll();

			$service_type = $db->query("select * from master_service_types where id = " . $request->service_type_id)->getRow();

			$template = 'admin/surveys/take';
			if ($request->lokasi_pengujian == 'luar') {
				$template = 'admin/surveys/take_luar';
			}
			return view ($template, [
				'survey' => $survey,
				'page' => $page,
				'questions' => $questions,
				'order_id' => $order_id,
				'jenis_sertifikat' => $jenis_sertifikat,
				'service_type' => $service_type,
				'request' => $request,
				'petugas' => $petugas,
				'doc' => $doc,
				'next_page' => $nextPage,
			]); 
		}

		
		//return redirect()->to(base_url('/certificate/print/'.$order_id.'/'.$jenis_sertifikat));

		return redirect()->to(base_url('/survey/finish/'.$order_id.'/'.$jenis_sertifikat));
	}

	public function input($order_id, $jenis_sertifikat, $page_id) {
		$db = \Config\Database::connect();
		$order = $db->query("select * from service_order_uttps where id = " . $order_id)->getRow();
		$request = $db->query("select * from service_request_uttps where id = " . $order->service_request_id)->getRow();
		$service_type = $db->query("select * from master_service_types where id = " . $request->service_type_id)->getRow();

		$pageModel = new SurveyPageModel();
		$page = $pageModel->find($page_id);

		$questionModel = new SurveyQuestionModel();
		$questions = $questionModel->where('page_id', $page->id)->orderBy('sequence', 'asc')->findAll();

		$lines = [];
		foreach ($questions as $q) {
			if ($q->question_type == 'data') {
				if ($q->possible_answers == 'service_type') {
					$val = $service_type->service_type;
				}
			} elseif ($q->question_type == 'file') {
				$file = $this->request->getFile('q'.$q->id);

				if (isset($file) && $file->getName() != '') { $path = $file->store(); }
				if (isset($file) && $file->getName() ) {
					$val = isset($file) ? $file->getClientName() : null;
					$path = isset($path) ? $path : null;
				}
			} else {
				$val = $this->request->getPost('q'.$q->id);
			}

			if ($val) {
				$line = [];
				$line['question_id'] = $q->id;
				if ($q->question_type != 'file') {
					$line['value_' . $q->question_type] = $val;
				} else {
					$line['value_filename'] = $val;
					$line['value_filepath'] = $path;
				}
				array_push($lines, $line);
			}
		}

		if (count($lines) > 0) {
			$input = [
				'survey_id' => $page->survey_id,
				'order_id' => $order_id,
				'request_id' => $order->service_request_id,
				'user_id' => session('user_id')
			];

			
			$db = \Config\Database::connect();
			$db->transStart(); 

			$inputModel = new SurveyInputModel();
			$lineModel = new SurveyInputLineModel();
			if ($inputModel->save($input)) {

				$newId = $inputModel->insertID;
				foreach ($lines as $line) {
					$line['input_id'] = $newId;

					$lineModel->save($line);
				}
			}

			$db->transComplete();
		}

		$nextPage = $pageModel
			->where('survey_id', $page->survey_id)
			->where('sequence >', $page_id)
			->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
			->orderBy('sequence', 'asc')->first();

		if ($nextPage) {
			return redirect()->to(base_url('/survey/take/'.$order_id.'/'.$jenis_sertifikat.'/'.$nextPage->id));
		}

		// return redirect()->to(base_url('/certificate/print/'.$order_id.'/'.$jenis_sertifikat));
		
		return redirect()->to(base_url('/survey/finish/'.$order_id.'/'.$jenis_sertifikat));
	}

	public function finish($order_id, $jenis_sertifikat) {
		return view ('admin/surveys/finish', [
			'order_id' => $order_id,
			'jenis_sertifikat' => $jenis_sertifikat,
		]);  
	}
	/*
	This for uut 
	*/

	public function takeuut($order_id,$jenis_sertifikat, $page_id = 0) {
		$surveyModel = new SurveyModel();
		$pageModel = new SurveyPageModel();

		$db = \Config\Database::connect();
		$order = $db->query("select * from service_orders where id = " . $order_id)->getRow();
		$request = $db->query("select * from service_requests where id = " . $order->service_request_id)->getRow();
		if ($request->lokasi_pengujian == 'luar') {
			$doc = $db->query("select * from service_request_uut_insitu_doc where id = " . $request->spuh_doc_id)->getRow();
		} else {
			$doc = null;
		}

		$petugas = $db->query("select pu.* from service_request_uut_insitu_staff sruis 
			inner join service_requests sru on sruis.doc_id  = sru.spuh_doc_id 
			inner join petugas_uttp pu on sruis.scheduled_id  = pu.id 
			where sru.id = " . $order->service_request_id . ' order by sruis.id')->getResult();
			
		if ($page_id == 0) {
			$inputModel = new SurveyInputModel();
			$input = $inputModel
				->where('request_uut_id', $request->id)
				//->where('order_id', $order_id)
				->where('user_id', session('user_id'))
				->findAll();

			if (count($input) > 0) {
				return redirect()->to(base_url('/certificate/print_uut/'.$order_id.'/'.$jenis_sertifikat));
			}

			$survey = $surveyModel->where('active', true)->orderBy('created_at', 'desc')->first();
			$page = $pageModel->where('survey_id', $survey->id)
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->first();
			if($request->service_type_id == 1 || $request->service_type_id == 2){
				$page->description = "Dalam rangka Balai Pengelolaan SUML Direktorat Metrologi menuju Zona Integritas (ZI), Wilayah Bebas dari Korupsi (WBK) dan Wilayah Birokrasi Bersih dan Melayani (WBBM), Kami mohon kesediaan Saudara untuk mengisi dengan objektif formulir penilaian Komitmen Layanan yang diberikan oleh Petugas Layanan Balai Pengelolaan SUML. 

				Informasi konsumen terjaga kerahasiaannya, tidak diketahui oleh petugas yang dinilai, tidak mempengaruhi hasil pekerjaan, dan langsung ditangani oleh Manajemen Balai Pengelolaan SUML.";
			}

			$nextPage = $pageModel
				->where('survey_id', $survey->id)
				->where('sequence >', $page->id)
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->first();
		} else {
			$page = $pageModel->find($page_id);
			$survey = $surveyModel->find($page->survey_id);
			if($request->service_type_id == 1 || $request->service_type_id == 2){
				$page->description = "Dalam rangka Balai Pengelolaan SUML Direktorat Metrologi menuju Zona Integritas (ZI), Wilayah Bebas dari Korupsi (WBK) dan Wilayah Birokrasi Bersih dan Melayani (WBBM), Kami mohon kesediaan Saudara untuk mengisi dengan objektif formulir penilaian Komitmen Layanan yang diberikan oleh Petugas Layanan Balai Pengelolaan SUML. 

				Informasi konsumen terjaga kerahasiaannya, tidak diketahui oleh petugas yang dinilai, tidak mempengaruhi hasil pekerjaan, dan langsung ditangani oleh Manajemen Balai Pengelolaan SUML.";
			}
			$nextPage = $pageModel
				->where('survey_id', $survey->id)
				->where('sequence >', $page->id)
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->first();
		}

		if ($page != null) {
			$questionModel = new SurveyQuestionModel();
			$questions = $questionModel->where('page_id', $page->id)->where('deleted_at is null')
				->where("(loc_applied is null or loc_applied = '". $request->lokasi_pengujian . "')")
				->orderBy('sequence', 'asc')->findAll();

			$service_type = $db->query("select * from master_service_types where id = " . $request->service_type_id)->getRow();

			$template = 'admin/surveys/take_uut';
			if ($request->lokasi_pengujian == 'luar') {
				$template = 'admin/surveys/take_luar_uut';
			}
			
			return view ($template, [
				'survey' => $survey,
				'page' => $page,
				'questions' => $questions,
				'order_id' => $order_id,
				'jenis_sertifikat' => $jenis_sertifikat,
				'service_type' => $service_type,
				'request' => $request,
				'petugas' => $petugas,
				'doc' => $doc,
				'next_page' => $nextPage,
			]); 
		}
		//return redirect()->to(base_url('/certificate/print/'.$order_id));
		return redirect()->to(base_url('/survey/finishuut/'.$order_id));
	}

	public function input_uut($order_id, $jenis_sertifikat, $page_id) {
		$db = \Config\Database::connect();
		$order = $db->query("select * from service_orders where id = " . $order_id)->getRow();

		$pageModel = new SurveyPageModel();
		$page = $pageModel->find($page_id);

		$questionModel = new SurveyQuestionModel();
		$questions = $questionModel->where('page_id', $page->id)->orderBy('sequence', 'asc')->findAll();
		
		$lines = [];
		foreach ($questions as $q) {
			$req = $this->request->getPost('q'.$q->id);
			if ($req) {
				$line = [];
				$line['question_id'] = $q->id;
				$line['value_' . $q->question_type] = $req;
				
				array_push($lines, $line);
			}
		}

		if (count($lines) > 0) {
			$input = [
				'survey_id' => $page->survey_id,
				'order_uut_id' => $order_id,
				'request_uut_id' => $order->service_request_id,
				'user_id' => session('user_id')
			];

			
			$db = \Config\Database::connect();
			$db->transStart(); 

			$inputModel = new SurveyInputModel();
			$lineModel = new SurveyInputLineModel();
			if ($inputModel->save($input)) {

				$newId = $inputModel->insertID;
				foreach ($lines as $line) {
					$line['input_id'] = $newId;

					$lineModel->save($line);
				}
			}

			$db->transComplete();
		}

		$nextPage = $pageModel
			->where('survey_id', $page->survey_id)
			->where('sequence >', $page_id)
			->orderBy('sequence', 'asc')->first();
		
			if ($nextPage) {
				return redirect()->to(base_url('/survey/takeuut/'.$order_id.'/'.$jenis_sertifikat.'/'.$nextPage->id));
			}
			return redirect()->to(base_url('/survey/finishuut/'.$order_id.'/'.$jenis_sertifikat));
			
	}

	public function finishuut($order_id, $jenis_sertifikat) {
		
		return view ('admin/surveys/finish_uut', [
			'order_id' => $order_id,
			'jenis_sertifikat' => $jenis_sertifikat,
		]);  
	}

	public function download($input_id, $question_id) {
		$db = \Config\Database::connect();
		$item = $db->table("survey_input_line")
			->where('input_id', $input_id)
			->where('question_id', $question_id)
			->orderBy('id ASC')
			->get()->getRow();
		
		$path = 'value_filepath';
		$file = 'value_filename';
		
		return $this->response->download('uploads/'.$path, null)->setFileName($file);
	}
}
