<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\BookingTermModel;

class Publicterm extends BaseController
{
	
	public function create($id) {
		$db = \Config\Database::connect();

		$booking = $db->query('select sru.*  from service_bookings sru 
			where sru.id = '. $id)->getRow();

		$term = $db->query('select * from master_service_types where id = ' . $booking->service_type_id)->getRow();
		
		$kaji_ulang_insitu = $term->kaji_ulang_insitu;
		$kaji_ulang_insitu = str_replace('{{keuangan_perusahaan}}', $booking->keuangan_perusahaan, $kaji_ulang_insitu);
		$kaji_ulang_insitu = str_replace('{{keuangan_pic}}', $booking->keuangan_pic, $kaji_ulang_insitu);
		$kaji_ulang_insitu = str_replace('{{keuangan_jabatan}}', $booking->keuangan_jabatan, $kaji_ulang_insitu);
		$kaji_ulang_insitu = str_replace('{{keuangan_hp}}', $booking->keuangan_hp, $kaji_ulang_insitu);
		
		if ($this->request->getMethod() === 'get'){
			return view ('publicTerm/terms', [
				'booking' => $booking,
				'term' => $term,
				'kaji_ulang_insitu' =>$kaji_ulang_insitu,
			]);
		}
		
		$bookingModel = new BookingModel();
		$booking = $bookingModel->find($id);
		$booking->kaji_ulang_insitu_at = date('Y-m-d H:i:s');
		$booking->kaji_ulang_insitu = true;

		if (! $bookingModel->save($booking)) {
			return redirect()->back()->withInput()->with('errors', $bookingModel->errors());
        }

		return redirect()->to(base_url('/booking/edit/' . $id))->with('success', 'Berhasil menyimpan kaji ulang permintaan');
	}

	public function create_kn($id) {
		$db = \Config\Database::connect();
		
		$item = $db->query('select srui.*, 
			mut.uttp_type, u.type_id, 
			u.serial_no, u.tool_brand, u.tool_capacity, u.tool_factory, u.tool_factory_address,
			u.tool_model, u.tool_type , u.tool_made_in
			from service_request_uttp_items srui 
			inner join uttps u on srui.uttp_id = u.id
			inner join master_uttp_types mut on u.type_id = mut.id 
			where srui.id = ' . $id)->getRow();

		$request = $db->query('select sru.*, mrs.status from service_request_uttps sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.id = '. $item->request_id)->getRow();

		$inspections = $db->query('select sruii.*,
			uip.inspection_type, uip.unit, mrs.status
			from service_request_uttp_item_inspections sruii 
			inner join service_request_uttp_items srui on sruii.request_item_id = srui.id 
			inner join uttp_inspection_prices uip on sruii.inspection_price_id = uip.id
			inner join master_request_status mrs on sruii.status_id = mrs.id
			where sruii.request_item_id = ' . $id)->getResult();

		$type = $db->query('select * from master_uttp_types mut 
			where mut.id = ' . $item->type_id)->getRow();
		$termKaji = $db->query("select * from master_uttp_type_kajiulang where service_type_id = ". $request->service_type_id ." and uttp_type_id = ". $type->id)->getRow();

		if ($this->request->getMethod() === 'get'){
			return view ('publicTerm/term_kn', [
				'request' => $request,
				'item' => $item,
				'inspections' => $inspections,
				'type' => $type,
				'termKaji' => $termKaji,
			]);
		}
		
		$termModel = new TermModel();
		$term = [
			'request_id' => $request->id,
			'request_item_id' => $id,
			'legalitas' => $this->request->getPost('legalitas') == 'on' ? 1 : 0,
			'metode' => $this->request->getPost('metode') == 'on' ? 1 : 0,
			'sign_at' => date('Y-m-d H:i:s'),
			'klausa' => true,
		];

		if (! $termModel->save($term)) {
			return redirect()->back()->withInput()->with('errors', $termModel->errors());
        }

		return redirect()->to(base_url('/tracking/read/' . $request->booking_id))->with('success', 'Berhasil menyimpan kaji ulang permintaan');
	}
}
