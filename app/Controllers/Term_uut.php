<?php

namespace App\Controllers;
use App\Models\BookingModel;
use App\Models\BookingItemModel;
use App\Models\RequestUttpModel;
use App\Models\RequestUttpItemModel;
use App\Models\RequestUttpItemInspectionModel;
use App\Models\TermModel;
use App\Models\TermUutModel;

class Term_uut extends BaseController
{
	
	public function create($id) {
		$db = \Config\Database::connect();

		$item = $db->query('select sri.*, u.type_id, sut.uut_type,sut.lab_id,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in
				from service_request_items sri 
				inner join standard_uut u on sri.uut_id = u.id
				inner join master_standard_types sut on sut.id = u.type_id 
				where sri.id = '. $id)->getRow();
		
		// dd($item->lab_id);
		$request = $db->query('select sru.*, mrs.status from service_requests sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.id = '. $item->service_request_id)->getRow();

		$inspections = $db->query('select sruii.*,
			uip.inspection_type, uip.unit, mrs.status, uip.lab_id
			from service_request_item_inspections sruii 
			inner join service_request_items srui on sruii.service_request_item_id = srui.id 
			inner join uut_inspection_prices uip on sruii.inspection_price_id = uip.id
			inner join master_request_status mrs on sruii.status_id = mrs.id
			where sruii.service_request_item_id = ' . $id)->getResult();

		$type = $db->query('select mut.*
			from master_standard_types mut 
			where mut.id = '.$item->type_id)->getRow();
		$kelompok = $type ? $type->kelompok : '';

		// $termKaji = $db->query("select * from master_laboratory where id = ". $request->service_type_id )->getRow();\
		$termKaji = $db->query("select * from master_laboratory where id = ". $item->lab_id )->getRow();
		$slas = $db->query("select * from master_uttp_sla where kelompok = '" . $kelompok . "'")->getResult();
		$klausas = $db->query("select * from master_uut_kajiklausa where kelompok = '" . $kelompok . "'")->getResult();

		if ($this->request->getMethod() === 'get'){
			return view ('admin/terms/create_uut', [
				'request' => $request,
				'item' => $item,
				'inspections' => $inspections,
				'type' => $type,
				'slas' => $slas,
				'klausas' => $klausas,
				'termKaji' => $termKaji,
			]);
		}

		// $termModel = new TermUutModel();
		// $term = $termModel->where('request_id', $request->id)->where('request_item_id', $id)->first();
		
		$termModel = new TermUutModel();
		$bookingItem = new BookingItemModel();
		// dd([$this->request->getPost('keterangan'),$request,$id,$bookingItem->first()]);
		// dd([$request->id, $id]);
		$term = [
			'request_id' => $request->id,
			'request_item_id' => $id,
			'keterangan'	=> $this->request->getPost('keterangan'),
			// 'legalitas' => $this->request->getPost('legalitas') == 'on' ? 1 : 0,
			// 'metode' => $this->request->getPost('metode') == 'on' ? 1 : 0,
			'sign_at' => date('Y-m-d H:i:s'),
		];

		$bitem = $bookingItem->where('booking_id',$request->booking_id)->where('uut_id',$item->uut_id)->first();
		if($this->request->getPost('keterangan') !=''){
			$data =['keterangan' => $this->request->getPost('keterangan')];
			$sql = "UPDATE service_request_items SET keterangan = ? WHERE id = ?";
			$db->query($sql, array($data,$id));

			$bookingItem->update($bitem->id,$data);
		}
		if (! $termModel->save($term)) {
			return redirect()->back()->withInput()->with('errors', $termModel->errors());
        }

		foreach ($klausas as $klausa) {
			if ($klausa->is_header != 't') {
				$skb = [
					'kajiulang_id' => $termModel->insertID,
					'klausa_id' => (int)$klausa->id,
					'checked' => $this->request->getPost('klausa_' . $klausa->id) == 'on',
				];

				$db->table('service_request_uttp_kajiulang_klausa')
					->insert($skb);
			}
		}

		$check = $db->query('select sum(case when checked then 1 else 0 end) count_checked, count(id) count_all
			from service_request_standard_kajiulang_klausa srukk 
			where kajiulang_id = '.$termModel->insertID)->getRow();
	
		$term = $termModel->where('id', $termModel->insertID)->first();
		$term->klausa = $check->count_checked >= $check->count_all;

		$termModel->save($term);
		
		return redirect()->to(base_url('/tracking/read/' . $request->booking_id))->with('success', 'Berhasil menyimpan kaji ulang permintaan');
	}

	public function edit($id) {
		$db = \Config\Database::connect();

		$item = $db->query('select sri.*, sut.uut_type,sut.lab_id,
				u.serial_no, u.tool_brand, u.tool_capacity, u.tool_capacity_unit,
				u.tool_factory, u.tool_factory_address,
				u.tool_model, u.tool_type , u.tool_made_in
				from service_request_items sri 
				inner join standard_uut u on sri.uut_id = u.id
				inner join master_standard_types sut on sut.id = u.type_id 
				where sri.id = '. $id)->getRow();
		// dd($item->service_request_id);

		$request = $db->query('select sru.*, mrs.status from service_requests sru 
			inner join master_request_status mrs on sru.status_id = mrs.id
			where sru.id = '. $item->service_request_id)->getRow();

		$inspections = $db->query('select sruii.*,
			uip.inspection_type, uip.unit, mrs.status, uip.lab_id
			from service_request_item_inspections sruii 
			inner join service_request_items srui on sruii.service_request_item_id = srui.id 
			inner join uut_inspection_prices uip on sruii.inspection_price_id  = uip.id
			inner join master_request_status mrs on sruii.status_id = mrs.id
			where sruii.service_request_item_id = ' . $id)->getResult();

		$type = $db->query('select mut.*, mo.oiml_name from master_standard_types mut 
			inner join master_oimls mo on mut.oiml_id = mo.id
			')->getRow();
		$kelompok = $type ? $type->kelompok : '';
		

		$termModel = new TermUutModel();
		$term = $termModel->where('request_id', $request->id)->where('request_item_id', $id)->first();
		$termKaji = $db->query("select * from master_laboratory where id = ". $item->lab_id )->getRow();
		$slas = $db->query("select * from master_uttp_sla where kelompok = '" . $kelompok . "'")->getResult();
		$klausas = $db->query("select * from master_uut_kajiklausa where kelompok = '" . $kelompok . "'")->getResult();
		

		if ($this->request->getMethod() === 'get'){
			return view ('admin/terms/edit_uut', [
				'request' => $request,
				'item' => $item,
				'inspections' => $inspections,
				'term' => $term,
				'termKaji'=> $termKaji
			]);
		}
		
		$termModel = new TermModel();
		$term->request_id = $request->id;
		$term->request_item_id = $id;
		$term->legalitas = $this->request->getPost('legalitas') == 'on' ? 1 : 0;
		$term->metode = $this->request->getPost('metode') == 'on' ? 1 : 0;
		$term->sign_at = date('Y-m-d H:i:s');
		
		if (! $termModel->save($term)) {
			return redirect()->back()->withInput()->with('errors', $termModel->errors());
        }
		
		return redirect()->to(base_url('/tracking/read/' . $request->booking_id))->with('success', 'Berhasil menyimpan kaji ulang permintaan');
	}
	
}
