<?= $this->extend('template-public/main') ?>

<?= $this->section('content') ?>
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div class="row">
            <!-- content -->
            <div class="content col-12">
                <!-- Blog -->
                <div id="blog" class="single-post">
                    <!-- Post single item-->
                    <div class="post-item">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <img alt="" src="<?= getenv('app.siksURL').'storage/'.$article->cover_image_path ?>">
                            </div>
                            <div class="post-item-description">
                                <h2><?= $article->title ?></h2>
                                <?= $article->content ?>
                            </div>
                            
                        </div>
                    </div>
                    <!-- end: Post single item-->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>