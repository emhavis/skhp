<?= $this->extend('template-public/main') ?>

<?= $this->section('content') ?>
<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider slider-halfscreen dots-creative" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide background-image" style="background-image:url('homepages/corporate-v4/images/7.jpg');">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="slide-captions text-center">
                <!-- Captions -->
                <h6 class="text-light">WELCOME TO</h6>
                <h2 class="text-uppercase text-medium text-light">BUSINESS COMPANY PRESENTATION</h2>
                <p class="lead text-light">Lorem ipsum dolor sit amet, consecte adipiscing elit.
                    <br /> Suspendisse condimentum porttitor cursumus.</p>
                <a class="btn btn-red" href="http://themeforest.net/item/polo-responsive-multipurpose-html5-template/13708923">Purchase POLO</a>

                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide background-image" style="background-image:url('homepages/corporate-v4/images/8.jpg');">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="slide-captions text-center">
                <!-- Captions -->
                <h6 class="text-light">WELCOME TO</h6>
                <h2 class="text-uppercase text-medium text-light">BUSINESS COMPANY</h2>
                <p class="lead text-light">Lorem ipsum dolor sit amet, consecte adipiscing elit.
                    <br /> Suspendisse condimentum porttitor cursumus.</p>
                <a class="btn btn-red" href="http://themeforest.net/item/polo-responsive-multipurpose-html5-template/13708923">Purchase POLO</a>

                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 2 -->
</div>
<!--end: Inspiro Slider -->

<!-- BLOG -->
<section class="content background-grey">
    <div class="container">
        <div class="heading-text heading-section">
            <h4>BERITA & ARTIKEL</h4>
        </div>

        
            <div class="carousel" data-items="3">
            <?php foreach($articles as $index=>$article): ?>
            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-item-description">
                        <h2>
                            <a href="#"><?= $article->title ?></a>
                        </h2>
                        <p><?= $article->excerpt ?></p>
                        <a href="#" class="item-link">Selengkapnya <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->
            <?php endforeach; ?>
            </div>

    </div>
</section>
<!-- end: BLOG -->
<?= $this->endSection() ?>