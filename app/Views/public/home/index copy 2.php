<?= $this->extend('template-public/main') ?>

<?= $this->section('content') ?>

<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider slider-halfscreen dots-creative" 
    data-height-xs="360" data-autoplay="5600" data-animate-in="fadeIn" 
    data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">

    <?php foreach($articles as $index=>$article): ?>
    <div class="slide background-image" 
        style="background-image:url('<?= getenv('app.siksURL').'storage/'.$article->cover_image_path ?>'); background-size: auto 100%">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="slide-captions text-center">
                <!-- Captions -->
                <h3 class="text-light"><?= $article->title ?></h3>
                <p class="text-light"><?= $article->excerpt ?></p>
                <a class="btn btn-default" href="<?= base_url('article/read/' . $article->id); ?>">Selengkapnya</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <?php endforeach ?>
</div>
<!--end: Inspiro Slider -->

<?= $this->endSection() ?>