<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Sertifikat</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('certificate');?>">Sertifikat</a>
        </li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Sertifikat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('tracking');?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nomor SKHP</label>
                        <input class="form-control" type="text" name="booking_no" 
                            value="<?= $order->no_sertifikat ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Tanggal Terbit</label>
                        <input class="form-control" type="text" name="order_no" 
                            value="<?= date("d M Y", strtotime($order->kabalai_date)) ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis UUT</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $order->tool_type ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Merek</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $order->tool_brand ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Model/Tipe</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $order->tool_model ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>No Seri</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $order->serial_no ?>" readonly>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <?php if($order->rev_id == null) { ?>
                            <a class="btn btn-default" href="<?=base_url('survey/takeuut/' . $order->id.'/skhp'); ?>"><i class="fas fa-file-contract"></i> Sertifikat</a>
                        <?php } else { ?>
                            <a class="btn btn-default" href="<?=base_url('certificate/downloaduut/' . $order->rev_id); ?>"><i class="fas fa-file-contract"></i> Sertifikat</a>
                        <?php } ?>
                        <!-- <a class="btn btn-default" href="<?=base_url('certificate/downloaduut/' . $order->id); ?>"><i class="fas fa-paperclip"></i> Lampiran</a> -->
                    </div>
                </div>
                <div class="row">
                <hr></hr>
                </div>
                <div class="row">
                    </hr>
                    <div class="col-12 text-center">
                        </br></b>Jika terdapat kendala dalam penggunaan aplikasi atau saat mengunduh sertifikat silahkan menghubungi Customer Service UPTP IV: 
                        </br></b>Telp :(022)4207066 Mobile : 08112238313 
                        </br>email ke: <a href='mailto:balai_snsu@yahoo.com'>balai_snsu@yahoo.com</a>
                    </div>
                </div>
        </div>
    </div> 
    
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#items').DataTable({
            pageLength: 10,
        });

    })
</script>
<?= $this->endSection() ?>