<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Sertifikat</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pelacakan Sertifikat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Layanan</div>
        </div>
        <div class="ibox-body">
            <?php setlocale(LC_TIME, "id_ID"); ?>
            <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nomor Sertifikat</th>
                        <th>Jenis Layanan</th>
                        <th>Alat/Standard</th>
                        <th>Masa Berlaku s/d</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($requests as $request):?>
                    <tr>
                        <?php
                            $sertifikatList = [];
                            if ($request->no_sertifikat != null) {
                                $sertifikatList[] = $request->no_sertifikat;
                            }
                            if ($request->no_sertifikat_tipe != null) {
                                $sertifikatList[] = $request->no_sertifikat_tipe;
                            }
                            if ($request->no_sertifikat != null) {
                                $sertifikatList[] = $request->no_surat_tipe;
                            }
                        ?>
                        <td><?= implode('<br/>', $sertifikatList) ?></td>
                        <td><?= $request->jenis_layanan ?></td>
                        <td><?= $request->tool_code ?><br/><small><?= $request->tool_type ?></small></td>
                        <td><?= $request->sertifikat_expired_at == null ? '-' : (strftime("%e %B %Y", strtotime($request->sertifikat_expired_at)))?></td>
                        <td><?= $request->is_expired == 't' ? '<span class="badge badge-danger">Expired</span>' : '<span class="badge badge-success">Aktif</span>' ?></td>
                        <td>
                            <?php if($request->service_type_id ==1 || $request->service_type_id ==2): ?>
                                <a class="btn btn-default" href="<?=base_url('certificate/readuut/' . $request->id); ?>"><i class="fas fa-book"></i> Lihat</a>
                            <?php else:?>
                            <a class="btn btn-default" href="<?=base_url('certificate/read/' . $request->id); ?>"><i class="fas fa-book"></i> Lihat</a>
                            <?php endif?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings').DataTable({
            pageLength: 10,
            order: [[ 3, "desc" ]],
        });
    })
</script>
<?= $this->endSection() ?>