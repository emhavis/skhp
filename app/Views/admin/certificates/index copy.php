<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Sertifikat</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pelacakan Sertifikat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Layanan</div>
        </div>
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nomor Booking</th>
                        <th>Nomor Pendaftaran</th>
                        <th>Jenis Layanan</th>
                        <th>Alat/Standard</th>
                        <th>Pengujian/Pemeriksaan</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($requests as $request):?>
                    <tr>
                        <td><?= $request->booking_no ?></td>
                        <td><?= $request->no_register ?></td>
                        <td><?= $request->jenis_layanan ?></td>
                        <td><?= $request->tool_code ?><br/><small><?= $request->tool_type ?></small></td>
                        <td><?= $request->inspection_type ?></td>
                        <td><?= $request->status ?></td>
                        <td>
                            <a class="btn btn-default" href="<?=base_url('certificate/read/' . $request->id); ?>"><i class="fas fa-book"></i> Lihat</a>
                            
                            <?php if($request->url_certificate_rev_download == null) { ?>
                            <!--
                            <a class="btn btn-default" href="/certificate/print/<?= $request->id ?>"><i class="fas fa-file-contract"></i> SKHP</a>
                            
                            <a class="btn btn-default" href="<?= $request->url_certificate_download ?>"><i class="fas fa-file-contract"></i> SKHP</a>
                            -->
                            <a class="btn btn-default" href="<?=base_url('survey/take/'. $request->id); ?>"><i class="fas fa-file-contract"></i> SKHP</a>
                            
                            <?php } else { ?>
                            <a class="btn btn-default" href="<?= $request->url_certificate_rev_download ?>"><i class="fas fa-file-contract"></i> SKHP</a>
                            <?php } ?>
                            <a class="btn btn-default" href="<?=base_url('certificate/download/' . $request->id); ?>"><i class="fas fa-paperclip"></i> Lampiran</a>
                            <!--
                            <a class="btn btn-default" href="<?= $request->url_attachment_download ?>"><i class="fas fa-paperclip"></i> Lampiran</a>
                            -->
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>