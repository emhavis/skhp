<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Laboratorium</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/laboratory">Laboratorium</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data Kuota</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Perubahan Kuota Laboratorium</div>
            <div>
                <a class="btn btn-default btn-sm" href="/closedevent"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="lab" action="<?= base_url('laboratory/edit/'.$lab->id); ?>" method="post">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-4 form-group">
                        <label>Laboratorium</label>
                        <input class="form-control" type="text" readonly
                            value="<?= $lab->nama_lab ?>"
                            name="nama_lab" id="nama_lab">
                    </div>
                    <div class="col-4 form-group">
                        <label>Jenis</label>
                        <input class="form-control" type="text" readonly
                            value="<?= $lab->tipe ?>"
                            name="tipe" id="tipe">
                    </div>
                    <div class="col-2 form-group">
                        <label>Kuota Online</label>
                        <input class="form-control" type="number" 
                            value="<?= $lab->quota_online ?>"
                            name="quota_online" id="quota_online">
                    </div>
                    <div class="col-2 form-group">
                        <label>Kuota Offline</label>
                        <input class="form-control" type="number" 
                            value="<?= $lab->quota_offline ?>"
                            name="quota_offline" id="quota_offline">
                    </div>
                </div>
               
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        $('#start_date, #end_date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('#event').validate({
            errorClass: "help-block",
            rules: {
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true,
                },
                name: {
                    required: true,
                },
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>