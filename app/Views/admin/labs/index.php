<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Laboratorium</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Laboratorium</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Laboratorium</div>
        </div>
        <div class="ibox-body">
            <div id="table">
            <table class="table table-striped table-bordered table-hover" id="labs" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Laboratorium</th>
                        <th>Kategori</th>
                        <th>Kuota Online</th>
                        <th>Kuota Offline</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($labs as $lab):?>
                    <tr>
                        <td><?= $lab->nama_lab ?></td>
                        <td><?= strtoupper($lab->tipe) ?></td>
                        <td><?= $lab->quota_online ?></td>
                        <td><?= $lab->quota_offline ?></td>
                        <td>
                            <a class="btn btn-default btn-sm" href="/laboratory/edit/<?= $lab->id ?>"><i class="fa fa-edit"></i> Edit</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

            <div id='calendar'></div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#labs').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>