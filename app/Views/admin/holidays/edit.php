<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Hari Libur Umum dan Keagamaan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Hari Libur</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Perubahan Hari Libur Umum dan Keagamaan</div>
            <div>
                <a class="btn btn-default btn-sm" href="/holiday"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="holiday" action="<?= base_url('holiday/edit/'.$holiday->id); ?>" method="post">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-4 form-group" >
                        <label>Tanggal</label>
                        <div class="input-group date" id="holiday_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                value="<?= date("d-m-Y", strtotime($holiday->holiday_date)); ?>"
                                name="holiday_date">
                        </div>
                    </div>
                    
                    <div class="col-4 form-group">
                        <label>Hari Libur</label>
                        <input class="form-control" type="text" 
                            value="<?= $holiday->name ?>"
                            name="name" id="name">
                    </div>
                </div>
               
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        $('#holiday_date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('#holiday').validate({
            errorClass: "help-block",
            rules: {
                holiday_date: {
                    required: true,
                },
                name: {
                    required: true,
                },
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>