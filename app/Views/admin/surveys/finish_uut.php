<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Survey</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Selesai Survei</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Selesai Survei</div>
        </div>
        <div class="ibox-body">
                <div class="row">
                    <div class="col-12">
                        <h3>Terima Kasih</h3>
                        <p class="lead">Terima kasih telah mengisi survey kami.</p>
                        <?php if($jenis_sertifikat != null): ?>
                        <p>Sertifikat akan otomatis ter-download. 
                            Jika sertifikat belum ter-download, silakan klik 
                            <a href="<?=base_url('/certificate/print_uut/'.$order_id.'/'.$jenis_sertifikat); ?>">link</a> berikut.</p>
                        <?php else: ?>
                        <p>Sertifikat akan otomatis ter-download. 
                            Jika sertifikat belum ter-download, silakan klik 
                            <a href="<?=base_url('/certificate/print_uut/'.$order_id); ?>">link</a> berikut.</p>
                        <?php endif; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script type="text/javascript">
$(function() {
    <?php if($jenis_sertifikat != null): ?>
    url = '<?= base_url(); ?>/certificate/print_uut/<?= $order_id ?>/<?= $jenis_sertifikat ?>';
    <?php else: ?>
    url = '<?= base_url(); ?>/certificate/print_uut/<?= $order_id ?>';
    <?php endif; ?>

    setTimeout(function() {
		
        location.assign(url);
	},201);
});
</script>

<?= $this->endSection() ?>