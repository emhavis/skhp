<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Survey</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Isi Survei</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title"><?= $survey->title ?></div>
        </div>
        <div class="ibox-body">
            <form id="holiday" action="<?= base_url('survey/input_uut') .'/'. $order_id .'/'. $jenis_sertifikat .'/'. $page->id; ?>" method="post">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-12">
                        <h3><?= $page->title ?></h3>
                        <p class="lead"><?= $page->description ?></p>
                    </div>
                </div>

                <?php foreach ($questions as $q): ?>
                <?php if($q->loc_applied == null || $q->loc_applied == $request->lokasi_pengujian): ?>
                
                <?php if($q->loc_applied == null || $q->loc_applied == 'dalam'): ?>
                <div class="row">
                    <div class="col-12 form-group" >
                        <label><?= $q->question ?></label>
                        <?php if ($q->question_type == 'text'): ?>
                        <input class="form-control" type="text" name="q<?= $q->id ?>" id="q<?= $q->id ?>" />
                        <?php endif; ?>
                        <?php if ($q->question_type == 'number'): ?>
                        <input class="form-control" type="number" name="q<?= $q->id ?>" id="q<?= $q->id ?>" />
                        <?php endif; ?>
                        <?php if ($q->question_type == 'textarea'): ?>
                        <textarea class="form-control" name="q<?= $q->id ?>" id="q<?= $q->id ?>"></textarea>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'radio'): ?>
                        <div class="check-list">
                            <?php $arr = explode(";", $q->possible_answers); ?>
                            <?php foreach ($arr as $a): ?>
                            <label class="ui-radio ui-radio-info">
                            <input type="radio" name="q<?= $q->id ?>" id="q<?= $q->id ?>" value="<?= $a ?>" />
                            <span class="input-span"></span><?= $a ?>
                            </label>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'data'): ?>
                            <?php if ($q->possible_answers == 'service_type'): ?>
                            <input class="form-control" type="text" name="q<?= $q->id ?>" id="q<?= $q->id ?>" readonly
                                value="<?= $service_type->service_type ?>" />
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php else: ?>
                <?php
                    $no = +substr($q->ref, -1) - 1;
                    if (count($petugas) > $no && $petugas[$no] != null) {
                ?>
                <div class="row">
                    <div class="col-12 form-group" >
                        <label><?= $q->question ?>: <?= $petugas[$no]->nama ?> (NIP: <?= $petugas[$no]->nip ?>) (untuk No Order: <?= $request->no_order ?>)</label><br/>
                        <span class="text-muted"><?= $q->description ?></span><br/><br/>
                        <?php if ($q->question_type == 'rating'): ?>
                        <div class="rating">
                            <input type="radio" name="q<?= $q->id ?>" value="5" id="q<?= $q->id ?>_5"><label for="q<?= $q->id ?>_5">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="4" id="q<?= $q->id ?>_4"><label for="q<?= $q->id ?>_4">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="3" id="q<?= $q->id ?>_3"><label for="q<?= $q->id ?>_3">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="2" id="q<?= $q->id ?>_2"><label for="q<?= $q->id ?>_2">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="1" id="q<?= $q->id ?>_1"><label for="q<?= $q->id ?>_1">☆</label>
                        </div>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'radio'): ?>
                        <div class="check-list">
                            <?php $arr = explode(";", $q->possible_answers); ?>
                            <?php foreach ($arr as $a): ?>
                            <label class="ui-radio ui-radio-info">
                            <input type="radio" name="q<?= $q->id ?>" id="q<?= $q->id ?>" value="<?= $a ?>" />
                            <span class="input-span"></span><?= $a ?>
                            </label>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php } ?>
                <?php endif; ?>

                <?php endif; ?>
                <?php endforeach; ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script type="text/javascript">
</script>

<?= $this->endSection() ?>