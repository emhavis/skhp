<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />

<style>
.rating {
    display: flex;
    flex-direction: row-reverse;
    justify-content: left;
}

.rating > input{ display:none;}

.rating > label {
    position: relative;
    width: 1em;
    font-size: 2vw;
    color: #fd7e14;
    cursor: pointer;
}
.rating > label::before{ 
    content: "\2605";
    position: absolute;
    opacity: 0;
}
.rating > label:hover:before,
.rating > label:hover ~ label:before {
    opacity: 1 !important;
}

.rating > input:checked ~ label:before{
    opacity:1;
}

.rating:hover > input:checked ~ label:before{ opacity: 0.4; }

</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Survey</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Isi Survei</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title"><?= $survey->title ?></div>
        </div>
        <div class="ibox-body">
            <form id="holiday" action="<?= base_url('survey/input_uut') .'/'. $order_id .'/'. $jenis_sertifikat .'/'. $page->id; ?>" 
                method="post" enctype="multipart/form-data">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-12">
                        <h3><?= $page->title ?></h3>
                        <p class="lead"><?= $page->description ?></p>
                        <h5>No Order: <?= $request->no_order ?></h5>
                    </div>
                </div>

                <?php
                    $not_luar = array_filter($questions, function($obj) {
                        return $obj->loc_applied != 'luar';
                    });
                    $luar = array_filter($questions, function($obj) {
                        return $obj->loc_applied == 'luar';
                    });
                ?>

                <?php foreach ($not_luar as $q): ?>
                <div class="row">
                    <div class="col-12 form-group" >
                        <label><?= $q->question ?></label><br/>
                        <span class="text-muted"><?= $q->description ?></span><br/><br/>
                        
                        <?php if ($q->question_type == 'text'): ?>
                        <input class="form-control" type="text" name="q<?= $q->id ?>" id="q<?= $q->id ?>" required />
                        <?php endif; ?>
                        <?php if ($q->question_type == 'number'): ?>
                        <input class="form-control" type="number" name="q<?= $q->id ?>" id="q<?= $q->id ?>" required />
                        <?php endif; ?>
                        <?php if ($q->question_type == 'textarea'): ?>
                        <textarea class="form-control" name="q<?= $q->id ?>" id="q<?= $q->id ?>" required></textarea>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'radio'): ?>
                        <div class="check-list">
                            <?php $arr = explode(";", $q->possible_answers); ?>
                            <?php foreach ($arr as $a): ?>
                            <label class="ui-radio ui-radio-info">
                            <input type="radio" name="q<?= $q->id ?>" id="q<?= $q->id ?>" value="<?= $a ?>" required />
                            <span class="input-span"></span><?= $a ?>
                            </label>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'data'): ?>
                            <?php if ($q->possible_answers == 'service_type'): ?>
                            <input class="form-control" type="text" name="q<?= $q->id ?>" id="q<?= $q->id ?>" readonly
                                value="<?= $service_type->service_type ?>" />
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endforeach; ?>

                <?php if ($page->loc_applied == 'luar'): ?>
                <?php foreach($petugas as $idx=>$p): ?>

                <br/>
                <h4>Petugas <?= $idx + 1 ?></h4>
                <h5><?= $p->nama ?> (NIP: <?= $p->nip ?>)</h5>
                <p class="lead">Tanggal: <?= date("d-m-Y", strtotime($doc->date_from)) ?> s/d <?= date("d-m-Y", strtotime($doc->date_to)) ?></p>
                
                <?php foreach ($luar as $q): ?>
                <?php 
                    $no = +substr($q->ref, -1) - 1;
                ?>
                <?php if ($idx == $no): ?>
                <div class="row">
                    <div class="col-12 form-group" >
                        <label><?= $q->question ?></label><br/>
                        <span class="text-muted"><?= $q->description ?></span><br/><br/>
                        <?php if ($q->question_type == 'rating'): ?>
                        <div class="rating">
                            <input type="radio" name="q<?= $q->id ?>" value="5" id="q<?= $q->id ?>_5" required><label for="q<?= $q->id ?>_5">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="4" id="q<?= $q->id ?>_4" required><label for="q<?= $q->id ?>_4">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="3" id="q<?= $q->id ?>_3" required><label for="q<?= $q->id ?>_3">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="2" id="q<?= $q->id ?>_2" required><label for="q<?= $q->id ?>_2">☆</label>
                            <input type="radio" name="q<?= $q->id ?>" value="1" id="q<?= $q->id ?>_1" required><label for="q<?= $q->id ?>_1">☆</label>
                        </div>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'radio'): ?>
                        <div class="check-list">
                            <?php $arr = explode(";", $q->possible_answers); ?>
                            <?php foreach ($arr as $a): ?>
                            <label class="ui-radio ui-radio-info">
                            <input type="radio" name="q<?= $q->id ?>" id="q<?= $q->id ?>" value="<?= $a ?>" 
                                class="<?= $q->related_id != null ? 'related' : '' ?>"
                                data-id="<?= $q->related_id != null ? $q->id : '' ?>" 
                                data-rel-id="<?= $q->related_id != null ? $q->related_id : '' ?>" 
                                data-rel-val="<?= $q->related_id != null ? $q->related_condition : '' ?>" required/>
                            <span class="input-span"></span><?= $a ?>
                            </label>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'text'): ?>
                            <?php if ($q->related_id == null): ?>
                        <input class="form-control" type="text" name="q<?= $q->id ?>" id="q<?= $q->id ?>" required />
                            <?php else: ?>
                        <input class="form-control <?= $q->related_id != null ? 'related-ref' : '' ?>" 
                            type="text" name="q<?= $q->id ?>" id="q<?= $q->id ?>" 
                            data-id="<?= $q->related_id != null ? $q->id : '' ?>" 
                            data-rel-id="<?= $q->related_id ?>" disabled required />
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($q->question_type == 'file'): ?>
                            <?php if ($q->related_id == null): ?>
                        <input class="form-control" type="file" accept="image/*" name="q<?= $q->id ?>" id="q<?= $q->id ?>" required />
                            <?php else: ?>
                        <input class="form-control <?= $q->related_id != null ? 'related-ref' : '' ?>" 
                            type="file" name="q<?= $q->id ?>" id="q<?= $q->id ?>" accept="image/*"
                            data-id="<?= $q->related_id != null ? $q->id : '' ?>" 
                            data-rel-id="<?= $q->related_id ?>" disabled required />
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>

                <?php endforeach; ?>
                <?php endif; ?>

                <?php if($next_page == null): ?>
                <hr/>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>
                        Survei ini diisi dengan penuh kesadaran dan pemahaman peraturan perundangan dalam mendukung layanan SUML yang bersih, akuntabel dan profesional.
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group" >
                        <label>
                        NB:<br/>
                        Jika menemukan PUNGLI dan GRATIFIKASI yang dilakukan oleh petugas, anda dapat melaporkan melalui WBS - Kemendag melalui link 
                        [<a href="https://itjen.kemendag.go.id/modules/pelaporan/wbs" target="_blank">https://itjen.kemendag.go.id/modules/pelaporan/wbs</a>].
                        </label>
                    </div>
                </div>
                <?php endif; ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan <?= $next_page != null ? ' dan Lanjut Ke Halaman ' . $next_page->sequence : '' ?></button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script type="text/javascript">
$(function() {
    $('.related').click(function() {
        var rel_id = $(this).data('id');
        var rel_cond = $(this).data('rel-val');

        var val = $(this).val();
        var arrRef = rel_cond.split(';');

        var find = arrRef.indexOf(val);
        var obj = $('.related-ref[data-rel-id="' + rel_id + '"]');
        if (find >= 0) {
            obj.prop('disabled', false);
            obj.prop('required', true);
        } else {
            obj.prop('disabled', true);
            obj.prop('required', false);
        }
    });
});
</script>

<?= $this->endSection() ?>