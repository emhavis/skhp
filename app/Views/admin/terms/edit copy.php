<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Pelacakan Layanan</a>
        </li>
        <li class="breadcrumb-item">Daftar</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Kaji Ulang Permintaan: <?= $item->uttp_type ?> - <?= $item->tool_brand ?>/<?= $item->tool_model ?> (<?= $item->serial_no ?>)
            </div>
            <div>
                <a class="btn btn-default btn-sm" href="/tracking/read/<?= $request->booking_id ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('term/edit/'.$item->id); ?>" method="post">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-12">
                        <h4>A. Kemampuan dan Sumber Daya Instalasi Uji <?= $type->kelompok ?></h4>
                        <p><?= $type->kemampuan ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>B. Legalitas</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <p>Apakah <?= $type->kelompok ?> yang diajukan adalah yang diatur dalam Peraturan Izin Tipe?</p>
                    </div>
                    <div class="col-4">
                        <label class="ui-checkbox ui-checkbox-success">
                            <input type="checkbox" name="legalitas" id="legalitas" checked <?= $term->legalitas == 't' ? 'checked' : '' ?> >
                            <span class="input-span"></span>Ya
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>C. Metode dan Persyaratan yang Harus Dipenuhi</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5>C.1. Metode</h5>
                        <p><?= $type->oiml_name ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5>C.2. Jenis pengujian (test items) untuk <?= $type->kelompok ?> & SLA per Alat (dalam Kondisi Normal)</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-bordered table-hover" id="sla" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Pengujian</th>
                                    <th>SLA (jam)</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php $i = 0; ?>
                                <?php foreach ($slas as $sla): ?>
                                <?php $i++; ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $sla->jenis_pengujian ?></td>
                                    <td><?= $sla->sla ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3">TOTAL WAKTU PENGUJIAN PER ALAT: <?= $type->kaji_sla ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <p>Apakah setuju dengan metode, SLA dan jenis pengujian yang ditetapkan instalasi uji?</p>
                    </div>
                    <div class="col-4">
                        <label class="ui-checkbox ui-checkbox-success">
                            <input type="checkbox" name="metode" id="metode" <?= $term->metode == 't' ? 'checked' : '' ?> >
                            <span class="input-span"></span>Ya
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5>C.3. Syarat dan Ketentuan</h5>
                        <p>Untuk dapat diuji di instalasi, <?= $type->kelompok ?> yang diajukan HARUS memenuhi terhadap SELURUH ketentuan-ketentuan pemeriksaan sesuai <?= $type->oiml_name ?> sebagai berikut:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-bordered table-hover" id="klausa" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Syarat dan Ketentuan</th>
                                    <th>Persetujuan</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php foreach ($klausas as $klausa): ?>
                                <?php if ($klausa->is_header == 't'): ?>
                                <tr>
                                    <td colspan="3"><strong><?= $klausa->klausa ?></strong></td>
                                </tr>
                                <?php else: ?>
                                <tr>
                                    <td><?= $klausa->nomor ?></td>
                                    <td><?= $klausa->klausa ?></td>
                                    <td>
                                        <label class="ui-checkbox ui-checkbox-success">
                                            <input type="checkbox" name="klausa_<?= $klausa->id ?>" id="klausa_<?= $klausa->id ?>"  >
                                            <span class="input-span"></span>Setuju
                                        </label>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>D. Service Level Agreement</h4>
                        <p>Lama pengujian izin tipe untuk 1 (satu) tipe <?= $type->kelompok ?> mengacu kepada Klausul-Klausul atau kepada Peraturan SLA yang berlaku (maksimum 90 hari kerja sejak order terdaftar) dengan memperhitungkan antrian.</p>
                    </div>
                </div>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Tanggal</label>
                        <input type="text" class="form-control" 
                            value="<?= date("d M Y") ?>" readonly />
                    </div>
                </div>

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Setuju</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>