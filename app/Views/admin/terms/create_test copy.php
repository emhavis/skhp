<?= $this->extend('template/main') ?>


<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('booking'); ?>">Pelacakan Layanan</a>
        </li>
        <li class="breadcrumb-item">Daftar</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Kaji Ulang Permintaan: <?= $item->uttp_type ?> - <?= $item->tool_brand ?>/<?= $item->tool_model ?> (<?= $item->serial_no ?>)
            </div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('tracking/read/'. $request->booking_id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('term/create/'.$item->id); ?>" method="post">
                <?= csrf_field() ?>
                
                <div class="row">
                    <div class="col-12">
                        <h4>A. Kemampuan dan Sumber Daya Instalasi Uji Meter Kadar Air(MKA)</h4>
                        <p>Instalasi uji MKA dalam kantor di Direktorat Metrologi hanya untuk MKA dengan prinsip kerja berjenis kapasitansi dan resistansi yang penunjukannya digital dengan komoditas berupa biji-bijian. Metode yang digunakan dapat berupa metode pengeringan oven atau master meter</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>B. Legalitas</h4>
                        <p>Meter Kadar Air yang diajukan adalah MKA yang diatur dalam :</p>
                        <ul>
                            <li>1.	Peraturan Menteri Perdagangan No. 67 Tahun 2018 Tentang UTTP yang Wajib Ditera dan Ditera Ulang;</li>
                            <li>2.	Peraturan Menteri Perdagangan No. 68 Tahun 2018 Tentang Tera dan Tera Ulang UTTP; dan</li>
                            <li>3.	Surat Keputusan Direktur Jenedral Perlindungan Konsumen dan Tertib Niaga (PKTN) No. 122 Tahun 2020 Tentang Syarat Teknis Meter Kadar Air (MKA)</li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-12">
                        <h4>C.1 Metode:  Syarat Teknis MKA (mengacu pada OIML R59:2016)</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h5>C.1. Metode</h5>
                        <p>OIML R 46-1/-2 Edisi 2012 tentang Meter energi listrik aktif.</p>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-12">
                        <h5>C.2 Jenis pengujian (test items) dan SLA (per alat dalam kondisi normal dan tidak ada antrian). </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Item Pengujian</th>
                                    <th>SLA (jam)</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>Pemeriksaan “Administrasi dan Visual”</td>
                                    <td>0,5</td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Persiapan Sampel.</td>
                                    <td>3</td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>Penentuan kadar air referensi  sampel.
                                        <ul>
                                            <li>Metode Oven</li>
                                            <li>Metode Master Meter</li>
                                        </ul>
                                    </td>
                                    <td>4 s.d. 18 (tergantung jenis dan jumlah komoditas yang diuji) 0,5 (per komoditas)</td>
                                </tr>
                                <tr>
                                    <td>4.</td>
                                    <td> Pengujian Kebenaran
                                    </td>
                                    <td>0,5</td>
                                </tr>
                                <tr>
                                    <td>5.</td>
                                    <td>Pengujian kemampuan ulang
                                    </td>
                                    <td>0,5</td>
                                </tr>
                                <!-- <tr>
                                    <td>6.</td>
                                    <td>Verifikasi laporan pengujian “Kompabilitas Elektromagnetik”
                                        Pelepasan Elektrostatik; Medan Elektromagnetik RF Terradiasi; Transien Cepat; Gangguan RF Terkonduksi; Sentakan Percikan; Gelombang Osilasi Terredam dan Gangguan Radio.
                                    </td>
                                    <td>4,0</td>
                                </tr>
                                <tr>
                                    <td>7.</td>
                                    <td>Verifikasi laporan pengujian “Pengaruh Klimatik”
                                    Panas-Kering; Dingin; dan Siklus Uap Panas.
                                    </td>
                                    <td>1,5</td>
                                </tr>
                                <tr>
                                    <td>8.</td>
                                    <td>Verifikasi laporan pengujian “Mekanik”
                                    Palu Berpegas; Kejut; Vibrasi; Ketahanan Panas Dan Api; Penetrasi Debu Dan Air; dan Radiasi Matahari (jika ada).
                                    </td>
                                    <td>3,0</td>
                                </tr> -->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2">Total Waktu Pengujian Per-Meter adalah 2 Hari 2 Jam Kerja</th>
                                    <th>18,0</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <h5>C.3. Syarat dan Ketentuan</h5>
                        <p>Permohonan Persetujuan Tipe Meter kWh, untuk dapat diverifikasi harus memenuhi semua ketentuan sebagai berikut:</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-bordered" id="klausa" cellspacing="0" width="100%">
                            <thead>
                            </thead> 
                            <tbody>
                                <tr>
                                    <th>No</th>
                                    <th>Administrasi dan Dokumentasi</th>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td><p>Memiliki Sertifikat OIML dilengkapi laporan hasil pengujian teknis atau Memiliki laporan pengujian tipe dari instalasi Pihak Ketiga Dalam Negeri atau Luar Negeri yang terakreditasi ISO 17025.</p>
                                        <p><strong>Jika Tidak</strong>, maka dilakukan <strong>&ldquo;Kunjungan Instalasi&rdquo;</strong> atau <strong>&ldquo;Mempresentasikan Laporan Pengujian termasuk </strong><strong>Standar Ukuran yang digunakan&rdquo;</strong> dengan jadwal yang ditentukan oleh Balai Pengujian UTTP Direktorat Metrologi.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Dalam hal produk Dalam Negeri tidak dapat dilakukan Pengujian Tipe menggunakan instalasi Pihak Ketiga Dalam Negeri, maka
                                        Pengujian Tipe dilakukan pada instalasi uji di Luar Negeri yang
                                        ditentukan oleh pemohon dengan disaksikan (witness)  oleh Direktorat Metrologi dengan pembiayaan sepenuhnya ditanggung
                                        oleh Pemohon sesuai dengan ketentuan peraturan perundangan yang berlaku
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>dentitas Meter kWh harus sesuai dengan Keputusan Dirjen PKTN Nomor 161 Tahun 2019 tentang Syarat Teknis Meter kWh. Jika belum sesuai maka harus dilakukan penggantian oleh pabrikan.</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Meter kWh <strong>&ldquo;Prabayar&rdquo;</strong> dengan menggunakan &ldquo;Token atau sistem data satu arah&rdquo;, harus dilengkapi <strong>&ldquo;Sertifikat STS&rdquo;</strong> yang diperuntukkan Nama Perusahaan dan Tipe.</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Meter kWh dilengkapi dengan tempat penyegelan.</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td><p>Meter kWh dianggap famili, hanya jika dengan kapasitas yang sama dalam hal Arus Dasar dan Maksimum.</p>          
                                        <p><strong>Catatan: </strong>Tidak mengenal peruntukan &ldquo;Down Grade&rdquo;.</p>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>D. Service Level Agreement</h4>
                        <p>Lama verifikasi laporan pengujian untuk 1 (satu) tipe Meter kWh mengacu pada C.2 atau Peraturan SLA yang berlaku (maksimum 90 hari kerja sejak order terdaftar dengan memperhitungkan antrian).</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h4>E. Lain-lain/Penunjang Pengujian</h4>
                        <p>Pemohon secara sadar dan penuh tanggung jawab memahami dan menyetujui seluruh isi kaji ulang permintaan dari UPTP IV Direktorat Metrologi.</p>
                    </div>
                </div>

                <br/>
                <br/>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Tanggal</label>
                        <input type="text" class="form-control" 
                            value="<?= date("d M Y") ?>" readonly />
                    </div>
                </div>

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Setuju</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

