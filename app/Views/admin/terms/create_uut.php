<?= $this->extend('template/main') ?>


<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('booking'); ?>">Pelacakan Layanan</a>
        </li>
        <li class="breadcrumb-item">Daftar</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Kaji Ulang Permintaan: <?= $item->uut_type ?> - <?= $item->tool_brand ?>/<?= $item->tool_model ?> (<?= $item->serial_no ?>)
            </div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('tracking/read/'. $request->booking_id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('term_uut/create/'.$item->id); ?>" method="post">
                <?= csrf_field() ?>
                
                <?= $termKaji->kaji_ulang ?>
                
                <br/>
                <br/>
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" rows="6" cols="12"class="form-control"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Tanggal</label>
                        <input type="text" class="form-control" 
                            value="<?= date("d M Y") ?>" readonly />
                    </div>
                </div>

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Setuju</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

