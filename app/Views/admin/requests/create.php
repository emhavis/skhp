<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Konfirmasi Pendaftaran Ulang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/request">Pendaftaran Ulang Layanan</a>
        </li>
        <li class="breadcrumb-item">Konfirmasi Pendaftaran Ulang</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran Ulang</div>
            <div>
                <a class="btn btn-default btn-sm" href="/request"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('request/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <select class="form-control" name="booking_id" id="booking_id">
                            <option value="">-- Pilih No Booking --</option>
                            <?php foreach ($bookings as $booking): ?>
                            <option value="<?= $booking->id ?>"><?= $booking->booking_no ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" >
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" >
                    </div>
                            -->
                    <div class="col-6 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            id="label_sertifikat">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly
                            id="addr_sertifikat"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Pendaftar</label>
                        <input class="form-control" type="text" name="pic_name" readonly
                            id="pic_name">
                    </div>
                    <div class="col-3 form-group">
                        <label>Jenis Tanda Pengenal</label>
                        <input class="form-control" type="text" name="id_type_id" readonly
                            id="id_type_id">
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor ID Tanda Pengenal</label>
                        <input class="form-control" type="text" name="pic_id_no" readonly
                            id="pic_id_no">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Telepon</label>
                        <input class="form-control" type="text" name="pic_phone_no" readonly
                            id="pic_phone_no">
                    </div>
                    <div class="col-3 form-group">
                        <label>Alamat Email</label>
                        <input class="form-control" type="text" name="pic_email" readonly
                            id="pic_email">
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan dan Lanjut</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        $('#booking_id').select2({
            theme: 'bootstrap4',
        });

        $("#booking_id").change(function(){
            $url = "<?php echo base_url('request/getBooking'); ?>/" + this.value;
            $.get($url,function(response){
                $("#for_sertifikat").val(response.for_sertifikat == 'lain' ? "Instansi Lain" : "Sendiri");
                $("#label_sertifikat").val(response.label_sertifikat);
                $("#addr_sertifikat").val(response.addr_sertifikat);

                $("#pic_name").val(response.full_name);
                if (response.kantor == "Perusahaan") {
                    $("#id_type_id").val("NIB");
                    $("#pic_id_no").val(response.nib);
                } else {
                    $("#id_type_id").val("NPWP");
                    $("#pic_id_no").val(response.npwp);
                }
                $("#pic_phone_no").val(response.phone);
                $("#pic_email").val(response.email);
                $("#jenis_layanan").val(response.jenis_layanan);


            });
        });
    })
</script>
<?= $this->endSection() ?>