<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Ulang Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pendaftaran Ulang Layanan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran Ulang Layanan</div>
            <div>
                <a class="btn btn-primary btn-sm" href="/request/create"><i class="fa fa-plus"></i> Konfirmasi Booking</a>
            </div>
        </div>
        <div class="ibox-body">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link active" href="#booking" data-toggle="tab">Booking Hari Ini</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#pendaftaran" data-toggle="tab">Pendaftaran</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#proses" data-toggle="tab">Proses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#selesai" data-toggle="tab">Selesai</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="booking">
                    <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Booking</th>
                                <th>Jenis Layanan</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemesan</th>
                                <th>Estimasi Total Biaya</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($bookings as $booking):?>
                            <tr>
                                <td><?= $booking->booking_no ?></td>
                                <td><?= $booking->jenis_layanan ?></td>
                                <td><?= $booking->label_sertifikat ?></td>
                                <td><?= $booking->full_name ?></td>
                                <td><?= number_format($booking->est_total_price, 2, ',', '.')  ?></td>
                                <td>
                                    
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="pendaftaran">
                    <table class="table table-striped table-bordered table-hover" id="todos" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Pendaftaran</th>
                                <th>Jenis Layanan</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemesan</th>
                                <th>Nominal Order</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($todos as $todo):?>
                            <tr>
                                <td><?= $todo->no_register ?></td>
                                <td><?= $todo->jenis_layanan ?></td>
                                <td><?= $todo->label_sertifikat ?></td>
                                <td><?= $todo->full_name ?></td>
                                <td><?= number_format($todo->total_price, 2, ',', '.')  ?></td>
                                <td>
                                    <?php if($todo->status_id == 1): ?>
                                    <a class="btn btn-default btn-sm" href="/request/edit/<?= $todo->id ?>"><i class="fa fa-edit"></i> Edit</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="proses">
                    <table class="table table-striped table-bordered table-hover" id="processings" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Pendaftaran</th>
                                <th>Jenis Layanan</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemesan</th>
                                <th>Nominal Order</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($processings as $processing):?>
                            <tr>
                                <td><?= $processing->no_register ?></td>
                                <td><?= $processing->jenis_layanan ?></td>
                                <td><?= $processing->label_sertifikat ?></td>
                                <td><?= $processing->full_name ?></td>
                                <td><?= number_format($processing->total_price, 2, ',', '.')  ?></td>
                                <td>
                                    
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="selesai">
                    <table class="table table-striped table-bordered table-hover" id="dones" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Pendaftaran</th>
                                <th>Jenis Layanan</th>
                                <th>Nama Pemilik</th>
                                <th>Nama Pemesan</th>
                                <th>Nominal Order</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dones as $done):?>
                            <tr>
                                <td><?= $done->no_register ?></td>
                                <td><?= $done->jenis_layanan ?></td>
                                <td><?= $done->label_sertifikat ?></td>
                                <td><?= $done->full_name ?></td>
                                <td><?= number_format($done->total_price, 2, ',', '.')  ?></td>
                                <td>
                                    
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>

            
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings, #todos, #processings, #dones').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>