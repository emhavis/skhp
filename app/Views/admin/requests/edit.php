<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Konfirmasi Pendaftaran Ulang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/request">Pendaftaran Ulang Layanan</a>
        </li>
        <li class="breadcrumb-item">Konfirmasi Pendaftaran Ulang</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran Ulang</div>
            <div>
                <a class="btn btn-default btn-sm" href="/request"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('request/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" readonly
                            id="booking_no" value="<?= $request->booking_no ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $request->jenis_layanan ?>">
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" >
                    </div>
                            -->
                    <div class="col-6 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $request->for_sertifikat ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            id="label_sertifikat" value="<?= $request->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly
                            id="addr_sertifikat"><?= $request->addr_sertifikat ?></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Pendaftar</label>
                        <input class="form-control" type="text" name="pic_name" readonly
                            id="pic_name" value="<?= $request->full_name ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Jenis Tanda Pengenal</label>
                        <input class="form-control" type="text" name="id_type_id" readonly
                            id="id_type_id" value="<?= $request->kantor == "Perusahaan" ? "NIB" : "NPWP" ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor ID Tanda Pengenal</label>
                        <input class="form-control" type="text" name="pic_id_no" readonly
                            id="pic_id_no" value="<?= $request->kantor == "Perusahaan" ? $request->nib : $request->npwp ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Telepon</label>
                        <input class="form-control" type="text" name="pic_phone_no" readonly
                            id="pic_phone_no" value="<?= $request->phone ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Alamat Email</label>
                        <input class="form-control" type="text" name="pic_email" readonly
                            id="pic_email" value="<?= $request->email ?>">
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan dan Lanjut</button>
            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Alat dan Pengujian</div>
            
        </div>
        <div class="ibox-body">
            <?php foreach ($items as $item):?>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?= $item->tool_code . ' (' . $item->quantity . ' pengujian)' ?></h4>
                    <div class="text-muted card-subtitle"><?= $item->type ?></div>

                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" readonly />
                        </div>
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="text" class="form-control" name="type_id" id="type_id" 
                                value="<?= $item->type ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $item->tool_made_in ?>" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->tool_brand ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                value="<?= $item->tool_type ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimal</label>
                            <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>
                    
                    <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Jenis Pengujian</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Harga Satuan</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $inspections_filtered = array_filter($inspections, function($arritem) use ($item) {
                                return $arritem->request_item_id == $item->id;
                            }); ?>
                            <?php foreach ($inspections_filtered as $inspection):?>
                            <tr>
                                <td><?= $inspection->inspection_type ?></td>
                                <td><?= $inspection->quantity ?></td>
                                <td><?= $inspection->unit ?></td>
                                <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Subtotal</th>
                                <th><?= number_format($item->subtotal, 2, ',', '.') ?></td>
                            </tr>
                        </tfoot>
                    </table>
                    
                    <div class="row">
                        <div class="col">
                            
                            <form action="<?= base_url('bookingitem/deleteuttp').'/'.$item->id; ?>" method="post">
                            <?= csrf_field() ?> 
                            <a class="btn btn-default btn-sm" href="/bookingiteminspection/index/<?= $item->request_id ?>/<?= $item->id ?>">
                                <i class="fa fa-edit"></i> Edit
                            </a>
                            <button type="submit" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i> Hapus</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>  
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        
    })
</script>
<?= $this->endSection() ?>