<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>SIMPEL UPTP IV</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url();?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/themify-icons/themify-icons.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url();?>/css/main.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/css/themes/orange-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <link href="<?=base_url();?>/css/auth-light.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content" style="max-width: 600px;">
        <div class="brand">
            <a class="link" href="index.html">SIMPEL UPTP IV</a>
        </div>
        <form id="register-form" action="<?= base_url('auth/register'); ?>" method="post">
            <?= csrf_field() ?>
            <h2 class="login-title">Registrasi</h2>

            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                    <?php foreach (session()->getFlashData('errors') as $field => $error) : ?>
                        <li><?= $error ?></li>
                    <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
                        
            <div id="spinner" class="d-none">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border text-info mb-2" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <select class="form-control" id="kantor" name="kantor" placeholder="Mendaftar Sebagai">
                            <option value="">Mendaftar Sebagai</option>
                            <option value="Pribadi">Pribadi/Perseorangan/Lainnya</option>
                            <option value="Perusahaan">Perusahaan</option>
                            <option value="Unit Metrologi Legal">Unit Metrologi Legal</option> 
                            <option value="Mahasiswa">Pelajar / Mahasiswa</option> 
                        </select>   
                        <input type="hidden" name="bentuk_usaha_id" id="bentuk_usaha_id" >
                        <input type="hidden" name="penanggung_jawab" id="penanggung_jawab" >
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <select class="form-control" id="uml_id" name="uml_id" placeholder="UML">
                            <option value="0">UML</option>
                            <?php foreach ($umls as $uml):?>
                            <option value="<?= $uml->id ?>"><?= $uml->nama_uml ?></option>
                            <?php endforeach;?>
                        </select>
                        <input type="hidden" name="nama_kepala_uml" id="nama_kepala_uml" >
                    </div>
                </div>
            </div>            
            
            <div class="row" id="rowNIBNPWP">
                <div class="col-6" id="colNIB">
                    <div class="input-group form-group">
                        <input class="form-control" type="text" name="nib" id="nib" placeholder="NIB" autocomplete="off">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="button" id="refresh_nib">
                                <i class="fa fa-redo"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="input-group form-group">
                        <input class="form-control" type="text" name="npwp" id="npwp" placeholder="NPWP" autocomplete="off" readonly>
                        <div class="input-group-btn" id="refreshNPWP">
                            <button class="btn btn-default" type="button" id="refresh_npwp">
                                <i class="fa fa-redo"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="rowKTP">
                <div class="input-group form-group">
                    <div class="col-6">
                        <input class="form-control" type="text" name="ktp" id="ktp" placeholder="NO KTP" autocomplete="off">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <input class="form-control" type="text" name="full_name" id="full_name"
                            placeholder="Nama lengkap" autocomplete="off" readonly>
                    </div>
                </div>
            </div>

            <div class="row" id="rowAlamat">
                <div class="col-12">
                    <div class="form-group">
                        <textarea class="form-control" name="alamat" id="alamat"
                            placeholder="Alamat" readonly></textarea>
                    </div>
                </div>
            </div>

            <div class="row" id="rowKota">
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="text" name="provinsi_id" id="provinsi"
                            placeholder="Provinsi" autocomplete="off" readonly>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="text" name="kota_id" id="kota_id"
                            placeholder="Kota" autocomplete="off" readonly>
                    </div>
                </div>
            </div>
            <div class="row" id="rowKota1">
                <div class="col-6">
                    <div class="form-group">
                        <input type="hidden" name="kota" id="kota_id">
                        <input class="form-control" type="text" name="kota" id="kota"
                            placeholder="Kota" autocomplete="off" readonly>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="text" name="provinsi" id="provinsi"
                            placeholder="Provinsi" autocomplete="off" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="email" name="email" id="email"
                            placeholder="Email" autocomplete="off">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="text" name="phone" id="phone"
                            placeholder="Nomor telepon" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" id="password" type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="password" name="password_confirmation" placeholder="Konfirmasi Password">
                    </div>
                </div>
            </div>
            <div class="form-group text-left">
                <label class="ui-checkbox ui-checkbox-info">
                    <input type="checkbox" name="agree">
                    <span class="input-span"></span>Saya setuju dengan persyaratan dan ketentuan yang berlaku
                </label>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block" type="submit">Daftar</button>
            </div>
            
            <div class="text-center">Sudah terdaftar?
                <a class="color-blue" href="<?= base_url('auth/login'); ?>">Login disini</a>
            </div>
        </form>

        <div class="modal" id="notFoundNIB" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Data identitas tidak ditemukan. Pastikan gunakan NIB yang valid.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="notFoundNPWP" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Data identitas tidak ditemukan. Pastikan gunakan NPWP yang valid.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="error" tabindex="-1" aria-labelledby="errorLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="errorLabel">Terjadi Kesalahan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Terjadi kesalahan. Ulangi kembali atau hubungi Administrator jika masih terjadi.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="<?=base_url();?>/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#kantor').select2({
                theme: 'bootstrap4',
            }).on('select2:select', function (e) {
                var data = e.params.data;
                if (data.id != "Unit Metrologi Legal") {
                    $('#uml_id').next().hide();
                    $('#rowAlamat').show();
                    //$('#rowKota').show();
                    $('#rowNIBNPWP').show();
                    $('#rowKTP').hide();
                    //$('#rowKota1').hide();

                    if (data.id == "Pribadi") {
                        $('#colNIB').hide();
                        $('#rowKTP').hide();
                        $("#npwp").attr("readonly", false); 
                        $('#refreshNPWP').show();
                        $('#npwp').show();
                        $('#rowKota1').hide();

                        var dataprov = $.map(<?= ($provinsis) ?>, function (obj) {
                                        obj.text = obj.nama || obj.name; // replace name with the property used for the text
                                        return obj;
                                    });
                        
                        $('#provinsi').select2({data: dataprov}).on('select2:select', function(e){
                            var dataprov = e.params.data;
                            $.post('<?= base_url(); ?>/auth/findKota/'+e.params.data.id, 
                                { 
                                    "<?= csrf_token() ?>": "<?= csrf_hash() ?>",
                                    "id": e.params.data.id,
                                }, 
                                function(res)
                                {
                                    var datas = $.map(res, function (obj) {
                                        obj.text = obj.nama || obj.name; // replace name with the property used for the text
                                        return obj;
                                    });
                                    
                                    $('#kota_id').select2({
                                        data : datas
                                    }).on('select2:select', function (e){
                                        var kota_id = e.params.data.id 
                                        $("kota_id").val(kota_id);
                                        console.log("Kota id :" + kota_id)
                                    });
                                });

                            var provid = e.params.data.id;
                            $("provinsi").val(provid);
                        });

                        $("#full_name").attr("readonly", false); 
                        $("#kota_id").attr("readonly", false); 
                        $("#provinsi").attr("readonly", false); 
                        $("#alamat").attr("readonly", false); 
                        $('#rowKota').show();
                    } else if (data.id == "Mahasiswa") {
                        $('#rowKTP').show();
                        $('#colNIB').hide();
                        $("#npwp").hide(); 
                        $('#refreshNPWP').hide();
                        var dataprov = $.map(<?= ($provinsis) ?>, function (obj) {
                                        obj.text = obj.nama || obj.name; // replace name with the property used for the text
                                        return obj;
                                    });
                        
                        $('#provinsi').select2({data: dataprov}).on('select2:select', function(e){
                            var dataprov = e.params.data;
                            $.post('<?= base_url(); ?>/auth/findKota/'+e.params.data.id, 
                                { 
                                    "<?= csrf_token() ?>": "<?= csrf_hash() ?>",
                                    "id": e.params.data.id,
                                }, 
                                function(res)
                                {
                                    var datas = $.map(res, function (obj) {
                                        obj.text = obj.nama || obj.name; // replace name with the property used for the text
                                        return obj;
                                    });
                                    
                                    $('#kota_id').select2({
                                        data : datas
                                    }).on('select2:select', function (e){
                                        var kota_id = e.params.data.id 
                                        $("kota_id").val(kota_id);
                                        console.log("Kota id :" + kota_id)
                                    });
                                });

                            var provid = e.params.data.id;
                            $("provinsi").val(provid);
                        });
                        // $('#kota_id1').select2().on('select2:select', function(e){
                        //     var dataprov = e.params.data;
                        //     $.post('<?= base_url(); ?>/auth/findProv', 
                        //         { 
                        //             "<?= csrf_token() ?>": "<?= csrf_hash() ?>",
                        //             "id": e.params.data.id,
                        //         }, 
                        //         function(res)
                        //     {
                        //         console.log(res)
                        //         $('#provinsi').val(res.nama);
                        //         $('#kota_id').val(data.kota_id);
                        //     });
                        //     // $('#provinsi').attr('placeholder',"H");
                        //     // $('#provinsi').select2();
                        // });
                        $("#full_name").attr("readonly", false); 
                        $("#kota_id").attr("readonly", false); 
                        // $("#kota").attr("readonly", false); 
                        $("#provinsi").attr("readonly", false); 
                        $("#alamat").attr("readonly", false); 
                        // $('#rowKota1').show();
                        $('#rowKota').show();
                    }else {
                        $('#colNIB').show();
                        $("#npwp").attr("readonly", true); 
                        $('#rowKota1').hide();
                        $('#rowKota').hide();
                        $('#refreshNPWP').hide();
                    }
                } else {
                    $('#uml_id').next().show();
                    $('#rowAlamat').hide();
                    $('#rowKota').hide();
                    $('#rowNIBNPWP').hide();
                    $('#rowKTP').hide();
                }

                $('#full_name').val(null);
                $('#npwp').val(null);
                $('#ktp').val(null);
                $('#alamat').val(null);
                $('#phone').val(null);
                $('#email').val(null);
                $('#kota_id').val(null);
                $('#kota').val(null);
                $('#provinsi').val(null);
            });
            $('#rowKTP').hide();
            $('#rowNIBNPWP').hide();
            $('#refreshNPWP').hide();
            $('#rowAlamat').hide();
            $('#rowKota').hide();
            $('#rowKota1').hide();
            $('#uml_id').select2({
                theme: 'bootstrap4',
            }).on('select2:select', function (e) {
                console.log(e.params.data.id);
                $.post('<?= base_url(); ?>/auth/findUmlById', 
                    { 
                        "<?= csrf_token() ?>": "<?= csrf_hash() ?>",
                        "id": e.params.data.id,
                    }, 
                    function(res)
                {
                    console.log(res);
                    var data = res;
                    $('#full_name').val(data.nama_uml);
                    $('#npwp').val(null);
                    $('#alamat').val(data.alamat);
                    $('#phone').val(data.no_tlp);
                    $('#email').val(data.email);
                    $('#kota_id').val(data.id_kota);
                    $('#kota').val(data.kota);
                    $('#provinsi').val(data.provinsi);
                    $('#nama_kepala_uml').val(data.nama_kepala_uml);
                });
            });
            /*
            $('#kota_id').select2({
                theme: 'bootstrap4',
            });
            */
            $('#register-form').validate({
                errorClass: "help-block",
                rules: {
                    full_name: {
                        required: true,
                        minlength: 2
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        confirmed: true
                    },
                    password_confirmation: {
                        equalTo: password
                    },
                    agree: {
                        required: true,
                    }
                },
                messages: {
                    full_name: 'Nama lengkap wajib terisi. Silakan gunakan NIB/NPWP untuk mencari data atau memilih UML yang ada.',
                    email: 'Email wajib diisi dengan format yang benar',
                    password: 'Password wajib diisi',
                    password_confirmation: 'Konfirmasi password tidak sama',
                    agree: 'Persetujuan diperlukan',
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });

            function check_data_nib(val) {
                $.post('<?= base_url(); ?>/auth/check_data/nib', 
                { 
                    "<?= csrf_token() ?>": "<?= csrf_hash() ?>",
                    "value": val,
                }, 
                function(res)
                {
                    if (res.kode == 200) {
                        var data = res.data;
                        $('#full_name').val(data.full_name);
                        $('#npwp').val(data.npwp);
                        $('#alamat').val(data.alamat);
                        $('#phone').val(data.telepon);
                        $('#email').val(data.email);
                        $('#kota_id').val(data.kota_id);
                        $('#kota').val(data.kota);
                        $('#provinsi').val(data.provinsi);
                        $('#bentuk_usaha_id').val(data.bentuk_badan_usaha_id);
                        $('#penanggung_jawab').val(data.penanggung_jawab);
                    } 
                    
                }).done(function() {
                    $('#spinner').addClass('d-none');
                }).fail(function(xhr, status, error) {
                    $('#spinner').addClass('d-none');

                    var res = xhr.responseJSON;
                    if (res.kode != null && res.kode == 404) {
                        $('#notFoundNIB').modal('show');
                    } else {
                        $('#error').modal('show');
                    }
                });
            };

            function check_data_npwp(val) {
                $.post('<?= base_url(); ?>/auth/check_data/npwp', 
                        { 
                            "<?= csrf_token() ?>": "<?= csrf_hash() ?>",
                            "value": val,
                        }, 
                        function(res)
                    {
                        if (res.kode == 200) {
                            var data = res.data;
                            $('#full_name').val(data.full_name);
                            $('#alamat').val(data.alamat);
                            $('#email').val(data.email);
                            $('#kota_id').val(data.kota_id);
                            $('#kota').val(data.kota);
                            $('#provinsi').val(data.provinsi);
                            $('#bentuk_usaha_id').val(data.bentuk_badan_usaha_id);
                        }
                    }).done(function() {
                        $('#spinner').addClass('d-none');
                    }).fail(function(xhr, status, error) {
                        $('#spinner').addClass('d-none');
                        
                        var res = xhr.responseJSON;
                        if (res.kode != null && res.kode == 404) {
                            $('#notFoundNPWP').modal('show');
                        } else {
                            $('#error').modal('show');
                        }
                    });
            }

            $('#nib').change(function(e) {
                e.preventDefault();
                $('#spinner').removeClass('d-none');

                var val = $(this).val();
            
                check_data_nib(val);
            });

            $('#npwp').change(function(e) {
                e.preventDefault();
                $('#spinner').removeClass('d-none');

                var val = $(this).val();
                var kantor = $('#kantor').val();
                if (kantor == 'Pribadi') {
                    check_data_npwp(val);
                }
            });

            $('#refresh_nib').click(function(e) {
                e.preventDefault();
                $('#spinner').removeClass('d-none');

                var val = $('#nib').val();
            
                check_data_nib(val);
            });

            $('#refresh_npwp').click(function(e) {
                e.preventDefault();
                $('#spinner').removeClass('d-none');

                var val = $('#npwp').val();
                var kantor = $('#kantor').val();
                if (kantor == 'Pribadi') {
                    check_data_npwp(val);
                }
            });
        });
    </script>
</body>

</html>