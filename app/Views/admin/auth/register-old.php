<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>SKHP</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url();?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/themify-icons/themify-icons.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url();?>/css/main.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/css/themes/orange-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <link href="<?=base_url();?>/css/auth-light.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content" style="max-width: 600px;">
        <div class="brand">
            <a class="link" href="index.html">SKHP</a>
        </div>
        <form id="register-form" action="<?= base_url('auth/register'); ?>" method="post">
            <?= csrf_field() ?>
            <h2 class="login-title">Registrasi</h2>

            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                    <?php foreach (session()->getFlashData('errors') as $field => $error) : ?>
                        <li><?= $error ?></li>
                    <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <div class="form-group">
                <input class="form-control" type="text" name="full_name" placeholder="Nama lengkap" autocomplete="off">
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <select class="form-control" id="kantor" name="kantor" placeholder="Kantor">
                            <option value="Unit Metrologi Legal">Unit Metrologi Legal</option>
                            <option value="Unit Kerja Pengawasan Kemetrologian">Unit Kerja Pengawasan Kemetrologian</option>
                            <option value="Unit Pengelola Pasar">Unit Pengelola Pasar</option>
                            <option value="Direktorat Metrologi">Direktorat Metrologi</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>   
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <select class="form-control" id="uml_id" name="uml_id" placeholder="UML">
                            <option value="0">UML</option>
                            <?php foreach ($umls as $uml):?>
                            <option value="<?= $uml->id ?>"><?= $uml->uml_name ?></option>
                            <?php endforeach;?>
                        </select>
                        <select class="form-control" id="kota_id" name="kota_id" placeholder="Kota">
                            <option value="0">Kabuapten/kota</option>
                            <?php foreach ($kotas as $kota):?>
                            <option value="<?= $kota->id ?>"><?= $kota->nama ?></option>
                            <?php endforeach;?>
                        </select>   
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="nip" placeholder="NIP" autocomplete="off">
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <select class="form-control" id="id_type" name="id_type" placeholder="Jenis identitas">
                            <?php foreach ($types as $type):?>
                            <option value="<?= $type->id ?>"><?= $type->id_type_name ?></option>
                            <?php endforeach;?>
                        </select>   
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="text" name="id_no" placeholder="Nomor identitas" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="email" name="email" placeholder="Email" autocomplete="off">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="text" name="phone" placeholder="Nomor telepon" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input class="form-control" type="username" name="username" placeholder="Username" autocomplete="off">
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" id="password" type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input class="form-control" type="password" name="password_confirmation" placeholder="Konfirmasi Password">
                    </div>
                </div>
            </div>
            <div class="form-group text-left">
                <label class="ui-checkbox ui-checkbox-info">
                    <input type="checkbox" name="agree">
                    <span class="input-span"></span>Saya setuju dengan persyaratan dan ketentuan yang berlaku</label>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block" type="submit">Daftar</button>
            </div>
            
            <div class="text-center">Sudah terdaftar?
                <a class="color-blue" href="/auth/login">Login disini</a>
            </div>
        </form>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="<?=base_url();?>/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#id_type').select2({
                theme: 'bootstrap4',
                placeholder: "Jenis identitas",
            });
            $('#kantor').select2({
                theme: 'bootstrap4',
                placeholder: "Kantor",
            }).on('select2:select', function (e) {
                var data = e.params.data;
                if (data.id != "Unit Metrologi Legal") {
                    $('#kota_id').next().show();
                    $('#uml_id').next().hide();
                } else {
                    $('#kota_id').next().hide();
                    $('#uml_id').next().show();
                }
            });
            $('#uml_id').select2({
                theme: 'bootstrap4',
                placeholder: "UML",
            });
            $('#kota_id').select2({
                theme: 'bootstrap4',
                placeholder: "Kabupaten/kota",
            }).next().hide();
            $('#register-form').validate({
                errorClass: "help-block",
                rules: {
                    full_name: {
                        required: true,
                        minlength: 2
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    username: {
                        required: true,
                    },
                    password: {
                        required: true,
                        confirmed: true
                    },
                    password_confirmation: {
                        equalTo: password
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });
        });
    </script>
</body>

</html>