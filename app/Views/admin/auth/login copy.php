<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>SIMPEL UPTP IV</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url();?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/themify-icons/themify-icons.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url();?>/css/main.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/css/themes/orange-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <link href="<?=base_url();?>/css/auth-light.css" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content" style="max-width: 100%; padding: 10px 50px;">
        <div class="row">
            <div class="col">
                <div class="brand">
                    <a class="link" href="/">SIMPEL UPTP IV</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col" id="news">

                        <?php foreach($articles as $index=>$article): ?>

                        <div class="card collapse <?= $index == 0 ? 'show' : '' ?>" id="card<?= $article->id ?>">
                            <?php if($article->cover_image): ?>
                            <img class="card-img-top" src="<?= getenv('app.siksURL').'storage/'.$article->cover_image_path ?>" height="250px"/>
                            <?php endif; ?>
                            <div class="card-body">
                                <h3 class="card-title"><?= $article->title ?></h3>
                                <div><?= $article->content ?></div>
                            </div>
                        </div>

                        <?php endforeach; ?>

                        <ul class="pagination justify-content-end m-t-10">
                            <?php foreach($articles as $index=>$article): ?>    

                            <li class="page-item <?= $index == 0 ? 'active' : '' ?>" data-id="<?= $article->id ?>">
                                <a class="page-link" href="#"><?= $index + 1 ?></a>
                            </li>

                            <?php endforeach; ?>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-4">

                <form id="login-form" action="<?= base_url('auth/login'); ?>" method="post">
                    <?= csrf_field() ?>
                    <h3 class="login-title">Log in</h3>

                    <?php if (session()->getFlashData('errors')) : ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            <ul>
                            <li><?= session()->getFlashData('errors') ?></li>
                            </ul>
                        </div>
                    <?php endif ?>
                    
                    <div class="form-group">
                        <div class="input-group-icon right">
                            <div class="input-icon"><i class="fa fa-user"></i></div>
                            <input class="form-control" type="text" name="username" placeholder="NPWP (Pribadi) / NIB (Perusahaan) / Kode UML" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group-icon right">
                            <div class="input-icon"><i class="fa fa-lock font-16"></i></div>
                            <input class="form-control" type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                    <!--
                    <div class="form-group d-flex justify-content-between">
                        <label class="ui-checkbox ui-checkbox-info">
                            <input type="checkbox">
                            <span class="input-span"></span>Remember me</label>
                        <a href="forgot_password.html">Forgot password?</a>
                    </div>
                    -->
                    <div class="form-group">
                        <button class="btn btn-info btn-block" type="submit">Login</button>
                    </div>
                    
                    <div class="text-center">Belum terdaftar?
                        <a class="color-blue" href="<?= base_url('auth/register'); ?>">Registrasi disini</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="<?=base_url();?>/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script type="text/javascript">
        $(function() {
            $('#login-form').validate({
                errorClass: "help-block",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });

            $('#news .card').collapse({
                toggle: false
            });

            $('.pagination .page-item').click(function() {
                console.log($(this).data("id"));
                $pageLastActiveId = $('.pagination .page-item.active').data('id');
                $('.pagination .page-item.active').removeClass('active');
                $(this).addClass('active');
                $('#card' + $pageLastActiveId).collapse('hide');
                console.log($('#card' + $pageLastActiveId));
                $('#card' + $(this).data('id')).collapse('show');
                console.log($('#card' + $(this).data('id')));
            })
        });
    </script>
</body>

</html>