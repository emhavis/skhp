<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Profil Pengguna</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Profil Pengguna</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Profil Pengguna</div>
            <div>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#passwordModal"><i class="fa fa-lock"></i> Ubah Password</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="booking" action="<?= base_url('auth/profil'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Lengkap</label>    
                        <input class="form-control" type="text" name="full_name" id="full_name"
                            value="<?= $user->full_name ?>" autocomplete="off" >
                    </div>
                    <div class="col-6 form-group">
                        <label>Profil</label> 
                        <?php if ($user->kantor == 'Pribadi'): ?>
                        <input class="form-control" type="text" name="kantor" id="kantor"
                            value="Pribadi/Perseorangan" autocomplete="off" readonly>
                        <?php else: ?>
                        <input class="form-control" type="text" name="kantor" id="kantor"
                            value="<?= $user->kantor ?>" autocomplete="off" readonly>
                        <?php endif ?>
                    </div>
                </div>

                <?php if ($user->kantor == 'Perusahaan' || $user->kantor == 'Pribadi'): ?>
                <div class="row">
                    <?php if ($user->kantor == 'Perusahaan'): ?>
                    <div class="col-3 form-group">
                        <label>NIB</label>    
                        <input class="form-control" type="text" name="nib" id="nib"
                            value="<?= $user->nib ?>" autocomplete="off" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>NPWP</label>    
                        <input class="form-control" type="text" name="npwp" id="npwp"
                            value="<?= $user->npwp ?>" autocomplete="off" readonly>
                    </div>
                    <?php endif ?>
                    <?php if ($user->kantor == 'Pribadi'): ?>
                    <div class="col-3 form-group">
                        <label>NPWP</label>    
                        <input class="form-control" type="text" name="npwp" id="npwp"
                            value="<?= $user->npwp ?>" autocomplete="off" readonly>
                    </div>
                    <?php endif ?>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Alamat</label>   
                        <textarea class="form-control" name="alamat" id="alamat"
                            placeholder="Alamat" ><?= $user->alamat ?>
                        </textarea> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kabupaten/Kota</label>    
                        <input class="form-control" type="text" name="kota" id="kota"
                            value="<?= $kota->nama ?>" autocomplete="off" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Provinsi</label>    
                        <input class="form-control" type="text" name="provinsi" id="provinsi"
                            value="<?= $provinsi->nama ?>" autocomplete="off" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Email</label>    
                        <input class="form-control" type="text" name="email" id="email"
                            value="<?= $user->email ?>" autocomplete="off" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Telepon</label>    
                        <input class="form-control" type="text" name="phone" id="phone"
                            value="<?= $user->phone ?>" autocomplete="off" readonly>
                    </div>
                </div>
                <?php endif ?>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="passwordModal" tabindex="-1" aria-labelledby="passwordModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="passwordModalLabel">Ubah Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="change-password" action="<?= base_url('auth/profilePassword'); ?>" method="post">
        <?= csrf_field() ?>
        <div class="modal-body">
            <div class="row">
                <div class="col form-group">
                    <label>Password Lama</label>
                    <input type="password" class="form-control" name="password" id="password" />
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    <label>Password Baru</label>
                    <input type="password" class="form-control" name="new_password" id="new_password"/>
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    <label>Konfirmasi Password Baru</label>
                    <input type="password" class="form-control" name="re_password" id="re_password"/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        $('#change-password').validate({
            errorClass: "help-block",
            rules: {
                password: {
                    required: true,
                },
                new_password: {
                    required: true,
                },
                re_password: {
                    required: true,
                    equalTo: "#new_password"
                }
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });

        $('#news .card').collapse({
            toggle: false
        });

        $('.pagination .page-item').click(function() {
            console.log($(this).data("id"));
            $pageLastActiveId = $('.pagination .page-item.active').data('id');
            $('.pagination .page-item.active').removeClass('active');
            $(this).addClass('active');
            $('#card' + $pageLastActiveId).collapse('hide');
            console.log($('#card' + $pageLastActiveId));
            $('#card' + $(this).data('id')).collapse('show');
            console.log($('#card' + $(this).data('id')));
        })
    });
</script>
<?= $this->endSection() ?>