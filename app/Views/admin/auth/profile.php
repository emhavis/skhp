<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Profil Pengguna</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Profil Pengguna</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Profil Pengguna</div>
            <div>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#passwordModal"><i class="fa fa-lock"></i> Ubah Password</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="profile" action="<?= base_url('auth/profile'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Lengkap</label>    
                        <input class="form-control" type="text" name="full_name" id="full_name"
                            value="<?= $user->full_name ?>" autocomplete="off" >
                    </div>
                    <div class="col-6 form-group">
                        <label>Profil</label> 
                        <?php if ($user->kantor == 'Pribadi'): ?>
                        <input class="form-control" type="text" name="kantor" id="kantor"
                            value="Pribadi/Perseorangan" autocomplete="off" readonly>
                        <?php else: ?>
                        <input class="form-control" type="text" name="kantor" id="kantor"
                            value="<?= $user->kantor ?>" autocomplete="off" readonly>
                        <?php endif ?>
                    </div>
                </div>

                <?php if ($user->kantor == 'Perusahaan' || $user->kantor == 'Pribadi' || $user->kantor == 'Mahasiswa'): ?>
                <div class="row">
                    <?php if ($user->kantor == 'Perusahaan'): ?>
                    <div class="col-3 form-group">
                        <label>NIB</label>    
                        <input class="form-control" type="text" name="nib" id="nib"
                            value="<?= $user->nib ?>" autocomplete="off" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>NPWP</label>    
                        <input class="form-control" type="text" name="npwp" id="npwp"
                            value="<?= $user->npwp ?>" autocomplete="off" readonly>
                    </div>
                    <?php endif ?>
                    <?php if ($user->kantor == 'Pribadi'): ?>
                    <div class="col-3 form-group">
                        <label>NPWP</label>    
                        <input class="form-control" type="text" name="npwp" id="npwp"
                            value="<?= $user->npwp ?>" autocomplete="off" readonly>
                    </div>
                    <?php endif ?>

                    <?php if ($user->kantor == 'Mahasiswa'): ?>
                    <div class="col-3 form-group">
                        <label>KTP</label>    
                        <input class="form-control" type="text" name="ktp" id="ktp"
                            value="<?= $user->ktp ?>" autocomplete="off">
                    </div>
                    <?php endif ?>

                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Alamat</label>   
                        <textarea class="form-control" name="alamat" id="alamat"
                            placeholder="Alamat" ><?= $user->alamat ?>
                        </textarea> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Provinsi</label>
                        <select class="form-control" name="provinsi_id" id="provinsi_id">
                            <?php foreach($provinsiList as $provinsi): ?>
                            <option value="<?= $provinsi->id ?>" <?= $provinsi->id == $kota->provinsi_id ? 'selected' : '' ?>><?= $provinsi->nama ?></option>
                            <?php endforeach ?> 
                        </select>
                    </div>
                    <div class="col-3 form-group">
                        <label>Kabupaten/Kota</label>    
                        <select class="form-control" name="kota_id" id="kota_id">
                            <option value="<?= $user->kota_id ?>" selected ><?= $kota->nama ?></option>
                        </select>
                    </div>
                    <div class="col-3 form-group">
                        <label>Email</label>    
                        <input class="form-control" type="text" name="email" id="email"
                            value="<?= $user->email ?>" autocomplete="off" >
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Telepon</label>    
                        <input class="form-control" type="text" name="phone" id="phone"
                            value="<?= $user->phone ?>" autocomplete="off" >
                    </div>
                </div>
                <?php endif ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="passwordModal" tabindex="-1" aria-labelledby="passwordModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="passwordModalLabel">Ubah Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="change-password" action="<?= base_url('auth/profilePassword'); ?>" method="post">
        <?= csrf_field() ?>
        <div class="modal-body">
            <div class="row">
                <div class="col form-group">
                    <label>Password Lama</label>
                    <input type="password" class="form-control" name="password" id="password" />
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    <label>Password Baru</label>
                    <input type="password" class="form-control" name="new_password" id="new_password"/>
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    <label>Konfirmasi Password Baru</label>
                    <input type="password" class="form-control" name="re_password" id="re_password"/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#change-password').validate({
            errorClass: "help-block",
            rules: {
                password: {
                    required: true,
                },
                new_password: {
                    required: true,
                },
                re_password: {
                    required: true,
                    equalTo: "#new_password"
                }
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });

        $('#provinsi_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            console.log(e);
            $('#kota_id').empty().trigger('change');
        });

        $('#kota_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var provinsi_id = $('#provinsi_id').val();
                    return '<?= base_url(); ?>/auth/findKota/'+provinsi_id;
                },
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    return {
                        q: params.term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
                
            }
        });

        $('#profile').validate({
            errorClass: "help-block",
            rules: {
                full_name: {
                    required: true,
                },
                alamat: {
                    required: true,
                },
                provinsi_id: {
                    required: true,
                },
                kota_id: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                phone: {
                    required: true,
                },
            },
            messages: {
                full_name: 'Nama lengkap wajib diisi',
                alamat: 'Alamat wajib diisi',
                provinsi_id: 'Provinsi wajib dipilih',
                kota_id: 'Kabupaten/kota wajib dipilih',
                email: 'Email wajib diisi dengan format yang benar',
                phone: 'Telepon wajib diisi',
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
    });
</script>
<?= $this->endSection() ?>