<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>SIMPEL UPTP IV</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url();?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/themify-icons/themify-icons.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url();?>/css/main.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/css/themes/orange-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <link href="<?=base_url();?>/css/auth-light.css" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content" style="max-width: 600px;">
        <div class="brand">
            <a class="link" href="index.html">SIMPEL UPTP IV</a>
        </div>
       
        <form id="register-form" >
        <h2 class="login-title">Aktivasi</h2>

        <?php if (session()->getFlashData('errors')) : ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <ul>
                <?php foreach (session()->getFlashData('errors') as $field => $error) : ?>
                    <li><?= $error ?></li>
                <?php endforeach ?>
                </ul>
            </div>
        <?php endif ?>

        <div class="text-center">Anda telah berhasil melakukan aktivasi Akun.
            <br/><a class="color-blue" href="<?= base_url('auth/login'); ?>">Login disini</a>
        </div>
        </form>
    </div>
    
</body>

</html>