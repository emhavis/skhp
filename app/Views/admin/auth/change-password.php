<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>SIMPEL UPTP IV</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url();?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/themify-icons/themify-icons.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url();?>/css/main.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/css/themes/orange-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <link href="<?=base_url();?>/css/auth-light.css" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content" style="max-width: 400px;">
        <div class="row">
            <div class="col">
                <div class="brand">
                    <a class="link" href="/">SIMPEL UPTP IV</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">

                <form id="login-form" action="<?= base_url('auth/changePassword/' . $user->token ); ?>" method="post">
                    <?= csrf_field() ?>
                    <h3 class="login-title">Ubah Password</h3>

                    <?php if (session()->getFlashData('errors')) : ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            <ul>
                            <li><?= session()->getFlashData('errors') ?></li>
                            </ul>
                        </div>
                    <?php endif ?>
                    
                    <div class="form-group">
                        <div class="input-group-icon right">
                            <div class="input-icon"><i class="fa fa-lock font-16"></i></div>
                            <input class="form-control" type="password" name="new_password" 
                            id="new_password" placeholder="Password Baru">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group-icon right">
                            <div class="input-icon"><i class="fa fa-lock font-16"></i></div>
                            <input class="form-control" type="password" name="re_password" 
                            id="re_password" placeholder="Konfirmasi Password Baru">
                        </div>
                    </div>


                    <div class="form-group">
                        <button class="btn btn-info btn-block" type="submit">Ubah Password</button>
                    </div>
                    
                </form>

            </div>
        </div>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS -->
    <script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="<?=base_url();?>/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script type="text/javascript">
        $(function() {
            $('#login-form').validate({
                errorClass: "help-block",
                rules: {
                    new_password: {
                        required: true,
                        confirmed: true,
                    },
                    re_password: {
                        required: true,
                        equalTo: "#new_password"
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });

            $('#news .card').collapse({
                toggle: false
            });

            $('.pagination .page-item').click(function() {
                console.log($(this).data("id"));
                $pageLastActiveId = $('.pagination .page-item.active').data('id');
                $('.pagination .page-item.active').removeClass('active');
                $(this).addClass('active');
                $('#card' + $pageLastActiveId).collapse('hide');
                console.log($('#card' + $pageLastActiveId));
                $('#card' + $(this).data('id')).collapse('show');
                console.log($('#card' + $(this).data('id')));
            })
        });
    </script>
</body>

</html>