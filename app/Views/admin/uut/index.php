<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">UUT (Unit Under Tested)</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">UUT</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar UUT</div>
            <div>
                <a class="btn btn-primary btn-sm" href="<?= base_url('uut/create'); ?>"><i class="fa fa-plus"></i> Data Baru</a>
            </div>
        </div>
        <div class="ibox-body">
            <div id="table">
            <table class="table table-striped table-bordered table-hover" id="labs" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pemilik</th>
                        <th>Jenis</th>
                        <th>Merek/Model/Tipe/Serial No</th>
                        <th>Kapasitas</th>
                        <th>Buatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($uuts as $uut):?>
                    <tr>
                        <td><?= $uut->owner ?></td>
                        <td><?= $uut->standard_type ?></td>
                        <td><?= $uut->tool_brand .'/'. $uut->tool_model .'/'. $uut->serial_no  ?></td>
                        <td><?= $uut->tool_capacity ?> <?= $uut->tool_capacity_unit ?></td>
                        <td><?= $uut->tool_made_in ?></td>
                        <td>
                            <a class="btn btn-default btn-sm" href="<?= base_url('uut/edit/' . $uut->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                            <a class="btn btn-default btn-sm" href="<?= base_url('uut/copy/' . $uut->id); ?>"><i class="fa fa-copy"></i> Salin</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

            <div id='calendar'></div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#labs').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>