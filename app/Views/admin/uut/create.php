<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">UUT</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('uut'); ?>">UUT</a>
        </li>
        <li class="breadcrumb-item">Penambahan Data UUT</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Penambahan Data UUT</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('uut'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                
            </div>
        </div>
        <div class="ibox-body" >
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            <form id="uttp" action="<?= base_url('uut/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label><span class="required_notification">*</span> Pemilik</label>
                        <select class="form-control" name="owner_id" id="owner_id">
                            <?php foreach ($owners as $owner): ?>
                            <option value="<?= $owner->id ?>"><?= $owner->nama ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label><span class="required_notification">*</span> Jenis</label>
                        <select class="form-control" name="type_id" id="type_id">
                            <?php foreach ($uutTypes as $type): ?>
                            <option value="<?= $type->id ?>"><?= $type->uut_type ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Merk</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" />
                    </div>
                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Model/Tipe</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" />
                    </div>  
                    
                    <!-- <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" />
                    </div> -->
                   
                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Nomor Seri / Identitas</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-1 form-group">
                        <label><span class="required_notification">*</span> Kapasitas</label>           
                        <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" />
                    </div>
                    <div class="col-2 form-group">
                        <label><span class="required_notification">*</span> Satuan </label>           
                        <div class="input-group">
                            <select class="form-control" name="tool_capacity_unit" id="tool_capacity_unit">
                                <option value=""></option>
                            </select>
                            <input type="text" class="form-control" name="tool_capacity_unit_1" id="tool_capacity_unit_1" />
                        </div>
                    </div>
                    <div class="col-1 form-group">
                        <label><span class="required_notification"></span> Daya Baca</label>           
                        <input type="text" class="form-control" name="tool_dayabaca" id="tool_dayabaca" />
                    </div>
                    <div class="col-2 form-group">
                        <label><span class="required_notification"></span> Satuan</label>           
                        <div class="input-group">
                            <select class="form-control" name="tool_dayabaca_unit" id="tool_dayabaca_unit">
                                <option value=""></option>
                            </select>
                            <input type="text" class="form-control" name="tool_dayabaca_unit_1" id="tool_dayabaca_unit_1" />
                        </div>
                    </div>
                    
                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Buatan</label>
                        <!--<innput type="text" class="form-control" name="tool_made_in" id="tool_made_in" /> -->
                       
                        <select class="form-control" name="tool_made_in_id" id="tool_made_in_id">
                            <option>-- Pilih Negara --</option>
                            <?php foreach ($negaras as $negara): ?>
                            <option value="<?= $negara->id ?>"><?= $negara->nama_negara ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pabrikan</label>
                        <input class="form-control" type="text" name="tool_factory" />
                    </div>
                    <!-- <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address"></textarea>
                    </div> -->
                    <div class="col-3 form-group">
                        <label>Kelas</label>
                        <input type="text" class="form-control" name="class" id="class" />
                    </div>
                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Jumlah</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" />
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#type_id, #owner_id, #tool_made_in_id').select2({
            theme: 'bootstrap4',
        });
        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#tool_capacity_unit').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/unit/'+type;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
            }
        });

        $('#tool_dayabaca_unit').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/unit/'+type;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
            }
        });
    })
</script>
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>