<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Perlengkapan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Alat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking/edit/' . $booking->id);?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body" >
       
            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" >
                <?= csrf_field() ?>

                <input type="hidden" id="service_type_id" value="<?= $booking->service_type_id ?>" />
                <input type="hidden" id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian ?>" />

                <?php if($booking->service_type_id == 1 || $booking->service_type_id == 2): ?>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <select class="form-control" name="type_id" id="type_id">
                                <?php foreach ($uutTypes as $type): ?>
                                <option value="<?= $type->id ?>" <?= $type->id == $uut->type_id ? 'selected' : '' ?> ><?= $type->standard_type ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label>Identitas Alat</label>
                            <select class="form-control" name="uut_id" id="uut_id">
                            <option value="<?= $item->uut_id ?>" selected>
                                <?= $uut->tool_brand .'/'. $uut->tool_model .'/'. $uut->tool_type .'/'. $uut->serial_no ?>
                            </option>
                            </select>
                            <a href="<?= base_url() ?>/uttp/create" target="_blank">
                                Klik disini untuk menambahkan data alat yang baru.
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $uut->serial_no ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                                value="<?= $uut->tool_brand ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Model</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $uut->tool_model ?>" readonly/>
                        </div>  
                        <!--
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                value="<?= $uut->tool_type ?>" readonly/>
                        </div>
                        -->
                        <div class="col-3 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $uut->tool_made_in ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimal</label>
                            <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $uut->tool_capacity ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                                value="<?= $uut->tool_capacity_unit ?>" readonly/>
                        </div>
                    </div>
                <?php else: ?>
                
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis</label>
                        <input type="text" class="form-control" name="type_id" id="type_id" 
                                value="<?= $item->type ?>" readonly/>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Seri</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" 
                            value="<?= $item->serial_no ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Merk</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                            value="<?= $item->tool_brand ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Model</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" 
                            value="<?= $item->tool_model ?>" readonly/>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimal</label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                            value="<?= $item->tool_capacity ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Kapasitas Minimal</label>
                        <input type="number" class="form-control" name="tool_capacity_min" id="tool_capacity_min" 
                            value="<?= $item->tool_capacity_min ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Satuan Kapasitas</label>
                        <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                            value="<?= $item->tool_capacity_unit ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Buatan</label>
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                            value="<?= $item->tool_made_in ?>" readonly/>
                    </div>
                </div>
                
                

                <?php if(($booking->service_type_id == 4 || $booking->service_type_id == 5) && ($booking->lokasi_pengujian == 'luar')): ?>
                <h5>Perlengkapan:</h5>

                <div class="row">
                    <div class="col-9 form-group">
                        <label>Tambah Perlengkapan</label>
                        <div class="input-group">
                            <select class="form-control" name="uttp_perlengkapan_id" id="uttp_perlengkapan_id">
                                <option value="">-</option>
                                <?php foreach($uttps as $uttp): ?>
                                <option value="<?= $uttp->id ?>"><?= $uttp->serial_no; ?>/<?= $uttp->tool_brand; ?>/<?= $uttp->tool_model ?> (<?= $uttp->uttp_type ?>)</option>
                                <?php endforeach ?> 
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-default" type="button" id="button-add-perlengkapan">
                                    <i class="fa fa-plus"></i> Tambah
                                </button>
                            </div>
                        </div>
                        <a href="<?= base_url() ?>/uttp/create" target="_blank">
                            Klik disini untuk menambahkan data alat yang baru.
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                    <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Jenis</th>
                                <th>Merek</th>
                                <th>Model/Tipe</th>
                                <th>Nomor Seri</th>
                                <th>Media Uji/Komoditas</th>
                                <th>Buatan</th>
                                <th>Kapasitas Maksimal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($ttuPerlengkapan as $perlengkapan):?>
                            <tr>
                                <td><?= $perlengkapan->uttp_type ?></td>
                                <td><?= $perlengkapan->tool_brand ?></td>
                                <td><?= $perlengkapan->tool_model ?></td>
                                <td><?= $perlengkapan->serial_no ?></td>
                                <td><?= $perlengkapan->tool_media ?></td>
                                <td><?= $perlengkapan->tool_made_in ?></td>
                                <td><?= $perlengkapan->tool_capacity . ' ' . $perlengkapan->tool_capacity_unit ?></td>
                                <td>
                                    <a class="btn btn-danger btn-sm" href="/bookingitemtool/delete/<?= $booking->id ?>/<?= $item->id ?>/<?= $perlengkapan->id ?>"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                    </div>
                </div>
                <?php endif ?>

                <?php endif ?>

            </form>
            <form id="perlengkapan" action="<?= base_url('bookingitemtool/create/'.$booking->id.'/'.$item->id); ?>" 
                method="post">
                <?= csrf_field() ?>
                <input type="hidden" id="perlengkapan_id" name="uttp_perlengkapan_id" value="" />
            </form>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {

        var owner = '<?= $booking->uttp_owner_id ?>';
        

        $('#button-add-perlengkapan').click(function(e) {
            e.preventDefault();

            console.log('disini');

            var form = $("#perlengkapan");
            form[0].submit();
        });

        $("#booking").validate({
            rules: {
                uttp_id: {
                    required: !0,
                },
                location: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) && $("#lokasi_pengujian").val() == 'luar';
                    },
                },
                file_type_approval_certificate: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 4);
                    },
                },
                reference_no: {
                    required: !0,
                },
                reference_date: {
                    required: !0,
                },
                file_last_certificate: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 5);
                    },
                },
                file_application_letter: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
                file_calibration_manual: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
                file_manual_book: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
            },
            messages: {
                uttp_id: 'Alat wajib dipilih',
                location: 'Lokasi alat wajib diisi',
                reference_no: 'Nomor surat wajib diisi',
                reference_date: 'Tanggal surat wajib diisi dengan format yang benar',
                file_type_approval_certificate: 'File surat persetujuan wajib diupload',
                file_last_certificate: 'File sertifikat sebelumnya wajib diupload',
                file_application_letter: 'File surat permoohonan wajib diupload',
                file_calibration_manual: 'File manual kalibrasi wajib diupload',
                file_manual_book: 'File buku manual wajib diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

        $('#uttp_perlengkapan_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#perlengkapan_id').val(e.params.data.id);
        });
    })
</script>
<?= $this->endSection() ?>