<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Laboratorium</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('uutowner'); ?>">Pemilik Alat</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data Pemilik Alat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Perubahan Data Pemilik Alat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('uutowner'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="lab" action="<?= base_url('uutowner/edit/'.$owner->id); ?>" method="post">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-4 form-group">
                        <label>Nama</label>
                        <input class="form-control" type="text" 
                            name="nama" id="nama" value="<?= $owner->nama ?>">
                    </div>
                    <div class="col-4 form-group">
                        <label>NIB</label>
                        <input class="form-control" type="text" 
                            name="nib" id="nib" value="<?= $owner->nib ?>">
                    </div>
                    <div class="col-4 form-group">
                        <label>NPWP</label>
                        <input class="form-control" type="text" 
                            name="npwp" id="npwp" value="<?= $owner->npwp ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" 
                            name="alamat" id="alamat"><?= $owner->alamat ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 form-group">
                        <label>Kota <?= $owner->kota_id ?></label>
                        <select class="form-control" name="kota_id" id="kota_id">
                            <option value="">-- Pilih Kota --</option>
                            <?php foreach ($kotas as $kota): ?>
                            <option value="<?= $kota->id ?>" <?= $owner->kota_id === $kota->id ? 'selected' : '' ?> ><?= $kota->nama ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-4 form-group">
                        <label>Telepon</label>
                        <input class="form-control" type="text" 
                            name="telepon" id="telepon" value="<?= $owner->telepon  ?>">
                    </div>
                    <div class="col-4 form-group">
                        <label>Email</label>
                        <input class="form-control" type="email" 
                            name="email" id="email" value="<?= $owner->email ?>">
                    </div>
                </div>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#kota_id').select2({
            theme: 'bootstrap4',
        });

    })
</script>
<?= $this->endSection() ?>