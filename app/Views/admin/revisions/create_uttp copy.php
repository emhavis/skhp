<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Perbaikan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('revision'); ?>">Perbaikan</a>
        </li>
        <li class="breadcrumb-item">Perbaikan Baru</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Perbaikan Baru</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('revision'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="revision" action="<?= base_url('revision/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>No SKHP/SKHPT/SET/Sertifikat</label>
                        <select class="form-control" name="order_id" id="order_id">
                            <option value="">-- Pilih Sertifikat --</option>
                            <?php foreach ($orders as $order): ?>
                            <option value="<?= $order->id ?>"><?= $order->no_sertifikat ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan">
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover" id="revisions" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Diubah?</th>
                        <th>Sertifikat</th>
                        <th>Seharusnya</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                    <tr>
                        <td>Jenis Alat</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_type_id" id="is_type_id"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input type="text" class="form-control" name="type_id" id="type_id" readonly/></td>
                        <td><select class="form-control" name="type_id_new" id="type_id_new" disabled>
                                <option value="">&nbsp;</option>
                                <?php foreach ($types as $type): ?>
                                <option value="<?= $type->id ?>"><?= $type->uttp_type ?></option>
                                <?php endforeach ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Merek</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_brand" id="is_tool_brand"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly /></td>
                        <td><input type="text" class="form-control" name="tool_brand_new" id="tool_brand_new" readonly /></td>
                    </tr>
                    <tr>
                        <td>Model</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_model" id="is_tool_model"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input type="text" class="form-control" name="tool_model" id="tool_model" readonly/></td>
                        <td><input type="text" class="form-control" name="tool_model_new" id="tool_model_new" readonly/></td>
                    </tr>
                    <tr>
                        <td>Tipe</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_type" id="is_tool_type"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input type="text" class="form-control" name="tool_type" id="tool_type" readonly/></td>
                        <td><input type="text" class="form-control" name="tool_type_new" id="tool_type_new" readonly/></td>
                    </tr>
                    <tr>
                        <td>Kapasitas Maksimal</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_capacity" id="is_tool_capacity"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input type="number" class="form-control" name="tool_capacity" id="tool_capacity" readonly/></td>
                        <td><input type="number" class="form-control" name="tool_capacity_new" id="tool_capacity_new" readonly/></td>
                    </tr>
                    <tr>
                        <td>Pabrikan</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_factory" id="is_tool_factory"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input class="form-control" type="text" name="tool_factory" id="tool_factory" readonly/></td>
                        <td><input class="form-control" type="text" name="tool_factory_new" id="tool_factory_new" readonly/></td>
                    </tr>
                    <tr>
                        <td>Alamat Pabrikan</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_factory_address" id="is_tool_factory_address"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><textarea class="form-control" name="tool_factory_address" id="tool_factory_address" readonly></textarea></td>
                        <td><textarea class="form-control" name="tool_factory_address_new" id="tool_factory_address_new" readonly></textarea></td>
                    </tr>
                    <tr>
                        <td>Buatan</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_tool_made_in" id="is_tool_made_in"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input class="form-control" type="text" name="tool_made_in" id="tool_made_in" readonly/></td>
                        <td><input class="form-control" type="text" name="tool_made_in_new" id="tool_made_in_new" readonly/></td>
                    </tr>
                    <tr>
                        <td>Pemohon</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_label_sertifikat" id="is_label_sertifikat"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><input class="form-control" type="text" name="label_sertifikat" id="label_sertifikat" readonly/></td>
                        <td><input class="form-control" type="text" name="label_sertifikat_new" id="label_sertifikat_new" readonly/></td>
                    </tr>
                    <tr>
                        <td>Alamat Pemohon</td>
                        <td><label class="ui-checkbox">
                            <input type="checkbox" class="form-control" name="is_addr_sertifikat" id="is_addr_sertifikat"/>
                            <span class="input-span"></span>Ya</label></td>
                        <td><textarea class="form-control" name="addr_sertifikat" id="addr_sertifikat" readonly></textarea></td>
                        <td><textarea class="form-control" name="addr_sertifikat_new" id="addr_sertifikat_new" readonly></textarea></td>
                    </tr>
                </table>

                <div class="row">
                    <div class="col-12 form-group">
                        <label>Perbaikan Lainnya</label>
                        <textarea class="form-control" name="others" id="others"></textarea>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {

        var array_types = <?php echo json_encode($types) ?>;

        $('#type_id_new').select2({
            theme: 'bootstrap4',
        });
        $('#order_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/revision/order/'+e.params.data.id,function(res)
            {
                $('#jenis_layanan').val(res.order.jenis_layanan);
                $('#type_id').val(res.order.uttp_type);
                $('#tool_model').val(res.order.tool_model);
                $('#tool_brand').val(res.order.tool_brand);
                $('#tool_type').val(res.order.tool_type);
                $('#tool_capacity').val(res.order.tool_capacity);
                $('#tool_factory').val(res.order.tool_factory);
                $('#tool_factory_address').val(res.order.tool_factory_address);
                $('#tool_made_in').val(res.order.tool_made_in);
                $('#label_sertifikat').val(res.order.label_sertifikat);
                $('#addr_sertifikat').val(res.order.addr_sertifikat);
            });
        });
        $('input[type=checkbox]').change(function(e) {
            var id = $(this).attr('id');
            var field = id.substr(-id.length + 3);
            var isChecked = $(this).is(":checked");
            
            if (field == 'type_id') {
                var selected = array_types.filter(function(v){ 
                    return v.uttp_type === $('#' + field).val();
                });
                $('#' + field + '_new').attr('disabled', !isChecked);
                $('#' + field + '_new').val(isChecked ? selected[0].id : "");
                $('#' + field + '_new').trigger('change');
            } else {
                $('#' + field + '_new').attr('readonly', !isChecked);
                $('#' + field + '_new').val(isChecked ? $('#' + field).val() : "");
            }
        });
    })
</script>
<?= $this->endSection() ?>