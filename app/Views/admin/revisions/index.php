<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Perbaikan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Perbaikan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Perbaikan</div>
            <div>
                <a class="btn btn-primary btn-sm" href="<?= base_url('revision/create'); ?>"><i class="fa fa-plus"></i> Pengaduan Baru</a>
            </div>
        </div>
        <div class="ibox-body">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-pengaduan" role="tab" aria-controls="pills-home" aria-selected="true">Pengaduan</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-proses" role="tab" aria-controls="pills-profile" aria-selected="false">Sedang Diproses</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-riwayat" role="tab" aria-controls="pills-contact" aria-selected="false">Riwayat</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-pengaduan" role="tabpanel" aria-labelledby="pills-home-tab">
                    <table class="table table-striped table-bordered table-hover" id="revisions" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Sertifikat</th>
                                <th>Jenis Layanan</th>
                                <th>Perbaikan</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($revisions_pengaduan as $revision):?>
                            <tr>
                                <td><?= $revision->no_sertifikat ?></td>
                                <td><?= $revision->service_type ?></td>
                                <td><?= $revision->perbaikan ?></td>
                                <td><?= $revision->status_text ?></td>
                                <td>
                                    <a class="btn btn-default" href="<?= base_url('revision/edit/' . $revision->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                                    <a class="btn btn-default" href="<?= base_url('revision/delete/' . $revision->id); ?>"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-proses" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <table class="table table-striped table-bordered table-hover" id="revisions" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Sertifikat</th>
                                <th>Jenis Layanan</th>
                                <th>Perbaikan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($revisions_proses as $revision):?>
                            <tr>
                                <td><?= $revision->no_sertifikat ?></td>
                                <td><?= $revision->service_type ?></td>
                                <td><?= $revision->perbaikan ?></td>
                                <td><?= $revision->status_text ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-riwayat" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <table class="table table-striped table-bordered table-hover" id="revisions" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nomor Sertifikat</th>
                                <th>Jenis Layanan</th>
                                <th>Perbaikan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($revisions_riwayat as $revision):?>
                            <tr>
                                <td><?= $revision->no_sertifikat ?></td>
                                <td><?= $revision->service_type ?></td>
                                <td><?= $revision->perbaikan ?></td>
                                <td><?= $revision->status_text ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>