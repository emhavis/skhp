<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />

<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Riwayat Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('history'); ?>">Riwayat Layanan</a>
        </li>
        <li class="breadcrumb-item">Daftar</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pelacakan
                <?php if ($request->status_id == 2): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI!</span>
                <?php elseif ($request->status_id == 4): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">KONFIRMASI JADWAL!</span>
                <?php elseif ($request->status_id == 6): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">INPUT BUKTI BAYAR!</span>
                <?php endif ?>
            </div>
        <?php if($booking->uttp_owner_id >0) :?>
            <div>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/kuitansi'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-money-check"></i> Kuitansi</a>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/buktiorder'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-tasks"></i> Bukti Order</a>
            </div>
        <?php else : ?>
            <div>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice_uut'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/kuitansi_uut'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-money-check"></i> Kuitansi</a>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/buktiorder_uut'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-tasks"></i> Bukti Order</a>
            </div>
        <?php endif ?>
        </div>
        <div class="ibox-body">
            <?php if($request->status_id == 12): ?>
                <div class="alert alert-danger"><strong>Pengujian Tertunda!</strong> Pengujian mengalami penundaan dikarenakan: <br/>
                    <?php foreach ($orders as $order): ?>
                        <?= $order->pending_notes ?><br/>
                        <strong>Penundaan direncanakan selesai pada tanggal <?= date("d-m-Y", strtotime($order->pending_estimated)) ?></strong>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
           
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" 
                            value="<?= $booking->booking_no ?>" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Pendaftaran</label>
                        <input class="form-control" type="text" name="no_register" 
                            value="<?= $request->no_register ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Status</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $request->status ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $request->jenis_layanan ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" value="<?= $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $request->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            value="<?= $request->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly><?= $request->addr_sertifikat ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <?php if($request->lokasi_pengujian == 'dalam'): ?>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengiriman Alat</label>
                        <input class="form-control" type="text" name="est_arrival_date" readonly
                            id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                    </div>
                    <?php else: ?>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengujian/Pemeriksaan</label>
                        <input class="form-control" type="text" name="scheduled_test_date_from" readonly
                            id="scheduled_test_date_from" value="<?= $request->scheduled_test_date_from . ' s/d ' .$request->scheduled_test_date_to ?>">
                    </div>
                    <?php endif ?>
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                </div>
        </div>
    </div>
    <?php if($booking->service_type_id ==1 || $booking->service_type_id ==2){ ?>
        <?php foreach ($items as $item):?>
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"> Informasi Alat
                        <?php if ($request->status_id == 2): ?>
                        <?php
                            $filterTerms = array_filter($terms, function($var) use ($item) { return $var->request_item_id == $item->id; });
                            if (count($filterTerms) == 0) {
                        ?>
                        <a class="btn btn-danger btn-sm" href="<?= base_url('term_uut/create/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <small>Pastikan Anda sudah menyetujui Kaji Ulang</small>
                        <?php } else { ?>
                        <a class="btn btn-warning btn-sm" href="<?= base_url('term_uut/edit/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <?php } ?>
                        <?php endif ?>
                    </div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <!--
                    <div>
                        <a class="btn btn-default btn-sm" href="<?= base_url('bookingitem/create'); ?>/<?= $booking->id ?>"><i class="fa fa-plus"></i> Alat</a>
                    </div>
                    -->
                </div>
                <div class="ibox-body">
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $item->tool_made_in ?>" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->tool_model ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" readonly/>
                        </div>  
                        <!--
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                value="<?= $item->tool_type ?>" readonly/>
                        </div>
                        -->
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimal</label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity .' '. $item->tool_capacity_unit ?>" readonly/>
                        </div>
                    </div>
                    <!--
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>
                    -->

                    <div class="row">
                        <div class="col-12">
                            <strong>Daftar Pemeriksaan (Jumlah: <?= $item->quantity ?>)</strong> <strong>ID Daftar Pemeriksaan (Jumlah: <?= $item->id ?>)</strong>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Jenis Pengujian</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (array_filter($inspections, function($var) use ($item) { return $var->service_request_item_id == $item->id; }) as $inspection):?>
                                    <tr>
                                        <td><?= $inspection->inspection_type ?></td>
                                       
                                        <td><?= $inspection->quantity ?></td>
                                        <td><?= $inspection->unit ?></td>
                                        <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                        <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="4">Subtotal</th>
                                        <th><?= number_format($item->subtotal, 2, ',', '.') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
    <?php }else{?>
        <?php foreach ($items as $item):?>
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"> Alat: <?= $item->uttp_type ?>
                        <?php if ($request->status_id == 2): ?>
                        <?php
                            $filterTerms = array_filter($terms, function($var) use ($item) { return $var->request_item_id == $item->id; });
                            if (count($filterTerms) == 0) {
                        ?>
                        <a class="btn btn-danger btn-sm" href="<?= base_url('term/create/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <small>Pastikan Anda sudah menyetujui Kaji Ulang</small>
                        <?php } else { ?>
                        <a class="btn btn-warning btn-sm" href="<?= base_url('term/edit/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <?php } ?>
                        <?php endif ?>
                    </div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <!--
                    <div>
                        <a class="btn btn-default btn-sm" href="<?= base_url('bookingitem/create'); ?>/<?= $booking->id ?>"><i class="fa fa-plus"></i> Alat</a>
                    </div>
                    -->
                </div>
                <div class="ibox-body">
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Nomor Order</label>
                            <input class="form-control" type="text" name="no_order" 
                                value="<?= $item->no_order ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="text" class="form-control" name="type_id" id="type_id" 
                                value="<?= $item->uttp_type ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $item->tool_made_in ?>" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->tool_brand ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" readonly/>
                        </div>  
                        <!--
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                value="<?= $item->tool_type ?>" readonly/>
                        </div>
                        -->
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimal</label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity .' '. $item->tool_capacity_unit ?>" readonly/>
                        </div>
                    </div>
                    <!--
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>
                    -->

                    <div class="row">
                        <div class="col-12">
                            <strong>Daftar Pemeriksaan (Jumlah: <?= $item->quantity ?>)</strong>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Jenis Pengujian</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (array_filter($inspections, function($var) use ($item) { return $var->request_item_id == $item->id; }) as $inspection):?>
                                    <tr>
                                        <td><?= $inspection->inspection_type ?></td>
                                        
                                        <td><?= $inspection->quantity ?></td>
                                        <td><?= $inspection->unit ?></td>
                                        <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                        <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="4">Subtotal</th>
                                        <th><?= number_format($item->subtotal, 2, ',', '.') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
    <?php } ?>
    <?php if($booking->service_type_id ==1 || $booking->service_type_id ==2){ ?>
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Riwayat</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered table-hover" id="history" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Tanggal/Waktu</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (array_filter($histories, function($var) use ($item) { return $var->request_item_id == $item->id && $var->request_id == $item->service_request_id; }) as $history):?>
                            <tr>
                                <td><?= date("d M Y H:i:s", strtotime($history->created_at)) ?></td>
                                <td>
                                    <?= $history->status ?>
                                    <?php
                                        if ($history->order_id != null) {
                                            if ($history->order_status_id < 3) {
                                                echo '<br/>Sertifikat dalam proses';
                                            }
                                            if ($history->order_status_id == 3) {
                                                echo '<br/>Sertifikat sudah selesai';
                                            }
                                            if ($history->warehouse_status_id == 1) {
                                                echo '<br/>Alat sudah dapat diambil';
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php }else{?>
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Riwayat</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered table-hover" id="history" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Tanggal/Waktu</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (array_filter($histories, function($var) use ($item) { return $var->request_item_id == $item->id && $var->request_id == $item->request_id; }) as $history):?>
                            <tr>
                                <td><?= date("d M Y H:i:s", strtotime($history->created_at)) ?></td>
                                <td>
                                    <?= $history->status ?>
                                    <?php
                                        if ($history->order_id != null) {
                                            if ($history->order_status_id < 3) {
                                                echo '<br/>Sertifikat dalam proses';
                                            }
                                            if ($history->order_status_id == 3) {
                                                echo '<br/>Sertifikat sudah selesai';
                                            }
                                            if ($history->warehouse_status_id == 1) {
                                                echo '<br/>Alat sudah dapat diambil';
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    
</div>

<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Konfirmasi Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <?php if ($check_terms): ?>
                    <div class="modal-body">
                        <p>
                            Periksa kembali data yang didaftarkan. 
                            <strong>Apakah data tersebut sudah benar?</strong>
                        </p>
                        <small>Pendaftaran dengan data yang sudah benar akan dilanjutan untuk proses berikutnya. 
                            Sedangkan, pendaftaran dengan data yang masih belum benar akan dikembalikan pada Front Desk untu diperbaiki.</small>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <form id="booking" action="<?= base_url('tracking/confirm'); ?>/<?= $booking->id ?>" method="post">
                            <?= csrf_field() ?>
                            <input type="hidden" name="status_id" value="<?= $booking->lokasi_pengujian == 'dalam' ? '5' : '3' ?>" />
                            <button type="submit" class="btn btn-success">Ya, Sudah Benar</button>
                        </form>
                        <form id="booking_noconfirm" action="<?= base_url('tracking/confirm'); ?>/<?= $booking->id ?>" method="post">
                            <?= csrf_field() ?>
                            <input type="hidden" name="status_id" value="1" />
                            <button type="submit" class="btn btn-danger">Tidak, Masih Terdapat Kesalahan</button>
                        </form>
                    </div>
                <?php else: ?>
                <div class="modal-body">
                    <p>
                        Formulir kaji ulang belum disetujui semua. 
                        <strong>Anda belum dapat mengkonfirmasi semua data.</strong>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
        <?php endif; ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmScheduleModal" tabindex="-1" aria-labelledby="confirmScheduleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmScheduleModalLabel">Konfirmasi Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Jadwal Pengujian, dari</label>
                        <input type="text" class="form-control" name="scheduled_test_date_from" id="scheduled_test_date_from"
                            value="<?= date("d M Y", strtotime($request->scheduled_test_date_from)) ?>" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Jadwal Pengujian, sampai dengan</label>
                        <input type="text" class="form-control" name="scheduled_test_date_to" id="scheduled_test_date_to"
                            value="<?= date("d M Y", strtotime($request->scheduled_test_date_to)) ?>" readonly />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <form id="booking" action="<?= base_url('tracking/confirmschedule'); ?>/<?= $booking->id ?>" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="status_id" value="5" />
                    <button type="submit" class="btn btn-success">Ya, Setuju</button>
                </form>
                <form id="booking_noconfirm" action="<?= base_url('tracking/confirmschedule'); ?>/<?= $booking->id ?>" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="status_id" value="3" />
                    <button type="submit" class="btn btn-danger">Jadwal Tidak Cocok</button>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="paymentModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="confirmModalLabel">Data Pembayaran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="payment" action="<?= base_url('tracking/pay'); ?>/<?= $booking->id ?>" method="post"
            enctype="multipart/form-data" >
        <?= csrf_field() ?>
        <div class="modal-body">
            <?php if ($request->total_price > 0 && $request->payment_date == null): ?>
                
            <div class="row">
                <div class="col-4 form-group">
                    <label>Total Tagihan</label>
                    <input type="text" class="form-control" name="total_price" id="total_price"
                        value="<?= number_format($request->denda_inv_price > 0 ? $request->denda_inv_price : $request->total_price, "2", ",", ".")  ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>Kode Billing</label>
                    <input type="text" class="form-control" name="billing_code" id="billing_code"
                        value="<?= $request->billing_code ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>Batas Waktu Pembayaran</label>
                    <input type="text" class="form-control" name="billing_to_date" id="billing_to_date"
                        value="<?= date("d M Y", strtotime($request->billing_to_date)) ?>" readonly />
                </div>
            </div>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Tanggal Pembayaran</label>
                    <input type="text" class="form-control" name="payment_date" id="payment_date"/>
                </div>
                <div class="col-4 form-group">
                    <label>Kode Pembayaran (NTPN)</label>
                    <input type="text" class="form-control" name="payment_code" id="payment_code"/>
                </div>
                <div class="col-4 form-group">
                    <label>Atas Nama Kuitansi</label>
                    <select class="form-control" name="an_kuitansi" id="an_kuitansi">
                        <option value="pemohon">Pemohon</option>
                        <option value="pemilik">Pemilik Alat</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Bukti Pembayaran</label>
                    <input class="form-control" id="file_payment_ref" 
                        type="file" name="file_payment_ref" 
                        accept="application/pdf, image/png, image/gif, image/jpeg" />
                </div>
            </div>
            <?php endif; ?>
            <?php if ($request->lokasi_pengujian == 'luar'  && $request->spuh_payment_date == null && ($request->spuh_add_price > 0 || $request->spuh_inv_price > 0)): ?>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Tagihan SPUH</label>
                    <input type="text" class="form-control" name="spuh_inv_price" id="spuh_inv_price"
                        value="<?= number_format($request->spuh_add_price > 0 ? $request->spuh_add_price : $request->spuh_inv_price, "2", ",", ".") ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>No SPUH</label>
                    <input type="text" class="form-control" name="spuh_no" id="spuh_no"
                        value="<?= $request->spuh_no ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>Tanggal Pembayaran</label>
                    <input type="text" class="form-control" name="spuh_payment_date" id="spuh_payment_date"/>
                </div>
            </div>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Bukti Pembayaran</label>
                    <input class="form-control" id="file_spuh_payment_ref" 
                        type="file" name="file_spuh_payment_ref" 
                        accept="application/pdf, image/png, image/gif, image/jpeg" />
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Konfirmasi Bayar</button>
        </div>
        </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#items').DataTable({
            pageLength: 10,
        });

        $('#an_kuitansi').select2({
            theme: 'bootstrap4',
        });

        $('#book').click(function(e) {
            e.preventDefault()
            var form = $('#booking');
            form.attr('action', '<?= base_url('booking/submit'); ?>/<?= $booking->id ?>')
            form.submit();
        });

        $('#payment_date').datepicker({
            format: 'dd-mm-yyyy',
            endDate: new Date() < new Date('<?= date("Y-m-d", strtotime($request->billing_to_date)) ?>') ? new Date() : <?= date("Y-m-d", strtotime($request->billing_to_date)) ?>,
        });

        $('#spuh_payment_date').datepicker({
            format: 'dd-mm-yyyy',
        });
    })
</script>
<?= $this->endSection() ?>