<?= $this->extend('template/main') ?>
<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="ibox bg-info color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong"><?= $booking_current_count->count ?></h2>
                    <div class="m-b-5">BOOKING BARU</div><i class="ti-shine widget-stat-icon"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="ibox bg-primary color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong"><?= $booking_all_count->count ?></h2>
                    <div class="m-b-5">TOTAL BOOKING</div><i class="ti-ticket widget-stat-icon"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-warning color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong"><?= $request_confirm_count->count ?></h2>
                    <div class="m-b-5">PERLU KONFIRMASI</div><i class="ti-check-box widget-stat-icon"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-danger color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong"><?= $request_pay_count->count ?></h2>
                    <div class="m-b-5">PERLU PEMBAYARAN</div><i class="ti-wallet widget-stat-icon"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-teal color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong"><?= $request_intest_count->count ?></h2>
                    <div class="m-b-5">DALAM PENGUJIAN</div><i class="ti-dashboard widget-stat-icon"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="ibox bg-success color-white widget-stat">
                <div class="ibox-body">
                    <h2 class="m-b-5 font-strong"><?= $request_done_count->count ?></h2>
                    <div class="m-b-5">SELESAI</div><i class="ti-bookmark-alt widget-stat-icon"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-body">
                    <div class="flexbox mb-4">
                        <div>
                            <h3 class="m-0">Permintaan Layanan UTTP</h3>
                            <hr>
                            <div>Jumlah Permintaan Layanan UTTP dalam Satu Tahun</div>
                        </div>
                        <div class="d-inline-flex">
                            <div class="px-12" style="border-right: 1px solid rgba(0,0,0,.1);">
                                <div class="text-muted">Tahun</div>
                                <hr>
                                <div>
                                <form method="post" id="tahunForm" action="<?= base_url()?>/home">
                                <?= csrf_field() ?>
                                    <select id="tahun" name="tahun" class="form-control">
                                        <?php foreach ($tahunList as $tahun): ?>
                                            <option value="<?= $tahun->tahun ?>" <?=$tahun->tahun === $Ynow ? 'selected' : '' ?> ><?= $tahun->tahun; ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div id="chartdiv" name ="chartdiv" ></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- another chart -->
        <hr>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-body">
                    <div class="flexbox mb-4">
                        <div>
                            <h3 class="m-0">Alat UTTP yang Dilayani</h3>
                            <hr>
                            <div>Jumlah UTTP per Jenis yang Dilayani dalam Satu Tahun</div>
                        </div>
                        <div class="d-inline-flex">
                        
                            <div class="px-3" style="border-right: 1px solid rgba(0,0,0,.1);">
                                <div class="text-muted">Tahun</div>
                                <hr>
                                <div>  
                                    <form method="post" id="layananForm" action="<?= base_url()?>/home">
                                        <?= csrf_field() ?>
                                        <select id="tahunLayanan" name="tahunLayanan" class="form-control">
                                            <option value="" ></option>
                                            <?php foreach ($tahunLayanan as $tahun): ?>
                                                <option value="<?= $tahun->tahun ?>" <?=$tahun->tahun === $Ynow ? 'selected' : '' ?> ><?= $tahun->tahun; ?> </option>
                                            <?php endforeach ?>
                                        </select>
                                </div>
                            </div>
                            <div class="px-3">
                                <div class="text-muted">Jenis Layanan</div>
                                <hr>
                                <div>
                                  
                                        <select id="layanan" name="layanan" class="form-control"> 
                                                <option value="" ></option>
                                                <?php foreach ($listLayanan as $layanan): ?>
                                                    <option value="<?= $layanan->layanan ?>"><?= $layanan->layanan; ?> </option>
                                                <?php endforeach ?>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div>
                        <div id="chartCanvas" name ="chartCanvas" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-body">
                    <div class="flexbox mb-4">
                        <div>
                            <h3 class="m-0">Alat UUT yang Dilayani</h3>
                            <hr>
                            <div>Jumlah UUT per Jenis yang Dilayani dalam Satu Tahun</div>
                        </div>
                        <div class="d-inline-flex">
                        
                            <div class="px-3" style="border-right: 1px solid rgba(0,0,0,.1);">
                                <div class="text-muted">Tahun</div>
                                <hr>
                                <div>  
                                    <form method="post" id="layananForm" action="<?= base_url()?>/home">
                                        <?= csrf_field() ?>
                                        <select id="tahunLayanan" name="tahunLayanan" class="form-control">
                                            <option value="" ></option>
                                            <?php foreach ($tahunLayanan as $tahun): ?>
                                                <option value="<?= $tahun->tahun ?>" <?=$tahun->tahun === $Ynow ? 'selected' : '' ?> ><?= $tahun->tahun; ?> </option>
                                            <?php endforeach ?>
                                        </select>
                                </div>
                            </div>
                            <div class="px-3">
                                <div class="text-muted">Jenis Layanan</div>
                                <hr>
                                <div>
                                  
                                        <select id="layanan" name="layanan" class="form-control"> 
                                                <option value="" ></option>
                                                <?php foreach ($listLayanan as $layanan): ?>
                                                    <option value="<?= $layanan->layanan ?>"><?= $layanan->layanan; ?> </option>
                                                <?php endforeach ?>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div>
                        <div id="chartCanvasUut" name ="chartCanvasUut" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .visitors-table tbody tr td:last-child {
            display: flex;
            align-items: center;
        }

        .visitors-table .progress {
            flex: 1;
        }

        .visitors-table .progress-parcent {
            text-align: right;
            margin-left: 10px;
        }
    </style>
</div>
<!--  -->
<!-- Styles -->
<style>
    #chartdiv {
    width: 100%;
    height: 500px;
    }
    #chartCanvas {
    width: 100%;
    height: 500px;
    }
    #chartCanvasUut {
    width: 100%;
    height: 500px;
    }
</style>
<?= $this->endSection() ?>
<?= $this->section('js') ?>
    <script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <!-- Resources -->
    <script src="<?=base_url();?>/assets/amcharts4/core.js"></script>
    <script src="<?=base_url();?>/assets/amcharts4/charts.js"></script>
    <script src="<?=base_url();?>/assets/amcharts4/themes/animated.js"></script>
<script>
    $(function(){  
        //set default data chart
        MainChart(<?=$data ?>)
        ChartDua(<?=$dataUttp ?>)
        ChartDuaUut(<?=$dataUut ?>)
        // getting token from field
        var token = document.getElementsByName("csrf_test_name").val
        // setting on change event
        $('#tahun').select2({
            theme: 'bootstrap4',
            placeholder :'Pilihan Tahun',
        }).on('select2:select', function(e){
            // initialize data form
            var data = $('#tahunForm').serialize();
            $.ajax('<?= base_url(); ?>/home/getDataByYear',{
                type:'POST',
                data: data,
                success: function(data,status,xhr){
                    console.log(data);
                    data = JSON.parse(data);
                    MainChart(data)
                }
            });
        });
        // layanan action
        $('#tahunLayanan').select2({
            theme: 'bootstrap4',
            placeholder : 'Pilih Tahun',
        }).on('select2:select', function(e){
            // initialize data form
                // initialize data form
                var data = $('#layananForm').serialize();
                $.ajax('<?= base_url(); ?>/home/getDataFilter',{
                    type:'POST',
                    data: data,
                    success: function(data,status,xhr){
                        // console.log(data);
                        data = JSON.parse(data);
                        ChartDua(data)
                    }
                });
        });
        $('#layanan').select2({
            theme: 'bootstrap4',
            placeholder :'Pilihan Layanan',
        }).on('select2:select', function(e){
            // initialize data form
            var data = $('#layananForm').serialize();
            $.ajax('<?= base_url(); ?>/home/getDataFilter',{
                type:'POST',
                data: data,
                success: function(data,status,xhr){
                    // console.log(data);
                    data = JSON.parse(data);
                    ChartDua(data)
                }
            });
        });
    });
</script>
<!-- Chart code -->
<script>
    // geting chart 2
function ChartDua(newData) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        var chart = am4core.create('chartCanvas', am4charts.XYChart)
        chart.colors.step = 2;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'top'
        chart.legend.paddingBottom = 20
        chart.legend.labels.template.maxWidth = 95

        var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'jenis_alat'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        /// custom label 
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = "right";
        xAxis.renderer.labels.template.verticalCenter = "middle";
        xAxis.renderer.labels.template.truncate = true;
        xAxis.renderer.labels.template.maxWidth = 120;
        // label.wrap = true;

        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        function createSeries(value, name) {
            var series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value
            series.dataFields.categoryX = 'jenis_alat'
            series.name = name

            series.events.on("hidden", arrangeColumns);
            series.events.on("shown", arrangeColumns);

            var bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        chart.data =newData;
        // var set = ;
    
        chart.validateData();


        createSeries('jumlah_alat', 'Jumlah Alat');
        // createSeries('jumlah_alat', 'Alat');
        // createSeries('jumlah_alat', 'Berlangsung');

        function arrangeColumns() {

            var series1 = chart.series.getIndex(0);

            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series1.dataItems.length > 1) {
                var x0 = xAxis.getX(series1.dataItems.getIndex(0), "categoryX");
                var x1 = xAxis.getX(series1.dataItems.getIndex(1), "categoryX");
                var delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    var middle = chart.series.length / 2;

                    var newIndex = 0;
                    chart.series.each(function(series) {
                        if (!series.isHidden && !series.isHiding) {
                            series.dummyData = newIndex;
                            newIndex++;
                        }
                        else {
                            series.dummyData = chart.series.indexOf(series);
                        }
                    })
                    // chart.validateData(); 
                    var visibleCount = newIndex;
                    var newMiddle = visibleCount / 2;

                    chart.series.each(function(series) {
                        var trueIndex = chart.series.indexOf(series);
                        var newIndex = series.dummyData;

                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                        series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                    })
                }
            }
        }

} 
//===========
function MainChart(newData) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        var chart = am4core.create('chartdiv', am4charts.XYChart)
        chart.colors.step = 2;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'top'
        chart.legend.paddingBottom = 20
        chart.legend.labels.template.maxWidth = 95

        var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'bulan'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;

        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        function createSeries(value, name, color) {
            var series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value
            series.dataFields.categoryX = 'bulan'
            series.name = name
            series.columns.template.fill = color;
            series.columns.template.stroke = color;
            series.events.on("hidden", arrangeColumns);
            series.events.on("shown", arrangeColumns);

            var bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        chart.data =newData;
        // var set = ;
        chart.validateData();
        // Exporting
        chart.exporting.menu = new am4core.ExportMenu();

        chart.exporting.dataFields = {
            "tahun": 'Tahun', "bulan" :'Bulan',"batal": "Batal","selesai": "Selesai","onprogress":"On Progress"
        };


        createSeries('selesai', 'Selesai','#2ECC71');
        createSeries('batal', 'Batal','#F39C12');
        createSeries('onprogress', 'Berlangsung','#23B7E5');

        function arrangeColumns() {

            var series = chart.series.getIndex(0);

            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                var delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    var middle = chart.series.length / 2;

                    var newIndex = 0;
                    chart.series.each(function(series) {
                        if (!series.isHidden && !series.isHiding) {
                            series.dummyData = newIndex;
                            newIndex++;
                        }
                        else {
                            series.dummyData = chart.series.indexOf(series);
                        }
                    })
                    // chart.validateData(); 
                    var visibleCount = newIndex;
                    var newMiddle = visibleCount / 2;

                    chart.series.each(function(series) {
                        var trueIndex = chart.series.indexOf(series);
                        var newIndex = series.dummyData;

                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                        series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                    })
                }
            }
        }

} // end am4core.ready()

function ChartDuaUut(newData) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        var chart = am4core.create('chartCanvasUut', am4charts.XYChart)
        chart.colors.step = 2;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'top'
        chart.legend.paddingBottom = 20
        chart.legend.labels.template.maxWidth = 95

        var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'jenis_alat'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        /// custom label 
        xAxis.renderer.labels.template.rotation = -45;
        xAxis.renderer.labels.template.horizontalCenter = "right";
        xAxis.renderer.labels.template.verticalCenter = "middle";
        xAxis.renderer.labels.template.truncate = true;
        xAxis.renderer.labels.template.maxWidth = 120;
        // label.wrap = true;

        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        function createSeries(value, name) {
            var series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = value
            series.dataFields.categoryX = 'jenis_alat'
            series.name = name

            series.events.on("hidden", arrangeColumns);
            series.events.on("shown", arrangeColumns);

            var bullet = series.bullets.push(new am4charts.LabelBullet())
            bullet.interactionsEnabled = false
            bullet.dy = 30;
            bullet.label.text = '{valueY}'
            bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        chart.data =newData;
        // var set = ;
    
        chart.validateData();


        createSeries('jumlah_alat', 'Jumlah Alat');
        // createSeries('jumlah_alat', 'Alat');
        // createSeries('jumlah_alat', 'Berlangsung');

        function arrangeColumns() {

            var series1 = chart.series.getIndex(0);

            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series1.dataItems.length > 1) {
                var x0 = xAxis.getX(series1.dataItems.getIndex(0), "categoryX");
                var x1 = xAxis.getX(series1.dataItems.getIndex(1), "categoryX");
                var delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    var middle = chart.series.length / 2;

                    var newIndex = 0;
                    chart.series.each(function(series) {
                        if (!series.isHidden && !series.isHiding) {
                            series.dummyData = newIndex;
                            newIndex++;
                        }
                        else {
                            series.dummyData = chart.series.indexOf(series);
                        }
                    })
                    // chart.validateData(); 
                    var visibleCount = newIndex;
                    var newMiddle = visibleCount / 2;

                    chart.series.each(function(series) {
                        var trueIndex = chart.series.indexOf(series);
                        var newIndex = series.dummyData;

                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                        series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                        series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                    })
                }
            }
        }

} 
</script>
<?= $this->endSection() ?>

