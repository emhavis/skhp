<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/fullcalendar/main.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Informasi Tutup Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Informasi Tutup Layanan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Tutup Layanan</div>
            <div>
                <a class="btn btn-primary btn-sm" href="/closedevent/create"><i class="fa fa-plus"></i> Data Baru</a>
                <button class="btn btn-default btn-sm" data-toggle="button" aria-pressed="false" id="toggle">
                    <span class="active-hidden"><i class="fa fa-calendar"></i></span>
                    <span class="active-visible"><i class="fa fa-table"></i></span>
                </button>
            </div>
        </div>
        <div class="ibox-body">
            <div id="table">
            <table class="table table-striped table-bordered table-hover" id="events" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Tanggal Mulai</th>
                        <th>Tanggal Selesai</th>
                        <th>Kejadian/Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($events as $event):?>
                    <tr>
                        <td><?= date("d-m-Y", strtotime($event->start_date)) ?></td>
                        <td><?= date("d-m-Y", strtotime($event->end_date)) ?></td>
                        <td><?= $event->name ?></td>
                        <td>
                            <a class="btn btn-default btn-sm" href="/closedevent/edit/<?= $event->id ?>"><i class="fa fa-edit"></i> Edit</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

            <div id='calendar'></div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/fullcalendar/main.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#table').hide();
        $('#events').DataTable({
            pageLength: 10,
        });

        var calendar = new FullCalendar.Calendar($('#calendar')[0], {
            initialView: 'dayGridMonth',
            events: [
                <?php foreach ($events as $event): ?>
                { 
                title: '<?= $event->name ?>', 
                start: '<?= $event->start_date ?>',
                end: '<?= $event->end_date ?>',
                url: '/closedevent/edit/<?= $event->id ?>',
                color: 'red',
                },
                <?php endforeach ?>
            ],   
        });
        calendar.render();

        $('#toggle').click(function() {
            if ($(this).hasClass('active')) {
                $('#table').hide();
                $('#calendar').show();
            } else {
                $('#table').show();
                $('#calendar').hide();
            }
        })
    })
</script>
<?= $this->endSection() ?>