<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Informasi Tutup Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/closedevent">Informasi Tutup Layanan</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Perubahan Informasi Tutup Layanan</div>
            <div>
                <a class="btn btn-default btn-sm" href="/closedevent"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="event" action="<?= base_url('closedevent/edit/'.$event->id); ?>" method="post">
                <?= csrf_field() ?>
                <div class="row">
                    <div class="col-4 form-group" >
                        <label>Tanggal Mulai</label>
                        <div class="input-group date" id="start_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                value="<?= date("d-m-Y", strtotime($event->start_date)); ?>"
                                name="start_date">
                        </div>
                    </div>
                    <div class="col-4 form-group" >
                        <label>Tanggal Berakhir</label>
                        <div class="input-group date" id="end_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                value="<?= date("d-m-Y", strtotime($event->end_date)); ?>"
                                name="end_date">
                        </div>
                    </div>
                    <div class="col-4 form-group">
                        <label>Hari Libur</label>
                        <input class="form-control" type="text" 
                            value="<?= $event->name ?>"
                            name="name" id="name">
                    </div>
                </div>
               
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        $('#start_date, #end_date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('#event').validate({
            errorClass: "help-block",
            rules: {
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true,
                },
                name: {
                    required: true,
                },
            },
            highlight: function(e) {
                $(e).closest(".form-group").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>