<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pemilik Standar Ukuran</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pemilik Standar Ukuran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Pemilik Standar Ukuran</div>
            <?php if($is_uml < 1 && $customerLogin >1): ?>
                <div>
                    <a class="btn btn-primary btn-sm" href="<?= base_url('useruutowner/create'); ?>"><i class="fa fa-plus"></i> Data Baru</a>
                </div>
            <?php endif?>
        </div>
        <div class="ibox-body">
            <div id="table">
            <table class="table table-striped table-bordered table-hover" id="labs" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pemilik Alat</th>
                        <th>Kota</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($owners as $owner):?>
                    <tr>
                        <td><?= $owner->nama ?></td>
                        <td><?= $owner->kota ?></td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('useruttpowner/delete/' . $owner->map_id); ?>"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

            <div id='calendar'></div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#labs').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>