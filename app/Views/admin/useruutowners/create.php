<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pemilik Standar Ukuran</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('useruttpowner'); ?>">Pemilik Standar Ukuran</a>
        </li>
        <li class="breadcrumb-item">Penambahan Data Pemilik Standar Ukuran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Penambahan Data Pemilik Standar Ukuran</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('useruutowner'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>

            <form id="lab" action="<?= base_url('useruutowner/create'); ?>" method="post">
                <?= csrf_field() ?>
                <input type="hidden" name="owner_id" id="owner_id"/>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kategori Pemilik Alat</label>
                        <select class="form-control" name="jenis_identitas" id="jenis_identitas">
                            <option value="uml">Unit Metrologi Legal</option>
                            <option value="nib">Perusahaan</option>
                            <option value="npwp">Pribadi/Perseorangan</option>
                        </select>
                    </div>
                    <div class="col-3 form-group">
                        <label>Identitas Pemilik Alat: <span id="identitas">UML</span></label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="id_no" id="id_no">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="find_uut_owner">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3 form-group">
                        <label>NIB</label>
                        <input type="text" class="form-control" name="nib" id="nib_owner" readonly />
                    </div>
                    <div class="col-3 form-group">
                        <label>NPWP</label>
                        <input type="text" class="form-control" name="npwp" id="npwp_owner" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama_owner" readonly />
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat_owner" readonly></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kabupaten/Kota</label>
                        <input type="hidden" name="kota_id" id="kota_id_owner" />
                        <input type="text" class="form-control" name="kota" id="kota_owner" readonly />
                    </div>
                    <div class="col-3 form-group">
                        <label>Provinsi</label>
                        <input type="text" class="form-control" name="provinsi" id="provinsi_owner" readonly />
                    </div>
                    <div class="col-3 form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" id="email_owner" readonly />
                    </div>
                    <div class="col-3 form-group">
                        <label>Telepon</label>
                        <input type="text" class="form-control" name="telepon" id="telepon_owner" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Penanggung Jawab</label>
                        <input type="hidden" name="bentuk_usaha_id" id="bentuk_usaha_id" />
                        <input type="text" class="form-control" name="penanggung_jawab" id="pj_owner" readonly />
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="notFound" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data pemilik alat tidak ditemukan. Pastikan gunakan NIB/NPWP yang valid.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        $('#jenis_identitas').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            val = e.params.data.id;
            $('#identitas').html(val.toUpperCase());
        });

        $('#find_uut_owner').click(function(e) {
            console.log('Its was clicked');
            by = $('#jenis_identitas').val();
            q = $('#id_no').val();
            /*
            if(by =='uml'){
                by = 'uml_id'
            }
            */

            url = '<?= base_url(); ?>/useruutowner/findByIdentity';
            console.log(url)
            $.ajax({
                type: 'post',
                url: url,
                data: {
                    by: by,
                    q: q,
                    <?= csrf_token() ?>: '<?= csrf_hash() ?>',
                },
                success: function (data) {                 
                    if (data) {
                        $('#owner_id').val(data.id);
                        $('#nib_owner').val(data.nib);
                        $('#npwp_owner').val(data.npwp);
                        $('#nama_owner').val(data.nama);
                        $('#alamat_owner').val(data.alamat);
                        $('#email_owner').val(data.email);
                        $('#telepon_owner').val(data.telepon);
                        $('#kota_id_owner').val(data.kota_id);
                        $('#kota_owner').val(data.kota);
                        $('#provinsi_owner').val(data.provinsi);
                        $('#pj_owner').val(data.penanggung_jawab);
                        $('#bentuk_usaha_id').val(data.bentuk_usaha_id);
                    } else {
                        if (by == 'kode_daerah') {

                            $.ajax({
                                type: 'post',
                                url: '<?= base_url(); ?>/uutowner/findUml',
                                data: {
                                    by: by,
                                    q: q,
                                    <?= csrf_token() ?>: '<?= csrf_hash() ?>',
                                },
                                success: function (data) { 
                                    $('#nib_owner').val(null);
                                    $('#npwp_owner').val(null);
                                    $('#kode_daerah_owner').val(data.kode_daerah);
                                    $('#nama_owner').val(data.nama_uml);
                                    $('#alamat_owner').val(data.alamat);
                                    $('#email_owner').val(data.email);
                                    $('#telepon_owner').val(data.no_tlp);
                                    $('#kota_id_owner').val(data.id_kota);
                                    $('#kota_owner').val(data.nama_kota);
                                    $('#provinsi_owner').val(data.nama_provinsi);
                                    $('#pj_owner').val(data.nama_kepala_uml);
                                    $('#bentuk_usaha_id').val(0);
                                }
                            });

                        } else {

                            $.ajax({
                                type: 'post',
                                url: '<?= base_url(); ?>/auth/check_data/' + by,
                                data: { 
                                    <?= csrf_token() ?>: "<?= csrf_hash() ?>",
                                    value: q,
                                },
                                success: function (res) {
                                    if (res.kode == 200) {
                                        var data = res.data;
                                        $('#nib_owner').val(data.nib);
                                        $('#npwp_owner').val(data.npwp);
                                        $('#nama_owner').val(data.full_name);
                                        $('#alamat_owner').val(data.alamat);
                                        $('#email_owner').val(data.email);
                                        $('#telepon_owner').val(data.telepon);
                                        $('#kota_id_owner').val(data.kota_id);
                                        $('#kota_owner').val(data.kota);
                                        $('#provinsi_owner').val(data.provinsi);
                                        $('#pj_owner').val(data.penanggung_jawab);
                                        $('#bentuk_usaha_id').val(data.bentuk_badan_usaha_id);
                                    } else {
                                        $('#notFound').modal('show');
                                    }
                                }, 
                                error: function(e) {
                                    $('#notFound').modal('show');
                                }
                            });

                        }
                    }
                    
                },
                error: function (err) {
                    $('#notFound').modal('show');
                }
            });
        });
    })
</script>
<?= $this->endSection() ?>