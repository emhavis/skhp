<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pelacakan Layanan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Layanan</div>
        </div>
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No Booking</th>
                        <th>No Pendaftaran</th>
                        <th>No Order</th>
                        <th>Jenis Layanan</th>
                        <th>Jenis Alat</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($bookings as $booking):?>
                    <tr>
                        <td><?= $booking->booking_no ?></td>
                        <td><?= $booking->no_register ?></td>
                        <td><?= $booking->no_order ?></td>
                        <td><?= $booking->jenis_layanan ?></td>
                        <td><?= $booking->tool_type ?></td>
                        <td>
                            <?= $booking->status ?>
                            <?php if ($booking->status_id == 2): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI!</span>
                            <?php elseif ($booking->status_id == 6): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif ($booking->status_id == 19 && $booking->payment_status_id == 6): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif (($booking->status_id == 12 || $booking->status_id == 13 || $booking->status_id == 14) 
                                && ($booking->payment_status_id == 6 || $booking->spuh_payment_status_id == 6)): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif ($booking->status_id == 15 && $booking->payment_status_id == 6): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PENGUJIAN TERTUNDA!</span>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif ($booking->status_id == 15): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PENGUJIAN TERTUNDA!</span>
                            <?php elseif ($booking->status_id == 23): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KELENGKAPAN DATA!</span>
                            <?php endif ?>
                        </td>
                        <td>
                            <a class="btn btn-default" href="<?=base_url('tracking/read/' . $booking->id);?>"><i class="fa fa-book"></i> Lihat</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>