<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pelacakan Layanan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Layanan</div>
        </div>
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nomor Booking</th>
                        <th>Jenis Layanan</th>
                        <th>Jadwal</th>
                        <th>Estimasi Total Biaya</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($bookings as $booking):?>
                    <tr>
                        <td><?= $booking->booking_no ?></td>
                        <td><?= $booking->jenis_layanan ?></td>
                        <td><?= $booking->lokasi_pengujian == 'dalam' ? $booking->est_arrival_date :
                            ($booking->est_schedule_date_from) .' s/d '. ($booking->est_schedule_date_to) ?></td>
                        <td><?= number_format($booking->total_price,2,",",".") ?></td>
                        <td>
                            <?= $booking->status ?>
                            <?php if ($booking->status_id == 2): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI!</span>
                            <?php elseif ($booking->status_id == 4): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI JADWAL!</span>
                            <?php elseif ($booking->status_id == 6): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif ($booking->status_id == 13 && $booking->payment_status_id == 6): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif ($booking->status_id == 15 && $booking->payment_status_id == 6): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PENGUJIAN TERTUNDA!</span>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU PEMBAYARAN!</span>
                            <?php elseif ($booking->status_id == 15): ?>
                            <span class="badge badge-danger badge-pill m-r-5 m-b-5">PENGUJIAN TERTUNDA!</span>
                            <?php endif ?>
                        </td>
                        <td>
                            <a class="btn btn-default" href="<?=base_url('tracking/read/' . $booking->id);?>"><i class="fa fa-book"></i> Lihat</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>