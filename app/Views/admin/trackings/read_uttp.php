<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />

<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('tracking'); ?>">Pelacakan Layanan</a>
        </li>
        <li class="breadcrumb-item">Data Layanan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox collapsed-mode">
        <div class="ibox-head">
            <div class="ibox-title">Pelacakan
                <?php if ($request->status_id == 2): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI!</span>
                <?php elseif (($request->status_id == 6) || ($request->payment_status_id == 6) || ($request->spuh_payment_status_id == 6)): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">INPUT BUKTI BAYAR!</span>
                <?php elseif ($request->status_id == 23): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">LENGKAPI DATA PENDAFTARAN!</span>
                <?php endif ?>
            </div>
        <?php if($booking->uttp_owner_id > 0) :?>
            <div>
                <?php if ($request->status_id == 2): ?>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-check"></i> Konfirmasi</a>
                <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#cancelConfirmModal"><i class="fa fa-calendar-times"></i> Batalkan Penjadwalan</a>
                <a href="<?= base_url('tracking/edit_pemilik/' . $booking->id ); ?>" class="btn btn-info btn-sm" ><i class="fa fa-edit"></i> Edit Data Pemilik</a>
                <?php elseif ($request->status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php elseif ($request->status_id == 19 && $request->payment_status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php elseif ($request->status_id == 15 && $request->payment_status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php elseif (($request->status_id == 12 || $request->status_id == 13 || $request->status_id == 14) && $request->payment_status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php endif ?>
                <?php if (($request->status_id == 6 || $request->status_id == 12 || $request->status_id == 13 || $request->status_id == 14 || $request->status_id == 19) 
                    && $request->spuh_payment_status_id == 6 ): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoicetuhp'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice TUHP</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentTUHPModal"><i class="fa fa-money-check"></i> Bukti Bayar TUHP</a>
                <?php endif ?>
                <?php if ($request->lokasi_pengujian == 'luar' && ($request->status_id == 6 || $request->status_id == 12 || $request->status_id == 13 || $request->status_id == 14 || $request->status_id == 19) 
                    && ($request->spuh_payment_status_id >= 7 || $request->payment_status_id >= 7)): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/kuitansi'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-money-check"></i> Kuitansi</a>
                <?php endif ?>
                <?php if ($request->status_id >= 8 && $request->status_id <= 20 && $request->lokasi_pengujian == 'dalam'): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/kuitansi'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-money-check"></i> Kuitansi</a>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/buktiorder'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-tasks"></i> Bukti Order</a>
                <?php endif ?>
                <?php if ($request->status_id == 12 && $request->lokasi_pengujian == 'luar'): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/surattugas'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-file"></i> Surat Tugas</a>
                <?php endif ?>
                <?php if ($request->status_id == 23 && $request->lokasi_pengujian == 'luar'): ?>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registerModal"><i class="fa fa-check"></i> Daftar Ulang</a>
                <?php endif ?>
                <a class="btn btn-default btn-sm" href="<?= base_url('tracking'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        <?php else : ?>
            <div>
                <?php if ($request->status_id == 2): ?>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-check"></i> Konfirmasi</a>
                <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#cancelConfirmModal"><i class="fa fa-check"></i> Batalkan Penjadwalan</a>
                <a href="<?= base_url('tracking/edit_pemilik/' . $booking->id ); ?>" class="btn btn-info btn-sm" ><i class="fa fa-edit"></i> Edit Data Pemilik</a>
                <?php elseif ($request->status_id == 4): ?>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmScheduleModal"><i class="fa fa-check"></i> Konfirmasi Jadwal</a>
                <?php elseif ($request->status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice_uut'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php elseif (($request->status_id == 12 || $request->status_id == 13 || $request->status_id == 14) && $request->payment_status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice_uut'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php elseif ($request->status_id == 15 && $request->payment_status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoice_uut'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money-check"></i> Bukti Bayar</a>
                <?php endif ?>
                <?php if (($request->status_id == 6 || $request->status_id == 12 || $request->status_id == 13 || $request->status_id == 14 || $request->status_id == 19) 
                    && $request->spuh_payment_status_id == 6): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/invoicetuhp_uut'); ?>/<?= $request->id ?>" target="_blank"><i class="fa fa-money-check"></i> Invoice TUHP</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#paymentTUHPModal"><i class="fa fa-money-check"></i> Bukti Bayar TUHP</a>
                <?php endif ?>
                <?php if ($request->status_id >= 7): ?>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/kuitansi_uut'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-money-check"></i> Kuitansi</a>
                <a class="btn btn-info btn-sm" href="<?= base_url('tracking/buktiorder_uut'); ?>/<?= $request->id ?>"  target="_blank"><i class="fa fa-tasks"></i> Bukti Order</a>
                <?php endif ?>
                <?php if ($request->status_id == 23 && $request->lokasi_pengujian == 'luar'): ?>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registerModal"><i class="fa fa-check"></i> Daftar Ulang</a>
                <?php endif ?>
                <a class="btn btn-default btn-sm" href="<?= base_url('tracking'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        <?php endif ?>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashdata('errors')) : ?>
                <?php foreach(session()->getFlashdata('errors') as $err): ?>
                <div class="alert alert-danger">
                    <?= $err ?>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if($request->status_id == 15): ?>
                <div class="alert alert-danger"><strong>Pengujian Tertunda!</strong> Pengujian mengalami penundaan dikarenakan: <br/>
                    <?php foreach ($orders as $order): ?>
                        <?= $order->pending_notes ?>
                        <!--
                        <strong>Penundaan direncanakan selesai pada tanggal <?= date("d-m-Y", strtotime($order->pending_estimated)) ?></strong>
                        -->
                    <?php endforeach ?>
                </div>
            <?php endif ?>
            <?php if($request->status_id == 23): ?>
                <div class="alert alert-danger">
                    <strong>Lengkapi Data Pendaftaran!</strong> Data pendaftaran perlu diperbaiki: <br/>
                    <?= $request->not_complete_notes ?>
                </div>
                <div class="row">
                    <div class="col-9">
                    </div>
                    <div class="col-3 d-flex justify-content-end">
                        <a class="btn btn-info btn-sm" href="<?= base_url('tracking/edit/' .$booking->id); ?>"><i class="fa fa-edit"></i> Edit Data Pendaftaran</a>
                    </div>
                </div>
            <?php endif ?>
           
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" 
                            value="<?= $booking->booking_no ?>" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Pendaftaran</label>
                        <input class="form-control" type="text" name="no_register" 
                            value="<?= $request->no_register ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Status</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $request->status ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $request->jenis_layanan ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <?php if($request->service_type_id == 1 or $request->service_type_id == 2): ?>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian"  
                            value="<?= $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        <?php else: ?>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian"  
                            value="<?= ($request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor') . ($request->lokasi_dl == 'dalam' ? ' (Dalam Negeri)' : ' (Luar Negeri)') ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $request->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            value="<?= $request->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly><?= $request->addr_sertifikat ?></textarea>
                    </div>
                </div>
                <?php if($request->lokasi_pengujian == 'luar'): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label id="file_surat_permohonan">File Surat Permohonan</label>
                        <input class="form-control" id="file_surat_permohonan" readonly
                            type="text" name="file_surat_permohonan" value="<?= $request->file_surat_permohonan ?>"/>
                    </div>
                    <div class="col-3 form-group">
                        <label id="no_surat_permohonan">No Surat Permohonan</label>
                        <input class="form-control" id="no_surat_permohonan" readonly
                            type="text" name="no_surat_permohonan" value="<?= $request->no_surat_permohonan ?>"/>
                    </div>
                    <div class="col-3 form-group" >
                        <label id="tgl_surat_permohonan">Tanggal Surat Permohonan</label>
                        <input class="form-control" type="text" name="tgl_surat_permohonan" readonly
                            id="tgl_surat_permohonan" value="<?= date("d-m-Y", strtotime($request->tgl_surat_permohonan)) ?>">
                    </div>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    
                    <?php if($request->lokasi_pengujian == 'dalam'): ?>
                        <div class="col-3 form-group" >
                        
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= date("d-m-Y", strtotime($booking->est_arrival_date)) ?>">
                        </div>
                    <?php else: ?>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengujian/Pemeriksaan</label>
                            <input class="form-control" type="text" name="scheduled_test_date_from" readonly
                                id="scheduled_test_date_from" value="<?= date("d-m-Y", strtotime($request->scheduled_test_date_from)) . ' s/d ' .date("d-m-Y", strtotime($request->scheduled_test_date_to)) ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Total Biaya SPUH</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($request->spuh_price, 2, ',', '.') ?>"readonly>
                        </div>
                    <?php endif ?>
                    <div class="col-3 from-group">
                        <label>Total Biaya PNBP</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($request->total_price, 2, ',', '.') ?>"readonly>
                    </div>
                </div>
        </div>
    </div>
    <?php if($booking->service_type_id ==1 || $booking->service_type_id ==2){ ?>
        <?php foreach ($items as $item):?>
            <?= $this->setVar('item' ,$item)->include('admin/trackings/read_ibox_uut'); ?>
        <?php endforeach;?>
    <?php }else{?>
        <?php foreach ($items as $item):?>
            <div class="ibox collapsed-mode">
                <div class="ibox-head">
                    <div class="ibox-title"> <small><strong>Alat: <?= $item->uttp_type ?></strong></small>
                        <?php if ($request->status_id == 2): ?>
                        <?php
                            $filterTerms = array_filter($terms, function($var) use ($item) { return $var->request_item_id == $item->id; });
                            if (count($filterTerms) == 0) {
                        ?>
                        <a class="btn btn-danger btn-sm" href="<?= base_url('term/create/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <small>Pastikan Anda sudah menyetujui Kaji Ulang</small>
                        <?php } else { ?>
                        <a class="btn btn-warning btn-sm" href="<?= base_url('term/edit/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <?php } ?>
                        <?php endif ?>
                    </div>

                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    
            
                </div>
                <div class="ibox-body" style="display: none;">
                    <?php if ($request->status_id == 2 || $request->status_id == 23): ?>
                    <div class="row">
                        <div class="col-9">
                        </div>
                        <div class="col-3 d-flex justify-content-end">
                            <a class="btn btn-info btn-sm" href="<?= base_url('tracking/edit_alat/' .$booking->id .'/'. $item->id); ?>"><i class="fa fa-edit"></i> Edit Data Alat</a>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Nomor Order</label>
                            <input class="form-control" type="text" name="no_order" 
                                value="<?= $item->no_order ?>" readonly>
                        </div>
                        <div class="col-3 form-group">
                            <label>Status</label>
                            <input class="form-control" type="text" name="status" 
                                value="<?= $item->status ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="text" class="form-control" name="type_id" id="type_id" 
                                value="<?= $item->uttp_type ?>" readonly/>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->tool_brand ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" 
                                value="<?= $item->tool_media ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                    
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimum</label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Minimum</label>
                            <input type="text" class="form-control" name="tool_capacity_min" id="tool_capacity_min" 
                                value="<?= $item->tool_capacity_min ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                                value="<?= $item->tool_capacity_unit ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $item->tool_made_in ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Nama Pabrik Pembuat</label>
                            <input class="form-control" type="text" name="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrik Pembuat</label>
                            <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>

                    <?php if(($request->lokasi_pengujian == 'luar')): ?>
                        <?php if ($request->lokasi_dl == 'dalam'): ?>
                        <?php $city = current(array_filter($cities, function($cityItem) use ($item) {
                            return $item->location_kabkot_id == $cityItem->id;
                        })); ?>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Lokasi Pengujian</label>
                            <textarea class="form-control" type="text" name="location" readonly><?= $item->location ?>, <?= $city->nama ?>, <?= $city->provinsi ?>
                            </textarea>
                        </div>
                    </div>
                        <?php else: ?>
                        <?php $negara = current(array_filter($negaras, function($negaraItem) use ($item) {
                            return $item->location_negara_id == $negaraItem->id;
                        })); ?>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Lokasi Pengujian</label>
                            <textarea class="form-control" type="text" name="location" readonly><?= $item->location ?>, <?= $negara->nama_negara ?>
                            </textarea>
                        </div>
                    </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($booking->service_type_id == 4 || $booking->service_type_id == 5): ?>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Lokasi Alat</label>
                            <textarea class="form-control" name="location" id="location" readonly><?= $item->location ?></textarea>
                        </div>
                    </div>
                        <?php endif ?>
                    <?php endif ?>

                    <div class="row">
                        <div class="col-12">
                            <strong>Daftar Pemeriksaan (Jumlah: <?= $item->quantity ?>)</strong>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Jenis Pengujian</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach (array_filter($inspections, function($var) use ($item) { return $var->request_item_id == $item->id; }) as $inspection):?>
                                    <tr>
                                        <td><?= $inspection->inspection_type ?></td>
                                        <td><?= $inspection->quantity ?></td>
                                        <td><?= $inspection->unit ?></td>
                                        <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                        <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="4">Subtotal</th>
                                        <th><?= number_format($item->subtotal, 2, ',', '.') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
    <?php } ?>
    <?php if($booking->service_type_id ==1 || $booking->service_type_id ==2){ ?>
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Riwayat</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered table-hover" id="history" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Tanggal/Waktu</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (array_filter($histories, function($var) use ($item) { return $var->request_item_id == $item->id && $var->request_id == $item->service_request_id; }) as $history):?>
                            <tr>
                                <td><?= date("d M Y H:i:s", strtotime($history->created_at)) ?></td>
                                <td>
                                    <?= $history->status ?>
                                    <?php
                                        if ($history->order_id != null) {
                                            if ($history->order_status_id < 3) {
                                                echo '<br/>Konsep sertifikat dalam proses';
                                            }
                                            if ($history->order_status_id == 3) {
                                                echo '<br/>Sertifikat sudah dapat didownload';
                                            }
                                            if ($history->warehouse_status_id == 1) {
                                                echo '<br/>Alat sudah dapat diambil';
                                            }
                                            if ($history->warehouse_status_id == 2) {
                                                echo '<br/>Alat sudah diambil';
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php }else{ ?>
        <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Riwayat</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered table-hover" id="history" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Tanggal/Waktu</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (array_filter($histories, function($var) use ($item) { return $var->request_item_id == $item->id && $var->request_id == $item->request_id; }) as $history):?>
                            <tr>
                                <td><?= date("d M Y H:i:s", strtotime($history->created_at)) ?></td>
                                <td>
                                    <?php 
                                        if ($request->lokasi_pengujian == 'dalam') {
                                    ?>
                                    <?= $history->status ?>
                                    <?php 
                                        } else {
                                            if ($history->request_status_id == 12) {
                                                if ($history->spuh_payment_status_id == 5) {
                                                    echo 'Surat Tugas sudah disetujui';
                                                } elseif ($history->spuh_payment_status_id == 6) {
                                                    echo 'Perlu pembayaran SPUH';
                                                } elseif ($history->spuh_payment_status_id == 7) {
                                                    echo 'Pembayaran SPUH diterima';
                                                } elseif ($history->spuh_payment_status_id == 8) {
                                                    echo 'Pembayaran SPUH tervalidasi';
                                                } else {
                                                    echo 'Sedang dalam proses penagihan SPUH';
                                                }
                                            } elseif ($history->request_status_id == 13) {
                                                if ($history->payment_status_id == 6) {
                                                    echo 'Perlu pembayaran PNBP';
                                                } elseif ($history->payment_status_id == 7) {
                                                    echo 'Pembayaran PNBP diterima';
                                                } elseif ($history->payment_status_id == 8) {
                                                    echo 'Pembayaran PNBP tervalidasi';
                                                } else {
                                                    echo 'Sedang dalam proses penagihan PNBP';
                                                }
                                            } else {
                                                echo $history->status;
                                            }
                                        }
                                        ?>
                                    <?php
                                        if ($history->order_id != null) {
                                            if ($history->order_status_id < 3) {
                                                echo '<br/>Konsep sertifikat dalam proses';
                                            }
                                            if ($history->order_status_id == 3) {
                                                echo '<br/>Sertifikat sudah dapat didownload';
                                            }
                                            if ($history->warehouse_status_id == 1) {
                                                echo '<br/>Alat sudah dapat diambil';
                                            }
                                            if ($history->warehouse_status_id == 2) {
                                                echo '<br/>Alat sudah diambil';
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    
</div>

<!-- Modal -->
<div class="modal fade" id="cancelConfirmModal" tabindex="-1" aria-labelledby="cancelConfirmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancelConfirmModalLabel">Konfirmasi Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Anda memutuskan untuk membatalkan pendaftaran pengujian.
                    <strong>Apakah Anda mengkonfirmasi pembatalan ini?</strong>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <form id="booking" action="<?= base_url('tracking/cancel'); ?>/<?= $booking->id ?>" method="post">
                    <?= csrf_field() ?>
                    <button type="submit" class="btn btn-danger">Ya, Konfirmasi Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Konfirmasi Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <?php if ($check_terms): ?>
                    <div class="modal-body">
                        <p>
                            Periksa kembali data yang didaftarkan. 
                            <strong>Apakah data tersebut sudah benar?</strong>
                        </p>
                        <small>
                            <p>Pendaftaran dengan data yang sudah benar, klik tombol <b>Ya, Sudah Benar</b>, dan akan dilanjutan untuk proses berikutnya.</p>
                            <p>Jika masih ada data yang belum benar, silakan perbaiki dengan mengklik tombol <b>Edit Data Pemilik</b> dan/atau <b>Edit Data Alat</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <form id="booking" action="<?= base_url('tracking/confirm'); ?>/<?= $booking->id ?>" method="post">
                            <?= csrf_field() ?>
                            <input type="hidden" name="status_id" value="<?= $booking->lokasi_pengujian == 'dalam' ? '5' : '3' ?>" />
                            <button type="submit" class="btn btn-success">Ya, Sudah Benar</button>
                        </form>
                        <!--
                        <form id="booking_noconfirm" action="<?= base_url('tracking/confirm'); ?>/<?= $booking->id ?>" method="post">
                            <?= csrf_field() ?>
                            <input type="hidden" name="status_id" value="1" />
                            <button type="submit" class="btn btn-danger">Tidak, Masih Terdapat Kesalahan</button>
                        </form>
                        -->
                    </div>
                <?php else: ?>
                <div class="modal-body">
                    <p>
                        Formulir kaji ulang belum disetujui semua. 
                        <strong>Anda belum dapat mengkonfirmasi semua data.</strong>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
        <?php endif; ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmScheduleModal" tabindex="-1" aria-labelledby="confirmScheduleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmScheduleModalLabel">Konfirmasi Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Jadwal Berangkat</label>
                        <input type="text" class="form-control" name="scheduled_test_date_from" id="scheduled_test_date_from"
                            value="<?= date("d M Y", strtotime($request->scheduled_test_date_from)) ?>" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Jadwal Pulang</label>
                        <input type="text" class="form-control" name="scheduled_test_date_to" id="scheduled_test_date_to"
                            value="<?= date("d M Y", strtotime($request->scheduled_test_date_to)) ?>" readonly />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <form id="booking" action="<?= base_url('tracking/confirmschedule'); ?>/<?= $booking->id ?>" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="status_id" value="5" />
                    <button type="submit" class="btn btn-success">Ya, Setuju</button>
                </form>
                <form id="booking_noconfirm" action="<?= base_url('tracking/confirmschedule'); ?>/<?= $booking->id ?>" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="status_id" value="3" />
                    <button type="submit" class="btn btn-danger">Jadwal Tidak Cocok</button>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="paymentModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="confirmModalLabel">Data Pembayaran</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="payment" action="<?= base_url('tracking/pay'); ?>/<?= $booking->id ?>" method="post"
            enctype="multipart/form-data" >
        <?= csrf_field() ?>
        <div class="modal-body">
            <?php if ($request->total_price > 0 && $request->payment_date == null): ?>
                
            <div class="row">
                <div class="col-4 form-group">
                    <label>Total Tagihan</label>
                    <input type="text" class="form-control" name="total_price" id="total_price"
                        value="<?= number_format($request->denda_inv_price > 0 ? $request->denda_inv_price : $request->total_price, "2", ",", ".")  ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>Kode Billing</label>
                    <input type="text" class="form-control" name="billing_code" id="billing_code"
                        value="<?= $request->billing_code ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>Batas Waktu Pembayaran</label>
                    <input type="text" class="form-control" name="billing_to_date" id="billing_to_date"
                        value="<?= date("d M Y", strtotime($request->billing_to_date)) ?>" readonly />
                </div>
            </div>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Tanggal Pembayaran</label>
                    <input type="text" class="form-control" name="payment_date" id="payment_date" autocomplete="off"/>
                </div>
                <div class="col-4 form-group">
                    <label>Kode Pembayaran (NTPN)</label>
                    <input type="text" class="form-control" name="payment_code" id="payment_code"/>
                </div>
                <div class="col-4 form-group">
                    <label>Atas Nama Kuitansi</label>
                    <select class="form-control" name="an_kuitansi" id="an_kuitansi">
                        <option value="pemohon">Pemohon</option>
                        <option value="pemilik">Pemilik Alat</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Bukti Pembayaran</label>
                    <input class="form-control" id="file_payment_ref" 
                        type="file" name="file_payment_ref" 
                        accept="application/pdf, image/png, image/gif, image/jpeg" />
                </div>
            </div>
            <?php endif; ?>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Konfirmasi Bayar</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="paymentTUHPModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="confirmModalLabel">Data Pembayaran TUHP</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form role="form" id="paymenttuhp" action="<?= base_url('tracking/paytuhp'); ?>/<?= $booking->id ?>" method="post"
            enctype="multipart/form-data" >
        <?= csrf_field() ?>
        <div class="modal-body">
            <?php if ($request->lokasi_pengujian == 'luar'  && $request->spuh_payment_date == null && 
                ($request->spuh_add_price > 0 || $request->spuh_inv_price > 0)): ?>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Tagihan SPUH</label>
                    <input type="text" class="form-control" name="spuh_inv_price" id="spuh_inv_price"
                        value="<?= number_format($request->spuh_add_price > 0 ? $request->spuh_add_price : $request->spuh_inv_price, "2", ",", ".") ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>No SPUH</label>
                    <input type="text" class="form-control" name="spuh_no" id="spuh_no"
                        value="<?= $request->spuh_spt ?>" readonly />
                </div>
                <div class="col-4 form-group">
                    <label>Tanggal Pembayaran</label>
                    <input type="text" class="form-control" name="spuh_payment_date" id="spuh_payment_date" autocomplete="off"/>
                </div>
            </div>
            <div class="row">
                <div class="col-4 form-group">
                    <label>Bukti Pembayaran</label>
                    <input class="form-control" id="file_spuh_payment_ref" 
                        type="file" name="file_spuh_payment_ref" 
                        accept="application/pdf, image/png, image/gif, image/jpeg" />
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Konfirmasi Bayar</button>
        </div>
        </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="registerModalLabel">Daftar Ulang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Anda memutuskan untuk melakukan pendaftaran kembali setelah perbaikan data.
                    <strong>Apakah Anda mengkonfirmasi pendaftaran ini?</strong>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <form id="booking" action="<?= base_url('tracking/daftarulang'); ?>/<?= $booking->id ?>" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="status_id" value="1" />
                    <button type="submit" class="btn btn-success">Ya, Konfirmasi Mendaftar Ulang</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#items').DataTable({
            pageLength: 10,
        });

        $('#an_kuitansi').select2({
            theme: 'bootstrap4',
        });

        $('#book').click(function(e) {
            e.preventDefault()
            var form = $('#booking');
            form.attr('action', '<?= base_url('booking/submit'); ?>/<?= $booking->id ?>')
            form.submit();
        });

        $('#payment_date').datepicker({
            format: 'dd-mm-yyyy',
            endDate: new Date() < new Date('<?= date("Y-m-d", strtotime($request->billing_to_date)) ?>') ? new Date() : new Date('<?= date("Y-m-d", strtotime($request->billing_to_date)) ?>'),
        });

        $('#spuh_payment_date').datepicker({
            format: 'dd-mm-yyyy',
            endDate: new Date(),
        });

        $("#payment").validate({
            rules: {
                payment_date: {
                    required: true,
                },
                payment_code: {
                    required: true,
                },
                file_payment_ref: {
                    required: true,
                },
            },
            messages: {
                payment_date: 'Tanggal pembayaran wajib diisi',
                payment_code: 'Kode pembayaran wajib diisi',
                file_payment_ref: 'File bukti pembayaran wajib diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
        
        $("#paymenttuhp").validate({
            rules: {
                payment_date: {
                    required: !0,
                },
                payment_code: {
                    required: !0,
                },
                file_payment_ref: {
                    required: !0,
                },
            },
            messages: {
                payment_date: 'Tanggal pembayaran wajib diisi',
                payment_code: 'Kode pembayaran wajib diisi',
                file_payment_ref: 'File bukti pembayaran wajib diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>