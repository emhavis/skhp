<div class="ibox collapsed-mode">
    <div class="ibox-head"> 
        <div class="ibox-title"> <small><strong>Alat: <?= $item->uut_type ?></strong></small> 
            <?php if ($request->status_id == 2): ?>
            <?php
                $filterTerms = array_filter($terms, function($var) use ($item) { return $var->request_item_id == $item->id; });
                if (count($filterTerms) == 0) {
            ?>
            
            <a class="btn btn-danger btn-sm" href="<?= base_url('term_uut/create/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
            <small>Pastikan Anda sudah menyetujui Kaji Ulang</small>
            <?php } else { ?>
            <a class="btn btn-warning btn-sm" href="<?= base_url('term_uut/edit/' . $item->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
            <?php } ?>
            <?php endif ?>
        </div>
        <div class="ibox-tools">
            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <!--
        <div>
            <a class="btn btn-default btn-sm" href="<?= base_url('bookingitem/create'); ?>/<?= $booking->id ?>"><i class="fa fa-plus"></i> Alat</a>
        </div>
        -->
    </div>
    <div class="ibox-body" style="display:none;">
        <?php if ($request->status_id == 2): ?>
            <div class="row">
                <div class="col-9">
                </div>
                <div class="col-3 d-flex justify-content-end">
                    <a class="btn btn-info btn-sm" href="<?= base_url('tracking/edit_alat/' .$booking->id .'/'. $item->id); ?>"><i class="fa fa-edit"></i> Edit Data Alat</a>
                </div>
            </div>
        <?php endif ?>
        <div class="row">
            <div class="col-3 form-group">
                <label>Buatan</label>
                <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                    value="<?= $item->tool_made_in ?>" readonly />
            </div>
        </div>
        <div class="row">
            <div class="col-3 form-group">
                <label>Nama Alat</label>
                <input Type="text" class="form-control" name="tool_name" id="tool_name"
                value="<?=$item->tool_name ?>" readonly/>
            </div>
            <div class="col-3 form-group">
                <label>Merk</label>
                <input type="text" class="form-control" name="tool_brand" id="tool_model"
                    value="<?= $item->tool_brand ?>" readonly />
            </div>
            <div class="col-3 form-group">
                <label>Model/Tipe</label>
                <input type="text" class="form-control" name="tool_model" id="tool_model" 
                    value="<?= $item->tool_model ?>" readonly/>
            </div>  
            <!--
            <div class="col-3 form-group">
                <label>Tipe</label>
                <input type="text" class="form-control" name="tool_type" id="tool_type" 
                    value="<?= $item->tool_type ?>" readonly/>
            </div>
            -->
            <div class="col-3 form-group">
                <label>Nomor Seri/Identitas</label>
                <input type="text" class="form-control" name="serial_no" id="serial_no" 
                    value="<?= $item->serial_no ?>" readonly />
            </div>
            <div class="col-3 form-group">
                <label>Media Uji/Komoditas</label>
                <input type="text" class="form-control" name="tool_media" id="tool_media" 
                    value="<?= $item->tool_media ?>" readonly/>
            </div>
            
        </div>
        <div class="row">
            <div class="col-3 form-group">
                <label>Kapasitas</label>
                <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                    value="<?= $item->tool_capacity ?>" readonly/>
            </div>
            <div class="col-3 form-group">
                <label>Satuan Kapasitas</label>
                <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                    value="<?= $item->tool_capacity_unit ?>" readonly/>
            </div>
            <div class="col-3 form-group">
                <label>Daya Baca</label>
                <input type="text" class="form-control" name="tool_dayabaca" id="tool_dayabaca" 
                    value="<?= $item->tool_dayabaca ?>" readonly/>
            </div>
            <div class="col-3 form-group">
                <label>Satuan Daya Baca</label>
                <input type="text" class="form-control" name="tool_dayabaca_unit" id="tool_dayabaca_unit" 
                    value="<?= $item->tool_dayabaca_unit ?>" readonly/>
            </div>
        </div>
        <div class="row">
            <div class="col-3 form-group">
                <label>Kelas</label>
                <input type="text" class="form-control" name="class" id="class" 
                    value="<?= $item->class ?>" readonly/>
            </div>
            <div class="col-3 form-group">
                <label>Jumlah</label>
                <input type="text" class="form-control" name="jumlah" id="jumlah" 
                    value="<?= $item->jumlah ?>" readonly/>
            </div>
            <div class="col-3 form-group">
                <label>Buatan</label>
                <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                    value="<?= $item->tool_made_in ?>" readonly/>
            </div>
        </div>
        <div class="row">
            <div class="col-6 form-group">
                <label>Pabrikan</label>
                <input class="form-control" type="text" name="tool_factory" 
                    value="<?= $item->tool_factory ?>" readonly/>
            </div>
            <div class="col-6 form-group">
                <label>Alamat Pabrikan</label>
                <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <strong>Daftar Pengujian (Jumlah: <?= $item->quantity ?>)</strong>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Jenis Pengujian</th>
                            <th>Status</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (array_filter($inspections, function($var) use ($item) { return $var->service_request_item_id == $item->id; }) as $inspection):?>
                        <tr>
                            <td><?= $inspection->inspection_type ?></td>
                            <td>
                                <?= $inspection->status ?>
                                <?php
                                    if ($inspection->status_id == 7) {
                                        /*
                                        $order = array_filter($orders, function($var) use($inspection) { return $var->service_request_item_inspection_id == $inspection->id; });
                                        if (sizeof($order) > 0){
                                            $order = array_values($order)[0];
                                        } else {
                                            $order = null;
                                        }
                                        if ($order != null) {
                                            if ($order->stat_service_order == 1) {
                                                if ($order->file_skhp != null) {
                                                    echo '<br/><small>Hasil Uji Sudah Diupload</small>';
                                                } 
                                                if ($order->stat_warehouse == 1) {
                                                    echo '<br/><small>Alat Sudah Dapat Diambil</small>';
                                                }
                                            }
                                        }
                                        */
                                        if ($inspection->status_sertifikat == 1) {
                                            echo '<br/><small>Sertifikat Sudah Dapat Didownload</small>';
                                        } else {
                                            echo '<br/><small>Sertifikat Sedang Diproses</small>'; 
                                        }
                                        if ($inspection->status_uttp == 1) {
                                            echo '<br/><small>Alat Sudah Dapat Diambil</small>';
                                        }
                                    }
                                ?>
                            </td>
                            <td><?= $inspection->quantity ?></td>
                            <td><?= $inspection->unit ?></td>
                            <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                            <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                    <thead>
                        <tr>
                            <th colspan="5">Subtotal</th>
                            <th><?= number_format($item->subtotal, 2, ',', '.') ?></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>