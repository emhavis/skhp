<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />

<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />

<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('tracking'); ?>">Pelacakan Layanan</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('tracking/read/' . $booking->id); ?>">Data Layanan</a>
        </li>
        <li class="breadcrumb-item">Edit Data Pemilik</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pelacakan
                <?php if ($request->status_id == 2): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI!</span>
                <?php endif ?>
            </div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('tracking/read/' .$booking->id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
           
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" 
                            value="<?= $booking->booking_no ?>" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Pendaftaran</label>
                        <input class="form-control" type="text" name="no_register" 
                            value="<?= $request->no_register ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Status</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $request->status ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $request->jenis_layanan ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" value="<?= $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $request->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <?php if($request->lokasi_pengujian == 'dalam'): ?>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengiriman Alat</label>
                        <input class="form-control" type="text" name="est_arrival_date" readonly
                            id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                    </div>
                    <?php else: ?>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengujian/Pemeriksaan</label>
                        <input class="form-control" type="text" name="scheduled_test_date_from" readonly
                            id="scheduled_test_date_from" value="<?= $request->scheduled_test_date_from . ' s/d ' .$request->scheduled_test_date_to ?>">
                    </div>
                    <?php endif ?>
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                </div>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Data Pemilik Alat
            </div>
            <div>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-check"></i> Simpan Perubahan</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="form_pemilik" action="<?= base_url('tracking/edit_pemilik'); ?>/<?= $booking->id ?>" method="post">
            <?= csrf_field() ?>
            <div class="row">
                <div class="col-6 form-group">
                    <label>Pemilik Alat<span class="required_notification">*</span></label>
                    <input class="form-control" type="text" name="label_sertifikat" id="label_sertifikat"
                        value="<?= $request->label_sertifikat ?>">
                </div>
                <div class="col-6 form-group">
                    <label>Alamat Pemilik Alat<span class="required_notification">*</span></label>
                    <textarea class="form-control" name="addr_sertifikat" id="addr_sertifikat"><?= $request->addr_sertifikat ?></textarea>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Konfirmasi Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
                <div class="modal-body">
                    <p>Periksa kembali data pemilik yang didaftarkan.</p> 
                    <p><strong>Apakah data tersebut sudah benar?</strong></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-success" id="confirm_btn">Konfirmasi Perubahan</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        
        $("#form_pemilik").validate({
            rules: {
                label_sertifikat: {
                    required: true,
                },
                addr_sertifikat: {
                    required: true,
                },
            },
            messages: {
                label_sertifikat: 'Nama pemilik wajib diisi',
                addr_sertifikat: 'Alamat pemilik wajib diisi',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

        $('#confirm_btn').click(function(e) {
            e.preventDefault()

            var form = $('#form_pemilik');
            if (form.valid()) {
                form[0].submit();
            }
        });

    })
</script>
<?= $this->endSection() ?>