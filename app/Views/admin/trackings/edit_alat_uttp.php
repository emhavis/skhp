<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />

<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pelacakan Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('tracking'); ?>">Pelacakan Layanan</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('tracking/read/' . $booking->id); ?>">Data Layanan</a>
        </li>
        <li class="breadcrumb-item">Edit Data Alat/Standar</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pelacakan
                <?php if ($request->status_id == 2): ?>
                <span class="badge badge-danger badge-pill m-r-5 m-b-5">PERLU KONFIRMASI!</span>
                <?php endif ?>
            </div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('tracking/read/' .$booking->id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
           
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" 
                            value="<?= $booking->booking_no ?>" readonly>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Pendaftaran</label>
                        <input class="form-control" type="text" name="no_register" 
                            value="<?= $request->no_register ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Status</label>
                        <input class="form-control" type="text" name="status" 
                            value="<?= $request->status ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $request->jenis_layanan ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" value="<?= $request->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $request->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" 
                            value="<?= $request->label_sertifikat ?>" readonly>
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly><?= $request->addr_sertifikat ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <?php if($request->lokasi_pengujian == 'dalam'): ?>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengiriman Alat</label>
                        <input class="form-control" type="text" name="est_arrival_date" readonly
                            id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                    </div>
                    <?php else: ?>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengujian/Pemeriksaan</label>
                        <input class="form-control" type="text" name="scheduled_test_date_from" readonly
                            id="scheduled_test_date_from" value="<?= $request->scheduled_test_date_from . ' s/d ' .$request->scheduled_test_date_to ?>">
                    </div>
                    <?php endif ?>
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                </div>
        </div>
    </div>
    <?php if($booking->service_type_id ==1 || $booking->service_type_id ==2){ ?>
        
        <form id="uut" action="<?= base_url('tracking/edit_alat/' .$booking->id . '/' .$item->id); ?>" method="post">
            <?= csrf_field() ?>
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"> Alat: <?= $item->uut_type ?></div>
                    <div>
                        <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-save"></i> Simpan Perubahan</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="hidden"  name="type_id" id="type_id" 
                                value="<?= $item->type_id ?>" readonly/>
                            <input type="text" class="form-control" name="uttp_type" id="uttp_type" 
                                value="<?= $item->uut_type ?>" readonly/>
                        </div> 
                        <div class="col-6 form-group">
                            <label>Nama Alat</label>
                            <input Type="text" class="form-control" name="tool_name" id="tool_name" value="<?= $item->tool_name ?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label>Merek<span class="required_notification">*</span></label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                                value="<?= $item->tool_brand ?>" />
                        </div>
                        <div class="col-4 form-group">
                            <label>Model/Tipe<span class="required_notification">*</span></label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" />
                        </div>
                        <div class="col-4 form-group">
                            <label><span class="required_notification">*</span> Nomor Seri / Identitas</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" value="<?=$item->serial_no?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" 
                                value="<?= $item->tool_media ?>"  />
                        </div>
                        <div class="col-4 form-group">
                            <label>Kapasitas<span class="required_notification">*</span></label>
                            <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity ?>" />
                        </div>
                        <div class="col-4 form-group">
                            <label>Satuan Kapasitas<span class="required_notification">*</span></label>
                            <select class="form-control" name="tool_capacity_unit_uut" id="tool_capacity_unit_uut">
                                <option value="<?= $item->tool_capacity_unit ?>" selected><?= $item->tool_capacity_unit ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label>Daya Baca</label>
                            <input type="number" class="form-control" name="tool_dayabaca" id="tool_dayabaca" 
                                value="<?= $item->tool_dayabaca ?>" />
                        </div>
                        <div class="col-4 form-group">
                            <label>Satuan Dayabaca<span class="required_notification">*</span></label>
                            <select class="form-control" name="tool_dayabaca_unit" id="tool_dayabaca_unit">
                                <option value="<?= $item->tool_dayabaca_unit ?>" selected><?= $item->tool_dayabaca_unit ?></option>
                            </select>
                        </div>
                        <div class="col-4 form-group">
                            <label>Kelas</label>
                            <input type="text" class="form-control" name="class" id="class" value="<?= $item->class ?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label><span class="required_notification">*</span> Jumlah</label>
                            <input type="text" class="form-control" name="jumlah" id="jumlah" 
                            value="<?= $item->jumlah ?>"/>
                        </div> 
                        <div class="col-4 form-group">
                            <label>Buatan<span class="required_notification">*</span></label>
                            <select class="form-control" name="tool_made_in_id" id="tool_made_in_id">
                                <?php foreach ($negaras as $negara): ?>
                                <option value="<?= $negara->id ?>"><?= $negara->nama_negara ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-4 form-group">
                            <label>Pabrikan<span class="required_notification">*</span></label>
                            <input class="form-control" type="text" name="tool_factory" id="tool_factory" 
                            value="<?= $item->tool_factory ?>"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Alamat Pabrikan<span class="required_notification">*</span></label>
                            <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" ><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>

                </div>
            </div>
            </form>

    <?php }else{?>
        
            <form id="uttp" action="<?= base_url('tracking/edit_alat/' .$booking->id . '/' .$item->id); ?>" method="post">
            <?= csrf_field() ?>
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"> Alat: <?= $item->uttp_type ?></div>
                    <div>
                        <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-save"></i> Simpan Perubahan</a>
                    </div>
                </div>
                <div class="ibox-body">
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="hidden"  name="type_id" id="type_id" 
                                value="<?= $item->type_id ?>" readonly/>
                            <input type="text" class="form-control" name="uttp_type" id="uttp_type" 
                                value="<?= $item->uttp_type ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merek<span class="required_notification">*</span></label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                                value="<?= $item->tool_brand ?>" />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe<span class="required_notification">*</span></label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" />
                        </div>  
                        <div class="col-3 form-group">
                            <label>Nomor Seri<span class="required_notification">*</span></label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" />
                        </div>
                        <div class="col-3 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" 
                                value="<?= $item->tool_media ?>"  />
                        </div>
                    </div>
                    <div class="row">
                    
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimum<span class="required_notification">*</span></label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity ?>" />
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Minimum</label>
                            <input type="text" class="form-control" name="tool_capacity_min" id="tool_capacity_min" 
                                value="<?= $item->tool_capacity_min ?>" />
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas<span class="required_notification">*</span></label>
                            <select class="form-control" name="tool_capacity_unit" id="tool_capacity_unit">
                                <option value="<?= $item->tool_capacity_unit ?>" selected><?= $item->tool_capacity_unit ?></option>
                            </select>
                        </div>
                        <div class="col-3 form-group">
                            <label>Negara Pembuat</label>
                            <select class="form-control" name="tool_made_in_id" id="tool_made_in_id">
                                <option value="">-- Pilih Negara --</option>
                                <?php foreach ($negaras as $negara): ?>
                                <option value="<?= $negara->id ?>" <?= $item->tool_made_in_id == $negara->id ? 'selected' : '' ?> ><?= $negara->nama_negara ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Nama Pabrikan<span class="required_notification">*</span></label>
                            <input class="form-control" type="text" name="tool_factory" id="tool_factory" 
                            value="<?= $item->tool_factory ?>"  />
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan<span class="required_notification">*</span></label>
                            <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" ><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>

                    <?php if($booking->service_type_id == 4): ?>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label id="file_type_approval_certificate_req">File Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                            <input class="form-control" id="file_type_approval_certificate" 
                                type="file" name="file_type_approval_certificate" accept="application/pdf" />
                                <?= $item->file_type_approval_certificate ?>
                        </div>
                        <div class="col-3 form-group">
                            <label id="reference_no_req">No Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                            <input class="form-control" id="reference_no" value="<?= $item->reference_no ?>"
                                type="text" name="reference_no" accept="application/pdf" />
                        </div>
                        <div class="col-3 form-group" >
                            <label id="reference_date_req">Tanggal Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                            <div class="input-group date" id="reference_date_grp">
                                <div class="input-group-prepend input-group-addon">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" id="reference_date"
                                    value="<?= $item->reference_date ? date("d-m-Y", strtotime($item->reference_date)) : ''; ?>"
                                    name="reference_date">
                            </div>
                        </div>
                    </div>
                    <?php elseif ($booking->service_type_id == 5): ?>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>File Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                            <input class="form-control" id="file_last_certificate" 
                                type="file" name="file_last_certificate" accept="application/pdf" />
                            <?= $item->file_last_certificate ?>
                        </div>
                        <div class="col-3 form-group">
                            <label>No Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                            <input class="form-control" id="reference_no" type="text" name="reference_no" 
                                value="<?= $item->reference_no ?>" />
                        </div>
                        <div class="col-3 form-group" >
                            <label>Tanggal Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                            <div class="input-group date" id="reference_date_grp">
                                <div class="input-group-prepend input-group-addon">
                                    <span class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" id="reference_date"
                                value="<?= $item->reference_date ? date("d-m-Y", strtotime($item->reference_date)) : ''; ?>"
                                    name="reference_date">
                            </div>
                        </div>
                    </div>
                    <?php endif ?>

                </div>
            </div>
            </form>

    <?php } ?>
</div>


<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Konfirmasi Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
                <div class="modal-body">
                    <p>Periksa kembali data alat/standar yang didaftarkan.</p> 
                    <p><strong>Apakah data tersebut sudah benar?</strong></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <?php if($booking->service_type_id ==1 || $booking->service_type_id ==2): ?>
                    <button type="button" class="btn btn-success" id="confirm_btn_uut">Konfirmasi Perubahan</button>
                    <?php else: ?>
                    <button type="button" class="btn btn-success" id="confirm_btn_uttp">Konfirmasi Perubahan</button>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#reference_date_grp').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#tool_made_in_id').select2({
            theme: 'bootstrap4',
        });
        $('#tool_capacity_unit').select2({
            theme: 'bootstrap4',
            tags: true,
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uttp/unit/'+type;
                },
                processResults: function (data) {
                    /*
                    data.push(
                        {id: "0", uttp_type_id: "0", unit: "lainnya"}
                    );
                    */
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
                
            }
        });

        $('#tool_capacity_unit_uut').select2({
            theme: 'bootstrap4',
            tags: true,
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/unit/'+type;
                },
                processResults: function (data) {
                    /*
                    data.push(
                        {id: "0", uttp_type_id: "0", unit: "lainnya"}
                    );
                    */
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
                
            }
        });

        $('#tool_dayabaca_unit').select2({
            theme: 'bootstrap4',
            tags: true,
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/unit/'+type;
                },
                processResults: function (data) {
                    /*
                    data.push(
                        {id: "0", uttp_type_id: "0", unit: "lainnya"}
                    );
                    */
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
                
            }
        });

        $("#uttp").validate({
            rules: {
                tool_brand: {
                    required: true
                },
                tool_model: {
                    required: true
                },
                serial_no: {
                    required: true
                },
                tool_capacity: {
                    required: true,
                },
                tool_capacity_unit: {
                    required: true
                },
                tool_factory: {
                    required: true
                },
                tool_factory_address: {
                    required: true
                },
                tool_made_in_id: {
                    required: true
                }
            },
            messages: {
                tool_brand: 'Merek wajib diisi',
                tool_model: 'Model/tipe wajib diisi',
                serial_no: 'No seri wajib diisi',
                tool_capacity: {
                    required: 'Kapasitas maksimum wajib diisi',
                },
                tool_capacity_unit: 'Satuan wajib diisi',
                tool_factory: 'Pabrikan wajib diisi',
                tool_factory_address: 'Alamat pabrikan wajib diisi',
                tool_made_in_id: 'Negara pembuat wajib dipilih',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

        $('#confirm_btn_uttp').click(function(e) {
            e.preventDefault()

            var form = $('#uttp');
            if (form.valid()) {
                form[0].submit();
            }
        });

        $('#confirm_btn_uut').click(function(e) {
            e.preventDefault()

            var form = $('#uut');
            if (form.valid()) {
                form[0].submit();
            }
        });
    })
</script>
<?= $this->endSection() ?>