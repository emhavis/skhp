<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Pemeriksaan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Pemeriksaan</div>
            <div>
                <a class="btn btn-default btn-sm" href="/bookingiteminspection/index/<?= $booking->id ?>/<?= $item->id ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body">

            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan ?>">
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Peruntukan Sertifikat</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                        <div class="col-9 form-group">
                            <label>Label Sertifikat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 form-group">
                            <label>Alamat Sertifikat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">
                        <label>Standard Alat</label>
                        <input type="text" class="form-control" name="uml_standard_id" 
                            value="<?= $standard->tool_code ?>" id="uml_standard_id" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Besaran</label>
                        <input type="text" class="form-control" name="standard_type" 
                            value="<?= $standard->standard_type ?>" id="standard_type" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Jenis</label>
                        <input type="text" class="form-control" name="attribute_name" 
                            value="<?= $standard->attribute_name ?>" id="attribute_name" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Rincian</label>
                        <input type="text" class="form-control" name="standard_detail_type_name" 
                            value="<?= $standard->standard_detail_type_name ?>" id="standard_detail_type_name" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Jumlah Per Set</label>
                        <input type="text" class="form-control" name="jumlah_per_set" 
                            value="<?= $standard->jumlah_per_set ?>" id="jumlah_per_set" readonly/>
                    </div>  
                    <div class="col-3 form-group">
                        <label>Jumlah Inspeksi</label>
                        <input type="text" class="form-control" name="quantity_insp" 
                            value="<?= $item->quantity ?>" id="quantity_insp" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Perkiraan Subtotal Harga</label>
                        <input type="text" class="form-control" name="est_subtotal" 
                            value="<?= number_format($item->est_subtotal, 2, ',', '.') ?>" id="est_subtotal" readonly/>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Pemeriksaan</div>
        </div>
        <div class="ibox-body">
            <form id="inspection" action="<?= base_url('bookingiteminspection/create/'.$booking->id.'/'.$item->id); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Pengujian</label>
                        <select class="form-control" name="inspection_price_id" id="inspection_price_id">
                            <?php foreach($prices as $price): ?>
                            <option value="<?= $price->id ?>"><?= $price->inspection_type ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Jumlah</label>
                        <input type="number" class="form-control" name="quantity" 
                            value="<?= $standard->jumlah_per_set ?>" id="quantity"/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Satuan</label>
                        <input type="text" class="form-control" name="satuan" 
                            value="<?= $prices[0]->measurement_unit ?>" id="satuan" readonly/>
                    </div>  
                    <div class="col-3 form-group">
                        <label>Harga Satuan</label>
                        <input type="text" class="form-control" name="price" 
                            value="<?= $prices[0]->price ?>" id="price" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Subtotal</label>
                        <input type="text" class="form-control" name="subtotal" 
                        value="<?= $standard->jumlah_per_set * $prices[0]->price  ?>" id="subtotal" readonly/>
                    </div>  
                </div>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
   
<script type="text/javascript">
    $(function() {
        $('#inspections').DataTable({
            pageLength: 10,
        });

        $('#inspection_price_id').select2({
            theme: 'bootstrap4',
        });

        $('#quantity').change(function() {
            var qty = $('#quantity').val();
            $('#subtotal').val(qty * $('#price').val());
        })
    })
</script>
<?= $this->endSection() ?>