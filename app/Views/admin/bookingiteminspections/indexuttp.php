<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Pemeriksaan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Pemeriksaan</div>
            <div>
                <a class="btn btn-default btn-sm" href="/booking/edit/<?= $booking->id ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body">

            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan ?>">
                        </div>
                        <!--
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        </div>
                        -->
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" action="<?= base_url('bookingitem/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    
                    <div class="col-6 form-group">
                        <label>Jenis</label>
                        <input type="text" class="form-control" name="type_id" id="type_id" 
                            value="<?= $uttpType->uttp_type ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Seri</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" 
                            value="<?= $uttp->serial_no ?>" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Merk</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_model"
                            value="<?= $uttp->tool_model ?>" readonly />
                    </div>
                    <div class="col-3 form-group">
                        <label>Model</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" 
                            value="<?= $uttp->tool_model ?>" readonly/>
                    </div>  
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" 
                            value="<?= $uttp->tool_type ?>" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimal</label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                            value="<?= $uttp->tool_capacity ?>" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pabrikan</label>
                        <input class="form-control" type="text" name="tool_factory" 
                            value="<?= $uttp->tool_factory ?>" readonly/>
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address" readonly><?= $uttp->tool_factory_address ?></textarea>
                    </div>
                </div>
                <?php if(($booking->service_type_id == 4 || $booking->service_type_id == 5) && ($booking->lokasi_pengujian == 'luar')): ?>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Lokasi Penempatan</label>
                        <input class="form-control" type="text" name="location" 
                            value="<?= $item->location ?>" readonly />
                    </div>
                </div>
                <?php endif ?>
            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Rencana Pemeriksaan</div>
            <div>
                <a class="btn btn-primary btn-sm" href="/bookingiteminspection/createuttp/<?= $booking->id ?>/<?= $item->id ?>"><i class="fa fa-plus"></i> Pemeriksaan Baru</a>
            </div>
        </div>
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Jenis Pengujian</th>
                        <th>Jumlah</th>
                        <th>Satuan</th>
                        <th>Harga Satuan</th>
                        <th>Subtotal</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($inspections as $inspection):?>
                    <tr>
                        <td><?= $inspection->inspection_type ?></td>
                        <td><?= $inspection->quantity ?></td>
                        <td><?= $inspection->unit ?></td>
                        <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                        <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                        <td>
                            <form action="<?= base_url('bookingiteminspection/deleteuttp').'/'.$inspection->id; ?>" method="post">
                            <?= csrf_field() ?> 
                            <button type="submit" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i> Hapus</a>
                            <!--
                            <a class="btn btn-default btn-sm" href="/bookingiteminspection/edit/<?= $booking->id ?>/<?= $item->id ?>/<?= $inspection->id ?>"><i class="fa fa-edit"></i> Edit</a>
                            -->
                            </form>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
   
<script type="text/javascript">
    $(function() {
        $('#inspections').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>