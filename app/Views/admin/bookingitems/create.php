<?php

use function PHPSTORM_META\type;
?>
<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Alat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Alat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking/edit/' . $booking->id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body" >

             <?php if (session()->getFlashdata('errors')) : ?>
                <?php foreach(session()->getFlashdata('errors') as $err): ?>
                <div class="alert alert-danger">
                    <?= $err ?>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
       
            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" 
                                value="<?= ($booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor') . ($booking->lokasi_dl == 'dalam' ? ' (Dalam Negeri)' : ' (Luar Negeri)') ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" action="<?= base_url('bookingitem/create/'.$booking->id); ?>" 
                enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>
                <input type="hidden" class="form-control" name="service_type_id" id="service_type_id" value="<?= $booking->service_type_id ?>"/>

                <?php if($booking->service_type_id == 1 || $booking->service_type_id == 2): ?>
                   <?= $this->include('admin/bookingitems/createitemuut') ?>

                <?php else: ?>
                
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <h5 class="alert-heading">Perhatian!</h5>
                    <p>Mohon diperhatikan:<br/>
                        <ol type="1">
                            <li>Jangan salah / tertukar dalam memilih alat ukur berikut:
                                <ol type="a">
                                    <li><strong>Meter Arus vs Sistem Meter Arus;</strong></li>
                                    <li><strong>Meter Gas vs Sistem Meter Gas;</strong></li>
                                    <li><strong>CTMS BBM vs CTMS LNG/LPG;</strong></li>
                                </ol>
                            </li>
                            <li>Meter Arus atau Meter Gas yang diuji bersamaan dengan perlengkapannya (di lokasi terpasang), didaftarkan sebagai <strong>Sistem Meter Arus/Sistem Meter Gas</strong>.</li>
                            <li>Meter Arus atau Meter Gas yang diuji mandiri (di lab atau instalasi), didaftarkan sebagai <strong>Meter Arus/Meter Gas</strong>.</li>
                        </ol>
                    </p>
                </div>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>ID Alat<span class="required_notification">*</span></label>
                        <select class="form-control" name="uttp_id" id="uttp_id">
                        </select>
                        <a href="<?= base_url('useruttp/create') ?>" target="_blank">
                            Klik disini untuk menambahkan data alat yang baru.
                        </a>
                    </div>
                    <div class="col-6 form-group">
                        <label>Jenis</label>
                        <input type="hidden" class="form-control" id="is_tera" value="true" readonly/>
                        <input type="hidden" class="form-control" id="is_ujitipe" value="true" readonly/>
                        <input type="text" class="form-control" name="uttp_type" id="uttp_type" readonly/>
                    </div>
                </div>
                <div class="row">
                   
                    <div class="col-3 form-group">
                        <label>Merek</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Model/Tipe</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" readonly/>
                    </div>  
                    <div class="col-3 form-group">
                        <label>Nomor Seri</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" readonly/>
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" readonly/>
                    </div>
                    -->
                    <div class="col-3 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" readonly />
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimum</label>
                        <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Kapasitas Minimum</label>
                        <input type="text" class="form-control" name="tool_capacity_min" id="tool_capacity_min" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Satuan Kapasitas</label>
                        <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Negara Pembuat</label>
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Pabrikan</label>
                        <input class="form-control" type="text" name="tool_factory" id="tool_factory" readonly/>
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" readonly></textarea>
                    </div>
                </div>
                <?php if(($booking->lokasi_pengujian == 'luar')): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Alamat Lokasi Pengujian<span class="required_notification">*</span></label>
                        <textarea class="form-control" name="location" id="location" ></textarea>
                    </div>
                    <?php if ($booking->lokasi_dl == 'dalam'): ?>
                    <div class="col-6 form-group">
                        <label>Alamat Lokasi Pengujian, Kabupaten/Kota</label>
                        <select class="form-control" name="location_kabkot_id" id="location_kabkot_id">
                            <option value="0">-</option>
                            <?php foreach($cities as $city): ?>
                            <option value="<?= $city->id ?>"><?= $city->nama ?>,  <?= $city->provinsi ?></option>
                            <?php endforeach ?> 
                        </select>
                    </div>
                    <?php else: ?>
                    <div class="col-6 form-group">
                    <label>Alamat Lokasi Pengujian, Negara</label>
                        <select class="form-control" name="location_negara_id" id="location_negara_id">
                            <option value="0">-</option>
                            <?php foreach($negaras as $negara): ?>
                            <option value="<?= $negara->id ?>"><?= $negara->nama_negara ?></option>
                            <?php endforeach ?> 
                        </select>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Alamat Lokasi Alat</label>
                        <textarea class="form-control" name="location_alat" id="location_alat" readonly></textarea>
                    </div>
                    <div class="col-3 form-group">
                        <label>Koordinat Lokasi Alat (Latitude)</label>
                        <input class="form-control" type="text" name="location_lat" id="location_lat" readonly />
                    </div>
                    <div class="col-3 form-group">
                        <label>Koordinat Lokasi Alat (Longitude)</label>
                        <input class="form-control" type="text" name="location_long" id="location_long" readonly />
                    </div>
                </div>
                <?php else: ?>
                    <?php if($booking->service_type_id == 4 || $booking->service_type_id == 5): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Lokasi Alat</label>
                        <textarea class="form-control" name="location" id="location" ><?= $booking->addr_sertifikat ?></textarea>
                    </div>
                </div>
                    <?php endif ?>
                <?php endif ?>
                <?php if($booking->service_type_id == 4): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label id="file_type_approval_certificate_req">File Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                        <label id="file_type_approval_certificate_noreq">File Surat Persetujuan Tipe</label>
                        <input class="form-control" id="file_type_approval_certificate" 
                            type="file" name="file_type_approval_certificate" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group">
                        <label id="reference_no_req">No Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                        <label id="reference_no_noreq">No Surat Persetujuan Tipe</label>
                        <input class="form-control" id="reference_no" 
                            type="text" name="reference_no" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group" >
                        <label id="reference_date_req">Tanggal Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                        <label id="reference_date_noreq">Tanggal Surat Persetujuan Tipe</label>
                        <div class="input-group date" id="reference_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="reference_date"
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <?php elseif ($booking->service_type_id == 5): ?>
                <div class="row">
                    <?php if(($booking->lokasi_pengujian == 'dalam')): ?>
                    <div class="col-6 form-group">
                        <label>File Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_last_certificate" 
                            type="file" name="file_last_certificate" accept="application/pdf" />
                    </div>
                    <?php else: ?>
                    <div class="col-6 form-group">
                        <label>File Sertifikat Sebelumnya</label>
                        <input class="form-control" id="file_last_certificate" 
                            type="file" name="file_last_certificate" accept="application/pdf" />
                    </div>
                    <?php endif; ?>
                    <div class="col-3 form-group">
                        <label>No Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                        <input class="form-control" id="reference_no" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                        <div class="input-group date" id="reference_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="reference_date"
                                name="reference_date">
                        </div>
                    </div>
                    
                </div>
                <?php elseif ($booking->service_type_id == 6 || $booking->service_type_id == 7): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Surat Permohonan<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_application_letter" 
                            type="file" name="file_application_letter" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group">
                        <label>No Surat Permohonan<span class="required_notification">*</span></label>
                        <input class="form-control" id="reference_no" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Surat Permohonan<span class="required_notification">*</span></label>
                        <div class="input-group date" id="reference_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="reference_date" autocomplete="off"
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Manual Kalibrasi</label>
                        <input class="form-control" id="file_calibration_manual" 
                            type="file" name="file_calibration_manual" accept="application/pdf" />
                    </div>
                    <div class="col-6 form-group">
                        <label>File Buku Manual<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_manual_book" 
                            type="file" name="file_manual_book" accept="application/pdf" />
                    </div>
                </div>
                <?php endif ?>
                <?php endif ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="notFound" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data UTTP tidak ditemukan. Pastikan kembali nomor seri UTTP sudah sesuai atau belum.</p>
                <p>Gunakan tombol Tambah Baru untuk menambahkan data UTTP baru. Kemudian, gunakan nomor seri UTTP yang baru ditambahkan untuk melakukan pencarian kembali.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" class="btn btn-primary" id="new_uttp">Tambah baru</button>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').show();
        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').hide();

        $('#location_kabkot_id, #location_negara_id').select2({
            theme: 'bootstrap4',
        });
        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#uttp_id').val(null).trigger('change');
            $('#uut_id').val(null).trigger('change');
            $('#serial_no').val(null);
            $('#tool_brand').val(null);
            $('#tool_model').val(null);
            $('#tool_capacity').val(null);
            $('#tool_capacity_min').val(null);
            $('#tool_capacity_unit').val(null);
            $('#tool_made_in').val(null);
            $('#tool_factory').val(null);
            $('#tool_factory_address').val(null);
            $('#tool_media').val(null);
            $('#class').val(null);
            $('#jumlah').val(null);
            $('#tool_dayabaca').val(null);
            $('#tool_dayabaca_unit').val(null);
            $('#location_lat').val(null);
            $('#location_long').val(null);
            $('#location').val(null);
            $('#location_alat').val(null);

            $.get('<?= base_url(); ?>/uttp/getType/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#is_tera').val(res.is_tera == 't');
                    $('#is_ujitipe').val(res.is_ujitipe == 't');

                    if (res.is_ujitipe != 't') {
                        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').hide();
                        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').show();
                    } else {
                        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').show();
                        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').hide();
                    }
                }
            });
        });

        var owner ='<?= $booking->uttp_owner_id ?>';
        
        $('#uttp_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/useruttp/find/'+'<?= $booking->uttp_owner_id ?>'
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uttp/getSertifikat/'+e.params.data.id,function(result)
            {
                if (result && result.uttp) {
                    res = result.uttp;

                    $('#uttp_type').val(res.uttp_type);
                    
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_min').val(res.tool_capacity_min);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                    $('#location_lat').val(res.location_lat);
                    $('#location_long').val(res.location_long);
                    $('#location').val(res.location);
                    $('#location_alat').val(res.location);

                    if ($("#service_type_id").val() == 5) {
                        if (result.sertifikat) {
                            sert = result.sertifikat;
                            $('#reference_no').val(sert.no_sertifikat);
                            $('#reference_date').val(sert.kabalai_date);
                        }
                    } else if ($("#service_type_id").val() == 4) {
                        if (result.sertifikat) {
                            sert = result.sertifikat;
                            if (sert.service_type_id == 6 || sert.service_type_id == 7) {
                                $('#reference_no').val(sert.no_sertifikat);
                                $('#reference_date').val(sert.kabalai_date);
                            }
                        }
                    }
                }
            });
        });

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });


        $('#reference_date_grp').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#cari_tool_code').click(function() {
            $.get('<?= base_url(); ?>/standard/read/'+$('#tool_code').val(),function(res)
            {
                if (res) {
                    $('#uml_standard_id').val(res.id);
                    $('#standard_type').val(res.standard_type);
                    $('#jumlah_per_set').val(res.jumlah_per_set);
                    $('#attribute_name').val(res.attribute_name);
                    $('#standard_detail_type_name').val(res.standard_detail_type_name);
                    $('#standard_detail_type_id').val(res.standard_detail_type_id);
                }
            });
        });

        // uut processors
        var owner = '<?= $booking->uut_owner_id ?>';
        $('#uut_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/useruut/find/'+'<?= $booking->uut_owner_id ?>'
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uut/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#uut_type').val(res.uut_type);
                    
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);

                    $('#tool_dayabaca').val(res.tool_dayabaca);
                    $('#tool_dayabaca_unit').val(res.tool_capacity_unit);
                    $('#class').val(res.class);
                    $('#jumlah').val(res.jumlah);

                    $('#location_lat').val(res.location_lat);
                    $('#location_long').val(res.location_long);
                }
            });
        });
        $("#booking").validate({
            rules: {
                uttp_id: {
                    required: !0,
                },
                <?php if(($booking->lokasi_pengujian == 'luar')): ?>
                location: {
                    required: true,
                    minlength: 1,
                },
                <?php endif; ?>
                file_type_approval_certificate: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                    accept: "application/pdf",
                },
                reference_no: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                reference_date: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                <?php if(($booking->lokasi_pengujian == 'dalam')): ?>
                file_last_certificate: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 5);
                    },
                    accept: "application/pdf",
                },
                <?php endif; ?>
                file_application_letter: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                    accept: "application/pdf",
                },
                file_manual_book: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                    accept: "application/pdf",
                },
                <?php if(($booking->lokasi_pengujian == 'luar')): ?>
                location_kabkot_id: {
                    min: 1
                },
                <?php endif; ?>
            },
            messages: {
                uttp_id: 'Alat wajib dipilih',
                location: 'Lokasi pengujian alat wajib diisi',
                reference_no: 'Nomor surat wajib diisi',
                reference_date: 'Tanggal surat wajib diisi dengan format yang benar',
                file_type_approval_certificate: 'File surat persetujuan wajib diupload dalam format PDF',
                file_last_certificate: 'File sertifikat sebelumnya wajib diupload dalam format PDF',
                file_application_letter: 'File surat permohonan wajib diupload dalam format PDF',
                file_manual_book: 'File buku manual wajib diupload dalam format PDF',
                location_kabkot_id: 'Kabupaten lokasi pengujian harus dipilih',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>