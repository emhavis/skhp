<div class="row">
    <div class="col-6 form-group">
        <label>Data Alat SUML</label>
        <select class="form-control" name="uut_id" id="uut_id">
        </select>
        <a href="<?= base_url() ?>/useruut/create" target="_blank">
            Klik disini untuk menambahkan data alat yang baru.
        </a>
    </div>
    <div class="col-6 form-group">
        <label>Jenis</label>
        <input type="hidden" class="form-control" id="is_tera" value="true" readonly/>
        <input type="hidden" class="form-control" id="is_ujitipe" value="true" readonly/>
        <input type="text" class="form-control" name="uut_type" id="uut_type" readonly/>
    </div>
    <!-- <div class="col-6 form-group">
        <label>Jenis</label>
        <select class="form-control" name="type_id" id="type_id">
            <?php foreach ($standard as $std): ?>
            <option value="<?= $std->id ?>"><?= $std->uut_type ?></option>
            <?php endforeach ?>
        </select>
    </div> -->
</div>

<div class="row">

    <div class="col-3 form-group">
        <label>Merk</label>
        <input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly/>
    </div>
    <div class="col-3 form-group">
        <label>Model/Tipe</label>
        <input type="text" class="form-control" name="tool_model" id="tool_model" readonly/>
    </div>  
    <div class="col-3 form-group">
        <label>Nomor Seri / Identitas</label>
        <input type="text" class="form-control" name="serial_no" id="serial_no" readonly/>
    </div>
    <!--
    <div class="col-3 form-group">
        <label>Tipe</label>
        <input type="text" class="form-control" name="tool_type" id="tool_type" readonly/>
    </div>
    -->
    <div class="col-3 form-group">
        <label>Media Uji/Komoditas</label>
        <input type="text" class="form-control" name="tool_media" id="tool_media" readonly />
    </div>
</div>
<div class="row">
    <!-- <div class="col-3 form-group">
        <label>Buatan</label>
        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" readonly/>
    </div> -->
    <div class="col-3 form-group">
        <label>Kapasitas Maksimum</label>
        <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" readonly/>
    </div>
    <div class="col-3 form-group">
        <label>Satuan Kapasitas</label>
        <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" readonly/>
    </div>

    <div class="col-3 form-group">
        <label>Kelas</label>
        <input class="form-control" type="text" name="class" id="class" readonly/>
    </div>
    <div class="col-3 form-group">
        <label>Jumlah</label>
        <input class="form-control" type="text" name="jumlah" id="jumlah" readonly/>
    </div>
</div>

<div class="row">
    <div class="col-3 form-group">
        <label>Daya Baca</label>
        <input class="form-control" type="text" name="tool_dayabaca" id="tool_dayabaca" readonly/>
    </div>
    <div class="col-3 form-group">
        <label>Unit Daya Baca</label>
        <input class="form-control" type="text" name="tool_dayabaca_unit" id="tool_dayabaca_unit" readonly/>
    </div>
</div>
<div class="row mt-4 mb-4">
    <div class="col-6 form-group">
        <div class="custom-file">
            <label class="custom-file-label" for="file_manual_book">Manual Book</label>
            <input class="form-control custom-file-input" type="file" name="file_manual_book" accept="application/pdf" />
        </div>
    </div>
    <div class="col-6 form-group">
        <div class="custom-file">
            <label class="custom-file-label">Sertifikat Sebelumnya (<span style="font-size:9px !important;"> Format harus pdf </span>)</label>
            <input class="form-control custom-file-input" type="file" name="file_last_certificate" id = "file_last_certificate" accept="application/pdf" />
        </div>
    </div>
</div>

<?php if(($booking->lokasi_pengujian == 'luar')): ?>
    <div class="row">
        <div class="col-6 form-group">
            <label>Alamat Lokasi Pengujian<span class="required_notification">*</span></label>
            <textarea class="form-control" name="location" id="location" ></textarea>
        </div>
        <?php if ($booking->lokasi_dl == 'dalam'): ?>
        <div class="col-6 form-group">
            <label>Alamat Lokasi Pengujian, Kabupaten/Kota</label>
            <select class="form-control" name="location_kabkot_id" id="location_kabkot_id">
                <option value="0">-</option>
                <?php foreach($cities as $city): ?>
                <option value="<?= $city->id ?>"><?= $city->nama ?>,  <?= $city->provinsi ?></option>
                <?php endforeach ?> 
            </select>
        </div>
        <?php else: ?>
        <div class="col-6 form-group">
        <label>Alamat Lokasi Pengujian, Negara</label>
            <select class="form-control" name="location_negara_id" id="location_negara_id">
                <option value="0">-</option>
                <?php foreach($negaras as $negara): ?>
                <option value="<?= $negara->id ?>"><?= $city->nama_negara ?></option>
                <?php endforeach ?> 
            </select>
        </div>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-3 form-group">
            <label>Koordinat Lokasi Alat (Latitude)</label>
            <input class="form-control" type="text" name="location_lat" id="location_lat" readonly />
        </div>
        <div class="col-3 form-group">
            <label>Koordinat Lokasi Alat (Longitude)</label>
            <input class="form-control" type="text" name="location_long" id="location_long" readonly />
        </div>
    </div>
<?php endif?>
<!-- </div> -->