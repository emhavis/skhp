<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Pemeriksaan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Pemeriksaan</div>
            <div>
                <a class="btn btn-default btn-sm" href="/booking/edit/<?= $booking->id ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body" >
       
            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" action="<?= base_url('bookingitem/create/'.$booking->id); ?>" 
                enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>

                <?php if($booking->service_type_id == 1 || $booking->service_type_id == 1): ?>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Standard Alat</label>
                        <div class="input-group">
                            <input type="hidden" name="uml_standard_id" id="uml_standard_id"/>
                            <input type="text" class="form-control" name="tool_code" id="tool_code"/>
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="cari_tool_code">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Besaran</label>
                        <input type="text" class="form-control" name="standard_type" id="standard_type" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Jumlah Per Set</label>
                        <input type="text" class="form-control" name="jumlah_per_set" id="jumlah_per_set" readonly/>
                    </div>  
                    <div class="col-3 form-group">
                        <label>Jenis</label>
                        <input type="text" class="form-control" name="attribute_name" id="attribute_name" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Rincian</label>
                        <input type="hidden" id="standard_detail_type_id"/>
                        <input type="text" class="form-control" name="standard_detail_type_name" id="standard_detail_type_name" readonly/>
                    </div>
                </div>
                <?php else: ?>
                
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis</label>
                        <select class="form-control" name="type_id" id="type_id">
                            <?php foreach ($uttpTypes as $type): ?>
                            <option value="<?= $type->id ?>"><?= $type->uttp_type ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Nomor Seri</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Merk</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Model</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" />
                    </div>  
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" />
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Buatan</label>
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimal</label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pabrikan</label>
                        <input class="form-control" type="text" name="tool_factory" />
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address"></textarea>
                    </div>
                </div>
                <?php if(($booking->service_type_id == 4 || $booking->service_type_id == 5) && ($booking->lokasi_pengujian == 'luar')): ?>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Lokasi Penempatan</label>
                        <input class="form-control" type="text" name="location" />
                    </div>
                </div>
                <?php endif ?>
                <?php if($booking->service_type_id == 4): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Surat Persetujuan Tipe</label>
                        <input class="form-control" type="file" name="file_type_approval_certificate" />
                    </div>
                    <div class="col-3 form-group">
                        <label>No Surat Persetujuan Tipe</label>
                        <input class="form-control" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Surat Persetujuan Tipe</label>
                        <div class="input-group date" id="reference_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <?php elseif ($booking->service_type_id == 5): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Sertifikat Sebelumnya</label>
                        <input class="form-control" type="file" name="file_last_certificate" />
                    </div>
                    <div class="col-3 form-group">
                        <label>No Sertifikat Sebelumnya</label>
                        <input class="form-control" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Sertifikat Sebelumnya</label>
                        <div class="input-group date" id="reference_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <?php elseif ($booking->service_type_id == 6 || $booking->service_type_id == 7): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Surat Permohonan</label>
                        <input class="form-control" type="file" name="file_application_letter" />
                    </div>
                    <div class="col-3 form-group">
                        <label>No Surat Permohonan</label>
                        <input class="form-control" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Surat Permohonan</label>
                        <div class="input-group date" id="reference_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Manual Kalibrasi</label>
                        <input class="form-control" type="file" name="file_calibration_manual" />
                    </div>
                    <div class="col-6 form-group">
                        <label>File Buku Manual</label>
                        <input class="form-control" type="file" name="file_manual_book" />
                    </div>
                </div>
                <?php endif ?>
                <?php endif ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#type_id').select2({
            theme: 'bootstrap4',
        });
        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#cari_tool_code').click(function() {
            $.get('<?= base_url(); ?>/standard/read/'+$('#tool_code').val(),function(res)
            {
                if (res) {
                    $('#uml_standard_id').val(res.id);
                    $('#standard_type').val(res.standard_type);
                    $('#jumlah_per_set').val(res.jumlah_per_set);
                    $('#attribute_name').val(res.attribute_name);
                    $('#standard_detail_type_name').val(res.standard_detail_type_name);
                    $('#standard_detail_type_id').val(res.standard_detail_type_id);
                }
            });
        });
    })
</script>
<?= $this->endSection() ?>