<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Alat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Alat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking/edit/' . $booking->id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body" >
       
            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" action="<?= base_url('bookingitem/copy/'.$booking->id.'/'.$item->id); ?>" 
                enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>

                <?php if($booking->service_type_id == 1 || $booking->service_type_id == 2): ?>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label>Jenis</label>
                                <select class="form-control" name="type_id" id="type_id">
                                    <?php foreach ($uutTypes as $type): ?>
                                    <option value="<?= $type->id ?>" <?= $uut->type_id == $type->id ? 'selected' : '' ?> ><?= $type->standard_type ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-6 form-group">
                                <label>Identitas Alat</label>
                                <select class="form-control" name="uut_id" id="uut_id">
                                <option value="<?= $item->uut_id ?>" selected>
                                    <?= $uut->tool_brand .'/'. $uut->tool_model .'/'. $uut->tool_type .'/'. $uut->serial_no ?>
                                </option>
                                </select>
                                <a href="<?= base_url('uttp/create') ?>" target="_blank">
                                    Klik disini untuk menambahkan data alat yang baru.
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3 form-group">
                                <label>Nomor Seri</label>
                                <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                    value="<?= $uut->serial_no ?>" readonly/>
                            </div>
                            <div class="col-3 form-group">
                                <label>Merk</label>
                                <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                                    value="<?= $uut->tool_brand ?>" readonly/>
                            </div>
                            <div class="col-3 form-group">
                                <label>Model</label>
                                <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                    value="<?= $uut->tool_model ?>" readonly/>
                            </div>  
                            <div class="col-3 form-group">
                                <label>Tipe</label>
                                <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                    value="<?= $uut->tool_type ?>" readonly/>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-3 form-group">
                                <label>Buatan</label>
                                <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                    value="<?= $uut->tool_made_in ?>" readonly/>
                            </div>
                            <div class="col-3 form-group">
                                <label>Kapasitas Maksimal</label>
                                <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                                    value="<?= $uut->tool_capacity ?>" readonly/>
                            </div>
                            <div class="col-3 form-group">
                                <label>Satuan Kapasitas</label>
                                <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                                    value="<?= $uut->tool_capacity_unit ?>" readonly/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label>Pabrikan</label>
                                <input class="form-control" type="text" name="tool_factory" id="tool_factory" 
                                    value="<?= $uut->tool_factory ?>" readonly/>
                            </div>
                            <div class="col-6 form-group">
                                <label>Alamat Pabrikan</label>
                                <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" readonly><?= $uut->tool_factory_address ?></textarea>
                            </div>
                        </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <select class="form-control" name="type_id" id="type_id">
                                <?php foreach ($uttpTypes as $type): ?>
                                <option value="<?= $type->id ?>" <?= $uttp->type_id == $type->id ? 'selected' : '' ?> ><?= $type->uttp_type ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label>Identitas Alat</label>
                            <select class="form-control" name="uttp_id" id="uttp_id">
                            <option value="<?= $item->uttp_id ?>" selected>
                                <?= $uttp->tool_brand .'/'. $uttp->tool_model .'/'. $uttp->tool_type .'/'. $uttp->serial_no ?>
                            </option>
                            </select>
                            <a href="<?= base_url('uttp/create') ?>" target="_blank">
                                Klik disini untuk menambahkan data alat yang baru.
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $uttp->serial_no ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                                value="<?= $uttp->tool_brand ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Model</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $uttp->tool_model ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                value="<?= $uttp->tool_type ?>" readonly/>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $uttp->tool_made_in ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimal</label>
                            <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $uttp->tool_capacity ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                                value="<?= $uttp->tool_capacity_unit ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" id="tool_factory" 
                                value="<?= $uttp->tool_factory ?>" readonly/>
                        </div>
                        <!-- <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" readonly><?= $uttp->tool_factory_address ?></textarea>
                        </div> -->
                    </div>
                    <?php if(($booking->service_type_id == 4 || $booking->service_type_id == 5) && ($booking->lokasi_pengujian == 'luar')): ?>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Lokasi Penempatan</label>
                            <input class="form-control" type="text" name="location" />
                        </div>
                    </div>
                <?php endif ?>
                <?php if($booking->service_type_id == 4): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Surat Persetujuan Tipe</label>
                        <input class="form-control" type="file" name="file_type_approval_certificate" accept="application/pdf" />
                        <?= $item->file_type_approval_certificate ?>
                    </div>
                    <div class="col-3 form-group">
                        <label>No Surat Persetujuan Tipe</label>
                        <input class="form-control" type="text" name="reference_no" accept="application/pdf" value="<?= $item->reference_no ?>" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Surat Persetujuan Tipe</label>
                        <div class="input-group date" id="reference_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="reference_date" value="<?= date("d-m-Y", strtotime($item->reference_date)) ?>">
                        </div>
                    </div>
                </div>
                <?php elseif ($booking->service_type_id == 5): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Sertifikat Sebelumnya</label>
                        <input class="form-control" type="file" name="file_last_certificate" accept="application/pdf" />
                        <?= $item->file_last_certificate ?>
                    </div>
                    <div class="col-3 form-group">
                        <label>No Sertifikat Sebelumnya</label>
                        <input class="form-control" type="text" name="reference_no" value="<?= $item->reference_no ?>"/>
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Sertifikat Sebelumnya</label>
                        <div class="input-group date" id="reference_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="reference_date" value="<?= date("d-m-Y", strtotime($item->reference_no)) ?>">
                        </div>
                    </div>
                </div>
                <?php elseif ($booking->service_type_id == 6 || $booking->service_type_id == 7): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Surat Permohonan</label>
                        <input class="form-control" type="file" name="file_application_letter" accept="application/pdf" />
                        <?= $item->file_application_letter ?>
                    </div>
                    <div class="col-3 form-group">
                        <label>No Surat Permohonan</label>
                        <input class="form-control" type="text" name="reference_no" value="<?= $item->reference_no ?>" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Surat Permohonan</label>
                        <div class="input-group date" id="reference_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="reference_date" value="<?= date("d-m-Y", strtotime($item->reference_date)) ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Manual Kalibrasi</label>
                        <input class="form-control" type="file" name="file_calibration_manual" accept="application/pdf" />
                        <?= $item->file_calibration_manual ?>
                    </div>
                    <div class="col-6 form-group">
                        <label>File Buku Manual</label>
                        <input class="form-control" type="file" name="file_manual_book" accept="application/pdf" />
                        <?= $item->file_manual_book ?>
                    </div>
                </div>
                <?php endif ?>
                <?php endif ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#uttp_id').val(null).trigger('change');
            $('#serial_no').val(null);
            $('#tool_brand').val(null);
            $('#tool_model').val(null);
            $('#tool_type').val(null);
            $('#tool_capacity').val(null);
            $('#tool_capacity_unit').val(null);
            $('#tool_made_in').val(null);
            $('#tool_factory').val(null);
            $('#tool_factory_address').val(null);
        });

        var owner = '<?= $booking->uttp_owner_id ?>';
        $('#uttp_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uttp/find/'+type+'/'+owner;
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.tool_type + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uttp/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_type').val(res.tool_type);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                }
            });
        });


        // uut processors
        var owner = '<?= $booking->uut_owner_id ?>';
        $('#uut_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/find/'+type+'/'+owner;
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand+" / "+obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uut/get/'+e.params.data.id,function(res)
            {
                $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                    $('#class').val(res.class);
                    $('#jumlah').val(res.jumlah);
                    $('#tool_dayabaca').val(res.tool_dayabaca);
                    $('#tool_dayabaca_unit').val(res.tool_dayabaca_unit);
                if (res) {
                    console.log(res.class)
                    console.log(res.jumlah)
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                    $('#class').val(res.class);
                    $('#jumlah').val(res.jumlah);
                    $('#tool_dayabaca').val(res.tool_dayabaca);
                    $('#tool_dayabaca_unit').val(res.tool_dayabaca_unit);
                }
            }
            );
        });


        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#cari_tool_code').click(function() {
            $.get('<?= base_url(); ?>/standard/read/'+$('#tool_code').val(),function(res)
            {
                if (res) {
                    $('#uml_standard_id').val(res.id);
                    $('#standard_type').val(res.standard_type);
                    $('#jumlah_per_set').val(res.jumlah_per_set);
                    $('#attribute_name').val(res.attribute_name);
                    $('#standard_detail_type_name').val(res.standard_detail_type_name);
                    $('#standard_detail_type_id').val(res.standard_detail_type_id);
                }
            });
        });
    })
</script>
<?= $this->endSection() ?>