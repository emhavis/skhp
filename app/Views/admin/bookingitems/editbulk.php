<?php

use function PHPSTORM_META\type;
?>
<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Alat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Alat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking/edit/' . $booking->id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body" >
       
            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" action="<?= base_url('bookingitem/create/'.$booking->id); ?>" 
                enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>
                <input type="hidden" class="form-control" name="service_type_id" id="service_type_id" value="<?= $booking->service_type_id ?>"/>

            </form>

            <button type="button" class="btn btn-primary" id="add-item"><i class="fa fa-plus"></i> Tambah</button>
            <table class="table table-striped table-bordered table-hover" id="items" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID Alat (Merek/Model/Tipe/Nomor Seri)</th>
                        <th>Jenis</th>
                        <th>Alamat Lokasi</th>
                        <th>Kab/Kota Lokasi</th>
                        <th>No Sertifikat Sebelumnya</th>
                        <th>Tgl Sertifikat Sebelumnya</th>
                        <th>Sertifikat Sebelumnya</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($items as $item):?>
                    <tr>
                        <td width="400px">
                            <select class="form-control uttp_id" name="uttp_id[]">
                            </select>
                        </td>
                        <td width="200px" class="uttp_type"><?= $item->uttp_type ?></td>
                        <td width="200px" class="location">
                            <textarea class="form-control location" name="location" id="location" ></textarea>
                        </td>
                        <td width="200px" class="location_kabkot_id">
                            <select class="form-control select_location_kabkot_ids" name="location_kabkot_id">
                                <?php foreach($cities as $city): ?>
                                <option value="<?= $city->id ?>"><?= $city->nama ?>,  <?= $city->provinsi ?></option>
                                <?php endforeach ?> 
                            </select>
                        </td>
                        <td width="100px" class="reference_no">
                            <input type="text" name="reference_no[]" class="form-control input_reference_no"/>
                        </td>
                        <td width="100px" class="reference_date"></td>
                        <td width="100px" class="file_last_certificate"></td>
                        <td>
                            <button type="button" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="notFound" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data UTTP tidak ditemukan. Pastikan kembali nomor seri UTTP sudah sesuai atau belum.</p>
                <p>Gunakan tombol Tambah Baru untuk menambahkan data UTTP baru. Kemudian, gunakan nomor seri UTTP yang baru ditambahkan untuk melakukan pencarian kembali.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" class="btn btn-primary" id="new_uttp">Tambah baru</button>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#items').DataTable({
            pageLength: 20,
        });

        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').show();
        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').hide();

        $('#location_kabkot_id, .select_location_kabkot_ids').select2({
            theme: 'bootstrap4',
        });
        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#uttp_id').val(null).trigger('change');
            $('#uut_id').val(null).trigger('change');
            $('#serial_no').val(null);
            $('#tool_brand').val(null);
            $('#tool_model').val(null);
            $('#tool_capacity').val(null);
            $('#tool_capacity_min').val(null);
            $('#tool_capacity_unit').val(null);
            $('#tool_made_in').val(null);
            $('#tool_factory').val(null);
            $('#tool_factory_address').val(null);
            $('#tool_media').val(null);
            $('#class').val(null);
            $('#jumlah').val(null);
            $('#tool_dayabaca').val(null);
            $('#tool_dayabaca_unit').val(null);
            $('#location_lat').val(null);
            $('#location_long').val(null);

            $.get('<?= base_url(); ?>/uttp/getType/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#is_tera').val(res.is_tera == 't');
                    $('#is_ujitipe').val(res.is_ujitipe == 't');

                    if (res.is_ujitipe != 't') {
                        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').hide();
                        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').show();
                    } else {
                        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').show();
                        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').hide();
                    }
                }
            });
        });

        $('.uttp_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/useruttp/find/'+'<?= $booking->uttp_owner_id ?>'
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            var tr = $(this).closest('tr');
            $.get('<?= base_url(); ?>/uttp/getSertifikat/'+e.params.data.id,function(res)
            {
                if (res) {
                    tr.find('.uttp_type').text(res.uttp.uttp_type);
                    tr.find('.input_reference_no').val(res.sertifikat.no_sertifikat);
                    tr.find('.reference_date').text(res.sertifikat.kabalai_date);
                }
            });
        });

        $('#add-item').on("click", function()
        {
            var table = $("#items");
            var insp_item = '<tr>' + 
                    '    <td width="400px">' + 
                    '        <select class="form-control uttp_id" name="uttp_id[]">' + 
                    '        </select>' + 
                    '    </td>' + 
                    '    <td width="200px" class="uttp_type"></td>' +
                    '    <td width="200px" class="location">' +
                    '        <textarea class="form-control location" name="location" id="location" ></textarea>' +
                    '    </td>' +
                    '    <td width="200px" class="location_kabkot_id">' +
                    '        <select class="form-control select_location_kabkot_ids" name="location_kabkot_id">' +
                    '    </td>' +
                    '    <td width="100px" class="reference_no"><input type="text" name="reference_no[]" class="form-control input_reference_no"/></td>' +
                    '    <td width="100px" class="reference_date"></td>' +
                    '    <td width="100px" class="file_last_certificate"></td>' +
                    '    <td><button type="button" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i> Hapus</button></td>' + 
                '</tr>' ;
            table.append(insp_item);
            $('.uttp_id').select2({
                theme: 'bootstrap4',
                ajax: {
                    url: function (params) {
                        var type = $('#type_id').val();
                        return '<?= base_url(); ?>/useruttp/find/'+'<?= $booking->uttp_owner_id ?>'
                    },
                    data: function (params) {
                        var queryParameters = {
                            q: params.term
                        }

                        return queryParameters;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj, index) {
                                return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                            })
                        };
                    },
                }
            }).on("select2:select", function (e) { 
                var tr = $(this).closest('tr');
                $.get('<?= base_url(); ?>/uttp/getSertifikat/'+e.params.data.id,function(res)
                {
                    if (res) {
                        tr.find('.uttp_type').text(res.uttp.uttp_type);
                        tr.find('.input_reference_no').val(res.sertifikat.no_sertifikat);
                        tr.find('.reference_date').text(res.sertifikat.kabalai_date);
                    }
                });
            });
        });
        $('#items').on("click","button.btn-delete",function()
        {
            console.log('hapus');
            $(this).closest("tr").remove();
        });

        var owner ='<?= $booking->uttp_owner_id ?>';
        
        $('#uttp_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/useruttp/find/'+'<?= $booking->uttp_owner_id ?>'
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uttp/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#uttp_type').val(res.uttp_type);
                    
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_min').val(res.tool_capacity_min);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                    $('#location_lat').val(res.location_lat);
                    $('#location_long').val(res.location_long);
                }
            });
        });

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });


        $('#reference_date_grp').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#cari_tool_code').click(function() {
            $.get('<?= base_url(); ?>/standard/read/'+$('#tool_code').val(),function(res)
            {
                if (res) {
                    $('#uml_standard_id').val(res.id);
                    $('#standard_type').val(res.standard_type);
                    $('#jumlah_per_set').val(res.jumlah_per_set);
                    $('#attribute_name').val(res.attribute_name);
                    $('#standard_detail_type_name').val(res.standard_detail_type_name);
                    $('#standard_detail_type_id').val(res.standard_detail_type_id);
                }
            });
        });

        // uut processors
        var owner = '<?= $booking->uut_owner_id ?>';
        $('#uut_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/useruut/find/'+'<?= $booking->uut_owner_id ?>'
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uut/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#uut_type').val(res.uut_type);
                    
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);

                    $('#tool_dayabaca').val(res.tool_dayabaca);
                    $('#tool_dayabaca_unit').val(res.tool_capacity_unit);
                    $('#class').val(res.class);
                    $('#jumlah').val(res.jumlah);
                }
            });
        });
        $("#booking").validate({
            rules: {
                uttp_id: {
                    required: !0,
                },
                /*
                location: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) && $("#lokasi_pengujian").val() == 'luar';
                    },
                },
                */
                file_type_approval_certificate: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                reference_no: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                reference_date: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                <?php if(($booking->lokasi_pengujian == 'dalam')): ?>
                file_last_certificate: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 5);
                    },
                },
                <?php endif; ?>
                file_application_letter: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
                file_manual_book: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
            },
            messages: {
                uttp_id: 'Alat wajib dipilih',
                //location: 'Lokasi alat wajib diisi',
                reference_no: 'Nomor surat wajib diisi',
                reference_date: 'Tanggal surat wajib diisi dengan format yang benar',
                file_type_approval_certificate: 'File surat persetujuan wajib diupload',
                file_last_certificate: 'File sertifikat sebelumnya wajib diupload',
                file_application_letter: 'File surat permohonan wajib diupload',
                file_manual_book: 'File buku manual wajib diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>