<?php

use function PHPSTORM_META\type;
?>
<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Rincian Alat</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Rincian Alat</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking/edit/' . $booking->id); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-default btn-sm" 
                    href="#bookingCollapse" data-toggle="collapse" role="button"
                    aria-expanded="false" aria-controls="bookingCollapse"><i class="fa fa-eye"></i> Data Booking</a>
            </div>
        </div>
        <div class="ibox-body" >
       
            <div class="collapse" id="bookingCollapse">
                <div class="card card-body m-b-20">
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group">
                            <label>Nomor Booking</label>
                            <input class="form-control" type="text" name="booking_no" 
                                value="<?= $booking->booking_no ?>" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis Layanan</label>
                            <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Lokasi Pengujian</label>
                            <input class="form-control" type="text" name="lokasi_pengujian" readonly
                                id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Peruntukan Layanan</label>
                            <input class="form-control" type="text" name="for_sertifikat" readonly
                                id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pemilik Alat</label>
                            <input class="form-control" type="text" name="label_sertifikat" readonly
                                value="<?= $booking->label_sertifikat ?>">
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pemilik Alat</label>
                            <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            
                        </div>
                        <div class="col-3 form-group" >
                            
                        </div>
                        <div class="col-3 form-group" >
                            <label>Jadwal Pengiriman Alat</label>
                            <input class="form-control" type="text" name="est_arrival_date" readonly
                                id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                        </div>
                        <div class="col-3 form-group">
                            <label>Perkiraan Total Biaya</label>
                            <input class="form-control" type="text" name="est_total_price" 
                                value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                        </div>
                    </div>
                </div>
            </div>

            <form id="booking" action="<?= base_url('bookingitem/create/'.$booking->id); ?>" 
                enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>
                <input type="hidden" class="form-control" name="service_type_id" id="service_type_id" value="<?= $booking->service_type_id ?>"/>

                <?php if($booking->service_type_id == 1 || $booking->service_type_id == 2): ?>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <select class="form-control" name="type_id" id="type_id">
                                <?php foreach ($standard as $std): ?>
                                <option value="<?= $std->id ?>"><?= $std->uut_type ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label>Data Alat</label>
                            <select class="form-control" name="uut_id" id="uut_id">
                            </select>
                            <a href="<?= base_url() ?>/uut/create" target="_blank">
                                Klik disini untuk menambahkan data alat yang baru.
                            </a>
                        </div>
                    </div>

                    <div class="row">
                   
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Nomor Seri / Identitas</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" readonly/>
                        </div>
                        <!--
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" readonly/>
                        </div>
                        -->
                        <div class="col-3 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-3 form-group">
                            <label>Buatan</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" readonly/>
                        </div> -->
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimal</label>
                            <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" readonly/>
                        </div>

                        <div class="col-3 form-group">
                            <label>Kelas</label>
                            <input class="form-control" type="text" name="class" id="class" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Jumlah</label>
                            <input class="form-control" type="text" name="jumlah" id="jumlah" readonly/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Daya Baca</label>
                            <input class="form-control" type="text" name="tool_dayabaca" id="tool_dayabaca" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Unit Daya Baca</label>
                            <input class="form-control" type="text" name="tool_dayabaca_unit" id="tool_dayabaca_unit" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Manual Book</label>
                            <input class="form-control" type="file" name="file_manual_book" accept="application/pdf" />
                        </div>
                    </div>
                <!-- </div> -->

                <?php else: ?>
                
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis</label>
                        <input type="hidden" class="form-control" id="is_tera" value="true" readonly/>
                        <input type="hidden" class="form-control" id="is_ujitipe" value="true" readonly/>
                        <select class="form-control" name="type_id" id="type_id">
                            <?php foreach ($uttpTypes as $type): ?>
                            <option value="<?= $type->id ?>"><?= $type->uttp_type ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label>ID Alat<span class="required_notification">*</span></label>
                        <select class="form-control" name="uttp_id" id="uttp_id">
                        </select>
                        <a href="<?= base_url('uttp/create') ?>" target="_blank">
                            Klik disini untuk menambahkan data alat yang baru.
                        </a>
                    </div>
                </div>
                <div class="row">
                   
                    <div class="col-3 form-group">
                        <label>Merek</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Model/Tipe</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" readonly/>
                    </div>  
                    <div class="col-3 form-group">
                        <label>Nomor Seri</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" readonly/>
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" readonly/>
                    </div>
                    -->
                    <div class="col-3 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" readonly />
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-3 form-group">
                        <label>Buatan</label>
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" readonly/>
                    </div> -->
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimal</label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Satuan Kapasitas</label>
                        <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" readonly/>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pabrikan</label>
                        <input class="form-control" type="text" name="tool_factory" id="tool_factory" readonly/>
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" readonly></textarea>
                    </div>
                </div>
                -->
                <?php if(($booking->service_type_id == 4 || $booking->service_type_id == 5) && ($booking->lokasi_pengujian == 'luar')): ?>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Lokasi Penempatan<span class="required_notification">*</span></label>
                        <input class="form-control" type="text" name="location" id="location" />
                    </div>
                </div>
                <?php endif ?>
                <?php if($booking->service_type_id == 4): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label id="file_type_approval_certificate_req">File Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                        <label id="file_type_approval_certificate_noreq">File Surat Persetujuan Tipe</label>
                        <input class="form-control" id="file_type_approval_certificate" 
                            type="file" name="file_type_approval_certificate" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group">
                        <label id="reference_no_req">No Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                        <label id="reference_no_noreq">No Surat Persetujuan Tipe</label>
                        <input class="form-control" id="reference_no" 
                            type="text" name="reference_no" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group" >
                        <label id="reference_date_req">Tanggal Surat Persetujuan Tipe<span class="required_notification">*</span></label>
                        <label id="reference_date_noreq">Tanggal Surat Persetujuan Tipe</label>
                        <div class="input-group date" id="reference_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="reference_date"
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <?php elseif ($booking->service_type_id == 5): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_last_certificate" 
                            type="file" name="file_last_certificate" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group">
                        <label>No Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                        <input class="form-control" id="reference_no" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Sertifikat Sebelumnya<span class="required_notification">*</span></label>
                        <div class="input-group date" id="reference_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="reference_date"
                                name="reference_date">
                        </div>
                    </div>
                    
                </div>
                <?php elseif ($booking->service_type_id == 6 || $booking->service_type_id == 7): ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Surat Permohonan<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_application_letter" 
                            type="file" name="file_application_letter" accept="application/pdf" />
                    </div>
                    <div class="col-3 form-group">
                        <label>No Surat Permohonan<span class="required_notification">*</span></label>
                        <input class="form-control" id="reference_no" type="text" name="reference_no" />
                    </div>
                    <div class="col-3 form-group" >
                        <label>Tanggal Surat Permohonan<span class="required_notification">*</span></label>
                        <div class="input-group date" id="reference_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="reference_date"
                                name="reference_date">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>File Manual Kalibrasi</label>
                        <input class="form-control" id="file_calibration_manual" 
                            type="file" name="file_calibration_manual" accept="application/pdf" />
                    </div>
                    <div class="col-6 form-group">
                        <label>File Buku Manual<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_manual_book" 
                            type="file" name="file_manual_book" accept="application/pdf" />
                    </div>
                </div>
                <?php endif ?>
                <?php endif ?>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').show();
        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').hide();

        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#uttp_id').val(null).trigger('change');
            $('#uut_id').val(null).trigger('change');
            $('#serial_no').val(null);
            $('#tool_brand').val(null);
            $('#tool_model').val(null);
            $('#tool_capacity').val(null);
            $('#tool_capacity_unit').val(null);
            $('#tool_made_in').val(null);
            $('#tool_factory').val(null);
            $('#tool_factory_address').val(null);
            $('#tool_media').val(null);
            $('#class').val(null);
            $('#jumlah').val(null);
            $('#tool_dayabaca').val(null);
            $('#tool_dayabaca_unit').val(null);

            $.get('<?= base_url(); ?>/uttp/getType/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#is_tera').val(res.is_tera == 't');
                    $('#is_ujitipe').val(res.is_ujitipe == 't');

                    if (res.is_ujitipe != 't') {
                        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').hide();
                        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').show();
                    } else {
                        $('#file_type_approval_certificate_req, #reference_no_req, #reference_date_req').show();
                        $('#file_type_approval_certificate_noreq, #reference_no_noreq, #reference_date_noreq').hide();
                    }
                }
            });
        });

        var owner ='<?= $booking->uttp_owner_id ?>';
        $('#uttp_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uttp/find/'+type+'/'+'<?= $booking->uttp_owner_id ?>'
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand + '/' + obj.tool_model + '/' + obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uttp/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                }
            });
        });


        $('#reference_date_grp').datepicker({
            format: 'dd-mm-yyyy',
        });

        $('#cari_tool_code').click(function() {
            $.get('<?= base_url(); ?>/standard/read/'+$('#tool_code').val(),function(res)
            {
                if (res) {
                    $('#uml_standard_id').val(res.id);
                    $('#standard_type').val(res.standard_type);
                    $('#jumlah_per_set').val(res.jumlah_per_set);
                    $('#attribute_name').val(res.attribute_name);
                    $('#standard_detail_type_name').val(res.standard_detail_type_name);
                    $('#standard_detail_type_id').val(res.standard_detail_type_id);
                }
            });
        });

        // uut processors
        var owner = '<?= $booking->uut_owner_id ?>';
        $('#uut_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/find/'+type+'/'+owner;
                },
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.tool_brand+" / "+obj.serial_no };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uut/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    console.log(res.class)
                    console.log(res.jumlah)
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                    $('#class').val(res.class);
                    $('#jumlah').val(res.jumlah);
                    $('#tool_dayabaca').val(res.tool_dayabaca);
                    $('#tool_dayabaca_unit').val(res.tool_dayabaca_unit);
                }
            }
            );
        });
        $("#booking").validate({
            rules: {
                uttp_id: {
                    required: !0,
                },
                location: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) && $("#lokasi_pengujian").val() == 'luar';
                    },
                },
                file_type_approval_certificate: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                reference_no: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                reference_date: {
                    required: function(element) {
                        if ($("#service_type_id").val() == 4 || $("#service_type_id").val() == 5) {
                            return $("#is_ujitipe").val() == 'true';
                        }
                        return true;
                    },
                },
                file_last_certificate: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 5);
                    },
                },
                file_application_letter: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
                file_manual_book: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 6 || $("#service_type_id").val() == 7);
                    },
                },
            },
            messages: {
                uttp_id: 'Alat wajib dipilih',
                location: 'Lokasi alat wajib diisi',
                reference_no: 'Nomor surat wajib diisi',
                reference_date: 'Tanggal surat wajib diisi dengan format yang benar',
                file_type_approval_certificate: 'File surat persetujuan wajib diupload',
                file_last_certificate: 'File sertifikat sebelumnya wajib diupload',
                file_application_letter: 'File surat permohonan wajib diupload',
                file_manual_book: 'File buku manual wajib diupload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>