<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Daftar Baru</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking');?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('booking/create'); ?>" enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <?php if($user->id <= 1){ ?>
                            <select class="form-control" name="service_type_id" id="service_type_id">
                                <option value="<?= $types->id ?>"><?= $types->service_type ?></option>
                            </select>
                        <?php }else{ ?>
                            <?php if($uml_id >0 ){ ?>
                                <select class="form-control" name="service_type_id" id="service_type_id">
                                    <option value="<?= $types[0]->id ?>"><?= $types[0]->service_type ?></option>
                                </select>
                            <?php }else{?>
                                <select class="form-control" name="service_type_id" id="service_type_id">
                                    <?php foreach ($types as $type): ?>
                                    <option value="<?= $type->id ?>"><?= $type->service_type ?></option>
                                    <?php endforeach ?>
                                </select>
                            <?php }?>
                        <?php }?>
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <select class="form-control" name="lokasi_pengujian" id="lokasi_pengujian">
                            <option value="dalam">Dalam Kantor</option>
                            <option value="luar">Luar Kantor</option>  
                        </select>
                    </div>
                    <div class="col-3 form-group" id="div-dl" style="display:none;">
                        <label>Lokasi Pengujian Luar Kantor</label>
                        <select class="form-control" name="lokasi_dl" id="lokasi_dl">
                            <option value="dalam">Dalam Negeri</option>
                            <option value="luar">Luar Negeri</option>  
                        </select>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Jenis Identitas Pemilik Alat</label>
                        <div>
                            <label class="ui-radio ui-radio-inline">
                                <input type="radio" name="jenis_identitas" id="jenis_identitas_nib" 
                                    value="nib" checked>
                                <span class="input-span"></span>NIB</label>
                            <label class="ui-radio ui-radio-inline">
                                <input type="radio" name="jenis_identitas" id="jenis_identitas_npwp"
                                    value="npwp">
                                <span class="input-span"></span>NPWP</label>
                        </div>
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Identitas Pemilik Alat</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="id_no" id="id_no">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="find_uttp_owner">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <div id="div-uttp">
                            <select class="form-control" name="uttp_owner_id" id="uttp_owner_id">
                            </select>
                            <span class="help-block" id="tout" name="tout">
                                <a href="<?= base_url() ?>/useruttpowner/create" target="_blank">
                                Klik disini untuk menambahkan data pemilik yang baru.
                            </a>
                            </span>
                        </div>
                        <!-- for another choice  -->
                        <div id="div-uut">
                            <select class="form-control" name="uut_owner_id" id="uut_owner_id">
                                
                            </select>
                            <?php if($uml_id <0 ){ ?>
                                <span class="help-block" id="touu" name="touu">
                                    <a href="<?= base_url() ?>/useruutowner/create" target="_blank">
                                    Klik disini untuk menambahkan data pemilik yang baru.
                                    </a>
                                </span>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Pemilik Alat</label>
                        <input class="form-control" id="label_sertifikat" 
                            type="text" name="label_sertifikat"  />
                        <span class="help-block" id="label_help" name="label_help">
                            Ubah jika diperlukan untuk menambahkan informasi Nama Pemilik Alat seperti Cabang, dsb
                        </span>
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" id="addr_sertifikat"></textarea>
                        <span class="help-block" id="label_help" name="label_help">
                            Ubah jika diperlukan untuk menyesuaikan alamat Pemilik Alat. Jangan sertakan nama Kabupaten/Kota dan nama Provinsi.
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Provinsi Pemilik Alat</label>
                        <select class="form-control" name="prov_sertifikat_id" id="prov_sertifikat_id">
                            <?php foreach($provinsiList as $provinsi): ?>
                            <option value="<?= $provinsi->id ?>" ?><?= $provinsi->nama ?></option>
                            <?php endforeach ?> 
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label>Kabupaten/Kota Pemilik Alat</label>    
                        <input type="hidden" name="kota_id" id="kota_id"/>
                        <select class="form-control" name="kabkota_sertifikat_id" id="kabkota_sertifikat_id">
                        </select>
                    </div>
                </div>
                <div class="row" id="div-pj">
                    <div class="col-6 form-group">
                        <label>Nama Perusahaan Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_perusahaan" 
                            type="text" name="keuangan_perusahaan"  />
                    </div>
                    <div class="col-6 form-group">
                        <label>Nama PIC Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_pic" 
                            type="text" name="keuangan_pic"  />
                    </div>
                </div>
                <div class="row" id="div-pj-2">
                    <div class="col-3 form-group">
                        <label>Jabatan PIC Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_jabatan" 
                            type="text" name="keuangan_jabatan"  />
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor HP PIC Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_hp" 
                            type="text" name="keuangan_hp"  />
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <div class="col-3 form-group" >
                        <!--
                        <label>Jadwal Pengiriman Alat</label>
                        <div class="input-group date" id="est_arrival_date">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" 
                                name="est_arrival_date">
                        </div>
                        -->
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format(0, 2, ',', '.') ?>"readonly>
                    </div>
                    -->
                    <!-- <div class="col-6 form-group">
                        <label>Tipe Layanan</label>
                        <select class="form-control" name="tipelayanan" id="tipelayanan">
                            <option value="1">Umum</option>
                            <option value="2">UML/Mahasiswa/Pelajar</option>
                            <option value="3">UPT</option>
                        </select>
                      </div> -->
                </div>
                <!-- <div class="row" id ="perifikasi">
                    <div class="col-6 form-group">
                        <label>File Manual Kalibrasi</label>
                        <input class="form-control" id="certificate_calibration" 
                            type="file" name="certificate_calibration" accept="application/pdf" />
                    </div>
                </div> -->
                <div class="row" id ="kalibrasi">
                    <div class="col-6 form-group">
                            <div class="custom-file">
                                <label class="custom-file-label">Surat Permohonan</label>
                                <input class="form-control custom-file-input" type="file" name="certificate_perification" id = "certificate_perification" accept="application/pdf" />
                            </div>
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="currentOwner" tabindex="-1" aria-labelledby="currentOwnerLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="currentOwnerLabel">Konfirmasi Data Pemilik Alat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data pemilik alat ditemukan sebagai berikut:</p>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Nama</label>
                        <input type="hidden" name="uttp_owner_current" id="uttp_id_owner_current" />
                        <input type="text" class="form-control" name="nama_owner" id="nama_owner_current" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat_owner" id="alamat_owner_current" readonly></textarea>
                    </div>
                </div>
                <p>Apakah data pemilik alat tersebut akan digunakan untuk data booking?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-success" id="confirm_current">Konfirmasi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="newOwner" tabindex="-1" aria-labelledby="newOwnerLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newOwnerLabel">Konfirmasi Data Pemilik Alat Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="create_owner">
                <?= csrf_field() ?>
                <p>Data pemilik alat tidak ditemukan. Data berikut akan ditambahkan:</p>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>NIB</label>
                        <input type="text" class="form-control" name="nib" id="nib_owner_new" readonly />
                    </div>
                    <div class="col-6 form-group">
                        <label>NPWP</label>
                        <input type="text" class="form-control" name="npwp" id="npwp_owner_new" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama_owner_new" readonly />
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat_owner_new" readonly></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Kabupaten/Kota</label>
                        <input type="hidden" name="kota_id_owner_new" id="kota_id_owner_new" />
                        <input type="text" class="form-control" name="kota" id="kota_owner_new" readonly />
                    </div>
                    <div class="col-6 form-group">
                        <label>Provinsi</label>
                        <input type="text" class="form-control" name="provinsi" id="provinsi_owner_new" readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Email</label>
                        <input type="hidden" name="telepon" id="telepon_owner_new" />
                        <input type="text" class="form-control" name="email" id="email_owner_new" readonly />
                    </div>
                    <div class="col-6 form-group">
                        <label>Penanggung Jawab</label>
                        <input type="hidden" name="bentuk_usaha_id" id="bentuk_usaha_id_new" />
                        <input type="text" class="form-control" name="penanggung_jawab" id="pj_owner_new" readonly />
                    </div>
                </div>
                </form>
                <p>Apakah data pemilik alat tersebut akan ditambahkan ke dalam database dan digunakan untuk data booking?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-success" id="confirm_new">Konfirmasi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="notFound" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data pemilik alat tidak ditemukan. Pastikan gunakan NIB/NPWP yang valid.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(function() {
        var uml_id = <?=$uml_id ? $uml_id :0 ?>;
        var ktp = <?=$user->ktp ? $user->ktp :0 ?>;
        if( uml_id >0 || ktp > 0){
            $('#kalibrasi').show();
            // $('#kalibrasi').show();
            // $('#service_type_id').select2({
            //     disabled: true
            //     });
        }else{
            $('#kalibrasi').hide();
            // $('#kalibrasi').hide();
        }
        
        // $('#tipelayanan').select2({
        //     theme: 'bootstrap4',
        // }).on("select2:select", function (e) { 
        //     if (e.params.data.id == 2){
        //         $('#perifikasi').hide();
        //         $('#kalibrasi').show();
        //     }
        //     else if(e.params.data.id == 3){
        //         $('#perifikasi').show();
        //         // $('#kalibrasi').hide();
        //     }else{
        //         // $('#kalibrasi').hide();
        //         $('#perifikasi').hide();
        //     }
        // });
        
        $('#div-dl').hide();
        $('#div-pj').hide();
        $('#div-pj-2').hide();
        $('#for_sertifikat, #lokasi_pengujian, #lokasi_dl').select2({
            theme: 'bootstrap4',
        });
        
        $('#service_type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            
            if (e.params.data.id == 1 || e.params.data.id == 2){
                $('#div-uttp').hide();
                $('#div-uut').show();
            }
            else{
                $('#div-uut').hide();
                $('#div-uttp').show();
            }
            $.toggleLokasiDL();
            $('#label_sertifikat').val(null);
            $('#addr_sertifikat').val(null);
            $('#uttp_owner_id').val(null);
            $('#uut_owner_id').val(null);
            $('#prov_sertifikat_id').val(null).trigger('change');
            $('#kabkota_sertifikat_id').val(null).trigger('change');
        });

        $.toggleLokasiDL = function(){
            console.log('toggle');

            var layanan = $('#service_type_id').val();
            var lokasi = $('#lokasi_pengujian').val();

            if (!(layanan == 1 || layanan == 2) && lokasi == 'luar') {
                $('#div-dl').show();
            } else {
                $('#div-dl').hide();
            }
        } 

        $('#lokasi_pengujian').on("select2:select", function (e) { 
            $.toggleLokasiDL();
            if (e.params.data.id == 'luar'){
                //$('#div-dl').show();
                $('#div-pj').show();
                $('#div-pj-2').show();
            }else{
                //$('#div-dl').hide();
                $('#div-pj').hide();
                $('#div-pj-2').hide();
            }
        });

        $('#div-uttp').hide();
        $('#uttp_owner_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: '<?= base_url(); ?>/useruttpowner/find',
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uttpowner/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#addr_sertifikat').val(res.alamat);
                    $('#label_sertifikat').val(res.nama);

                    console.log(res.provinsi_id);
                    $('#prov_sertifikat_id').val(res.provinsi_id).trigger('change');
                    $('#kabkota_sertifikat_id').val(res.kota_id).trigger('change');
                    $('#kota_id').val(res.kota_id);

                    var option = new Option(res.kota, res.kota_id, true, true);
                    $('#kabkota_sertifikat_id').append(option).trigger('change');
                }
            });
        });

        $('#est_arrival_date').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date(),
            daysOfWeekDisabled: '0,6',
            datesDisabled: [
                <?php foreach ($holidays as $holiday):?>
                '<?= date("d-m-Y", strtotime($holiday->holiday_date)) ?>',
                <?php endforeach;?>
                <?php foreach ($events as $event):?>
                '<?= date("d-m-Y", strtotime($event->closed_date)) ?>',
                <?php endforeach;?>
                <?php foreach ($quotas as $quota):?>
                '<?= date("d-m-Y", strtotime($quota->quota_date)) ?>',
                <?php endforeach;?>
            ],
            beforeShowDay: function (date) {
                if (date.getDay() == 0 || date.getDay() == 6) {
                    return 'bg-danger text-white';
                }
                var holidays = [
                    <?php foreach ($holidays as $holiday):?>
                    new Date('<?= date("Y-m-d", strtotime($holiday->holiday_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), holidays) > -1) {
                    return 'bg-danger text-white';
                }
                var events = [
                    <?php foreach ($events as $event):?>
                    new Date('<?= date("Y-m-d", strtotime($event->closed_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), events) > -1) {
                    return 'bg-warning text-white';
                }
                var quotas = [
                    <?php foreach ($quotas as $quota):?>
                    new Date('<?= date("Y-m-d", strtotime($quota->quota_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), quotas) > -1) {
                    return 'bg-grey text-white';
                }
            },
        });

        var label = '';
        var addr = '';

        $('#for_sertifikat').on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data);
            if (data.id == 'lain') {
                $('#label_sertifikat').attr('readonly', false);
                $('#addr_sertifikat').attr('readonly', false);
            } else {
                $('#label_sertifikat').attr('readonly', true);
                $('#addr_sertifikat').attr('readonly', true);
                $('#label_sertifikat').val(label);
                $('#addr_sertifikat').val(addr);
            }
        });

        $("#service_type_id").on('change', function(e){
            if ($("#service_type_id").val() == 1 || $("#service_type_id").val() == 2){
                $('#div-uttp').hide();
                $('#div-uut').show();
            }
            else{
                $('#div-uut').hide();
                $('#div-uttp').show();
            }

        });

        // setting action on change
        $('#uut_owner_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: '<?= base_url(); ?>/useruutowner/find',
                data: function (params) {
                    var queryParameters = {
                        q: params.term
                    }

                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
            }
        }).on("select2:select", function (e) { 
            $.get('<?= base_url(); ?>/uutowner/get/'+e.params.data.id,function(res)
            {
                if (res) {
                    $('#addr_sertifikat').val(res.alamat);
                    $('#label_sertifikat').val(res.nama);

                    $('#prov_sertifikat_id').val(res.provinsi_id).trigger('change');
                    $('#kabkota_sertifikat_id').val(res.kota_id);

                    var option = new Option(res.kota, res.kota_id, true, true);
                    $('#kabkota_sertifikat_id').append(option).trigger('change');
                }
            });
        });
        $("#booking").validate({
            rules: {
                uttp_owner_id: {
                    required: function(element) {
                        return ($("#service_type_id").val() != 1 && $("#service_type_id").val() != 2);
                    },
                },
                uut_owner_id: {
                    required: function(element) {
                        return ($("#service_type_id").val() == 1 || $("#service_type_id").val() == 2);
                    },
                },
            },
            messages: {
                uttp_owner_id: 'Pemilik alat wajib dipilih, jika belum ada tambahkan terlebih dahulu',
                uut_owner_id: 'Pemilik alat wajib dipilih, jika belum ada tambahkan terlebih dahulu',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

        $('#find_uttp_owner').click(function(e) {
            by = $('input[name="jenis_identitas"]:checked').val();
            q = $('#id_no').val();

            type = $('#service_type_id').val();
            if (type == 1 || type == 2) {
                url = '<?= base_url(); ?>/uutowner/findByIdentity';
            } else {
                url = '<?= base_url(); ?>/uttpowner/findByIdentity';
            }

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    by: by,
                    q: q,
                    <?= csrf_token() ?>: '<?= csrf_hash() ?>',
                },
                success: function (data) {
                    
                    if (data) {
                        $('#nama_owner_current').val(data.nama);
                        $('#alamat_owner_current').val(data.alamat);
                        $('#uttp_id_owner_current').val(data.id);
                        $('#currentOwner').modal('show');
                    } else {
                        console.log('disini');

                        $.ajax({
                            type: 'post',
                            url: '<?= base_url(); ?>/auth/check_data/' + by,
                            data: { 
                                <?= csrf_token() ?>: "<?= csrf_hash() ?>",
                                value: q,
                            },
                            success: function (res) {
                                if (res.kode == 200) {
                                    var data = res.data;
                                    $('#nib_owner_new').val(data.nib);
                                    $('#npwp_owner_new').val(data.npwp);
                                    $('#nama_owner_new').val(data.full_name);
                                    $('#alamat_owner_new').val(data.alamat);
                                    $('#email_owner_new').val(data.email);
                                    $('#telepon_owner_new').val(data.telepon);
                                    $('#kota_id_owner_new').val(data.kota_id);
                                    $('#kota_owner_new').val(data.kota);
                                    $('#provinsi_owner_new').val(data.provinsi);
                                    $('#pj_owner_new').val(data.penanggung_jawab);
                                    $('#bentuk_usaha_id_new').val(data.bentuk_badan_usaha_id);

                                    $('#newOwner').modal('show');
                                } else {
                                    $('#notFound').modal('show');
                                }
                            }, 
                            error: function(e) {
                                $('#notFound').modal('show');
                            }
                        });
                    }
                    
                },
                error: function (err) {
                    $('#notFound').modal('show');
                }
            });
        });
        $('#confirm_current').click(function(e){
            $('#label_sertifikat').val($('#nama_owner_current').val());
            $('#addr_sertifikat').val($('#alamat_owner_current').val());
            $('#uttp_owner_id').val($('#uttp_id_owner_current').val());

            $('#currentOwner').modal('hide');
        });

        $('#confirm_new').click(function(e){
            var form = $('#create_owner');
            var form_data = form.serialize();

            type = $('#service_type_id').val();
            if (type == 1 || type == 2) {
                url = '<?= base_url(); ?>/uutowner/store';
            } else {
                url = '<?= base_url(); ?>/uttpowner/store';
            }

            $.ajax({
                type: 'post',
                url: url,
                data: form_data,
                success: function (data) {
                    $('#label_sertifikat').val(data.nama);
                    $('#addr_sertifikat').val(data.alamat);
                    $('#uttp_owner_id').val(data.id);
                }, 
                error: function(e) {

                }
            });

            $('#newOwner').modal('hide');

            
        });

        $('#prov_sertifikat_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            //console.log(e);
            $('#kabkota_sertifikat_id').empty().trigger('change');
        });

        $('#kabkota_sertifikat_id').select2({
            theme: 'bootstrap4',
            ajax: {
                url: function (params) {
                    var provinsi_id = $('#prov_sertifikat_id').val();
                    return '<?= base_url(); ?>/auth/findKota/'+provinsi_id;
                },
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    return {
                        q: params.term,
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.nama };
                        })
                    };
                },
                
            }
        });
    })


</script>
<?= $this->endSection() ?>