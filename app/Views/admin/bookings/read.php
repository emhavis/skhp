<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Data Pendaftaran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking');?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editSchedule"><i class="fa fa-calendar"></i> Ubah Rencana Pengantaran</a>
                
            </div>
        </div>
        <div class="ibox-body">
            
            <form id="booking" action="<?= base_url('booking/edit'); ?>/<?= $booking->id ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                                id="jenis_layanan" value="<?= $booking->jenis_layanan ?>">
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <select class="form-control" name="lokasi_pengujian" id="lokasi_pengujian">
                            <option value="dalam" <?= $booking->lokasi_pengujian == 'dalam' ? 'selected' : '' ?> >Dalam Kantor</option>
                            <option value="luar" <?= $booking->lokasi_pengujian == 'luar' ? 'selected' : '' ?>>Luar Kantor</option>  
                        </select>
                    </div>
                    -->
                    
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            value="<?= $booking->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
                        <label>Rencana Pengiriman Alat</label>
                        <input class="form-control" type="text" name="est_arrival_date" readonly
                            id="est_arrival_date" value="<?= $booking->est_arrival_date ? date("d-m-Y", strtotime($booking->est_arrival_date)) : ''; ?>">
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                    -->
                </div>
            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Alat dan Pengujian</div>
        </div>
        <div class="ibox-body">
        <?php foreach ($items as $item):?>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?= $item->tool_code . ' (' . $item->quantity . ' pengujian)' ?></h4>
                    <div class="text-muted card-subtitle"><?= $item->standard_type ?></div>

                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Besaran</label>
                            <input type="text" class="form-control" name="standard_type" id="standard_type" 
                                value="<?= $item->standard_type ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Jenis</label>
                            <input type="text" class="form-control" name="attribute_name" id="attribute_name" 
                                value="<?= $item->attribute_name ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Jumlah Per Set</label>
                            <input type="text" class="form-control" name="jumlah_per_set" id="jumlah_per_set" 
                                value="<?= $item->jumlah_per_set ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Rincian</label>
                            <input type="hidden" id="standard_detail_type_id"/>
                            <input type="text" class="form-control" name="standard_detail_type_name" id="standard_detail_type_name" 
                                value="<?= $item->standard_detail_type_name ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->brand ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->model ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Tipe</label>
                            <input type="text" class="form-control" name="tool_type" id="tool_type" 
                                value="<?= $item->tipe ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->capacity ?>" readonly/>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Jenis Pengujian</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Harga Satuan</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $inspections_filtered = array_filter($inspections, function($arritem) use ($item) {
                                return $arritem->booking_item_id == $item->id;
                            }); ?>
                            <?php foreach ($inspections_filtered as $inspection):?>
                            <tr>
                                <td><?= $inspection->inspection_type ?></td>
                                <td><?= $inspection->quantity ?></td>
                                <td><?= $inspection->unit ?></td>
                                <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Subtotal</th>
                                <th><?= number_format($item->est_subtotal, 2, ',', '.') ?></td>
                            </tr>
                        </tfoot>
                    </table>
                
                </div>
            </div>  
            <?php endforeach; ?>
            
        </div>
    </div>
</div>

<div class="modal fade" id="editSchedule" tabindex="-1" aria-labelledby="editScheduleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="editScheduleLabel">Rencana Pengantaran Alat</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="resubmit" action="<?= base_url('booking/resubmit'); ?>/<?= $booking->id ?>" method="post">
        <?= csrf_field() ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-6 form-group" >
                    <?php if ($booking->lokasi_pengujian == 'dalam'): ?>
                    <label>Rencana Pengantaran Alat</label>
                    <div class="input-group date" id="est_arrival_date_grp">
                        <div class="input-group-prepend input-group-addon">
                            <span class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" id="est_arrival_date"
                            value="<?= $booking->est_arrival_date ? date("d-m-Y", strtotime($booking->est_arrival_date)) : ''; ?>"
                            name="est_arrival_date" autocomplete="off">
                    </div> 
                        <?php else: ?>
                    <label>Usulan Jadwal</label>
                    <div class="input-group input-daterange" id="est_schedule_date_grp">
                        <input type="text" class="form-control" id="est_schedule_date_from"
                            value="<?= $booking->est_schedule_date_from ? date("d-m-Y", strtotime($booking->est_schedule_date_from)) : ''; ?>"
                            name="est_schedule_date_from">
                            <span class="input-group-addon input-group-text p-l-10 p-r-10">s/d</span>
                        <input type="text" class="form-control" id="est_schedule_date_to"
                            value="<?= $booking->est_schedule_date_to ? date("d-m-Y", strtotime($booking->est_schedule_date_to)) : ''; ?>"
                            name="est_schedule_date_to">
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan Perubahan</button>
        </div>
        </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#est_arrival_date_grp').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date(),
            daysOfWeekDisabled: '0,6',
            datesDisabled: [
                <?php foreach ($holidays as $holiday):?>
                '<?= date("d-m-Y", strtotime($holiday->holiday_date)) ?>',
                <?php endforeach;?>
                <?php foreach ($events as $event):?>
                '<?= date("d-m-Y", strtotime($event->closed_date)) ?>',
                <?php endforeach;?>
                <?php foreach ($quotas as $quota):?>
                '<?= date("d-m-Y", strtotime($quota->quota_date)) ?>',
                <?php endforeach;?>
            ],
            beforeShowDay: function (date) {
                if (date.getDay() == 0 || date.getDay() == 6) {
                    return 'bg-danger text-white';
                }
                var holidays = [
                    <?php foreach ($holidays as $holiday):?>
                    new Date('<?= date("Y-m-d", strtotime($holiday->holiday_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), holidays) > -1) {
                    return 'bg-danger text-white';
                }
                var events = [
                    <?php foreach ($events as $event):?>
                    new Date('<?= date("Y-m-d", strtotime($event->closed_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), events) > -1) {
                    return 'bg-warning text-white';
                }
                var quotas = [
                    <?php foreach ($quotas as $quota):?>
                    new Date('<?= date("Y-m-d", strtotime($quota->quota_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), quotas) > -1) {
                    return 'bg-grey text-white';
                }
            },
        });

        $('#est_schedule_date').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date(),
        });
    })
</script>
<?= $this->endSection() ?>