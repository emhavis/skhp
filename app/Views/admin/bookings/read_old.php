<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="/booking">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Daftar</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran</div>
            <div>
                <a class="btn btn-default btn-sm" href="/booking"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <form id="booking" action="<?= base_url('booking/edit'); ?>/<?= $booking->id ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >

                    </div>
                    <div class="col-3 form-group" >

                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" 
                            value="<?= $booking->booking_no ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $booking->jenis_layanan == 'verifikasi' ? 'Verifikasi' : 'Kalibrasi' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            value="<?= $booking->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        
                    </div>
                    <div class="col-3 form-group" >
                        
                    </div>
                    <div class="col-3 form-group" >
                        <label>Jadwal Pengiriman Alat</label>
                        <input class="form-control" type="text" name="est_arrival_date" readonly
                            id="est_arrival_date" value="<?= $booking->est_arrival_date ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Alat dan Pengujian</div>
        </div>
        <div class="ibox-body">
        <?php foreach ($items as $item):?>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?= $item->tool_code . ' (' . $item->quantity . ' pengujian)' ?></h4>
                    <div class="text-muted card-subtitle"><?= $item->type ?></div>
                    
                    <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Jenis Pengujian</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Harga Satuan</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $inspections_filtered = array_filter($inspections, function($arritem) use ($item) {
                                return $arritem->booking_item_id == $item->id;
                            }); ?>
                            <?php foreach ($inspections_filtered as $inspection):?>
                            <tr>
                                <td><?= $inspection->inspection_type ?></td>
                                <td><?= $inspection->quantity ?></td>
                                <td><?= $inspection->unit ?></td>
                                <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"><strong>Subtotal</strong></td>
                                <td><strong><?= number_format($item->est_subtotal, 2, ',', '.') ?></strong></td>
                            </tr>
                        </tfoot>
                    </table>
                    
                    <a class="btn btn-default btn-sm" href="/bookingiteminspection/indexread/<?= $item->booking_id ?>/<?= $item->id ?>">
                        <i class="fa fa-eye"></i> Lihat
                    </a>
                </div>
            </div>  
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#items').DataTable({
            pageLength: 10,
        });

        $('#book').click(function(e) {
            e.preventDefault()
            var form = $('#booking');
            form.attr('action', '<?= base_url('booking/submit'); ?>/<?= $booking->id ?>')
            form.submit();
        });
    })
</script>
<?= $this->endSection() ?>