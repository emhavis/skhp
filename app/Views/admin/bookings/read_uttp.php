<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data Pendaftaran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking');?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                <?php if ($booking->lokasi_pengujian == 'dalam'): ?>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editSchedule"><i class="fa fa-calendar"></i> Ubah Rencana Pengantaran</a>
                <?php endif ?>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            
            <form id="booking" action="<?= base_url('booking/edit/' . $booking->id); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $booking->jenis_layanan ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor'?>">
                        
                    </div>
                </div>
                <div class="row">
                    <!--
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <input class="form-control" type="text" name="for_sertifikat" readonly
                            id="for_sertifikat" value="<?= $booking->for_sertifikat == 'sendiri' ? 'Sendiri' : 'Instansi Lain' ?>">
                    </div>
                        -->
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" readonly
                            value="<?= $booking->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" readonly><?= $booking->addr_sertifikat ?>, <?= $kota->nama ?>, <?= $provinsi->nama ?>
                        </textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <?php if ($booking->lokasi_pengujian == 'dalam'): ?>
                    <div class="col-3 form-group" >
                        <label>Rencana Pengantaran Alat</label>
                        <input class="form-control" type="text" name="est_arrival_date" readonly
                            id="est_arrival_date" 
                            value="<?= $booking->est_arrival_date ? date("d-m-Y", strtotime($booking->est_arrival_date)) : ''; ?>">
                    </div>
                    <?php else: ?>
                    <div class="col-3 form-group" >
                    <label>Usulan Jadwal</label>
                        <div class="input-group input-daterange" id="est_schedule_date">
                            <input type="text" class="form-control" id="est_schedule_date_from" autocomplete="off"
                                value="<?= $booking->est_schedule_date_from ? date("d-m-Y", strtotime($booking->est_schedule_date_from)) : ''; ?>"
                                name="est_schedule_date_from" readonly>
                                <span class="input-group-addon input-group-text p-l-10 p-r-10">s/d</span>
                            <input type="text" class="form-control" id="est_schedule_date_to" autocomplete="off"
                                value="<?= $booking->est_schedule_date_to ? date("d-m-Y", strtotime($booking->est_schedule_date_to)) : ''; ?>"
                                name="est_schedule_date_to" readonly>
                        </div>
                    </div>
                    <?php endif ?>
                    <!--
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                    -->
                </div>
            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Alat</div>
        </div>
        <div class="ibox-body">
            <?php foreach ($items as $item):?>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?= $item->tool_code ?></h4>
                    <div class="text-muted card-subtitle"><?= $item->type ?></div>

                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="text" class="form-control" name="type_id" id="type_id" 
                                value="<?= $item->type ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->tool_brand ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" 
                                value="<?= $item->tool_media ?>" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimum</label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Minimum</label>
                            <input type="text" class="form-control" name="tool_capacity_min" id="tool_capacity_min" 
                                value="<?= $item->tool_capacity_min ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                                value="<?= $item->tool_capacity_unit ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Negara Pembuat</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $item->tool_made_in ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Nama Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>
                    
                </div>
            </div>  
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="modal fade" id="editSchedule" tabindex="-1" aria-labelledby="editScheduleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="editScheduleLabel">Rencana Pengantaran Alat</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="resubmit" action="<?= base_url('booking/resubmit'); ?>/<?= $booking->id ?>" method="post">
        <?= csrf_field() ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-6 form-group" >
                    <?php if ($booking->lokasi_pengujian == 'dalam'): ?>
                    <label>Rencana Pengantaran Alat</label>
                    <div class="input-group date" id="est_arrival_date_grp">
                        <div class="input-group-prepend input-group-addon">
                            <span class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" id="est_arrival_date"
                            value="<?= $booking->est_arrival_date ? date("d-m-Y", strtotime($booking->est_arrival_date)) : ''; ?>"
                            name="est_arrival_date" autocomplete="off">
                    </div> 
                        <?php else: ?>
                    <label>Usulan Jadwal</label>
                    <div class="input-group input-daterange" id="est_schedule_date_grp">
                        <input type="text" class="form-control" id="est_schedule_date_from"
                            value="<?= $booking->est_schedule_date_from ? date("d-m-Y", strtotime($booking->est_schedule_date_from)) : ''; ?>"
                            name="est_schedule_date_from">
                            <span class="input-group-addon input-group-text p-l-10 p-r-10">s/d</span>
                        <input type="text" class="form-control" id="est_schedule_date_to"
                            value="<?= $booking->est_schedule_date_to ? date("d-m-Y", strtotime($booking->est_schedule_date_to)) : ''; ?>"
                            name="est_schedule_date_to">
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Simpan Perubahan</button>
        </div>
        </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        
    })
</script>
<?= $this->endSection() ?>