<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('booking');?>">Pendaftaran Layanan</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data Pendaftaran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Pendaftaran</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('booking');?>"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            
            <form id="booking" action="<?= base_url('booking/edit'); ?>/<?= $booking->id ?>" 
            enctype="multipart/form-data" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor Booking</label>
                        <input class="form-control" type="text" name="booking_no" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis Layanan</label>
                        <input class="form-control" type="text" name="jenis_layanan" readonly
                            id="jenis_layanan" value="<?= $booking->jenis_layanan ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <input class="form-control" type="text" name="lokasi_pengujian" readonly
                            id="lokasi_pengujian" 
                            value="<?= $booking->lokasi_pengujian == 'dalam' ? 'Dalam Kantor' : 'Luar Kantor' ?>">
                    </div>
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian Luar Kantor</label>
                        <input type="hidden" name="lokasi_dl" readonly
                            id="lokasi_dl" 
                            value="<?= $booking->lokasi_dl ?>">
                        <input class="form-control" type="text" name="lokasi_dl_lengkap" readonly
                            id="lokasi_dl_lengkap" 
                            value="<?= $booking->lokasi_dl == 'dalam' ? 'Dalam Negeri' : 'Luar Negeri' ?>">
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Lokasi Pengujian</label>
                        <select class="form-control" name="lokasi_pengujian" id="lokasi_pengujian">
                            <option value="dalam" <?= $booking->lokasi_pengujian == 'dalam' ? 'selected' : '' ?> >Dalam Kantor</option>
                            <option value="luar" <?= $booking->lokasi_pengujian == 'luar' ? 'selected' : '' ?>>Luar Kantor</option>  
                        </select>
                    </div>
                    -->
                </div>
                <div class="row">
                    <!--
                    <div class="col-3 form-group">
                        <label>Peruntukan Layanan</label>
                        <select class="form-control" name="for_sertifikat" id="for_sertifikat">
                            <option value="sendiri"
                                <?= $booking->for_sertifikat == 'sendiri' ? 'selected' : '' ?> >Sendiri</option>
                            <option value="lain"
                                <?= $booking->for_sertifikat == 'lain' ? 'selected' : '' ?> >Instansi lain</option>  
                        </select>
                    </div>
                    -->
                    
                    <div class="col-6 form-group">
                        <label>Pemilik Alat</label>
                        <input class="form-control" type="text" name="label_sertifikat" 
                            id= "label_Sertifikat" readonly
                            value="<?= $booking->label_sertifikat ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pemilik Alat</label>
                        <textarea class="form-control" name="addr_sertifikat" 
                        id="addr_sertifikat"
                        readonly ><?= $booking->addr_sertifikat ?>, <?= $kota->nama ?>, <?= $provinsi->nama ?></textarea>
                    </div>
                </div>
                <?php if ($booking->lokasi_pengujian == 'luar'): ?>
                    <div class="row" id="div-pj">
                    <div class="col-6 form-group">
                        <label>Nama Perusahaan Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_perusahaan" 
                            type="text" name="keuangan_perusahaan" value="<?=  $booking->keuangan_perusahaan ?>" />
                    </div>
                    <div class="col-6 form-group">
                        <label>Nama PIC Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_pic" 
                            type="text" name="keuangan_pic" value="<?=  $booking->keuangan_pic ?>" />
                    </div>
                </div>
                <div class="row" id="div-pj-2">
                    <div class="col-3 form-group">
                        <label>Jabatan PIC Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_jabatan" 
                            type="text" name="keuangan_jabatan" value="<?=  $booking->keuangan_jabatan ?>" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Nomor HP PIC Penanggungjawab Admisistrasi Keuangan</label>
                        <input class="form-control" id="keuangan_hp" 
                            type="text" name="keuangan_hp" value="<?=  $booking->keuangan_hp ?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label id="file_surat_permohonan">File Surat Permohonan<span class="required_notification">*</span></label>
                        <input class="form-control" id="file_surat_permohonan" 
                            type="file" name="file_surat_permohonan" accept="application/pdf" />
                        <?= $booking->file_surat_permohonan ?>
                    </div>
                    <div class="col-3 form-group">
                        <label id="no_surat_permohonan">No Surat Permohonan<span class="required_notification">*</span></label>
                        <input class="form-control" id="no_surat_permohonan" 
                            type="text" name="no_surat_permohonan" value="<?= $booking->no_surat_permohonan ?>"/>
                    </div>
                    <div class="col-3 form-group" >
                        <label id="tgl_surat_permohonan">Tanggal Surat Permohonan<span class="required_notification">*</span></label>
                        <div class="input-group date" id="tgl_surat_permohonan_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="tgl_surat_permohonan"
                                value="<?= $booking->tgl_surat_permohonan ? date("d-m-Y", strtotime($booking->tgl_surat_permohonan)) : ''; ?>"
                                name="tgl_surat_permohonan">
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-3 form-group">

                    </div>
                    <div class="col-3 form-group" >
          
                    </div>
                    <div class="col-3 form-group" >
          
                    </div>

                    <div class="col-3 form-group" >
                        <?php if(count($items) > 0): ?>
                            <?php if ($booking->lokasi_pengujian == 'dalam'): ?>
                        <label>Rencana Pengantaran Alat<span class="required_notification">*</span></label>
                        <div class="input-group date" id="est_arrival_date_grp">
                            <div class="input-group-prepend input-group-addon">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" id="est_arrival_date" autocomplete="off"
                                value="<?= $booking->est_arrival_date ? date("d-m-Y", strtotime($booking->est_arrival_date)) : ''; ?>"
                                name="est_arrival_date">
                        </div> 
                            <?php else: ?>
                        <label>Usulan Jadwal<span class="required_notification">*</span></label>
                        <div class="input-group input-daterange" id="est_schedule_date">
                            <input type="text" class="form-control" id="est_schedule_date_from" autocomplete="off"
                                value="<?= $booking->est_schedule_date_from ? date("d-m-Y", strtotime($booking->est_schedule_date_from)) : ''; ?>"
                                name="est_schedule_date_from">
                                <span class="input-group-addon input-group-text p-l-10 p-r-10">s/d</span>
                            <input type="text" class="form-control" id="est_schedule_date_to" autocomplete="off"
                                value="<?= $booking->est_schedule_date_to ? date("d-m-Y", strtotime($booking->est_schedule_date_to)) : ''; ?>"
                                name="est_schedule_date_to">
                        </div>
                            <?php endif ?>
                        <?php endif ?>
                    </div>
                    <!--
                    <div class="col-3 form-group">
                        <label>Perkiraan Total Biaya</label>
                        <input class="form-control" type="text" name="est_total_price" 
                            value="<?= number_format($booking->est_total_price, 2, ',', '.') ?>"readonly>
                    </div>
                    -->
                </div>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                <?php if(count($items) > 0): ?>
                    <?php if($booking->lokasi_pengujian == 'luar'): ?> 
                        <?php if($check_terms->kaji_ulang_insitu != null): ?>
                <button type="button" class="btn btn-success" id="book">
                    <i class="fa fa-calendar-check"></i> Booking
                </button>
                <a class="btn btn-warning" href="<?= base_url('bookingterm/create/' . $booking->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <?php else: ?>
                <a class="btn btn-danger" href="<?= base_url('bookingterm/create/' . $booking->id); ?>"><i class="fa fa-asterisk"></i> Kaji Ulang</a>
                        <?php endif ?>
                    <?php else: ?>
                <button type="button" class="btn btn-success" id="book">
                    <i class="fa fa-calendar-check"></i> Booking
                </button>
                    <?php endif ?>
                <?php endif ?>
            </form>
        </div>
    </div>

    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Alat (<?= count($items) ?> alat) </div>
            <div>
                <a class="btn btn-primary btn-sm" href="<?= base_url('bookingitem/create'); ?>/<?= $booking->id ?>"><i class="fa fa-plus"></i> Alat</a>
            </div>
        </div>
        <div class="ibox-body">
            <?php if ($booking->lokasi_pengujian == 'luar') : ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <span>Khusus Tangki Ukur Tetap Silinder Tegak, maksimal 5 unit untuk satu permohonan</span>
            </div>
            <?php endif ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <h5 class="alert-heading">Perhatian!</h5>
                <p>Mohon diperhatikan:<br/>
                    <ol type="1">
                        <li>Jangan salah / tertukar dalam memilih alat ukur berikut:
                            <ol type="a">
                                <li><strong>Meter Arus vs Sistem Meter Arus;</strong></li>
                                <li><strong>Meter Gas vs Sistem Meter Gas;</strong></li>
                                <li><strong>CTMS BBM vs CTMS LNG/LPG;</strong></li>
                            </ol>
                        </li>
                        <li>Meter Arus atau Meter Gas yang diuji bersamaan dengan perlengkapannya (di lokasi terpasang), didaftarkan sebagai <strong>Sistem Meter Arus/Sistem Meter Gas</strong>.</li>
                        <li>Meter Arus atau Meter Gas yang diuji mandiri (di lab atau instalasi), didaftarkan sebagai <strong>Meter Arus/Meter Gas</strong>.</li>
                    </ol>
                </p>
            </div>

            <?php if ($booking->lokasi_pengujian == 'dalam'): ?>
            <?php foreach ($items as $item):?>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?= $item->tool_code  ?></h4>
                    <div class="text-muted card-subtitle"><?= $item->type ?></div>

                   

                    <div class="row">
                       
                        <div class="col-6 form-group">
                            <label>Jenis</label>
                            <input type="text" class="form-control" name="type_id" id="type_id" 
                                value="<?= $item->type ?>" readonly/>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Merk</label>
                            <input type="text" class="form-control" name="tool_brand" id="tool_model"
                                value="<?= $item->tool_brand ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Model/Tipe</label>
                            <input type="text" class="form-control" name="tool_model" id="tool_model" 
                                value="<?= $item->tool_model ?>" readonly/>
                        </div>  
                        <div class="col-3 form-group">
                            <label>Nomor Seri</label>
                            <input type="text" class="form-control" name="serial_no" id="serial_no" 
                                value="<?= $item->serial_no ?>" readonly />
                        </div>
                        <div class="col-3 form-group">
                            <label>Media Uji/Komoditas</label>
                            <input type="text" class="form-control" name="tool_media" id="tool_media" 
                                value="<?= $item->tool_media ?>" readonly />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label>Kapasitas Maksimum</label>
                            <input type="text" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $item->tool_capacity ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Kapasitas Minimum</label>
                            <input type="text" class="form-control" name="tool_capacity_min" id="tool_capacity_min" 
                                value="<?= $item->tool_capacity_min ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Satuan Kapasitas</label>
                            <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" 
                                value="<?= $item->tool_capacity_unit ?>" readonly/>
                        </div>
                        <div class="col-3 form-group">
                            <label>Negara Pembuat</label>
                            <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                                value="<?= $item->tool_made_in ?>" readonly/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Nama Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" id="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" id="tool_factory_address" readonly><?= $item->tool_factory_address ?>
                            </textarea>
                        </div>
                    </div>
                    <?php if(($booking->lokasi_pengujian == 'luar')): ?>
                    <?php $city = current(array_filter($cities, function($cityItem) use ($item) {
                            return $item->location_kabkot_id == $cityItem->id;
                        })); ?>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Lokasi Pengujian</label>
                            <textarea class="form-control" type="text" name="location" readonly><?= $item->location ?>, <?= $city->nama ?>, <?= $city->provinsi ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Koordinat Lokasi Alat</label>
                            <input class="form-control" type="text" name="tool_factory" id="tool_factory" 
                                value="<?= $item->location_lat ?>, <?= $item->location_long ?>" readonly/>
                        </div>
                    </div>
                    <?php else: ?>
                        <?php if($booking->service_type_id == 4 || $booking->service_type_id == 5): ?>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Lokasi Alat</label>
                            <textarea class="form-control" name="location" id="location" readonly><?= $item->location ?></textarea>
                        </div>
                    </div>
                        <?php endif ?>
                    <?php endif ?>
                    <!--
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Pabrikan</label>
                            <input class="form-control" type="text" name="tool_factory" 
                                value="<?= $item->tool_factory ?>" readonly/>
                        </div>
                        <div class="col-6 form-group">
                            <label>Alamat Pabrikan</label>
                            <textarea class="form-control" name="tool_factory_address" readonly><?= $item->tool_factory_address ?></textarea>
                        </div>
                    </div>
                    -->
                    
                    <!--
                    <table class="table table-striped table-bordered table-hover" id="inspections" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Jenis Pengujian</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Harga Satuan</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $inspections_filtered = array_filter($inspections, function($arritem) use ($item) {
                                return $arritem->booking_item_id == $item->id;
                            }); ?>
                            <?php foreach ($inspections_filtered as $inspection):?>
                            <tr>
                                <td><?= $inspection->inspection_type ?></td>
                                <td><?= $inspection->quantity ?></td>
                                <td><?= $inspection->unit ?></td>
                                <td><?= number_format($inspection->price, 2, ',', '.') ?></td>
                                <td><?= number_format($inspection->quantity * $inspection->price, 2, ',', '.') ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Subtotal</th>
                                <th><?= number_format($item->est_subtotal, 2, ',', '.') ?></td>
                            </tr>
                        </tfoot>
                    </table>
                    -->
                    
                    <?php if(($booking->service_type_id == 4 || $booking->service_type_id == 5) && ($booking->lokasi_pengujian == 'luar')): ?>
                    <!--
                    <h6>Perlengkapan:</h6>
                    <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Merek</th>
                                <th>Model/Tipe</th>
                                <th>Nomor Seri</th>
                                <th>Media Uji/Komoditas</th>
                                <th>Buatan</th>
                                <th>Kapasitas Maksimal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $ttuPerlengkapans_filtered = array_filter($ttuPerlengkapans, function($arritem) use ($item) {
                                return $arritem->booking_item_id == $item->id;
                            }); ?>
                            <?php foreach ($ttuPerlengkapans_filtered as $perlengkapan):?>
                            <tr>
                                <td><?= $perlengkapan->tool_name ?></td>
                                <td><?= $perlengkapan->tool_brand ?></td>
                                <td><?= $perlengkapan->tool_model ?></td>
                                <td><?= $perlengkapan->serial_no ?></td>
                                <td><?= $perlengkapan->tool_media ?></td>
                                <td><?= $perlengkapan->tool_made_in ?></td>
                                <td><?= $perlengkapan->tool_capacity . ' ' . $perlengkapan->tool_capacity_unit ?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                    -->
                    <?php endif ?>
                    
                    <div class="row">
                        <div class="col">
                            
                            <form action="<?= base_url('bookingitem/deleteuttp').'/'.$item->id; ?>" method="post">
                            <?= csrf_field() ?> 
                            <!--
                            <a class="btn btn-default btn-sm" href="<?=base_url('bookingitem/edit/' .$item->booking_id . '/' . $item->id); ?>">
                                <i class="fa fa-edit"></i> Edit
                            </a>
                            <a class="btn btn-default btn-sm" href="<?=base_url('bookingitem/copy/' .$item->booking_id . '/' . $item->id); ?>">
                                <i class="fa fa-copy"></i> Salin
                            </a>
                            -->
                            <button type="submit" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i> Hapus</button>
                            </form>
                           
                        </div>
                    </div>
                </div>
            </div>  
            <?php endforeach; ?>

            <?php else: ?>
            <table class="table table-striped table-bordered table-hover" id="booking-items" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Jenis</th>
                        <th>Merek</th>
                        <th>Model/tipe</th>
                        <th>Nomor Seri</th>
                        <th>Media Uji/Komoditas</th>
                        <th>Kapasitas Min</th>
                        <th>Kapasitas Maks</th>
                        <th>Negara Pembuat</th>
                        <th>Nama Pabrikan</th>
                        <th>Alamat Pabrikan</th>
                        <th>Lokasi Pengujian</th>
                        <th>Koordinat (Lat, Long)</th>
                        <th>Perlengkapan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $item):?>
                    <tr>
                    <td><?= $item->type ?></td>
                    <td><?= $item->tool_brand ?></td>
                    <td><?= $item->tool_model ?></td>
                    <td><?= $item->serial_no ?></td>
                    <td><?= $item->tool_media ?></td>
                    <td><?= $item->tool_capacity_min ?> <?= $item->tool_capacity_unit ?></td>
                    <td><?= $item->tool_capacity ?> <?= $item->tool_capacity_unit ?></td>
                    <td><?= $item->tool_made_in ?></td>
                    <td><?= $item->tool_factory ?></td>
                    <td><?= $item->tool_factory_address ?></td>
                    <td>
                        <?php if ($booking->lokasi_dl == 'dalam'): ?>
                        <?= $item->location ?>, <?= $item->kabkot ?>, <?= $item->provinsi ?>
                        <?php else: ?>
                        <?= $item->location ?>, <?= $item->negara ?>
                        <?php endif; ?>
                    </td>
                    <td><?= $item->location_lat ?>, <?= $item->location_long ?></td>
                    <td>
                        <?php
                            $tools = array_filter($ttuPerlengkapans, function($obj) use ($item){
                                return $obj->booking_item_id == $item->id;
                            });
                            $tools = array_map(function($obj) {
                                return $obj->serial_no . '/' . $obj->tool_brand . '/' . $obj->tool_model . ' (' . $obj->uttp_type . ')';
                            }, $tools);
                            
                            echo implode("<br/>", $tools);
                        ?>
                    </td>
                    <td>
                        <form action="<?= base_url('bookingitem/deleteuttp').'/'.$item->id; ?>" method="post">
                        <?= csrf_field() ?> 
                        <button type="submit" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i> Hapus</button>
                        </form>
                        <a class="btn btn-default btn-sm" href="<?= base_url('bookingitemtool/create').'/'.$booking->id.'/'.$item->id;?>">
                            <i class="fa fa-toolbox"></i> Perlengkapan
                        </a>
                    </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif ?>
            
        </div>
    </div>
</div>

<div class="modal fade" id="perlengkapan" tabindex="-1" aria-labelledby="editScheduleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="editScheduleLabel">Perlengkapan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>
 
<script type="text/javascript">
    $(function() {
        $('#service_type_id, #for_sertifikat, #uttp_owner_id, #inspection_prov_id, #inspection_kabkot_id').select2({
            theme: 'bootstrap4',
        });
        $('#est_arrival_date_grp').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date(),
            daysOfWeekDisabled: '0,6',
            datesDisabled: [
                <?php foreach ($holidays as $holiday):?>
                '<?= date("d-m-Y", strtotime($holiday->holiday_date)) ?>',
                <?php endforeach;?>
                <?php foreach ($events as $event):?>
                '<?= date("d-m-Y", strtotime($event->closed_date)) ?>',
                <?php endforeach;?>
                <?php foreach ($quotas as $quota):?>
                '<?= date("d-m-Y", strtotime($quota->quota_date)) ?>',
                <?php endforeach;?>
            ],
            beforeShowDay: function (date) {
                if (date.getDay() == 0 || date.getDay() == 6) {
                    return 'bg-danger text-white';
                }
                var holidays = [
                    <?php foreach ($holidays as $holiday):?>
                    new Date('<?= date("Y-m-d", strtotime($holiday->holiday_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), holidays) > -1) {
                    return 'bg-danger text-white';
                }
                var events = [
                    <?php foreach ($events as $event):?>
                    new Date('<?= date("Y-m-d", strtotime($event->closed_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), events) > -1) {
                    return 'bg-warning text-white';
                }
                var quotas = [
                    <?php foreach ($quotas as $quota):?>
                    new Date('<?= date("Y-m-d", strtotime($quota->quota_date)) ?>').setHours(0),
                    <?php endforeach;?>
                ];
                if ($.inArray(date.getTime(), quotas) > -1) {
                    return 'bg-grey text-white';
                }
            },
        });

        $('#est_schedule_date').datepicker({
            format: 'dd-mm-yyyy',
            startDate: new Date(),
        });
        $('#tgl_surat_permohonan_grp').datepicker({
            format: 'dd-mm-yyyy',
        });

        var label = $("#label_sertifikat").val;
        var addr = $("addr_sertifikat").val;
        $('#for_sertifikat').on('select2:select', function (e) {
            var data = e.params.data;
            if (data.id == 'lain') {
                $('#label_sertifikat').attr('readonly', false);
                $('#addr_sertifikat').attr('readonly', false);
            } else {
                $('#label_sertifikat').attr('readonly', true);
                $('#addr_sertifikat').attr('readonly', true);
                $('#label_sertifikat').val(label);
                $('#addr_sertifikat').val(addr);
            }
        });

        $('#items').DataTable({
            pageLength: 10,
        });

        $('#book').click(function(e) {
            e.preventDefault()
            var form = $('#booking');
            form.attr('action', '<?= base_url('booking/submit'); ?>/<?= $booking->id ?>')
            form.submit();
        });

        $("#booking").validate({
            rules: {
                
                est_arrival_date: {
                    required: function(element) {
                        return ($("#lokasi_pengujian").val() == 'Dalam Kantor');
                    },
                },
                est_schedule_date_from: {
                    required: function(element) {
                        return ($("#lokasi_pengujian").val() == 'Luar Kantor');
                    },
                },
                est_schedule_date_to: {
                    required: function(element) {
                        return ($("#lokasi_pengujian").val() == 'Luar Kantor');
                    },
                },
                no_surat_permohonan: {
                    required: function(element) {
                        return ($("#lokasi_pengujian").val() == 'Luar Kantor');
                    },
                },
                tgl_surat_permohonan: {
                    required: function(element) {
                        return ($("#lokasi_pengujian").val() == 'Luar Kantor');
                    },
                },
                <?php if ($booking->file_surat_permohonan == null): ?>
                file_surat_permohonan: {
                    required: function(element) {
                        return ($("#lokasi_pengujian").val() == 'Luar Kantor');
                    },
                },
                <?php endif; ?>
            },
            messages: {
                est_arrival_date: 'Rencana pengantaran wajib diisi',
                est_schedule_date_from: 'Usulan jadwal wajib diisi',
                est_schedule_date_to: 'Usulan jadwal wajib diisi',
                no_surat_permohonan: 'Nomor surat permohonan wajib diisi',
                tgl_surat_permohonan: 'Tanggal surat permohonan wajib diisi',
                file_surat_permohonan: 'Surat permohonan wajib di-upload',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

        $('#booking-items').DataTable({
            pageLength: 10,
            scrollX: true,
        });
    })
</script>
<?= $this->endSection() ?>