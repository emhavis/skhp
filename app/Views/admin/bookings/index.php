<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Pendaftaran Layanan</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Booking</div>
            <div>
                <a class="btn btn-primary btn-sm" href="<?= base_url('booking/create'); ?>"><i class="fa fa-plus"></i> Booking Baru</a>
            </div>
        </div>
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="bookings" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nomor Booking</th>
                        <th>Jenis Layanan</th>
                        <th>Jadwal</th>
                        <!--
                        <th>Estimasi Total Biaya</th>
                        -->
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($bookings as $booking):?>
                    <tr>
                        <td><?= $booking->booking_no ?></td>
                        <td><?= $booking->jenis_layanan ?></td>
                        <td><?= $booking->lokasi_pengujian == 'dalam' ?
                            ($booking->est_arrival_date ? date("d-m-Y", strtotime($booking->est_arrival_date)) : '') :
                            ($booking->est_schedule_date_from ? 
                                date("d-m-Y", strtotime($booking->est_schedule_date_from)) . ' s/d ' . date("d-m-Y", strtotime($booking->est_schedule_date_to)): '') ?></td>
                        <!-- 
                        <td><?= number_format($booking->est_total_price, 2, ',', '.')  ?></td>
                        -->
                        <td>
                            <?php if($booking->booking_no == null): ?>
                            <a class="btn btn-default btn-sm" href="<?= base_url('booking/edit/' . $booking->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('booking/delete/' . $booking->id); ?>"><i class="fa fa-trash"></i> Hapus</a>
                            <?php else: ?>
                            <a class="btn btn-default btn-sm" href="<?= base_url('booking/read/' . $booking->id); ?>"><i class="fa fa-book"></i> Lihat</a>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#bookings').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>