<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">UTTP</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('uut'); ?>">UUT</a>
        </li>
        <li class="breadcrumb-item">Perubahan Data UUT</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Perubahan Data UTTP</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('uut'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                
            </div>
        </div>
        <div class="ibox-body" >
       
            <form id="uttp" action="<?= base_url('uut/edit/' . $uttp->id); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik<span class="required_notification">*</span></label>
                        <select class="form-control" name="owner_id" id="owner_id">
                            <?php foreach ($owners as $owner): ?>
                            <option value="<?= $owner->id ?>" <?= $uttp->owner_id == $owner->id ? 'selected' : '' ?> ><?= $owner->nama ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label>Jenis<span class="required_notification">*</span></label>
                        <select class="form-control" name="type_id" id="type_id">
                            <?php foreach ($uttpTypes as $type): ?>
                            <option value="<?= $type->id ?>" <?= $uttp->type_id == $type->id ? 'selected' : '' ?> ><?= $type->uttp_type ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Nama Alat</label>
                        <input type="text" class="form-control" name="tool_name" id="tool_name" 
                            value="<?= $uttp->tool_name ?>" <?= $uttp->type_id != 49 ? 'readonly' : '' ?> />
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Merk<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" 
                            value="<?= $uttp->tool_brand ?>"/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Model/Tipe<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" 
                            value="<?= $uttp->tool_model ?>"/>
                    </div>  
                    <!--
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" 
                            value="<?= $uttp->tool_type ?>"/>
                    </div>
                    -->
                    <div class="col-3 form-group">
                        <label>Nomor Seri<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" 
                            value="<?= $uttp->serial_no ?>"/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" 
                            value="<?= $uttp->tool_media ?>"/>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimal<span class="required_notification">*</span></label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" 
                                value="<?= $uttp->tool_capacity ?>"/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Kapasitas Minimal</label>
                        <input type="number" class="form-control" name="tool_capacity_min" id="tool_capacity_min" 
                                value="<?= $uttp->tool_capacity_min ?>"/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Satuan<span class="required_notification">*</span></label>
                        <select class="form-control" name="tool_capacity_unit" id="tool_capacity_unit">
                            <option value="<?= $uttp->tool_capacity_unit ?>" selected><?= $uttp->tool_capacity_unit ?></option>
                        </select>
                    </div>
                    <div class="col-3 form-group">
                        <label>Buatan</label>
                        <!--
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" 
                            value="<?= $uttp->tool_made_in ?>"/>
                        -->
                        <select class="form-control" name="tool_made_in_id" id="tool_made_in_id">
                            <option value="">-- Pilih Negara --</option>
                            <?php foreach ($negaras as $negara): ?>
                            <option value="<?= $negara->id ?>" <?= $uttp->tool_made_in_id == $negara->id ? 'selected' : '' ?> ><?= $negara->nama_negara ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pabrikan<span class="required_notification">*</span></label>
                        <input class="form-control" type="text" name="tool_factory" 
                            value="<?= $uttp->tool_factory ?>"/>
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address"><?= $uttp->tool_factory_address ?></textarea>
                    </div> 
                </div>
                

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#owner_id, #tool_made_in_id').select2({
            theme: 'bootstrap4',
        });
        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#tool_name').val(e.params.data.text);
            if (e.params.data.id == 49) {
                $("#tool_name").attr("readonly", false); 
            } else {
                $("#tool_name").attr("readonly", true); 
            }
        });

        $('#tool_capacity_unit').select2({
            theme: 'bootstrap4',
            tags: true,
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uttp/unit/'+type;
                },
                processResults: function (data) {
                    /*
                    data.push(
                        {id: "0", uttp_type_id: "0", unit: "lainnya"}
                    );
                    */
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.id, text: obj.unit };
                        })
                    };
                },
            }
        });

        $("#uttp").validate({
            rules: {
                tool_brand: {
                    required: !0
                },
                tool_model: {
                    required: !0
                },
                serial_no: {
                    required: !0
                },
                tool_capacity: {
                    required: !0
                },
                tool_capacity_unit: {
                    required: !0
                },
                tool_factory: {
                    required: !0
                },
            },
            messages: {
                tool_brand: 'Merek wajib diisi',
                tool_model: 'Model/tipe wajib diisi',
                serial_no: 'No seri wajib diisi',
                tool_capacity: 'Kapasitas maksimum wajib diisi',
                tool_capacity_unit: 'Satuan wajib diisi',
                tool_factory: 'Pabrikan wajib diisi',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
    })
</script>
<?= $this->endSection() ?>