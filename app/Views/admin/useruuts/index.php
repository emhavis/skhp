<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Standar Ukuran</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">Standar Ukuran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar Standar Ukuran</div>
            <div>
                <a class="btn btn-primary btn-sm" href="<?= base_url('useruut/create'); ?>"><i class="fa fa-plus"></i> Data Baru</a>
            </div>
        </div>
        <div class="ibox-body">
            <div id="table">
            <table class="table table-striped table-bordered table-hover" id="labs" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pemilik</th>
                        <th>Jenis</th>
                        <th>Merek/Model/Tipe/Serial No</th>
                        <th>Kapasitas</th>
                        <th>Buatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($uttps as $uttp):?>
                    <tr>
                        <td><?= $uttp->owner ?></td>
                        <td><?= $uttp->uut_type ?></td>
                        <td><?= $uttp->tool_brand .'/'. $uttp->tool_model .'/'. $uttp->serial_no  ?></td>
                        <td><?= $uttp->tool_capacity ?> <?= $uttp->tool_capacity_unit ?></td>
                        <td><?= $uttp->tool_made_in ?></td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('useruut/delete/' . $uttp->map_id); ?>"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

            <div id='calendar'></div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#labs').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>