<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Standar Ukuran</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('useruut'); ?>">Standar Ukuran</a>
        </li>
        <li class="breadcrumb-item">Penambahan Data Standar Ukuran</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Penambahan Data Standar Ukuran</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('uut'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                
            </div>
        </div>
        <div class="ibox-body" >
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            <form id="uttp" action="<?= base_url('useruut/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik<span class="required_notification">*</span></label>
                        <select class="form-control" name="owner_id" id="owner_id">
                            <?php foreach ($owners as $owner): ?>
                            <option value="<?= $owner->id ?>"><?= $owner->nama ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label>Jenis<span class="required_notification">*</span></label>
                        <select class="form-control" name="type_id" id="type_id">
                            <?php foreach ($uttpTypes as $type): ?>
                            <option value="<?= $type->id ?>"><?= $type->uut_type ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pencarian UUT Berdasar Nomor Seri</label>
                        <input type="hidden" name="uut_id" id="uut_id">
                        <div class="input-group">
                            <input class="form-control" type="text" name="serial_no_find" id="serial_no_find">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="find_uut">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-3 form-group">
                        <label>Merk<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Model/Tipe<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" readonly/>
                    </div>  
                    <!--
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" />
                    </div>
                    -->
                    <div class="col-3 form-group">
                        <label>Nomor Seri<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kapasitas<span class="required_notification">*</span></label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" readonly />
                    </div>

                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Daya Baca</label>           
                        <input type="text" class="form-control" name="tool_dayabaca" id="tool_dayabaca" readonly/>
                    </div>

                    <div class="col-3 form-group">
                        <label>Satuan<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" readonly/>
                    </div>
                    
                    <div class="col-3 form-group">
                        <label>Kelas</label>
                        <input type="text" class="form-control" name="class" id="class" readonly/>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-3 form-group">
                        <label><span class="required_notification">*</span> Jumlah</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Buatan</label>
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" readonly/>
                    </div>
                    <div class="col-6 form-group">
                        <label>Pabrikan<span class="required_notification">*</span></label>
                        <input class="form-control" type="text" ID="tool_factory" name="tool_factory"readonly />
                    </div>
                </div>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="notFound" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data tidak ditemukan. Pastikan kembali nomor seri Alat sudah sesuai atau belum.</p>
                <p>Klik tombol Tambah Baru untuk menambahkan data baru.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" class="btn btn-primary" id="new_uut">Tambah baru</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#owner_id, #tool_made_in_id').select2({
            theme: 'bootstrap4',
        });
        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#serial_no').val(null);
            $('#tool_brand').val(null);
            $('#tool_model').val(null);
            $('#tool_capacity').val(null);
            $('#tool_capacity_min').val(null);
            $('#tool_dayabaca').val(null);
            $('#tool_dayabaca_unit').val(null);
            $('#tool_made_in').val(null);
            $('#tool_factory').val(null);
            $('#jumlah').val(null);
            $('#class').val(null);
            $('#tool_factory_address').val(null);
            $('#tool_media').val(null);
        });

        /*
        $("#uttp").validate({
            rules: {
                tool_brand: {
                    required: !0
                },
                tool_model: {
                    required: !0
                },
                serial_no: {
                    required: !0
                },
                tool_capacity: {
                    required: !0
                },
                tool_capacity_unit: {
                    required: !0
                },
                tool_factory: {
                    required: !0
                },
            },
            messages: {
                tool_brand: 'Merek wajib diisi',
                tool_model: 'Model/tipe wajib diisi',
                serial_no: 'No seri wajib diisi',
                tool_capacity: 'Kapasitas maksimum wajib diisi',
                tool_capacity_unit: 'Satuan wajib diisi',
                tool_factory: 'Pabrikan wajib diisi',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });
        */

        $('#find_uut').click(function(e) {
            e.preventDefault();

            var uttp_type = $('#type_id').val();
            var q = $('#serial_no_find').val();
            var owner_id = $('#owner_id').val();

            url = '<?= base_url(); ?>/uut/findBySeries';

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    type_id: uttp_type,
                    owner_id: owner_id,
                    q: q,
                    <?= csrf_token() ?>: '<?= csrf_hash() ?>',
                },
                success: function (data) {
                    if (data) {
                        $('#uut_id').val(data.id);
                        $('#serial_no').val(data.serial_no);
                        $('#tool_brand').val(data.tool_brand);
                        $('#tool_model').val(data.tool_model);
                        $('#tool_capacity').val(data.tool_capacity);
                        $('#tool_capacity_min').val(data.tool_capacity_min);
                        $('#tool_capacity_unit').val(data.tool_capacity_unit);
                        $('#tool_made_in').val(data.tool_made_in);
                        $('#tool_factory').val(data.tool_factory);
                        $('#tool_factory_address').val(data.tool_factory_address);
                        $('#tool_media').val(data.tool_media);
                        $('#jumlah').val(data.jumlah);
                        $('#class').val(data.class);
                        $('#tool_dayabaca').val(data.tool_dayabaca);
                        $('#tool_dayabaca_unit').val(data.tool_dayabaca_unit);
                    } else {
                        $('#notFound').modal('show');
                    }
                },
                error: function (data) {
                    $('#notFound').modal('show');
                }
            });
        });
        $('#new_uut').click(function(e) {
            e.preventDefault();

            var owner_id = $('#owner_id').val();
            var type_id = $('#type_id').val();
            window.location.href = "<?= base_url('useruut/createNew') ?>/" + owner_id + "/" + type_id ;
        })
    })
</script>
<?= $this->endSection() ?>