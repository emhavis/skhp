<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">Pendaftaran Layanan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('useruut'); ?>">UUT</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?=base_url('useruut/create');?>">Penambahan Data UUT</a>
        </li>
        <li class="breadcrumb-item">Data UUT Baru</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Penambahan Data UUT</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?=base_url('useruut/create');?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                
            </div>
        </div>
        <div class="ibox-body" >
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            <form id="uttp" action="<?= base_url('useruut/createNew/' .$owner->id. '/' .$uttpType->id); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="uttp_owner" id="uttp_owner" value="<?= $owner->nama ?>" readonly />
                        <input type="hidden" class="form-control" name="owner_id" id="owner_id" value="<?= $owner->id ?>"  />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Jenis<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="uttp_type" id="uttp_type" value="<?= $uttpType->uut_type ?>" readonly />
                        <input type="hidden" class="form-control" name="type_id" id="type_id" value="<?= $uttpType->id ?>"  />
                    </div>
                    <div class="col-6 form-group">
                        <label>Nama Alat <span class="required_notification"> * </span></label>
                        <input type="text" class="form-control" name="tool_name" id="tool_name" value=""/>
                    </div>

                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Merk<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Model/Tipe<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" />
                    </div>  
                    <div class="col-3 form-group">
                        <label>Nomor Seri<span class="required_notification">*</span></label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Kelas (Spesifikasi Pabrikan)</label>
                        <input type="text" class="form-control" name="class" id="class" />
                    </div>
                </div>
                <?php if(in_array($uttpType->id,t_excepts)): ?>
                    <div class="row" id="thermo">
                        <div class="col-6 form-group">
                            <label>Kapasitas & satuan (1-10 kg<span style="font-size:14px; font-weight:bold;">;</span>12-20°C) jika lebih dari 1 satuan dipisahkan tanda <span style="font-size:20px; font-weight:bold;">";" </span>(titik koma)<span class="required_notification">* </span></label>
                            <input type="text" class="form-control" name="capacity_opt" id="tool_capacity" />
                        </div>
                        <div class="col-6 form-group">
                            <label>Daya baca  & satuan (1-10 kg;12-20°C) jika lebih dari 1 satuan dipisahkan tanda <span style="font-size:20px; font-weight:bold;">";" </span>(titik koma)<span class="required_notification">* </span></label>
                            <input class="form-control" type="text" name="dayabaca_opt" />
                        </div>
                    </div>
                <?php endif ?>
                <?php if(!in_array($uttpType->id,t_excepts)): ?>
                </br>
                <div class="row">
                    <div class="col-3 form-group">
                        <input type="checkbox" class="custom-form-control" name="setbox" id="setbox"/>
                        <label>Satuan Set / Range</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group gmin" id="gmin" style="pointer-events: none;opacity: 0.4;">
                        <label>Kapasitas Min.</label>
                        <div class="input-group" disabled="true">
                            <input type="text" class="form-control" name="tool_capacity_min" id="tool_capacity_min" />
                            <select class="form-control" name="tool_capacity_min_unit" id="tool_capacity_min_unit">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-3 form-group">
                        <label>Kapasitas <span id="max">Max.</span><span class="required_notification">*</span></label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" />
                    </div>
                    <div class="col-6 form-group">
                        <label>Satuan<span class="required_notification">*</span></label>
                        <div class="input-group">
                            <select class="form-control" name="tool_capacity_unit" id="tool_capacity_unit">
                                <option value=""></option>
                            </select>
                            <input type="text" class="form-control" name="tool_capacity_unit_1" id="tool_capacity_unit_1" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Daya Baca / Out Put</label>
                        <input type="text" class="form-control" name="tool_dayabaca" id="tool_dayabaca" />
                    </div>
                    <div class="col-6 form-group">
                        <label>Satuan<span class="required_notification"></span></label>
                        <div class="input-group">
                            <select class="form-control" name="tool_dayabaca_unit" id="tool_dayabaca_unit">
                                <option value=""></option>
                            </select>
                            <input type="text" class="form-control" name="tool_dayabaca_unit_1" id="tool_capacity_unit_1" />
                        </div>
                    </div>
                </div>
                <?php endif ?>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" />
                    </div>
                    <div class="col-6 form-group">
                        <label><span class="required_notification">*</span> Jumlah</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Buatan<span class="required_notification">*</span></label>
                        <!--
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" />
                        -->
                        <select class="form-control" name="tool_made_in_id" id="tool_made_in_id">
                            <?php foreach ($negaras as $negara): ?>
                            <option value="<?= $negara->id ?>"><?= $negara->nama_negara ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label>Pabrikan<span class="required_notification"></span></label>
                        <input class="form-control" type="text" name="tool_factory" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-group">
                        <label>Alamat Pabrikan<span class="required_notification"></span></label>
                        <textarea class="form-control" name="tool_factory_address"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3 form-group">
                        <label>Koordinat Lokasi Alat (Latitude)</label>
                        <input class="form-control" type="text" name="location_lat" id="location_lat" />
                    </div>
                    <div class="col-3 form-group">
                        <label>Koordinat Lokasi Alat (Longitude)</label>
                        <input class="form-control" type="text" name="location_long" id="location_long" />
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        var tool_d = $('#tool_dayabaca');
        var tool_du = $('#tool_dayabaca_unit');
        $("#max").css("opacity",0);
        // var gm = document.getElementsByClassName("gmin");
        $('#tool_dayabaca').focusout(function(){
           if(tool_d.val() !=null){
                $('#tool_dayabaca_unit').select2({
                    disabled : false,
                theme: 'bootstrap4',
                tags: true,
                ajax: {
                    url: function (params) {
                        var type = $('#type_id').val();
                        return '<?= base_url(); ?>/uut/unit/'+type;
                    },
                    processResults: function (data) {
                        
                        return {
                            results: $.map(data, function(obj, index) {
                                return { id: obj.unit, text: obj.unit };
                            })
                        };
                    },
                    
                }
            });
           }
           if(tool_d.val().length < 1 ){
            $('#tool_dayabaca_unit').select2().attr('readonly','readonly');
           }
        });

        $('#tool_made_in_id').select2({
            theme: 'bootstrap4',
        });
        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        

        $('#tool_capacity_unit').select2({
            theme: 'bootstrap4',
            tags: true,
            ajax: {
                url: function (params) {
                    var type = $('#type_id').val();
                    return '<?= base_url(); ?>/uut/unit/'+type;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function(obj, index) {
                            return { id: obj.unit, text: obj.unit };
                        })
                    };
                },
                
            }
        });

        $("#uttp").validate({
            rules: {
                tool_brand: {
                    required: !0
                },
                tool_model: {
                    required: !0
                },
                serial_no: {
                    required: !0
                },
                tool_capacity: {
                    required: !0,
                },
                tool_name:{
                    required: !0,
                },
                // tool_capacity_min: {
                //     max: function(element) {
                //         return ($("#tool_capacity").val());
                //     },
                // },
                tool_capacity_unit: {
                    required: !0
                },
                // tool_factory: {
                //     required: !0
                // },
                // tool_factory_address: {
                //     required: !0
                // },
                tool_made_in_id: {
                    required: !0
                }
            },
            messages: {
                tool_brand: 'Merek wajib diisi',
                tool_model: 'Model/tipe wajib diisi',
                serial_no: 'No seri wajib diisi',
                tool_capacity: {
                    required: 'Kapasitas maksimum wajib diisi',
                    min: 'Kapasitas maksimum harus lebih besar dari kapasitas minimum',
                },
                // tool_capacity_min: 'Kapasitas minimum harus lebih kecil dari kapasitas maksimum',
                tool_capacity_unit: 'Satuan wajib diisi',
                tool_name : 'Nama alat wajib diisi',
                // tool_factory: 'Pabrikan wajib diisi',
                // tool_factory_address: 'Alamat pabrikan wajib diisi',
                tool_made_in_id: 'Negara pembuat wajib dipilih',
            },
            errorClass: "help-block error",
            highlight: function(e) {
                $(e).closest(".form-group.row").addClass("has-error")
            },
            unhighlight: function(e) {
                $(e).closest(".form-group.row").removeClass("has-error")
            },
        });

        $("#setbox").change(function(){
            if(this.checked){
                $('#gmin').css("pointer-events","auto");
                $('#gmin').css("opacity","1");
                $("#max").css("opacity",1);
                if(tool_d.val() !=null){
                    $('#tool_capacity_min_unit').select2({
                    disabled : false,
                    theme: 'bootstrap4',
                    tags: true,
                    ajax: {
                        url: function (params) {
                            var type = $('#type_id').val();
                            return '<?= base_url(); ?>/uut/unit/'+type;
                        },
                        processResults: function (data) {
                            /*
                            data.push(
                                {id: "0", uttp_type_id: "0", unit: "lainnya"}
                            );
                            */
                            return {
                                results: $.map(data, function(obj, index) {
                                    return { id: obj.unit, text: obj.unit };
                                })
                            };
                        },
                        
                    }
                    });
                }
            }else {
                $('#gmin').css("pointer-events","none");
                $('#gmin').css("opacity","0.4");
                $("#max").css("opacity",0);
            }
        })
    })
</script>
<?= $this->endSection() ?>