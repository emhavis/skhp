<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/select2/css/select2.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/select2-bootstrap4/select2-bootstrap4.min.css" rel="stylesheet" />
<link href="<?=base_url();?>/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<style>
    .required_notification {
	color:#d45252; 
	margin:px 0 0 0; 
	display:inline;
	float:left;
}
</style>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">UTTP</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="<?= base_url('useruttp'); ?>">UTTP</a>
        </li>
        <li class="breadcrumb-item">Penambahan Data UTTP</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Penambahan Data UTTP</div>
            <div>
                <a class="btn btn-default btn-sm" href="<?= base_url('useruttp'); ?>"><i class="fa fa-chevron-left"></i> Kembali</a>
                
            </div>
        </div>
        <div class="ibox-body" >
            <?php if (session()->getFlashData('errors')) : ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <ul>
                        <?php foreach (session()->getFlashData('errors') as $err):?>
                        <li ><?= $err ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            <form id="uttp" action="<?= base_url('useruttp/create'); ?>" method="post">
                <?= csrf_field() ?>

                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pemilik</label>
                        <select class="form-control" name="owner_id" id="owner_id">
                            <?php foreach ($owners as $owner): ?>
                            <option value="<?= $owner->id ?>"><?= $owner->nama ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-6 form-group">
                        <label>Jenis</label>
                        <select class="form-control" name="type_id" id="type_id">
                            <?php foreach ($uttpTypes as $type): ?>
                            <option value="<?= $type->id ?>"><?= $type->uttp_type ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pencarian UTTP Berdasar Nomor Seri</label>
                        <input type="hidden" name="uttp_id" id="uttp_id">
                        <div class="input-group">
                            <input class="form-control" type="text" name="serial_no_find" id="serial_no_find">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="find_uttp">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-3 form-group">
                        <label>Merk</label>
                        <input type="text" class="form-control" name="tool_brand" id="tool_brand" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Model/Tipe</label>
                        <input type="text" class="form-control" name="tool_model" id="tool_model" readonly/>
                    </div>  
                    <!--
                    <div class="col-3 form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" name="tool_type" id="tool_type" />
                    </div>
                    -->
                    <div class="col-3 form-group">
                        <label>Nomor Seri</label>
                        <input type="text" class="form-control" name="serial_no" id="serial_no" readonly/>
                    </div>
                    <div class="col-3 form-group">
                        <label>Media Uji/Komoditas</label>
                        <input type="text" class="form-control" name="tool_media" id="tool_media" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 form-group">
                        <label>Kapasitas Maksimal</label>
                        <input type="number" class="form-control" name="tool_capacity" id="tool_capacity" readonly />
                    </div>

                    <div class="col-3 form-group">
                        <label>Kapasitas Minimal</label>
                        <input type="number" class="form-control" name="tool_capacity_min" id="tool_capacity_min" readonly/>
                    </div>

                    <div class="col-3 form-group">
                        <label>Satuan</label>
                        <input type="text" class="form-control" name="tool_capacity_unit" id="tool_capacity_unit" readonly/>
                    </div>
                    
                    <div class="col-3 form-group">
                        <label>Buatan</label>
                        <input type="text" class="form-control" name="tool_made_in" id="tool_made_in" readonly/>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-6 form-group">
                        <label>Pabrikan</label>
                        <input class="form-control" type="text" name="tool_factory"readonly />
                    </div>
                    <div class="col-6 form-group">
                        <label>Alamat Pabrikan</label>
                        <textarea class="form-control" name="tool_factory_address" readonly></textarea>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="notFound" tabindex="-1" aria-labelledby="notFoundLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notFoundLabel">Data Tidak Ditemukan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Data UTTP tidak ditemukan. Pastikan kembali nomor seri UTTP sudah sesuai atau belum.</p>
                <p>Klik tombol Tambah Baru untuk menambahkan data UTTP baru.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" class="btn btn-primary" id="new_uttp">Tambah baru</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="found" tabindex="-1" aria-labelledby="foundLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="foundLabel">Konfirmasi Penambahan UTTP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="table">
                <table class="table table-striped table-bordered table-hover" id="tbl_uttp" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Merek</th>
                            <th>Model/Tipe</th>
                            <th>Nomor Seri</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                <button type="button" class="btn btn-primary" id="new_uttp_found">Tambah baru</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function() {
        $('#owner_id, #tool_made_in_id').select2({
            theme: 'bootstrap4',
        });
        $('#reference_date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('#type_id').select2({
            theme: 'bootstrap4',
        }).on("select2:select", function (e) { 
            $('#uttp_id').val(null);
            $('#serial_no').val(null);
            $('#tool_brand').val(null);
            $('#tool_model').val(null);
            $('#tool_capacity').val(null);
            $('#tool_capacity_min').val(null);
            $('#tool_capacity_unit').val(null);
            $('#tool_made_in').val(null);
            $('#tool_factory').val(null);
            $('#tool_factory_address').val(null);
            $('#tool_media').val(null);
        });

        var tbl = $('#tbl_uttp').DataTable({
            pageLength: 10,
        });

        $('#find_uttp').click(function(e) {
            e.preventDefault();

            var uttp_type = $('#type_id').val();
            var q = $('#serial_no_find').val();
            var owner_id = $('#owner_id').val();

            url = '<?= base_url(); ?>/uttp/findBySeries';

            $.ajax({
                type: 'post',
                url: url,
                data: {
                    type_id: uttp_type,
                    owner_id: owner_id,
                    q: q,
                    <?= csrf_token() ?>: '<?= csrf_hash() ?>',
                },
                success: function (data) {
                    $('#found').modal('show');

                    tbl
                        .rows()
                        .remove()
                        .draw(false);
                    jQuery.each(data, function(index, element) {
                        tbl.row.add([
                            element.tool_brand,
                            element.tool_model,
                            element.serial_no,
                            '<button class="btn btn-default choose" type="button" data-id="'+ element.id +'">Pilih</button>'
                        ]).node().id = element.id;
                        tbl.draw(false);
                    });

                },
                error: function (data) {
                    $('#notFound').modal('show');
                }
            });
        });
        $('#new_uttp, #new_uttp_found').click(function(e) {
            e.preventDefault();

            var owner_id = $('#owner_id').val();
            var type_id = $('#type_id').val();
            window.location.href = "<?= base_url('useruttp/createNew') ?>/" + owner_id + "/" + type_id ;
        });

        tbl.on('click', 'tbody .choose', function(e) {
            e.preventDefault();

            var row_node = tbl.row($(this).closest('tr')).node();

            $.get('<?= base_url(); ?>/uttp/get/'+row_node.id,function(res)
            {
                if (res) {
                    $('#uttp_id').val(res.id);
                    
                    $('#serial_no').val(res.serial_no);
                    $('#tool_brand').val(res.tool_brand);
                    $('#tool_model').val(res.tool_model);
                    $('#tool_capacity').val(res.tool_capacity);
                    $('#tool_capacity_min').val(res.tool_capacity_min);
                    $('#tool_capacity_unit').val(res.tool_capacity_unit);
                    $('#tool_made_in').val(res.tool_made_in);
                    $('#tool_factory').val(res.tool_factory);
                    $('#tool_factory_address').val(res.tool_factory_address);
                    $('#tool_media').val(res.tool_media);
                }
            });

            $('#found').modal('hide');
        });
        
    })
</script>
<?= $this->endSection() ?>