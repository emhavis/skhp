<?= $this->extend('template/main') ?>

<?= $this->section('css') ?>
<link href="<?=base_url();?>/plugins/dataTables/datatables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="page-heading">
    <h1 class="page-title">UTTP</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/">Dashboard</a>
        </li>
        <li class="breadcrumb-item">UTTP</li>
    </ol>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Daftar UTTP</div>
            <div>
                <a class="btn btn-secondary btn-sm" href="<?= base_url('useruttp/downloadtemplate'); ?>"><i class="fa fa-download"></i> Template</a>
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#importModal"><i class="fa fa-upload"></i> Impor</a>
                <a class="btn btn-primary btn-sm" href="<?= base_url('useruttp/create'); ?>"><i class="fa fa-plus"></i> Data Baru</a>
            </div>
        </div>
        <div class="ibox-body">
            <div id="table">
            <table class="table table-striped table-bordered table-hover" id="labs" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pemilik</th>
                        <th>Jenis</th>
                        <th>Merek/Model/Tipe/Serial No</th>
                        <th>Kapasitas</th>
                        <th>Buatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($uttps as $uttp):?>
                    <tr>
                        <td><?= $uttp->owner ?></td>
                        <td><?= $uttp->uttp_type ?></td>
                        <td><?= $uttp->tool_brand .'/'. $uttp->tool_model .'/'. $uttp->serial_no  ?></td>
                        <td><?= $uttp->tool_capacity ?> <?= $uttp->tool_capacity_unit ?></td>
                        <td><?= $uttp->tool_made_in ?></td>
                        <td>
                            <a class="btn btn-default btn-sm" href="<?= base_url('uttp/edit/' . $uttp->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('useruttp/delete/' . $uttp->map_id); ?>"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            </div>

            <div id='calendar'></div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="importModal" tabindex="-1" aria-labelledby="importModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="booking" action="<?= base_url('useruttp/import'); ?>" 
            enctype="multipart/form-data" method="post">
            <?= csrf_field() ?>
            <div class="modal-header">
                <h5 class="modal-title" id="importModalLabel">Impor Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="file_import">File Excel</label>
                        <input class="form-control" type="file" name="file_import" id="file_import"
                            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-success">Import</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="<?=base_url();?>/plugins/dataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#labs').DataTable({
            pageLength: 10,
        });
    })
</script>
<?= $this->endSection() ?>