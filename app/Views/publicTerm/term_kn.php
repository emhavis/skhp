<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <meta name="author" content="Direktorat Metrologi" />    
	<meta name="description" content="SIMPEL UPTP IV">
    <title>SIMPEL UPTP IV</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url();?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/plugins/themify-icons/themify-icons.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url();?>/css/main.min.css" rel="stylesheet" />
    <link href="<?=base_url();?>/css/themes/orange-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <?= $this->renderSection('css') ?>
</head>

<body class="fixed-navbar fixed-layout">
    <div class="page-wrapper">
        <div class="" style="margin:50px !important">
            <div class="page-heading">
                <h1 class="page-title">Pendaftaran Layanan</h1>
            </div>
            <div class="page-content fade-in-up">
                <? if($ter) ?>
                    <div class="ibox">
                        <div class="ibox-head">
                            <div class="ibox-title">Kaji Ulang Permintaan: <?= $item->uttp_type ?> - <?= $item->tool_brand ?>/<?= $item->tool_model ?> (<?= $item->serial_no ?>)
                            </div>
                        </div>
                        <div class="ibox-body">
                            <form id="booking" action="<?= base_url('term/create/'.$item->id); ?>" method="post">
                                <?= csrf_field() ?>
                                
                                <?= $termKaji->kaji_ulang ?>
                                
                                <br/>
                                <br/>
                                <div class="row">
                                    <div class="col-3 form-group">
                                        <label>Tanggal</label>
                                        <input type="text" class="form-control" 
                                            value="<?= date("d M Y") ?>" readonly />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
    
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="<?=base_url();?>/plugins/jquery/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="<?=base_url();?>/js/app.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <?= $this->renderSection('js') ?>
</body>

</html>