<footer class="page-footer">
    <div class="font-13">2021 &copy; <b>Direktorat Metrologi, Kementerian Perdagangan Republik Indonesia</b> - All rights reserved.</div>
    <div class="to-top"><i class="fa fa-angle-up"></i></div>
</footer>