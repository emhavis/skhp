<!-- Footer -->
<footer id="footer">
    <div class="copyright-content">
        <div class="container">
            <div class="copyright-text text-center">&copy; 2021 Direktorat Metrologi, Kementerian Perdagangan Republik Indonesia. All Rights Reserved.<a href="//www.inspiro-media.com" target="_blank" rel="noopener"> INSPIRO</a> </div>
        </div>
    </div>
</footer>
<!-- end: Footer -->