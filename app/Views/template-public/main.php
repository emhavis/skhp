<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta name="author" content="Direktorat Metrologi" />    
	<meta name="description" content="SIMPEL UPTP IV">
    <link rel="icon" type="image/png" href="images/favicon.png">   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Document title -->
    <title>SIMPEL UPTP IV</title>
    <!-- Stylesheets & Fonts -->
    <link href="<?=base_url();?>/plugins/polo/css/plugins.css" rel="stylesheet">
    <link href="<?=base_url();?>/plugins/polo/css/style.css" rel="stylesheet">
</head>

<body>

    <!-- Body Inner -->
    <div class="body-inner">

        <?= $this->include('template-public/header') ?>

        <?= $this->renderSection('content') ?>

        

        <?= $this->include('template-public/footer') ?>
        

    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
    <!--Plugins-->
    <script src="<?=base_url();?>/plugins/jquery/jquery-3.5.1.min.js"></script>
    <script src="<?=base_url();?>/plugins/polo/js/plugins.js"></script>

    <!--Template functions-->
    <script src="<?=base_url();?>/plugins/polo/js/functions.js"></script>
</body>

</html>