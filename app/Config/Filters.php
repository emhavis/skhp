<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\CSRF;
use CodeIgniter\Filters\DebugToolbar;
use CodeIgniter\Filters\Honeypot;

class Filters extends BaseConfig
{
	/**
	 * Configures aliases for Filter classes to
	 * make reading things nicer and simpler.
	 *
	 * @var array
	 */
	public $aliases = [
		'csrf'     => CSRF::class,
		'toolbar'  => DebugToolbar::class,
		'honeypot' => Honeypot::class,
		'auth'     => \App\Filters\Auth::class,
	];

	/**
	 * List of filter aliases that are always
	 * applied before and after every request.
	 *
	 * @var array
	 */
	public $globals = [
		'before' => [
			// 'honeypot',
			'csrf',
			'auth' => [
				'except' => [
					'auth/login', 
					'auth/register', 
					'auth/internalLogin',
					'auth/activate/*',
					'auth/check_data/*',
					'auth/forget',
					'auth/forgetSuccess',
					'auth/changePassword/*',
					'auth/registerSuccess',
					'auth/findUmlById',
					'auth/findProv',
					'auth/findKota/*',
					'/',
					'article/*',
					'tracking/download/*',
					'tracking/download_paymentref/*',
					'tracking/download_paymentref_uut/*',
					'tracking/download_uut/*',
					'booking/download_permohonan/*',
					'visitor/*',
					'home/update_visitor/*',
					'tracking/permohonan/*',
					'tracking/download_permohonan/*',
					'tracking/download_permohonan_uut/*',
					'survey/download/*',
					'uploads/*',
					'storage/*',
					'publicterm/create/*',
					'publicterm/create_kn/*',
				]
			],
		],
		'after'  => [
			'toolbar',
			// 'honeypot',
		],
	];

	/**
	 * List of filter aliases that works on a
	 * particular HTTP method (GET, POST, etc.).
	 *
	 * Example:
	 * 'post' => ['csrf', 'throttle']
	 *
	 * @var array
	 */
	public $methods = [];

	/**
	 * List of filter aliases that should run on any
	 * before or after URI patterns.
	 *
	 * Example:
	 * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
	 *
	 * @var array
	 */
	public $filters = [];
}
